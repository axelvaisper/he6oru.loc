{include file="$incpath/header/style.tpl"}
{include file="$incpath/header/jquery.tpl"}
<script src="{$jspath}/jcookie.js"></script>
<script src="{$jspath}/jtoggle.js"></script>
<script src="{$jspath}/jvalidate.js"></script>
<script src="{$jspath}/jsuggest.js"></script>
<script src="{$jspath}/jblock.js"></script>
<script src="{$jspath}/jform.js"></script>
<script src="{$themepath}/js/ddaccordion.js"></script>
<script>
<!-- //
$(function() {
    $('.colorbox').colorbox({ height: '97%', width: '90%', iframe: true, fastIframe: false });
    $('.colorbox-sm').colorbox({ height: '70%', width: '60%', iframe: true, fastIframe: false });
    $('.stip').tooltip();
    $('#com').show();
    setTimeout(function() {
        $('#com_loader').hide();
    }, 500);
});
//-->
</script>
