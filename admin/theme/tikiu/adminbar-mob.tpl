<nav data-uk-navbar="mode:click" class="uk-hidden@s uk-visible zindex-3 uk-navbar-container" data-uk-sticky="show-on-up: true; animation: uk-animation-slide-bottom; sel-target: .uk-navbar-container; cls-active: fixed-bottom shadow-top; cls-inactive: uk-navbar-transparent;">

  {* LEFT *}
  <div class="uk-navbar-left">
    <ul class="uk-navbar-nav">
      <li><a data-uk-toggle="target: #offcanvas-left" data-uk-tooltip title="Админ панель" data-uk-icon="icon:menu; ratio:1.5"></a></li>
      <li><a href="index.php" data-uk-icon="home" title="{#StartPage#}" data-uk-tooltip></a></li>
    </ul>
  </div>

  {* CENTER *}
  <div class="uk-navbar-center">
    <ul class="uk-navbar-nav">
      <li><a href="{$baseurl}" title="{#Global_Site#}" target="_blank" data-uk-tooltip>
      <img src="{$baseurl}/logo45.png" alt=""> 
      <div class="uk-visible@s uk-margin-small-left">
      <small>{$settings.Firma|sanitize}</small>
      <h6 class="uk-margin-remove uk-text-bold uk-text-uppercase">{$settings.Seitenname|sanitize}</h6></div></a>
      </li>
    </ul>
  </div>

  {* RIGHT *}
  <div class="uk-navbar-right">
    <ul class="uk-navbar-nav">
      {if perm('notes') && $admin_settings.Aktiv_Notes == 1}
        <li><a data-uk-icon="edit" data-uk-toggle="target: #offcanvas-note" title="{#Notes#}" data-uk-tooltip></a></li>
      {/if}
      <li><a data-uk-icon="cog" data-uk-toggle="target: #offcanvas-right"></a></li>
    </ul>
  </div>

</nav>
