<nav data-uk-navbar="mode:click" class="uk-visible@s zindex-3 uk-navbar-container" data-uk-sticky="show-on-up: true; animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-sticky-fixed uk-box-shadow-small; cls-inactive: uk-navbar-transparent;">

  {* LEFT *}
  <div class="uk-navbar-left">
    <ul class="uk-navbar-nav">
      <li><a data-uk-toggle="target: #offcanvas-left" title="Админ панель" data-uk-icon="icon:menu; ratio:1.5"></a></li>
      <li><a data-uk-toggle="target: #sidemenu; " title="скрыть Админ панель" data-uk-icon="icon:chevron-left; ratio:1"></a></li>
    </ul>
  </div>

  {* CENTER *}
  <div class="uk-navbar-center">
    <ul class="uk-navbar-nav">
      <li><a href="{$baseurl}" title="{#Global_Site#}" target="_blank" data-uk-tooltip>
      <img src="{$baseurl}/logo45.png" alt=""> 
      <div class="uk-margin-small-left">
      <small>{$settings.Firma|sanitize}</small>
      <h6 class="uk-margin-remove uk-text-bold uk-text-uppercase">{$settings.Seitenname|sanitize}</h6></div></a>
      </li>
      <li><a href="index.php" data-uk-icon="icon:home; ratio:1.5" title="{#StartPage#}" data-uk-tooltip></a></li>
    </ul>
  </div>

  {* RIGHT *}
  <div class="uk-navbar-right">
    <ul class="uk-navbar-nav">
      {if perm('notes') && $admin_settings.Aktiv_Notes == 1}
        <li><a data-uk-icon="edit" data-uk-toggle="target: #offcanvas-note">{#Notes#}</a></li>
      {/if}
      <li><a data-uk-icon="cog" data-uk-toggle="target: #offcanvas-right"></a></li>
    </ul>
  </div>

</nav>
