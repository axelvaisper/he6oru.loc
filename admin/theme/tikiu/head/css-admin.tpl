{literal}
<style>
@charset "UTF-8";

/* переменные адаптивности*/
media all and (max-width: 600px) {
    :root {
      --font-size: 12px;
    }
  }

/*аним.плавной загрузки */
#preloader{overflow:hidden;background:var(--gray-vk);height:100%;left:0;position:fixed;top:0;width:100%;z-index:999999}
.uk-spinner{left:calc(50% - 20px);position:relative;top:calc(50% - 20px);z-index:9}
</style>
{/literal}