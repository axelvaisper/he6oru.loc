<meta charset="{$charset}"/>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />

<meta name="robots" content="none" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

{literal}
<style>
@charset "UTF-8";
/*аним.плавной загрузки */
#preloader{overflow:hidden;background:var(--gray-vk);height:100%;left:0;position:fixed;top:0;width:100%;z-index:999999}
.uk-spinner,progress{left:calc(50% - 20px);position:relative;top:calc(50% - 20px);z-index:9}
:focus {outline:none}
select:-moz-focusring{color:transparent;text-shadow:0 0 0 #000}
</style>
{/literal}

<link rel="stylesheet" href="{$csspath}/uikit.css" />
<link rel="stylesheet" href="{$csspath}/uikit-custom.css" />
<link rel="stylesheet" href="{$csspath}/admin.css" />

<script src="{$themepath}/js/jquery-2.1.1.js"></script>
<script src="{$jspath}/jpatch.js"></script>
<script src="{$themepath}/js/admin.js"></script>

<script src="{$jspath}/jcookie.js"></script>
<script src="{$jspath}/jvalidate.js"></script>
<script src="{$jspath}/jsuggest.js"></script>
<script src="{$jspath}/jform.js"></script>

<script>
<!-- //
$(function() {

  $('#preloader').fadeOut('slow', function () { $(this).remove(); });

});
//-->
</script>
