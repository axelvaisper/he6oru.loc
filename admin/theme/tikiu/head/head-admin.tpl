<meta http-equiv="Content-Type" content="text/html; charset={$charset}" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="robots" content="none" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

{* {include file="$incpath/head/css-admin.tpl"} *}

<link rel="stylesheet" href="{$csspath}/admin-uikit.css" />
<link rel="stylesheet" href="{$csspath}/admin-uikit-custom.css" />
<link rel="stylesheet" href="{$csspath}/ui.css" />
<link rel="stylesheet" href="{$csspath}/colorbox.css" />
<link rel="stylesheet" href="{$csspath}/admin.css" />
{* <link rel="stylesheet" href="{$csspath}/jq_gridder.css" /> *}

<script src="{$baseurl}/admin/theme/tikiu/js/jquery-2.1.1.js"></script>
<script src="{$jspath}/jpatch.js"></script>
<script src="{$jspath}/jquery.ui.js"></script>
<script src="{$jspath}/jtooltip.js"></script>
<script src="{$jspath}/jcolorbox.js"></script>

<script src="{$baseurl}/admin/theme/tikiu/js/admin-uikit.js"></script>
<script src="{$baseurl}/admin/theme/tikiu/js/admin-uikit-icon.js"></script>

<script src="{$baseurl}/admin/theme/tikiu/js/admin.js"></script>

<script src="{$jspath}/jcookie.js"></script>
<script src="{$jspath}/jtoggle.js"></script>
<script src="{$jspath}/jvalidate.js"></script>
<script src="{$jspath}/jsuggest.js"></script>
<script src="{$jspath}/jblock.js"></script>
<script src="{$jspath}/jform.js"></script>
<script src="{$baseurl}/admin/theme/tikiu/js/ddaccordion.js"></script>

{* <script src="{$baseurl}/admin/theme/tikiu/js/jq_gridder.js"></script> *}

<script>
$(function() {

 $('.colorbox, .colorbox-sm').colorbox({
    height: '90%',
    width: '100%',
    iframe: true
  });  
  {* // Функция ресайза ColorBox *}
  var resizeTimer;
  function resizeColorBox()
  {
    if (resizeTimer) clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
              if (jQuery('#cboxOverlay').is(':visible')) {
                        jQuery.colorbox.resize({ width:'100%', height:'90%' })
              }
    }, 200);
  }
  {* // Ресайз при изменении размера окна браузера и
  // изменении ориентации мобильного устроиства *}
  jQuery(window).resize(resizeColorBox);
  window.addEventListener("orientationchange", resizeColorBox, false);

  $('.stip').tooltip();

  $('body').show();setTimeout(function(){ $('#preloader').remove(); }, 250);

});
</script>
