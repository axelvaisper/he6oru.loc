<!DOCTYPE html>
<html class="uk-height-1-1">
<head>
<title>{#LoginName#}</title>
{include file="$incpath/head/head-admin-login.tpl"}
</head>
<body class="uk-height-1-1">
  {$content}
</body>
</html>
