<!DOCTYPE html>
<html>
<head>
<title>{#Admin_Global#} | {$settings.Seitenname|sanitize}</title>
{include file="$incpath/head/head-admin.tpl"}
</head>
<body style="display:none">

{* анимация загрузки *}
<div id="preloader"><img id="anim_floating" src="{$baseurl}/logo45.png" /></div>

{* главный контейнер всего визуального *}
<div class="uk-offcanvas-content">

  {include file="$incpath/adminbar-mob.tpl"}
  {include file="$incpath/adminbar-pc.tpl"}

  <div data-uk-grid class="uk-grid-small">
    {* side menu *}
    {include file="$incpath/navielements/navielements.tpl"}

    {* main content *}
    <div class="uk-width-expand">
      <main uk-height-viewport="offset-bottom: 5">
        {if !empty($mesage_save)}{$mesage_save}{/if}
        {$content}
      </main>

      <footer>
        <hr>
        <ul class="uk-subnav uk-flex-center">
          <li><a href="https://sx-cms.ru">
            <span data-uk-icon="sxcms"></span> SX CMS {$version}</a>
          </li>
          <li><a href="http://getuikit.com">
            <span data-uk-icon="uikit"></span> UI KIT</a>
          </li>
          <li><a href="https://vk.com/topic-6717488_38778267">Оставить отзыв</a></li>
        </ul>
      </footer>
    </div>

  </div>

  {include file="$incpath/other/offcanvas-right.tpl"}
  {include file="$incpath/notes/offcanvas-note.tpl"}

</div>
{* /offcanvas *}

</body>
</html>
