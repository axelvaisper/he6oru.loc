{if perm('bannerperm')}
<li>
  <a>
   <div>
      AD
      <br>
      <small>{#Banners#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'banners' && isset($smarty.request.sub) && $smarty.request.sub == 'overview'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=banners">{#Global_Overview#}</a></li>
      <li><a class="nav_subs colorbox" title="{#BannersNew#}" href="index.php?do=banners&amp;sub=new&amp;noframes=1">{#BannersNew#}</a></li>
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'banners' && isset($smarty.request.sub) && $smarty.request.sub == 'categs'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=banners&amp;sub=categs">{#BannersCategs#}</a></li>
    </ul>
  </div>
</li>
{/if}
