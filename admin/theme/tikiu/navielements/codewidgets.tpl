{if perm('settings')}
<li>
  <a>
    <div>
      <i data-uk-icon="code"></i>
      <br>
      <small>{#CodeWidgets#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li>
        <a class="{if isset($smarty.request.do) && $smarty.request.do == 'codewidgets' && isset($smarty.request.sub) && $smarty.request.sub == 'overview'}uk-active{else}nav_subs{/if}" href="index.php?do=codewidgets">{#Global_Overview#}</a>
      </li>
      <li>
        <a title="{#Global_Add#}" class="{if isset($smarty.request.do) && $smarty.request.do == 'codewidgets' && isset($smarty.request.sub) && $smarty.request.sub == 'new'}uk-active{else}nav_subs{/if} colorbox" href="index.php?do=codewidgets&amp;sub=new&amp;noframes=1">{#Global_Add#}</a>
      </li>
      <li>
        <a title="{#Global_Add#}" class="{if isset($smarty.request.do) && $smarty.request.do == 'codewidgets' && isset($smarty.request.sub) && $smarty.request.sub == 'new'}uk-active{else}nav_subs{/if} colorbox" href="index.php?do=codewidgets&amp;sub=new&amp;html=1&amp;noframes=1">{#Global_Add#} + CKEditor</a>
      </li>
  </ul>
</div>

</li>
{/if}
