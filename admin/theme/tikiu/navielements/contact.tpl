{if perm('contact_forms')}
<li>
  <a>
    <div>
      <i data-uk-icon="form"></i>
      <br>
      <small>{#ContactForms#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.do) && isset($smarty.request.do) && $smarty.request.do == 'contactforms'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=contactforms">{#Global_Overview#}</a></li>
      <li><a class="nav_subs colorbox" title="{#ContactForms_new#}" href="index.php?do=contactforms&amp;sub=new&amp;noframes=1">{#ContactForms_new#}</a></li>
    </ul>
  </div>
</li>
{/if}
