{if perm('content') && admin_active('content')}
<li>
  <a>
    <div>
      <i data-uk-icon="file"></i>
      <br>
      <small>{#Content#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'content' && isset($smarty.request.sub) && $smarty.request.sub == 'overview'}uk-active{else}nav_subs{/if}" href="index.php?do=content&amp;sub=overview">{#Global_Overview#}</a></li>
        {if perm('content_new')}
        <li><a  title="{#Content_new#}" class="colorbox {if isset($smarty.request.do) && $smarty.request.do == 'content' && isset($smarty.request.sub) && $smarty.request.sub == 'addcontent'}uk-active{else}nav_subs{/if}" href="index.php?do=content&amp;sub=addcontent&amp;noframes=1">{#Content_new#}</a></li>
        {/if}
        {if perm('content_category')}
        <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'content' && isset($smarty.request.sub) && $smarty.request.sub == 'categories'}uk-active{else}nav_subs{/if}" href="index.php?do=content&amp;sub=categories">{#Global_Categories#}</a></li>
        {/if}
    </ul>
  </div>
</li>
{/if}
