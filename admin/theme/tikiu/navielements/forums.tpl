{if perm('forum') && admin_active('forums')}
<li>
  <a>
    <div>
      Forum{* <i data-uk-icon="file"></i> *}
      <br>
      <small>{#Forums_nt#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      {if perm('forum')}
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'overview'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=overview">{#Global_Overview#}</a></li>
        {/if}
        {if perm('forum_deltopics')}
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'deltopics'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=deltopics">{#Forums_Del_Topics#}</a></li>
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'delratings'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=delratings">{#Forums_Del_Ratings#}</a></li>
        {/if}
        {if perm('forum_attachments')}
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'showattachments'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=showattachments">{#Forums_Att_Show#}</a></li>
        {/if}
        {if perm('forum_userrankings')}
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'userrankings'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=userrankings">{#Forums_URank_title#}</a></li>
        {/if}
        {if perm('forum_helppages')}
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'forumshelp'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=forumshelp">{#Forums_Help#}</a></li>
        {/if}
        {if perm('comment_emoticons')}
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'emoticons'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=emoticons">{#ForumsEmoticons#}</a></li>
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'posticons'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=posticons">{#Forums_TIcons_title#}</a></li>
        {/if}
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'settings'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=settings">{#SettingsModule#}</a></li>
    </ul>
  </div>
</li>
{else}
  {if perm('comment_emoticons')}
    <li>
      <a>
        <div>
          <i data-uk-icon="heart"></i>
          <br>
          <small>{#Forums_TIcons_title#}</small>
        </div>
      </a>

      <div class="uk-navbar-dropdown">
        <ul class="uk-nav">
          <li>
            <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'emoticons'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=emoticons">{#ForumsEmoticons#}</a>
          </li>
          <li>
            <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'posticons'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=forums&amp;sub=posticons">{#Forums_TIcons_title#}</a>
          </li>
        </ul>
      </div>
    </li>
  {/if}
{/if}
