{if perm('navigation_edit')}
<li>
  <a>
    <div>
      <i data-uk-icon="list"></i>
      <br>
      <small>{#Navigation#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'navigation' && isset($smarty.request.sub) && $smarty.request.sub == 'list'}uk-active{else}nav_subs{/if}" href="index.php?do=navigation&amp;sub=list">{#Global_Overview#}</a></li>
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'navigation' && isset($smarty.request.sub) && $smarty.request.sub == 'list'}uk-active{else}nav_subs{/if}" href="index.php?do=navigation&sub=edit&id=1">{#Edit#}</a></li>
        {if admin_active('flashtag')}
        <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'navigation' && isset($smarty.request.sub) && $smarty.request.sub == 'flashtag'}uk-active{else}nav_subs{/if}" href="index.php?do=navigation&amp;sub=flashtag">{#Flashtag#}</a></li>
        {/if}
    </ul>
  </div>
</li>
{/if}
