{if empty($smarty.request.noframes)}
{* <script>
<!-- //
{if $admin_settings.Navi_Anime == 1}
  $(function() {
      toggleCookie('navielements', 'offcanvas-left', 30, '{$basepath}');
  });
{/if}
//-->
</script> *}

{* MOBILE MENU *}
<aside id="offcanvas-left" data-uk-offcanvas="overlay: true">
<div class="uk-offcanvas-bar uk-height-1-1 uk-padding-remove uk-background-default">

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/settings.tpl"}
        {include file="$incpath/navielements/theme.tpl"}
        {include file="$incpath/navielements/navi.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>
 
    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/usergroups.tpl"}
        {include file="$incpath/navielements/users.tpl"}
        {include file="$incpath/navielements/forums.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/insert.tpl"}
        {include file="$incpath/navielements/codewidgets.tpl"}
        {include file="$incpath/navielements/content.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/shop.tpl"}
        {include file="$incpath/navielements/manufacturer.tpl"}
        {include file="$incpath/navielements/products.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/news.tpl"}
        {include file="$incpath/navielements/articles.tpl"}
        {include file="$incpath/navielements/banners.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/linksdownloads.tpl"}
        {include file="$incpath/navielements/gallery.tpl"}
        {include file="$incpath/navielements/newsletter.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/contact.tpl"}
        {include file="$incpath/navielements/polls.tpl"}
        {include file="$incpath/navielements/faq.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/seo.tpl"}
        {include file="$incpath/navielements/roadmap.tpl"}
        {include file="$incpath/navielements/stats.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/media.tpl"}
        {include file="$incpath/navielements/cheats.tpl"}
        {include file="$incpath/navielements/other.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/settings/navi_modul.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

</div>
</aside>



{* PC MENU *}
<aside id="sidemenu" class="uk-width-1-5@m uk-visible@m">
  <div class="uk-background-default uk-overflow-auto uk-height-1-1" data-uk-sticky="offset:0;top:0">

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/settings.tpl"}
        {include file="$incpath/navielements/theme.tpl"}
        {include file="$incpath/navielements/navi.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>
 
    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/usergroups.tpl"}
        {include file="$incpath/navielements/users.tpl"}
        {include file="$incpath/navielements/forums.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/insert.tpl"}
        {include file="$incpath/navielements/codewidgets.tpl"}
        {include file="$incpath/navielements/content.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/shop.tpl"}
        {include file="$incpath/navielements/manufacturer.tpl"}
        {include file="$incpath/navielements/seo.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/news.tpl"}
        {include file="$incpath/navielements/articles.tpl"}
        {include file="$incpath/navielements/banners.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/linksdownloads.tpl"}
        {include file="$incpath/navielements/gallery.tpl"}
        {include file="$incpath/navielements/newsletter.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/contact.tpl"}
        {include file="$incpath/navielements/polls.tpl"}
        {include file="$incpath/navielements/faq.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/roadmap.tpl"}
        {include file="$incpath/navielements/stats.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navielements/media.tpl"}
        {include file="$incpath/navielements/cheats.tpl"}
        {include file="$incpath/navielements/other.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/settings/navi_modul.tpl"}
        {include file="$incpath/navielements/products.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

  </div>
</aside>
{/if}