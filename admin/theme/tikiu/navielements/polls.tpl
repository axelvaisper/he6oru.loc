{if perm('polls') && admin_active('poll')}
<li>
  <a>
    <div>
      {* <i data-uk-icon="news"></i> *}Polls
      <br>
      <small>{#Polls#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'poll'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=poll">{#Global_Overview#}</a></li>
      <li><a class="nav_subs colorbox" title="{#Polls_new#}" href="index.php?do=poll&amp;sub=new&amp;noframes=1">{#Polls_new#}</a></li>
    </ul>
  </div>
</li>
{/if}
