{if perm('roadmaps') && admin_active('roadmap')}
<li>
  <a>
    <div>
      {* <i data-uk-icon="news"></i> *}Roadmaps
      <br>
      <small>{#Roadmaps#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'roadmap' && isset($smarty.request.sub) && empty($smarty.request.sub)}nav_subs_active{else}nav_subs{/if}" href="?do=roadmap">{#Global_Overview#}</a></li>
      <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'newroadmap'}nav_subs_active{else}nav_subs{/if}" href="?do=roadmap&amp;sub=newroadmap">{#NewRoadmap#}</a></li>
    </ul>
  </div>
</li>
{/if}