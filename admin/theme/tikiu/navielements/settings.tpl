{if perm('settings')}
<li>
  <a>
    <div>
      <i data-uk-icon="sxcms"></i>
      <br>
      <small>{#Global_Settings#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'global'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=global">{#Settings_general#}</a>
      </li>
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'admin_global'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=admin_global">{#Admin_Global#}</a>
      </li>
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'sectionsdisplay'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=sectionsdisplay">{#Sections#}</a>
      </li>
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'sectionsettings'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=sectionsettings">{#Global_SettingsSections#}</a>
      </li>
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'widgets'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=widgets">{#Global_Widgets#}</a>
      </li>
      <br>
      <li>
      <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'languages'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=languages">{#Settings_languages#}</a>
      </li>
      <li>
      <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'adminlanguages'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=adminlanguages">{#Settings_languages_a#}</a>
      </li>
      {if perm('lang_edit')}
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'lang_edit'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=lang_edit">{#SettingsLangEdit#}</a>
      </li>
      {/if}
      <br>
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'money'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=money">{#MoneySite#}</a>
      </li>
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'secure'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=secure">{#SecureSettings#}</a>
      </li>
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'cron'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=cron">{#Scheduler#}</a>
      </li>
      <li>
        <a class="{if isset($smarty.request.do) && $smarty.request.do == 'expimp'}uk-active{else}nav_subs{/if}" href="index.php?do=expimp">{#Admin_ExpImp#}</a>
      </li>
      <br>
      {if $smarty.session.benutzer_id == 1}
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'phpedit'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=phpedit">{#ConfPhp#}</a>
      </li>
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'htaccess'}uk-active{else}nav_subs{/if}" href="index.php?do=settings&amp;sub=htaccess">{#HtaccessSettings#}</a>
      </li>
      {/if}
      <li>
        <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'logs'}uk-active{else}nav_subs{/if}" href="?do=settings&amp;sub=logs">{#Admin_Logs#}</a>
      </li>
    </ul>
  </div>
</li>
{/if}