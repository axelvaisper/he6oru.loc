{if perm('shop') && admin_active('shop')}
<li>
  <a>
    <div>
      <i data-uk-icon="shop"></i>
      <br>
      <small>{#Global_Shop#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      {if perm('shop_articleedit')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'articles'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=articles">{#Shop_articlesS#}</a>
        </li>
      {/if}

      {if perm('shop_articlenew')}
        <li data-uk-lightbox>
          <a data-type="iframe" class="{if isset($smarty.request.sub) && $smarty.request.sub == 'new'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=new&amp;noframes=1">{#Shop_articles_addnew#}</a>
        </li>
      {/if}

      {if perm('shop_catdeletenew')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'categories'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=categories">{#Global_Categories#}</a>
        </li>
        <li data-uk-lightbox>
          <a title="{#Global_AddCateg#}" data-type="iframe" href="?do=shop&amp;sub=new_categ&amp;noframes=1"><img src="{$imgpath}/add.png" alt="" /> {#Global_AddCateg#}</a>
        </li>
      {/if}

      {if perm('shop_settings')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'settings'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=settings">{#Global_Settings#}</a>
        </li>
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'regions'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=regions">{#Settings_countries_title#}</a>
        </li>
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'infomsg'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=infomsg">{#ShopInfoM#}</a>
        </li>
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'shopinfos'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=shopinfos">{#Shop_Infos#}</a>
        </li>
      {/if}

      {if perm('edit_order')}
        <li>
          <a title="" class="{if isset($smarty.request.sub) && $smarty.request.sub == 'orders'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=orders">{#Shop_ordersS#}</a>
        </li>
        {/if}

      {if perm('shop_settings')}
      <li>
        <a title="{#Shop_showmoney_title#}" class="colorbox {if isset($smarty.request.sub) && $smarty.request.sub == 'showmoney'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=showmoney&amp;noframes=1">{#Shop_showmoney_title#}</a>
      </li>
      {/if}

      {if perm('shop_addons')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'categories_addons'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=categories_addons">{#Shop_CategoriesAddons#}</a>
        </li>
      {/if}

      {if perm('shop_paymentmethods')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'paymentmethods'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=paymentmethods">{#Shop_payment_title#}</a>
        </li>
      {/if}

      {if perm('shop_shipper')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'shipper'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=shipper">{#Shop_shipper_title#}</a>
        </li>
      {/if}

      {if perm('shop_settings')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'tracking'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=tracking">{#Shop_Tracking#}</a>
        </li>
      {/if}

      {if perm('shop_taxes')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'taxes'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=taxes">{#Shop_taxes_title#}</a>
        </li>
      {/if}

      {if perm('shop_shippingready')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'shippingready'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=shippingready">{#Shop_shippingready_title#}</a>
        </li>
      {/if}

      {if perm('shop_availability')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'availabilities'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=availabilities">{#Shop_availabilities_title#}</a>
        </li>
      {/if}

      {if perm('shop_couponcodes')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'couponcodes'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=couponcodes">{#Shop_couponcodes_title#}</a>
        </li>
      {/if}

      {if perm('shop_units')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'units'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=units">{#Shop_units_title#}</a>
        </li>
      {/if}

      {if perm('shop_groupsettings')}
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'groupsettings'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=groupsettings">{#Shop_groupsettings#}</a>
        </li>
      {/if}

      {if perm('shop_settings')}
        <li>
          <a class="{if isset($smarty.request.do) && $smarty.request.do == 'shopimport'}uk-active{else}nav_subs{/if}" href="index.php?do=shopimport">{#Import_art#}</a>
        </li>
        <li>
          <a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'yamarket'}uk-active{else}nav_subs{/if}" href="index.php?do=shop&amp;sub=yamarket">{#YaMarket#}</a>
        </li>
      {/if}

    </ul>
  </div>
</li>
{/if}
