{if perm('settings') && perm('templates')}
<li>
  <a>
    <div>
      <i data-uk-icon="palette"></i>
      <br>
      <small>{#ThemeSite#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.sub) && ($smarty.request.sub == 'show_all_tpl' || $smarty.request.sub == 'show_tpl')} uk-active{else}nav_subs{/if}" href="index.php?do=theme&amp;sub=show_all_tpl">{#Templates#}</a></li>
      <li><a class="{if isset($smarty.request.sub) && ($smarty.request.sub == 'show_all_css' || $smarty.request.sub == 'show_css')} uk-active{else}nav_subs{/if}" href="index.php?do=theme&amp;sub=show_all_css">{#ThemeStyle#}</a></li>
    </ul>
  </div>
</li>
{/if}
