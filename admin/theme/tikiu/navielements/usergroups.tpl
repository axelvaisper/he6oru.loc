{if perm('user_groups')}
<li>
  <a>
    <div>
      <i data-uk-icon="users"></i>
      <br>
      <small>{#Groups_Name#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'useroverview'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=groups&amp;sub=useroverview">{#Global_Overview#}</a></li>
      <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'permissions'}nav_subs_active{else}nav_subs{/if}" href="index.php?do=groups&amp;sub=permissions">{#GlobalPerm#}</a></li>
    </ul>
  </div>
</li>
{/if}
