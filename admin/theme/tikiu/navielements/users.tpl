{if perm('users') || perm('settings')}
<li>
  <a>
    <div>
      <i data-uk-icon="user"></i>
      <br>
      <small>{#User_nameS#}</small>
    </div>
  </a>

  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      {if perm('users')}
      <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'showusers'}uk-active{else}nav_subs{/if}" href="index.php?do=user&amp;sub=showusers">{#Global_Overview#}</a></li>
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'banned'}uk-active{else}nav_subs{/if}" href="index.php?do=banned">{#Banned#}</a></li>
      <li><a title="{#User_Add#}" class="colorbox {if isset($smarty.request.sub) && $smarty.request.sub == 'adduser'}uk-active{else}nav_subs{/if}" href="index.php?do=user&amp;sub=adduser&amp;new=1&amp;noframes=1">{#User_Add#}</a></li>
      {/if}
      {if perm('settings')}
      <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'user' && isset($smarty.request.sub) && $smarty.request.sub == 'settings'}uk-active{else}nav_subs{/if}" href="index.php?do=user&amp;sub=settings">{#SettingsModule#}</a></li>
      {/if}
    </ul>
  </div>
</li>
{/if}
