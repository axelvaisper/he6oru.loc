<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$.validator.setDefaults({
    submitHandler: function() {
	document.forms['new'].submit();
    }
});

$(function() {
    $('#new').validate({
	rules: {
	    Name_1: { required: true, minlength: 2 },
	    Position: { required: true, range: [1,50] }
	},
	messages: { }
    });
});
//-->
</script>

<h1>{#Navigation_list#}</h1>
<div class="subheaders">
  {if $admin_settings.Ahelp == 1}
    <span data-uk-lightbox>
      <a data-type="iframe" href="index.php?do=help&amp;sub={$helpquery}&amp;noframes=1"><img src="{$imgpath}/s_help.png" alt="" /> {#GlobalHelp#}</a>
    </span>
    {/if}
  <span data-uk-lightbox>
    <a data-type="iframe" href="index.php?do=support&amp;sub=send_order&amp;noframes=1"><img src="{$imgpath}/send.png" alt="" /> {#SendOrder#}</a>
  </span>
</div>
<form action="" method="post" class="uk-overflow-auto">
  <table class="uk-table uk-table-middle uk-table-striped">
    <thead>
      <tr>
        <td class="headers">{$language.name.1}</td>
        <td class="headers">{$language.name.2}</td>
        <td class="headers">{$language.name.3}</td>
        <td class="headers">{#Navigation_tag#}</td>
        <td align="center" class="headers">{#Global_Position#}</td>
        <td align="center" class="headers">{#Global_Active#}</td>
        <td class="headers">{#Global_Actions#}</td>
      </tr>
    </thead>
    <tbody>
    {foreach from=$navis item=c}
      {assign var=pos value=$pos+1}
      <tr>
        <td ><input type="hidden" name="nid[{$c->Id}]" value="{$c->Id}" />
          <input class="uk-input uk-border-rounded" name="Name_1[{$c->Id}]" type="text" value="{$c->Name_1|sanitize}" /></td>
        <td >
        <input class="uk-input uk-border-rounded" name="Name_2[{$c->Id}]" type="text" value="{$c->Name_2|sanitize}" /></td>
        <td >
        <input class="uk-input uk-border-rounded" name="Name_3[{$c->Id}]" type="text" value="{$c->Name_3|sanitize}" /></td>
        <td ><strong>&#123;navigation id={$c->Id}&#125;</strong></td>
        <td align="center">
        <input class="uk-input uk-border-rounded" name="Position[{$c->Id}]" type="text" size="3" maxlength="3" value="{$c->Position}" /></td>
        <td>
          <label class="uk-display-inline-block"><input class="uk-radio uk-border-rounded" type="radio" name="Aktiv[{$c->Id}]" value="1" {if $c->Aktiv == 1}checked="checked"{/if} /> {#Yes#}</label>
          <label class="uk-display-inline-block"><input class="uk-radio uk-border-rounded" type="radio" name="Aktiv[{$c->Id}]" value="0" {if $c->Aktiv == 0}checked="checked"{/if} /> {#No#}</label>
        </td>
        <td>
          <a class="uk-button uk-button-small" href="index.php?do=navigation&amp;sub=edit&amp;id={$c->Id}" data-uk-icon="edit"></a>
          <br>
          <a class="uk-button uk-button-small" href="javascript: void(0);" onclick="if (confirm('{#Navigation_navdelc#}')) location.href = 'index.php?do=navigation&amp;sub=deletenavi&amp;id={$c->Id}';" data-uk-icon="close"></a>
        </td>
      </tr>
    {/foreach}
    </tbody>
  </table>
  <input type="submit" class="uk-button uk-button-small uk-button-primary uk-border-rounded" value="{#Save#}" />
  <input name="save" type="hidden" id="save" value="1" />
</form>

<br />
<br />

<form method="post" action="" autocomplete="off" name="new" id="new">
  {assign var=newpos value=$pos+1}
  <h3>{#Navigation_newnav#}</h3>
  <table class="uk-table">
    <tr>
      <td>{#Global_Name#} ({$language.name.1})</td>
      <td><input class="uk-input uk-border-rounded" name="Name_1" type="text" size="30" /></td>
    </tr>
    <tr>
      <td>{#Global_Name#}  ({$language.name.2})</td>
      <td><input class="uk-input uk-border-rounded" name="Name_2" type="text" size="30" /></td>
    </tr>
    <tr>
      <td>{#Global_Name#}  ({$language.name.3})</td>
      <td><input class="uk-input uk-border-rounded" name="Name_3" type="text" size="30" /></td>
    </tr>
    <tr>
      <td>{#Global_Position#}</td>
      <td><input name="Position" type="text" class="uk-input uk-border-rounded" size="4" maxlength="3" value="{$newpos}" /></td>
    </tr>
    <tr>
      <td>{#Global_Active#}</td>
      <td>
        <label><input name="Aktiv" class="uk-radio uk-border-rounded" type="radio" value="1" checked="checked" />{#Yes#}</label>
        <label><input type="radio" class="uk-radio uk-border-rounded" name="Aktiv" value="0" />{#No#}</label>
      </td>
    </tr>
  </table>
  <br />
  <input type="submit" class="uk-button uk-button-small uk-button-primary uk-border-rounded" value="{#Save#}" />
  <input name="new" type="hidden" id="new" value="1" />
</form>
