<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{$title|default:'Панель управления'}</title>
{include file="$incpath/head/head-admin-blank.tpl"}
</head>
<body style="display:none">

{* анимация загрузки *}
<div id="preloader"><img id="anim_floating" src="{$baseurl}/logo45.png" /></div>

  {if !empty($mesage_save)}{$mesage_save}{/if}
  {$content}

</body>
</html>