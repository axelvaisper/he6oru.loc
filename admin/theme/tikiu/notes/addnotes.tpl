<script>
<!-- //
$(function() {
    var id = '#{$types}notes';
    $(id).submit(function() {
        var options = {
            target: '#outajax',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return false;
    });
});
//-->
</script>

{if $types == 'add'}
  <form method="post" action="index.php?do=notes&sub=addnotes" id="addnotes" class="uk-text-center">
    <textarea class="uk-textarea uk-height-large" name="text_notes" placeholder="{#User_multiaction_message_nt#}"></textarea>
    <br />
    <br />
    <strong>{#Global_Type#}: </strong>
    <label><input type="radio" class="uk-radio" name="type" value="main" checked="checked" />{#NotesMain#}</label>
    <label><input type="radio" class="uk-radio" name="type" value="pub" />{#NotesPub#}</label>
    <br />
    <br />
    <input name="save" type="hidden" id="save" value="1" />
    <input type="submit" class="uk-button uk-button-default uk-button-small uk-width-1-1" value="{#Save#}" />
  </form>
{/if}
{if $types == 'edit'}
  <form method="post" action="index.php?do=notes&sub=editnotes" id="editnotes" class="uk-text-center">
    <input name="save" type="hidden" id="save" value="1" />
    <textarea class="uk-textarea uk-height-large" name="text_notes">{$enotes->Text|sanitize}</textarea>
    <br />
    <br />
    <strong>{#Global_Type#}: </strong>
    <label><input type="radio" class="uk-radio" name="type" value="main" {if $enotes->Type == 'main'}checked="checked"{/if} />{#NotesMain#}</label>
    <label><input type="radio" class="uk-radio" name="type" value="pub" {if $enotes->Type == 'pub'}checked="checked"{/if} />{#NotesPub#}</label>
    <br />
    <br />
    <input type="hidden" name="notid" value="{$enotes->Id}" />
    <input type="hidden" name="edit" value="1" />
    <input type="submit" class="uk-button uk-button-default uk-button-small uk-width-1-1" value="{#Save#}" />
  </form>
{/if}
