{if perm('notes') && $admin_settings.Aktiv_Notes == 1}

<script>
<!-- //
$(function() {
  $('#shownotes').on('click', function() {
      var options = {
          target: '#outajax',
          url: 'index.php?do=notes&sub=shownotes&type=all&key=' + Math.random(),
          timeout: 3000
      };
      $(this).ajaxSubmit(options);
      return true;
  });
  $('#main_notes').on('click', function() {
      var options = {
          target: '#outajax',
          url: 'index.php?do=notes&sub=shownotes&type=main&key=' + Math.random(),
          timeout: 3000
      };
      $(this).ajaxSubmit(options);
      return true;
  });
  $('#pub_notes').on('click', function() {
      var options = {
          target: '#outajax',
          url: 'index.php?do=notes&sub=shownotes&type=pub&key=' + Math.random(),
          timeout: 3000
      };
      $(this).ajaxSubmit(options);
      return true;
  });
  $('#addnotes').on('click', function() {
      var options = {
          target: '#outajax',
          url: 'index.php?do=notes&sub=addnotes&key=' + Math.random(),
          timeout: 3000
      };
      $(this).ajaxSubmit(options);
      return true;
  });
});
//-->
</script>

<div id="offcanvas-note" data-uk-offcanvas="flip: true; overlay: true">
  <div id="slidedown_content" class="uk-offcanvas-bar uk-padding-remove">

    <div id="outajax" data-uk-height-viewport="offset-bottom: 10"></div> 

    <input class="uk-button uk-button-small uk-button-primary uk-width-1-1" id="addnotes" type="button" onclick="javascript: void(0);" value="{#Global_Add#}" />

    <div class="uk-button-group uk-width-1-1">
      <input class="uk-button uk-button-small uk-button-default uk-width-1-3" id="shownotes" type="button" onclick="javascript: void(0);" value="{#Global_All#}" />
      <input class="uk-button uk-button-small uk-button-default uk-width-1-3" id="main_notes" type="button" onclick="javascript: void(0);" value="{#NotesMain#}" />
      <input class="uk-button uk-button-small uk-button-default uk-width-1-3" id="pub_notes" type="button" onclick="javascript: void(0);" value="{#NotesPub#}" />
    </div>

    <button class="uk-offcanvas-close uk-close uk-width-1-1 data-uk-icon" type="button" data-uk-close></button>

  </div>
</div>
{/if}