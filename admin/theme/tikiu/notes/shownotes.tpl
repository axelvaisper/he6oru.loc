{foreach from=$notes item=note}
<script>
<!-- //
$(function() {
    $('#delnotes_{$note->Id}').on('click', function() {
        var options = {
            target: '#outajax',
            url: 'index.php?do=notes&sub=delnotes&notid={$note->Id}',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
    $('#editnotes_{$note->Id}').on('click', function() {
        var options = {
            target: '#outajax',
            url: 'index.php?do=notes&sub=editnotes&notid={$note->Id}&edit=1',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>

<div class="uk-background-secondary">
  {$note->Datum|date_format: $lang.DateFormat}&nbsp;|&nbsp;
  <strong>{$note->Autor}</strong>&nbsp;|&nbsp;
  {if $note->Type == 'main'}{#NotesMain#}{else}{#NotesPub#}{/if}

  <div style="color:var(--gray-dark);background:var(--gray-vk)">{$note->Text|nl2br}</div>

  {if $note->UserId == $smarty.session.benutzer_id || $smarty.session.benutzer_id == 1}
    <div class="uk-button-group uk-width-1-1">
      <a id="editnotes_{$note->Id}" class="uk-button uk-button-default uk-button-small" href="javascript: void(0);">{#Edit#}</a>
      <a id="delnotes_{$note->Id}" href="javascript: void(0);" class="uk-button uk-button-small uk-button-default">{#Global_Delete#}</a>
    </div>
  {/if}
</div>
<hr>
{/foreach}
