<div id="offcanvas-right" data-uk-offcanvas="flip: true; overlay: true">
  <div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
 
    <div data-uk-height-viewport="offset-bottom: 10">
      {#Global_LoggedInAs#} <strong>{$smarty.session.user_name}</strong>
      {$section_switch}
      {$theme_switch}
      <ul class="uk-nav uk-nav-default">
        <li><a href="index.php?logout=1">{#Global_Logout#}</a></li>
      </ul>
    </div>

   <button class="uk-offcanvas-close uk-close uk-width-1-1 data-uk-icon" type="button" data-uk-close></button>

  </div>
</div> 
