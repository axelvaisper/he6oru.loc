<div class="header">{#Shop_articles_spez#} &bdquo;{$smarty.get.name|sanitize}&rdquo;</div>
<div class="header_inf">{#Shop_speci_inf#}</div>
<form method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr>
      <td class="headers">&nbsp;</td>
      <td class="headers">({$language.name.1})</td>
      <td class="headers">({$language.name.2})</td>
      <td class="headers">({$language.name.3})</td>
    </tr>
    <tr class="second">
      <td>{#Shop_speci_spec#}1 </td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_1" id="textfield" value="{$res.Spez_1}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_1_2" id="textfield" value="{$res.Spez_1_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_1_3" id="textfield" value="{$res.Spez_1_3}" /></td>
    </tr>
    <tr class="first">
      <td>{#Shop_speci_spec#}2</td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_2" id="textfield" value="{$res.Spez_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_2_2" id="textfield" value="{$res.Spez_2_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_2_3" id="textfield" value="{$res.Spez_2_3}" /></td>
    </tr>
    <tr class="second">
      <td>{#Shop_speci_spec#}3</td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_3" id="textfield" value="{$res.Spez_3}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_3_2" id="textfield" value="{$res.Spez_3_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_3_3" id="textfield" value="{$res.Spez_3_3}" /></td>
    </tr>
    <tr class="first">
      <td>{#Shop_speci_spec#}4</td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_4" id="textfield" value="{$res.Spez_4}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_4_2" id="textfield" value="{$res.Spez_4_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_4_3" id="textfield" value="{$res.Spez_4_3}" /></td>
    </tr>
    <tr class="second">
      <td>{#Shop_speci_spec#}5 </td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_5" id="textfield" value="{$res.Spez_5}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_5_2" id="textfield" value="{$res.Spez_5_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_5_3" id="textfield" value="{$res.Spez_5_3}" /></td>
    </tr>
    <tr class="first">
      <td>{#Shop_speci_spec#}6</td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_6" id="textfield" value="{$res.Spez_6}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_6_2" id="textfield" value="{$res.Spez_6_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_6_3" id="textfield" value="{$res.Spez_6_3}" /></td>
    </tr>
    <tr class="second">
      <td>{#Shop_speci_spec#}7</td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_7" id="textfield" value="{$res.Spez_7}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_7_2" id="textfield" value="{$res.Spez_7_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_7_3" id="textfield" value="{$res.Spez_7_3}" /></td>
    </tr>
    <tr class="first">
      <td>{#Shop_speci_spec#}8</td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_8" id="textfield" value="{$res.Spez_8}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_8_2" id="textfield" value="{$res.Spez_8_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_8_3" id="textfield" value="{$res.Spez_8_3}" /></td>
    </tr>
    <tr class="second">
      <td>{#Shop_speci_spec#}9 </td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_9" id="textfield" value="{$res.Spez_9}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_9_2" id="textfield" value="{$res.Spez_9_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_9_3" id="textfield" value="{$res.Spez_9_3}" /></td>
    </tr>
    <tr class="first">
      <td>{#Shop_speci_spec#}10 </td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_10" id="textfield" value="{$res.Spez_10}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_10_2" id="textfield" value="{$res.Spez_10_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_10_3" id="textfield" value="{$res.Spez_10_3}" /></td>
    </tr>
    <tr class="second">
      <td>{#Shop_speci_spec#}11 </td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_11" id="textfield" value="{$res.Spez_11}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_11_2" id="textfield" value="{$res.Spez_11_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_11_3" id="textfield" value="{$res.Spez_11_3}" /></td>
    </tr>
    <tr class="first">
      <td>{#Shop_speci_spec#}12</td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_12" id="textfield" value="{$res.Spez_12}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_12_2" id="textfield" value="{$res.Spez_12_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_12_3" id="textfield" value="{$res.Spez_12_3}" /></td>
    </tr>
    <tr class="second">
      <td>{#Shop_speci_spec#}13 </td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_13" id="textfield" value="{$res.Spez_13}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_13_2" id="textfield" value="{$res.Spez_13_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_13_3" id="textfield" value="{$res.Spez_13_3}" /></td>
    </tr>
    <tr class="first">
      <td>{#Shop_speci_spec#}14</td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_14" id="textfield" value="{$res.Spez_14}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_14_2" id="textfield" value="{$res.Spez_14_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_14_3" id="textfield" value="{$res.Spez_14_3}" /></td>
    </tr>
    <tr class="second">
      <td>{#Shop_speci_spec#}15</td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_15" id="textfield" value="{$res.Spez_15}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_15_2" id="textfield" value="{$res.Spez_15_2}" /></td>
      <td><input class="input" type="text" style="width: 190px" name="Spez_15_3" id="textfield" value="{$res.Spez_15_3}" /></td>
    </tr>
  </table>
  <input class="button" type="submit" name="button" id="button" value="{#Save#}" />
  <input name="save" type="hidden" id="save" value="1" />
  <input type="button" onclick="closeWindow();" class="button" value="{#Close#}" />
</form>
