{if perm('shop') && admin_active('shop') && !empty($votes)}
<script>
<!-- //
$(function() {
    toggleCookie('votes_navi', 'votes_open', 30, '{$basepath}');
});
//-->
</script>

<div class="header">
  <div id="votes_navi" class="navi_toggle"><img src="{$imgpath}/toggle.png" alt="" /></div>
  <img src="{$imgpath}/votes.png" alt="" /> {#ShopStartVotes#}
</div>

<div id="votes_open" class="sysinfos">
    <table class="uk-table">
      {foreach from=$votes item=o}
        <tr class="{cycle values='second,first'}">
          <td>
            {#ShopNewVotes#}: <strong>{$o.count}</strong>
          </td>
          <td data-uk-lightbox>
            <a data-type="iframe" title="{$lang.Shop_prodvotes}" href="index.php?do=shop&amp;sub=prodvotes&amp;id={$o.Produkt}&amp;name={$o.Titel|sanitize}&amp;noframes=1">{$o.Titel|slice: 60: '...'|sanitize}</a>
          </td>
          <td data-uk-lightbox nowrap="nowrap">
            <a data-type="iframe" title="{$lang.Shop_articles_edit}" href="index.php?do=shop&amp;sub=edit_article&amp;id={$o.Produkt}&amp;noframes=1" data-uk-icon="edit"></a>
          </td>
        </tr>
      {/foreach}
    </table>
</div>
{/if}
