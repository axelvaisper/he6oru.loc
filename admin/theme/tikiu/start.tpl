<script>
<!-- //
$(function() {
    $('#cc').on('click', function(){
        var options = {
            target: '#ccc',
            url: 'index.php?do=main&sub=cache&key=' + Math.random(),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
    $('#ctc').on('click', function() {
        var options = {
            target: '#ctcc',
            url: 'index.php?do=main&sub=compiled&key=' + Math.random(),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
    $('#dbopt').submit(function() {
        var options = {
            target: '#db_res',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return false;
    });
   $('#sqlquery').submit(function() {
        var options = {
            target: '#query_res',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return false;
    });
});
//-->
</script>

  {if perm('settings')}
    {if !empty($sx_update)}
          <div class="error_box">
              <h4>{$sx_update}</h4>
                <strong>{#Start_Version#}: {$settings.Version|default:'Неизвестно'}</strong>
                <strong>{#New_Version#}: {$version|default:'Неизвестно'}</strong>
                <input type="button" class="button" onclick="location.href='index.php?do=update';" value="{#Forums_delT_submit#}" />
          </div>
    {/if}
    {if $warning}
      <div class="error_box"><h4>{$warning}</h4></div>
    {/if}
  {/if}

{* {$StartInfos} *}
{$startVotes}
{$startOrders}
{$NewFaq}
{$ErrorLinks}
{$NewComments}
{$NewUsers}
{$OnlineUser}
{* {$dbopt} *}

{$NewForumPosts}
{$CacheDel}
{$Sql}
{* {$sysactive} *}
{$sysinfo}
