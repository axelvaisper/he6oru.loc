<?php
########################################################################
# ******************  SX CONTENT MANAGEMENT SYSTEM  ****************** #
# *       Copyright © Alexander Voloshin * All Rights Reserved       * #
# ******************************************************************** #
# *  http://sx-cms.ru   *  cms@sx-cms.ru  *   http://www.status-x.ru * #
# ******************************************************************** #
########################################################################
if (!defined('SX_DIR')) {
    header('Refresh: 0; url=/index.php?p=notfound', true, 404); exit;
}
$config['dbhost']   = 'localhost'; // Адрес хоста базы MySQL
$config['dbport']   = '3306'; // Порт базы MySQL
$config['dbuser']   = 'root'; // Пользователь базы MySQL
$config['dbpass']   = ''; // Пароль базы MySQL
$config['dbname']   = 'he6oru'; // Название базы MySQL
$config['dbprefix'] = 'sx'; // Префикс базы MySQL
$config['dbcharset']  = 'utf8'; // Кодировка базы MySQL
$config['dbsesslife'] = '7200'; // Время хранения сессии в секундах в базе MySQL
$config['type_sess']  = 'base'; // Способ хранения сессий, в базе - base, на сервере - file

# u28939_he6oru
# tB8irifV80Ek
