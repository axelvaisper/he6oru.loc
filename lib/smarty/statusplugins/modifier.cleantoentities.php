<?php
function smarty_modifier_cleantoentities($text) {
    $text = str_replace(array('₽', '₴', '€', '»', '«', '©', '®', '™', '„', '“'), array('&#8381;', '&#8372;', '&euro;', '&raquo;', '&laquo;', '&copy;', '&reg;', '&trade;', '&bdquo;', '&ldquo;'), $text);
    return $text;
}

?>
