<?php
function smarty_modifier_specialchars($text) {
    $text = str_replace(array('©', '®', '\'', '₽', '₴', '€', '»', '«', '™', '„', '“'), array('&copy;', '&reg;', '&#039;', '&#8381;', '&#8372;', '&euro;', '&raquo;', '&laquo;', '&trade;', '&bdquo;', '&ldquo;'), $text);
    return $text;
}

?>
