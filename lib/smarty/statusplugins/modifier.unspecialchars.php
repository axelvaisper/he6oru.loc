<?php
function smarty_modifier_unspecialchars($string) {
	return str_replace(array(' &amp; ', '&#8381;', '&#8372;', '&euro;', '&reg;', '&copy;'), array(' & ', '₽', '₴', '€', '®', '©'), $string);
}
?>
