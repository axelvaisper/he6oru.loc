{if $toparticleitems}
    <div>
      {foreach from=$toparticleitems item=content}
          <div class="box-content clearfix">
            {if !empty($content.TopcontentBild)}
                <a href="index.php?p=articles&amp;area={$content.Sektion}&amp;action=displayarticle&amp;id={$content.Id}&amp;name={$content.Titel|translit}">
                  <img class="img-fluid img-left" src="uploads/articles/{$content.TopcontentBild}" alt="{$content.Titel|sanitize}" />
                </a>
            {elseif !empty($content.Bild)}
                <a href="index.php?p=articles&amp;area={$content.Sektion}&amp;action=displayarticle&amp;id={$content.Id}&amp;name={$content.Titel|translit}">
                  <img class="img-fluid {if $content.Bildausrichtung == 'right'}img-right{else}img-left{/if}" src="uploads/articles/{$content.Bild}" alt="{$content.Titel|sanitize}" />
                </a>
            {/if}
            <h3>
              <a href="index.php?p=articles&amp;area={$content.Sektion}&amp;action=displayarticle&amp;id={$content.Id}&amp;name={$content.Titel|translit}">{$content.Titel|sanitize}</a>
            </h3>
            {$content.Inhalt|html_truncate:400}
          </div>
      {/foreach}
    </div>
{/if}
