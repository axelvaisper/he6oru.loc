<h1 class="title">{#Birthdays_Today#}</h1>
<div class="box-content">
  {if !empty($birthdays)}
      {foreach from=$birthdays item=bd}
          <div class="text-truncate">
            <a href="index.php?p=user&amp;id={$bd.Id}&amp;area={$area}">
              <i class="icon-star"></i>{$bd.Benutzername|sanitize} ({$bd.Age})
            </a>
          </div>
      {/foreach}
  {else}
      {#Birthdays_Today_No#}
  {/if}
</div>
