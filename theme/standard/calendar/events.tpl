{if $events}
    {foreach from=$events item=event} 
        <div class="h3 title">{$event->Titel|sanitize}</div>
        <div class="box-block">{$event->descr}</div>
        <div class="box-content">
          <div class="row">
            <div class="col-3">{#Calendar_addedfrom#}:</div>
            <div class="col-9">
              <a href="index.php?p=user&amp;id={$event->Benutzer}&amp;area={$area}">{$event->UserName}</a>
            </div>
          </div>

          <div class="row">
            <div class="col-3">{#Calendar_addedon#}:</div>
            <div class="col-9">{$event->Datum}</div>
          </div>

          <div class="row">
            <div class="col-3">{#Calendar_period#}:</div>
            <div class="col-9">
              {if $event->wd == 1}
                  {#Calendar_wholeDay#}
                  {if $event->Erledigt == 1}
                      ({#GlobalOk#})
                  {/if}
              {else}
                  {$event->Start|date_format: $lang.DateFormat} - {$event->Ende|date_format: $lang.DateFormat}
                  {if $event->Erledigt == 1}
                      ({#GlobalOk#})
                  {/if}
              {/if}
            </div>
          </div>

          <div class="row">
            <div class="col-3">{#Calendar_weight#}:</div>
            <div class="col-9">{$event->weight}</div>
          </div>

          {if $smarty.session.benutzer_id == $event->Benutzer || permission('edit_all_events')}
              <div class="text-center pt-2">
                <a class="btn btn-sm btn-primary" href="index.php?p=calendar&amp;action=editevent&amp;show={$smarty.request.show}&amp;month={$smarty.request.month}&amp;year={$smarty.request.year}&amp;day={$smarty.request.day}&amp;id={$event->Id}&amp;area={$area}">{#Calendar_editEvent#}</a>
                <a class="btn btn-sm btn-primary" onclick="return confirm('{#Calendar_delC#}');" href="index.php?p=calendar&amp;action=delevent&amp;show={$smarty.request.show}&amp;month={$smarty.request.month}&amp;year={$smarty.request.year}&amp;day={$smarty.request.day}&amp;id={$event->Id}&amp;area={$area}">{#Calendar_eventDel#}</a>
              </div>
          {/if}
        </div>
    {/foreach}
{else}
    <div class="box-content">
      {#Calendar_noEvents#}
    </div>
{/if}
