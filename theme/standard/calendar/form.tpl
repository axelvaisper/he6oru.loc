<div class="h3 title">{#Calendar_jumpdate#}</div>
<div class="box-content">
  <form method="post" action="index.php">
    <div class="input-group">
      <select class="form-control" name="month">
        {foreach from=$month name=month item=m}
            <option value="{$smarty.foreach.month.index+1}"{if $smarty.foreach.month.index+1 == $currentmonth} selected="selected"{/if}>{$m}</option>
        {/foreach}
      </select>

      <select class="form-control" name="year">
        {section name=year loop=$startYear+10 name=year start=$startYear}
            <option value="{$smarty.section.year.index}"{if $Year == $smarty.section.year.index} selected="selected"{/if}>{$smarty.section.year.index}</option>
        {/section}
      </select>

      <select class="form-control" name="show">
        <option value="public"{if $privatePublic == 'public'} selected="selected"{/if}>{#Calendar_public#}</option>
        {if $loggedin}
            <option value="private"{if $privatePublic == 'private'} selected="selected"{/if}>{#Calendar_private#}</option>
        {/if}
      </select>

      <span class="input-group-append">
        <input class="btn btn-primary" name="submit" type="submit" value="{#Calendar_jumpB#}" />
      </span>
    </div>

    <input type="hidden" name="p" value="calendar" />
    <input type="hidden" name="area" value="{$area}" />
  </form>
</div>
