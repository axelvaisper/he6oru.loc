<h1 class="title">{#Calendar_MyEvents#}</h1>
<div class="wrapper">
  {if $results}
      {foreach from=$results item=res}
          <div class="row {cycle name='row' values='row-primary,row-secondary'}">
            <div class="col text-left text-truncate">
              <a href="index.php?p=calendar&amp;action=events&amp;show={$res->Typ}&amp;month={$res->month}&amp;year={$res->year}&amp;day={$res->day}&amp;area={$smarty.request.area}#{$res->Id}">
                {$res->Titel|sanitize}
              </a>
            </div>

            <div class="col-auto text-right text-nowrap">
              {$res->Start|date_format:$lang.DateFormatExtended}
            </div>
          </div>
      {/foreach}
  {else}
      <div class="box-content">
        {#Calendar_search_noinsert#}
      </div>
  {/if}
</div>

<div class="h3 title">{#Calendar_search#}</div>
<div class="box-content">
  <form name="sf" action="index.php" method="get">
    <div class="input-group">
      <input class="form-control" type="text" name="qc" value="{$smarty.request.qc|sanitize}" />
      <span class="input-group-append">
        <input class="btn btn-primary" type="submit" value="{#Calendar_search#}" />
      </span>
    </div>

    <input name="area" type="hidden" value="{$area}" />
    <input name="p" type="hidden" value="calendar" />
    <input name="action" type="hidden" value="myevents" />
  </form>
</div>
