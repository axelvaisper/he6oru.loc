<script>
<!-- //
$(function() {
    var options_prev = {
        target: '#calraw', url: '{$prevMonth}', timeout: 3000
    };
    var options_next = {
        target: '#calraw', url: '{$nextMonth}', timeout: 3000
    };
    $('#switchcal_prev').on('click', function() {
        $(this).ajaxSubmit(options_prev);
        return false;
    });
    $('#switchcal_next').on('click', function() {
        $(this).ajaxSubmit(options_next);
        return false;
    });
});
//-->
</script>

<div class="row no-gutters calendarTable">
  <div class="col text-left"><a id="switchcal_prev" href="javascript:void(0);"><i class="icon-left-open"></i></a></div>
  <div class="col-auto text-center font-weight-bold">{$header}</div>
  <div class="col text-right"><a id="switchcal_next" href="javascript:void(0);"><i class="icon-right-open"></i></a></div>
</div>

<table class="calendarTable">
  <tr>
    <td class="calendarHeader">&nbsp;</td>
    {foreach from=$DayNamesShortArray item=day}
        <td title="{$day}" class="calendarHeader"> {$day|truncate:2:false}. </td>
    {/foreach}
  </tr>
  {foreach from=$cal_data item=cd}
      <tr>
        <td class="text-center calendarBlanc">
          <a title="{$cd->StartWeek|date_format:'%d.%m.%Y'} - {$cd->EndWeek|date_format:'%d.%m.%Y'} " style="font-weight: bold" href="{$baseurl}/index.php?p=calendar&amp;show=public&amp;action=week&amp;weekstart={$cd->StartWeek}&amp;weekend={$cd->EndWeek}&amp;area={$area}">
            <i class="icon-right-open"></i>
          </a>
        </td>
        {foreach from=$cd->CalDataInner item=td}
            <td class="text-right {$td->tdclass}">
              {$td->thelink}
            </td>
        {/foreach}
      </tr>
  {/foreach}
</table>
