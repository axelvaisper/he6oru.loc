<h1 class="title">{#Calendar_search_res#} &bdquo;{$smarty.request.qc|sanitize}&ldquo;</h1>
<div class="wrapper">
  {if $results}
      {foreach from=$results item=res}
          <div class="row {cycle name='row' values='row-primary,row-secondary'}">
            <div class="col text-left text-truncate">
              <a href="index.php?p=calendar&amp;action=events&amp;show={$res->Typ}&amp;month={$res->month}&amp;year={$res->year}&amp;day={$res->day}&amp;area={$smarty.request.area}#{$res->Id}">
                <i class="icon-right-open"></i>{$res->Titel|sanitize}
              </a>
            </div>

            <div class="col-auto text-right text-nowrap">
              {$res->Start|date_format:$lang.DateFormatExtended}
            </div>
          </div>
      {/foreach}
  {else}
      <div class="box-content">
        {#Calendar_search_noinsert#}
      </div>
  {/if}
</div>

{include file="$incpath/calendar/search.tpl"}
