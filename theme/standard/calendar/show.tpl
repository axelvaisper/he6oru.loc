<div class="wrapper">
  {$calendar}
</div>

<div class="row">
  <div class="col-md-6">
    {$calendar_prev}
  </div>
  <div class="col-md-6">
    {$calendar_next}
  </div>
</div>

<div class="wrapper">
  {include file="$incpath/calendar/actions.tpl"}
  {include file="$incpath/calendar/form.tpl"}
  {include file="$incpath/calendar/search.tpl"}
</div>

<div class="d-flex justify-content-center">
  <div class="p-2">
    <i class="icon-star"></i>{#Birthdays_Today#}
  </div>
  <div class="p-2">
    <i class="icon-warning-empty"></i>{#Calendar_leg_import#}
  </div>
  <div class="p-2">
    <i class="icon-cw"></i>{#Calendar_period#}
  </div>
</div>
