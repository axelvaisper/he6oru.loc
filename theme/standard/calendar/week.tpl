<h1 class="title">
  {#Calendar_weekview#} ({$smarty.request.weekstart|date_format:'%d.%m.%Y'} - {$smarty.request.weekend|date_format:'%d.%m.%Y'})
</h1>

<div class="row wrapper">
  <div class="col text-left">
    <a href="index.php?p=calendar&amp;show={$smarty.request.show}&amp;action=week&amp;weekstart={$week_pref_start}&amp;weekend={$week_pref_end}&amp;area={$area}">
      <i class="icon-left-open"></i>{#Calendar_prefWeek#} ({$week_pref_start|date_format:'%d.%m.%Y'})
    </a>
  </div>
  <div class="col text-right">
    <a href="index.php?p=calendar&amp;show={$smarty.request.show}&amp;action=week&amp;weekstart={$week_next_start}&amp;weekend={$week_next_end}&amp;area={$area}">
      ({$week_next_start|date_format:'%d.%m.%Y'}) {#Calendar_nextWeek#}<i class="icon-right-open"></i>
    </a>
  </div>
</div>

{assign var=y_1 value=$smarty.request.weekstart|date_format:'%Y'}
{assign var=y_2 value=$smarty.request.weekend|date_format:'%Y'}
{assign var=m_1 value=$smarty.request.weekstart|date_format:'%m'}
{assign var=m_2 value=$smarty.request.weekend|date_format:'%m'}

<div class="row wrapper">
  <div class="col-md-9">
    {assign var=cal_month value=$m_1|regex_replace:'/^0/':''}
    {assign var=cal_year  value=$y_1}

    {section name=wotag loop=7 step=1 max=10}
        {assign var=t value=$smarty.section.wotag.index}
        {assign var=day_start value=$smarty.request.weekstart|date_format:'%d'}
        {assign var=month_start_pre value=$smarty.request.weekstart|date_format:'%m'|regex_replace:'/^0/':''}
        {assign var=month_start value=$month_start_pre-1}
        {assign var=day_date value=$dates_array[$t]|date_format:'%d'|regex_replace:'/^0/':''}
        {if $smarty.section.wotag.first}
            <div class="row">
              <div class="col calendarHeaderWeekBig">
                {assign var=m_p value=$smarty.request.weekstart|date_format:'%m'}
                {assign var=m_a value=$m_p-1}
                {$month_array[$m_a]} {$smarty.request.weekstart|date_format:'%Y'}
              </div>
            </div>
        {/if}

        <div class="row calendarHeaderWeek">
          <div class="col font-weight-bold">
            {$days.$t}
          </div>
          {if permission('calendar_event') || permission('calendar_event_new')}
              <div class="col-auto text-right">
                <a href="index.php?p=calendar&amp;action=newevent&amp;day={$day_date}&amp;month={$cal_month}&amp;year={$cal_year}&amp;area={$area}&amp;show={$smarty.request.show}">{#Calendar_newEvent#}</a>
              </div>
          {/if}
        </div>

        <div class="row">
          <div class="col-1 h2" title="{$day_date}.{$cal_month}.{$cal_year}">
            {$day_date}
          </div>
          <div class="col">
            {foreach from=$items item=ditem name=is}
                {assign var=dow_pref value=$ditem->Start|date_format:'%w'}
                {assign var=dow value=$dow_pref-1}
                {if $dow == "-1"}
                    {assign var=dow value=6}
                {/if}
                {if $dow == $t}
                    <div class="row">
                      <div class="col text-truncate">
                        <a href="index.php?p=calendar&amp;action=events&amp;show=public&amp;month={$ditem->Start|date_format:'%m'}&amp;year={$ditem->Start|date_format:'%Y'}&amp;day={$ditem->Start|date_format:'%d'}&amp;area={$area}#{$ditem->Id}">
                          <i class="icon-right-open"></i>{$ditem->Titel|sanitize}
                        </a>
                      </div>
                      {if permission('calendar_event') || permission('calendar_event_new')}
                          <div class="col-auto text-right">
                            {if $ditem->wd == 1}
                                {#Calendar_wholeDay#}
                            {else}
                                {$ditem->Start|date_format:'%H:%M'} - {$ditem->Ende|date_format:'%H:%M'}
                            {/if}
                          </div>
                      {/if}
                    </div>
                {/if}
            {/foreach}
            {if !empty($birthdays.$day_date)}
                {$birthdays.$day_date}
            {/if}
          </div>
        </div>

            {if $day_date == $days_inmonth[$month_start]}
            <div class="title">
              {if $day_date == $days_inmonth[$month_start]}
                  {assign var=cal_month value=$second_month[0]|regex_replace:'/^0/':''}
                  {assign var=cal_year  value=$second_month[1]}
              {/if}
              {assign var=m_n_pref value=$second_month[0]|regex_replace:'/^0/':''}
              {assign var=m_n value=$m_n_pref-1}
              {$month_array[$m_n]} {$second_month[1]}
            </div>
        {/if}
    {/section}
  </div>

  <div class="col-md-3">
    <div class="wrapper">
      {$calendar_now}
    </div>
    <div class="wrapper">
      {$calendar_next}
    </div>
  </div>
</div>

{include file="$incpath/calendar/form.tpl"}
