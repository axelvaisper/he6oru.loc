<h1 class="title">{#Calendar_yearView#}</h1>

<div class="wrapper">
  {include file="$incpath/calendar/actions.tpl"}
</div>

<div class="row wrapper">
  <div class="col-2 text-left">
    <a class="text-nowrap" href="index.php?p=calendar&amp;area={$area}&amp;action=displayyear&amp;show={$showtype}&amp;year={$year_prev}">
      <i class="icon-left-open"></i>{$year_prev}
    </a>
  </div>

  <div class="col text-center">
    <form method="post" action="index.php?sy=1&amp;p=calendar&amp;area={$area}&amp;action=displayyear">
      <div class="input-group">
        <select class="form-control" name="year">
          {section name=year loop=$startYear+10 name=year start=$startYear}
              <option value="{$smarty.section.year.index}"{if $Year == $smarty.section.year.index} selected="selected"{/if}>{$smarty.section.year.index}</option>
          {/section}
        </select>

        <select class="form-control" name="show">
          <option value="public"{if $showtype == 'public'} selected="selected"{/if}>{#Calendar_public#}</option>
          {if $loggedin}
              <option value="private"{if $showtype == 'private'} selected="selected"{/if}>{#Calendar_private#}</option>
          {/if}
        </select>

        <span class="input-group-append">
          <input class="btn btn-primary" name="submit" type="submit" value="{#Calendar_jumpB#}" />
        </span>
      </div>
    </form>
  </div>

  <div class="col-2 text-right">
    <a class="text-nowrap" href="index.php?p=calendar&amp;area={$area}&amp;action=displayyear&amp;show={$showtype}&amp;year={$year_next}">
      {$year_next}<i class="icon-right-open"></i>
    </a>
  </div>
</div>

{$years}

{include file="$incpath/calendar/actions.tpl"}
