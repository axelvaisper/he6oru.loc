<div class="calendarBorder">
  <table class="calendarTable">
    <tr>
      <td class="text-center font-weight-bold" colspan="8">
        <a href="index.php?p=calendar&amp;month={$linkmonth}&amp;year={$linkyear}&amp;area={$area}&amp;show={$showtype}">{$header}</a>
      </td>
    </tr>
    <tr>
      <td class="calendarHeader">&nbsp;</td>
      {foreach from=$DayNamesShortArray item=day}
          <td class="calendarHeader" title="{$day}">
            {$day|truncate:2:false}.
          </td>
      {/foreach}
    </tr>
    {foreach from=$cal_data item=cd}
        <tr>
          <td class="text-center calendarBlanc">
            <a title="{$cd->StartWeek|date_format:'%d.%m.%Y'} - {$cd->EndWeek|date_format:'%d.%m.%Y'}" href="index.php?p=calendar&amp;show={$showtype}&amp;action=week&amp;weekstart={$cd->StartWeek}&amp;weekend={$cd->EndWeek}&amp;area={$area}">
              <i class="icon-right-open"></i>
            </a>
          </td>
          {foreach from=$cd->CalDataInner item=td}
              <td class="text-right {$td->tdclass}">
                {$td->thelink}
              </td>
          {/foreach}
        </tr>
    {/foreach}
  </table>
</div>
