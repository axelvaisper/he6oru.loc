<div class="wrapper">
  <h1 class="title">{#Gaming_cheats#}</h1>
  {include file="$incpath/cheats/search.tpl"}

  {if !empty($Categs) && empty($smarty.request.plattform)}
      <div class="box-content">
        <div class="row">
          {assign var=lcc value=0}
          {foreach from=$Categs item=c name=categ}
              <div class="col-md-6 my-1">
                <a class="font-weight-bold" href="{$c->HLink}">
                  <i class="icon-folder-open"></i>{$c->Name|sanitize}{if $c->LinkCount >= 1} ({$c->LinkCount}){/if}
                </a>
              </div>
              {assign var=lcc value=$lcc+1}
              {if $lcc % 2 == 0 && !$smarty.foreach.categ.last}
              </div>
              <div class="row">
              {/if}
          {/foreach}
        </div>
      </div>
  {/if}

  {if !empty($smarty.request.plattform)}
      {if $Entries}
          <div class="spacer flex-line justify-content-center">
            <span class="mr-3">{#SortBy#}:</span>
            <a class="mr-3" href="index.php?p=cheats&amp;area={$area}&amp;plattform={$smarty.request.plattform|default:1}&amp;name={$CategName}&amp;page={$smarty.request.page|default:1}&amp;sort={$datesort|default:'dateasc'}">
              {#SortDate#}<i class="{$img_date|default:'icon-sort'}"></i>
            </a>
            <a class="mr-3" href="index.php?p=cheats&amp;area={$area}&amp;plattform={$smarty.request.plattform|default:1}&amp;name={$CategName}&amp;page={$smarty.request.page|default:1}&amp;sort={$namesort|default:'nameasc'}">
              {#SortName#}<i class="{$img_name|default:'icon-sort'}"></i>
            </a>
            <a class="mr-3" href="index.php?p=cheats&amp;area={$area}&amp;plattform={$smarty.request.plattform|default:1}&amp;name={$CategName}&amp;page={$smarty.request.page|default:1}&amp;sort={$hitssort|default:'hitsdesc'}">
              {#Downloads#}<i class="{$img_hits|default:'icon-sort'}"></i>
            </a>
          </div>
          {include file="$incpath/cheats/list.tpl"}
      {else}
          <div class="box-content">{#Gaming_cheats_no#}</div>
      {/if}
  {else}
      {if $Entries}
          <div class="h3 title">{#New_Cheats#}</div>
          {include file="$incpath/cheats/list.tpl"}
      {/if}
  {/if}
</div>
