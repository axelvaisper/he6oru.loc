{if $contact_fields}
{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#{$form_id}').validate({
        rules: {
            {foreach from=$contact_fields item=cf name=valic}
           '{$cf->Name}': {
                {if $cf->Email == 1}
                email: true{if $cf->Pflicht == 1 || $cf->Zahl == 1},{/if}
                {/if}
                {if $cf->Pflicht == 1}
                required: true{if $cf->Zahl == 1},{/if}
                {/if}
                {if $cf->Zahl == 1}
                 number: true
                {/if}
            },
            {/foreach}
            '{#Contact_myName#}': { required: true },
            '{#SendEmail_Email#}': { required: true, email: true }
       },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                success: function(data) {
                    if (data === 'true') {
                        $(form).resetForm();
                        $('#contact-text-{$form_id}').text('{#Contact_thankyou#}');
                    } else {
                        $('#contact-text-{$form_id}').text('{#Global_error#}');
                    }
                     showModal($('#contact-{$form_id}'), 5000);
                },
                clearForm: false
            });
        }
    });
});
function submitOtherData() {
    document.getElementById('___hmail').value = document.getElementById('ye_{$form_id}').value;
    document.getElementById('___hname').value = document.getElementById('yn_{$form_id}').value;
}
//-->
</script>

    <div class="box-content">
      {if $form_intro}
          <div class="wrapper">
            {$form_intro|sanitize}
          </div>
      {/if}
      <form onsubmit="submitOtherData();" id="{$form_id}" method="post" enctype="multipart/form-data" action="index.php?p=contact">
        <input type="hidden" name="__hmail" id="___hmail" />
        <input type="hidden" name="__hname" id="___hname" />
        <input type="hidden" name="id" value="{$form_id_raw}" />

        <div class="row form-group">
          <div class="col-md-3 col-sm-12">
            <label class="col-form-label" for="yn_{$form_id}">{#Contact_myName#}</label>
          </div>
          <div class="col-md-9 col-sm-12">
            <input class="form-control" id="yn_{$form_id}" name="{#Contact_myName#}" type="text" value="{$smarty.session.user_name}" />
          </div>
        </div>
        <div class="row form-group">
          <div class="col-md-3 col-sm-12">
            <label class="col-form-label" for="ye_{$form_id}">{#SendEmail_Email#}</label>
          </div>
          <div class="col-md-9 col-sm-12">
            <input class="form-control" id="ye_{$form_id}" name="{#SendEmail_Email#}" type="text" value="{$smarty.session.login_email}" />
          </div>
        </div>

        {foreach from=$contact_fields item=cf}
            {if $cf->Typ == 'textfield'}
                <div class="row form-group">
                  <div class="col-md-3 col-sm-12">
                    <label class="col-form-label" for="cf_{$cf->Id}">{$cf->Name|sanitize}</label>
                  </div>
                  <div class="col-md-9 col-sm-12">
                    <input class="form-control" id="cf_{$cf->Id}" name="{$cf->Name}" type="text" value="{$cf->Werte|sanitize}" />
                  </div>
                </div>
            {elseif $cf->Typ == 'radio'}
                <div class="row form-group">
                  <div class="col-md-3 col-sm-12">
                    <label class="col-form-label">{$cf->Name|sanitize}</label>
                  </div>
                  <div class="col-md-9 col-sm-12">
                    <div id="cf_{$cf->Id}"></div>
                    {foreach from=$cf->OutElemVal item=rw key=key}
                        <div class="form-check">
                          <input class="form-check-input" id="cf_{$cf->Id}_{$key}" name="{$cf->Name}" value="{$rw}" type="radio"{if $smarty.foreach.rwn.first} checked="checked"{/if} />
                          <label class="form-check-label" for="cf_{$cf->Id}_{$key}">{$cf->Name|sanitize}</label>
                        </div>
                    {/foreach}
                  </div>
                </div>
            {elseif $cf->Typ == 'checkbox'}
                <div class="row form-group">
                  <div class="col-md-3 col-sm-12">
                    <label class="col-form-label">{$cf->Name|sanitize}</label>
                  </div>
                  <div class="col-md-9 col-sm-12">
                    <div id="cf_{$cf->Id}"></div>
                    {foreach from=$cf->OutElemVal item=rw key=key}
                        <div class="form-check">
                          <input class="form-check-input" id="cf_{$cf->Id}_{$key}" name="{$cf->Name}[]" value="{$rw}" type="checkbox" {if $smarty.foreach.rwn.first}checked="checked"{/if} />
                          <label class="form-check-label" for="cf_{$cf->Id}_{$key}">{$cf->Name|sanitize}</label>
                        </div>
                    {/foreach}
                  </div>
                </div>
            {elseif $cf->Typ == 'dropdown'}
                <div class="row form-group">
                  <div class="col-md-3 col-sm-12">
                    <label class="col-form-label" for="cf_{$cf->Id}">{$cf->Name|sanitize}</label>
                  </div>
                  <div class="col-md-9 col-sm-12">
                    <select class="form-control" id="cf_{$cf->Id}" name="{$cf->Name}">
                      {foreach from=$cf->OutElemVal item=rw}
                          <option value="{$rw}">{$rw}</option>
                      {/foreach}
                    </select>
                  </div>
                </div>
            {elseif $cf->Typ == 'textarea'}
                <div class="row form-group">
                  <div class="col-md-3 col-sm-12">
                    <label class="col-form-label" for="cf_{$cf->Id}">{$cf->Name|sanitize}</label>
                  </div>
                  <div class="col-md-9 col-sm-12">
                    <textarea class="form-control" id="cf_{$cf->Id}" name="{$cf->Name}" rows="5"></textarea>
                  </div>
                </div>
            {/if}
        {/foreach}
        {if $form_attachment}
            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label">{#Contact_attachment_mes#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                {section name=xx loop=$form_attachment}
                    <input class="form-control" name="files[]" type="file" />
                {/section}
              </div>
            </div>
        {/if}

        {include file="$incpath/other/captcha.tpl"}

        {if $loggedin}
            <div class="row form-group">
              <div class="col-md-12 col-sm-12">
                <div class="form-check">
                  <input class="form-check-input" id="mailcopy" name="mailcopy" value="1" type="checkbox" checked="checked" />
                  <label class="form-check-label" for="mailcopy">{#Contact_wish_mailcopy#}</label>
                </div>
              </div>
            </div>
        {/if}

        <div style="text-align: center">
          <input class="btn btn-primary btn-block-sm" type="submit" value="{$contact_button|sanitize|default:$lang.ButtonSend}" />
        </div>
      </form>
    </div>

    <div class="modal fade" id="contact-{$form_id}" tabindex="-1" role="dialog" aria-labelledby="contact-title-{$form_id}" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4" id="contact-title-{$form_id}">{#Notification#}</div>
            <button type="button" class="close" data-dismiss="modal" aria-label="{#WinClose#}">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center h5" id="contact-text-{$form_id}">{#Contact_thankyou#}</div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-primary" data-dismiss="modal">{#WinClose#}</button>
          </div>
        </div>
      </div>
    </div>
{/if}
