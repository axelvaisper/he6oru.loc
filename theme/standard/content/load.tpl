{if $content_ex_res}
    <div class="wrapper">
      <div class="box-content clearfix">
        {if !empty($content_ex_res.TopcontentBild)}
            <a href="index.php?p=content&amp;id={$content_ex_res.Id}&amp;name={$content_ex_res.Titel|translit}&amp;area={$content_ex_res.Sektion}">
              <img class="img-fluid img-left" src="uploads/content/{$content_ex_res.TopcontentBild}" alt="{$content_ex_res.Titel|sanitize}" />
            </a>
        {elseif !empty($content_ex_res.Bild)}
            <a href="index.php?p=content&amp;id={$content_ex_res.Id}&amp;name={$content_ex_res.Titel|translit}&amp;area={$content_ex_res.Sektion}">
              <img class="img-fluid {if $content_ex_res.BildAusrichtung == 'right'}img-right{else}img-left{/if}" src="uploads/content/{$content_ex_res.Bild}" alt="{$content_ex_res.Titel|sanitize}" />
            </a>
        {/if}
        <h3>
          <a href="index.php?p=content&amp;id={$content_ex_res.Id}&amp;name={$content_ex_res.Titel|translit}&amp;area={$content_ex_res.Sektion}">{$content_ex_res.Titel|sanitize}</a>
        </h3>
        {$content_ex_res.Inhalt}
      </div>
    </div>
{/if}
