<div class="wrapper">
  <h1 class="title">{$res.Titel|sanitize}</h1>
  <div class="clearfix">
    {if !empty($res.Bild) && $smarty.request.artpage < 2}
        <img class="img-fluid {if $res.BildAusrichtung == 'left'}img-left{else}img-right{/if}" src="uploads/content/{$res.Bild}" alt="{$res.Titel|sanitize}" />
    {/if}
    {$res.Inhalt}
  </div>
</div>

{if !empty($article_pages)}
    <div class="wrapper">
      {$article_pages}
    </div>
{/if}

{if $res.Bewertung == 1}
    <div class="wrapper">
      <div class="flex-line justify-content-center">
        <div class="mr-2">{#Rating_Rating#}</div>
        <div class="rating-star" data-rating="{$res.Wertung}"></div>
      </div>
    </div>
{/if}

{if $res.Bewertung == 1}
    {$RatingForm}
{/if}
{$IncludedGalleries}
{$IncludedContent}
{$IncludedArticles}
{$IncludedNews}
{$GetComments}
