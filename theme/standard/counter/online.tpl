<div>
  <span class="font-weight-bold">{#Forums_OnlineUsers#}</span> {$UserOnline},
  <span class="font-weight-bold">{#Forums_OnlineGuests#}</span> {$GuestsOnline}
</div>
{foreach from=$userOnlineLinks item=uo name=useronline}
    <a href="index.php?p=user&amp;id={$uo.Uid}&amp;area={$area}">{$uo.Benutzername|sanitize}</a>{if !$smarty.foreach.useronline.last}, {/if}
{/foreach}

<div>
  <span class="font-weight-bold">{#Forums_OnlineBot#}</span> {$BotOnline}
</div>
{foreach from=$botOnlineLinks item=bot name=botonline}
    <span class="text-link">{$bot.Benutzername|sanitize}{if $bot.CountBotName > 1}({$bot.CountBotName}){/if}</span>{if !$smarty.foreach.botonline.last}, {/if}
{/foreach}
