<div class="wrapper panel">
  <div class="panel-head">{#Stats#}</div>
  <div class="panel-main">
    <div class="row">
      <div class="col-8">{#Stats_UserOnline#}</div>
      <div class="col-4 text-right text-link">{$Counter_Online}</div>
    </div>
    <div class="row">
      <div class="col-8">{#Stats_Visits#}</div>
      <div class="col-4 text-right text-link">{$Counter_Gesamt}</div>
    </div>
    <div class="row">
      <div class="col-8">{#Stats_VisitsToday#}</div>
      <div class="col-4 text-right text-link">{$Counter_Heute}</div>
    </div>
    <div class="row">
      <div class="col-8">{#Stats_VisitsTWeek#}</div>
      <div class="col-4 text-right text-link">{$Counter_Woche}</div>
    </div>
    <div class="row">
      <div class="col-8">{#Stats_VisitsTMonth#}</div>
      <div class="col-4 text-right text-link">{$Counter_Monat}</div>
    </div>
    <div class="row">
      <div class="col-8">{#Stats_VisitsTYear#}</div>
      <div class="col-4 text-right text-link">{$Counter_Jahr}</div>
    </div>
    <div class="row">
      <div class="col-8">{#Stats_RecordOn#} {$Counter_RekordAm}</div>
      <div class="col-4 text-right text-link">{$Counter_Rekord}</div>
    </div>
  </div>
</div>
