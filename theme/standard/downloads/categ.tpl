<div class="wrapper">
  <h1 class="title">{#Downloads#}</h1>
  {include file="$incpath/downloads/search.tpl"}

  {if $Categs}
      <div class="box-content">
        <div class="row">
          {assign var=lcc value=0}
          {foreach from=$Categs item=c name=categ}
              <div class="col-md-6 my-1">
                <a class="font-weight-bold" href="{$c->HLink}">
                  <i class="icon-folder-open"></i>{$c->Name|sanitize}{if $c->LinkCount >= 1} ({$c->LinkCount}){/if}
                </a>
                  <div class="ml-4 small">{$c->Beschreibung|sanitize}</div>
              </div>
              {assign var=lcc value=$lcc+1}
              {if $lcc % 2 == 0 && !$smarty.foreach.categ.last}
              </div>
              <div class="row">
              {/if}
          {/foreach}
        </div>
      </div>
  {/if}

  {if !empty($smarty.request.categ)}
      {if !empty($Entries)}
          <div class="spacer flex-line justify-content-center">
            <span class="mr-3">{#SortBy#}:</span>
            <a class="mr-3" href="index.php?p=downloads&amp;area={$area}&amp;categ={$smarty.request.categ|default:1}&amp;name={$CategName}&amp;page={$smarty.request.page|default:1}&amp;sort={$datesort|default:'dateasc'}">
              {#SortDate#}<i class="{$img_date|default:'icon-sort'}"></i>
            </a>
            <a class="mr-3" href="index.php?p=downloads&amp;area={$area}&amp;categ={$smarty.request.categ|default:1}&amp;name={$CategName}&amp;page={$smarty.request.page|default:1}&amp;sort={$namesort|default:'nameasc'}">
              {#SortName#}<i class="{$img_name|default:'icon-sort'}"></i>
            </a>
            <a class="mr-3" href="index.php?p=downloads&amp;area={$area}&amp;categ={$smarty.request.categ|default:1}&amp;name={$CategName}&amp;page={$smarty.request.page|default:1}&amp;sort={$hitssort|default:'hitsdesc'}">
              {#Downloads#}<i class="{$img_hits|default:'icon-sort'}"></i>
            </a>
          </div>
          {include file="$incpath/downloads/list.tpl"}
      {else}
          <div class="box-content">{#Links_NoInCateg#}</div>
      {/if}
  {else}
      {if $Entries}
          <div class="h3 title">{#Downloads_new#}</div>
          {include file="$incpath/downloads/list.tpl"}
      {/if}
  {/if}
</div>
