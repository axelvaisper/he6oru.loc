{if $NewDownloadsEntries}
    <div>
      <div class="h3 title">{#Downloads_new#}</div>
      {foreach from=$NewDownloadsEntries item=e}
          <div class="box-content">
            <h3 class="listing-head text-truncate">
              <a title="{$e->Name|sanitize}" href="{$e->Link_Details}">{$e->Name|sanitize}</a>
            </h3>
            <div class="clearfix">
              {if !empty($e->Bild)}
                  <a href="{$e->Link_Details}">
                    <img class="img-fluid listing-img-right" src="uploads/downloads/{$e->Bild}" alt="{$e->Name|sanitize}" />
                  </a>
              {/if}
              <div class="text-justify">
                {$e->Beschreibung|truncate:500}
              </div>
            </div>
          </div>
      {/foreach}
    </div>
{/if}
