<div class="wrapper">
  <h1 class="title">{#Downloads#}</h1>
  {include file="$incpath/downloads/search.tpl"}
  {if empty($Entries)}
      <div class="box-content">{#Links_SNotFound#}</div>
  {/if}
  {$Results}
</div>
