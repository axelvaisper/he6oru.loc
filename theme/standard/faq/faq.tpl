<script>
<!-- //
    $(function () {
        $('.user_pop').colorbox({
            height: "600px",
            width: "550px",
            iframe: true
        });
    });
//-->
</script>

<h1 class="title clearfix">
  {#Faq#}
  {if permission('faq_sent')}
      <div class="float-right">
        <a class="user_pop" href="index.php?p=faq&amp;action=mail&amp;faq_id=0&amp;area={$area}">
          <i class="icon-help-circled" data-toggle="tooltip" title="{#New_Guest#}"></i>
        </a>
      </div>
  {/if}
</h1>

{if $categs}
    <div class="form-group">
      <label class="sr-only" for="articles-jump">{#GotoArchive#}</label>
      <select class="form-control" id="articles-jump" onchange="eval(this.options[this.selectedIndex].value);selectedIndex = 0;">
        <option value="location.href = 'index.php?p=faq&amp;area={$area}'" {if empty($smarty.request.faq_id)}selected="selected"{/if}>{#AllCategs#}</option>
        {foreach from=$categs item=fc}
            <option value="location.href = 'index.php?p=faq&amp;action=display&amp;faq_id={$fc->Id}&amp;area={$area}&amp;name={$fc->Name|translit}'" {if $fc->Id == $smarty.request.faq_id}selected="selected"{/if}>{$fc->visible_title|sanitize}</option>
        {/foreach}
      </select>
    </div>
{/if}

<div class="box-content">
  <h3>{$faq->Name|sanitize}</h3>
  {$faq->text}
</div>
