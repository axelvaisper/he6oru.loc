{include file="$incpath/forums/user_panel.tpl"}
<div class="wrapper">
  {$navigation}
</div>

<h1 class="title">{#Forums_ForumTitle_login#}</h1>
<div class="box-content">
  {if $error}
      <div class="box-error">
        {$error}
      </div>
  {/if}

  <div class="wrapper">
    {#Forums_TitleLockedByPass#}
  </div>

  <form action="index.php?p=forum&amp;action=login" method="post">
    <div class="form-row">
      <div class="col-md-3 form-group">
        <input class="form-control" type="password" name="pass" />
      </div>

      <div class="col-md-auto">
        <input class="btn btn-primary btn-block-sm" type="submit" value="{#Forums_ButtonLogin#}" />
      </div>
    </div>

    <input type="hidden" name="fid" value="{$fid}" />
  </form>
</div>

{include file="$incpath/forums/footer.tpl"}
