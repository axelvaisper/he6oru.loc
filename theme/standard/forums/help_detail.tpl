{include file="$incpath/forums/user_panel.tpl"}

<h1 class="title">
  <a href="index.php?p=forum&amp;action=help">{#Help_General#}</a>
</h1>

<div class="row">
  <div class="col-md-9">
    <div class="box-content">
      <div class="h3 wrapper">{$row_faq->FaqName|sanitize}</div>
      {$row_faq->FaqText}
    </div>
  </div>

  <div class="col-md-3">
    {foreach from=$faq_categ item=fc}
        <div class="box-content">
          <div class="h5">{$fc->Name}</div>
          <ul>
            {foreach from=$fc->Items item=items}
                <li>
                  <a style="{if $items->Id == $smarty.request.hid}text-decoration: none{/if}" href="index.php?p=forum&amp;action=help&amp;hid={$items->Id}&amp;sub={$items->FaqName|translit}">{$items->FaqName}</a>
                </li>
            {/foreach}
          </ul>
        </div>
    {/foreach}
  </div>
</div>
