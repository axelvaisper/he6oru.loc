<form method="post" action="index.php">
  <div class="row form-group">
    <div class="col-2">
      <label class="col-form-label" for="login_email_r">{#Email#}</label>
    </div>
    <div class="col">
      <input class="form-control" type="text" name="login_email" id="login_email_r" required="required" />
    </div>
    <div class="col-4">
      <label class="col-form-label">
        <input name="staylogged" type="checkbox" value="1" checked="checked" />
        <span data-toggle="tooltip" title="{$lang.PassCookieT|tooltip}">{#PassCookieHelp#}</span>
      </label>
    </div>
  </div>

  <div class="row form-group">
    <div class="col-2">
      <label class="col-form-label" for="login_pass_r">{#Pass#}</label>
    </div>
    <div class="col">
      <input class="form-control" type="password" name="login_pass" id="login_pass_r" required="required" />
    </div>
    <div class="col-4">
      <input class="btn btn-primary" type="submit" value="{#Login_Button#}" />
    </div>
  </div>

  <input type="hidden" name="p" value="userlogin" />
  <input type="hidden" name="action" value="newlogin" />
  <input type="hidden" name="backurl" value="{page_link|base64encode}" />
</form>
