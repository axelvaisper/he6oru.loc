<form action="index.php?p=forum&amp;action=rating" method="post">
  <input type="hidden" name="t_id" value="{$topic->id}" />

  <div class="d-flex justify-content-center align-items-center">
    <div class="p-2">{#Forums_Label_rating#}</div>
    <div class="p-2"><div class="rating-star" data-rating="4" data-rating-active="true" data-rating-input="rating"></div></div>
    <div class="p-2"><input class="btn btn-primary" type="submit" value="{#RateThis#}" /></div>
  </div>

</form>
