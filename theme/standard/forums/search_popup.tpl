<script>
<!-- //
function userName(user) {
    parent.document.f.tofromname.value = user;
    closeWindow();
}
//-->
</script>

<h1 class="title">{#PN_SearchUser#}</h1>
<div class="wrapper">
  <form name="form1" method="post" action="?p=misc&amp;do=searchuser&amp;search=1">
    <div class="input-group">
      <input class="form-control" name="name" type="text" />
      <span class="input-group-append">
        <input class="btn btn-primary" type="submit" name="Submit" value="{#StartSearch#}" />
      </span>
    </div>
  </form>
</div>

{if isset($userfound) && $userfound == 1}
    <div class="h3 title">{#PN_searchuser_found#}</div>
    {$usererg}
{/if}
