<form action="index.php?p=showforum" method="post" name="fp" id="fp">
  <div class="input-group">
    <select class="form-control" name="fid">
      {foreach from=$categories_dropdown item=category}
          <optgroup label="{$category->title}">
            {foreach from=$category->forums item=forum_dropdown}
                {if $forum_dropdown->category_id == 0}
                    <option class="text-mutted" value="{$forum_dropdown->id}" disabled="disabled">{$forum_dropdown->visible_title}</option>
                {else}
                    <option value="{$forum_dropdown->id}"{if isset($smarty.request.fid) && $smarty.request.fid == $forum_dropdown->id} selected="selected" {/if}>{$forum_dropdown->visible_title}</option>
                {/if}
            {/foreach}
          </optgroup>
      {/foreach}
    </select>

    <span class="input-group-append">
      <input class="btn btn-primary" type="submit" value="{#GotoButton#}" />
    </span>
  </div>
</form>
