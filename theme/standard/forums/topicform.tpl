<div class="box-content">
  <div class="wrapper">
    <div class="h4 title">{#GlobalTheme#}</div>
    <input class="form-control" type="text" name="topic" value="{$topic|escape:'html'}" />
  </div>

  <div class="h4 title">{#Forums_PostIconTitle#}</div>
  {foreach from=$posticons item=posticon}
      {$posticon}
  {/foreach}
</div>
