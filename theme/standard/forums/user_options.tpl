<script>
<!-- //
$(function() {
    $('.user_pop').colorbox({ height: '600px', width: '550px', iframe: true });
});
//-->
</script>

<div class="p-2">
    <a class="d-block" href="index.php?p=user&amp;id={$theuid}&amp;area={$area}">
      <i class="icon-user"></i>{#Forums_ShowUserProfile#}
    </a>
    {if $loggedin}
        <a class="d-block" href="index.php?p=pn&amp;action=new&amp;to={$theuname}">
          <i class="icon-chat"></i>{#Forums_SendPnUser_Link#}
        </a>
        {if $send_email == 1}
            <a class="d-block user_pop" href="index.php?p=misc&amp;do=email&amp;uid={$theuid}">
              <i class="icon-mail"></i>{#SendEmail_Inf#}
            </a>
        {/if}
    {/if}
    <a class="d-block" href="index.php?p=forum&amp;action=print&amp;what=posting&amp;id={$theuid}">
      <i class="icon-eye"></i>{#Forums_ShowpostsUser_Link#}
    </a>
    {if !isset($smarty.request.action) || $smarty.request.action != 'ignorelist'}
        {if $ignore_options == 1}
            {if $is_ignore == 1}
                <a class="d-block" href="index.php?p=forum&amp;action=ignorelist&amp;sub=del&amp;id={$theuid}">
                  <i class="icon-user-times"></i>{#Ignorelist_Del#}
                </a>
            {else}
                <a class="d-block" href="index.php?p=forum&amp;action=ignorelist&amp;sub=add&amp;id={$theuid}">
                  <i class="icon-user-times"></i>{#Ignorelist_Add#}
                </a>
            {/if}
        {/if}
    {/if}
</div>