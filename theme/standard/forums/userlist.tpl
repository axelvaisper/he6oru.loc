<script>
<!-- //
$(function () {
    $('.user_pop').colorbox({ height: "550px", width: "550px", iframe: true });
});
//-->
</script>

{include file="$incpath/forums/user_panel.tpl"}

<h1 class="title">{#Users#}</h1>
<form action="index.php?p=members&amp;area={$area}" method="post" name="userlist">
  <div class="wrapper">

    <table width="100%" class="forum_tableborder">
      <tr class="forum_header">
        <td width="20%">{#Username#}</td>
        <td width="10%" class="text-center">{#Forums_avatar#}</td>
        <td width="10%" class="text-center">{#RegNew#}</td>
        <td width="5%" class="text-center">{#Forums_Postings#}</td>
        <td width="5%" class="text-center">{#Forums_User_Website#}</td>
        <td width="5%" class="text-center">{#Forums_Pn#}</td>
        <td width="5%" class="text-center">{#Forums_SkypeMe#}</td>
        <td width="5%" class="text-center">{#Profile_ICQ#}</td>
        <td width="5%" class="text-center">{#Email#}</td>
      </tr>

      {if $found == 1}
          {foreach from=$table_data item=user}
              <tr class="{cycle name='ul' values='row-primary,row-secondary'}">
                <td>
                  {if $user.Profil_public == 1}
                      <a href="{$user.userlink}">
                        {onlinestatus uname=$user.name}{$user.name|sanitize}
                      </a>
                  {else}
                      {onlinestatus uname=$user.name}{$user.name|sanitize}
                  {/if}
                  <div class="small">
                    {if $user.team == 1}
                        {$user.teamName}
                    {else}
                        {$user.rank}
                    {/if}
                  </div>
                </td>
                <td class="text-center">{$user.avatar|default:'&nbsp;'}</td>
                <td class="text-center">{$user.regtime|date_format:$lang.DateFormat|default:'&nbsp;'}</td>
                <td class="text-center">{$user.posts|default:'&nbsp;'} </td>
                <td class="text-center">
                  {if $user.Webseite}
                      <a rel="nofollow" href="{$user.Webseite}" target="_blank">
                        <i class="icon-link size-xl"></i>
                      </a>
                  {/if}
                </td>
                <td class="text-center">{$user.Pn_User|default:'&nbsp;'}</td>
                <td class="text-center">{$user.Skype_User|default:'&nbsp;'}</td>
                <td class="text-center">{$user.Icq_User|default:'&nbsp;'}</td>
                <td class="text-center">{$user.Email_User|default:'&nbsp;'}</td>
              </tr>
          {/foreach}
      {/if}

    </table>
  </div>

  <div class="wrapper input-group">
    <input class="form-control" name="suname" type="text" value="{$smarty.request.suname}" placeholder="{#Username#}" />

    <select class="form-control" name="selby">
      <option value="joindate" {if isset($sel3)}{$sel3}{/if}>{#show#} {#by_t#} {#Sort_RegDate#}</option>
      <option value="posts" {if isset($sel2)}{$sel2}{/if}>{#show#} {#by_t#} {#Sort_Postings#}</option>
      <option value="username" {if isset($sel1)}{$sel1}{/if}>{#show#} {#by_t#} {#Sort_Username#}</option>
    </select>

    <select class="form-control" name="ud">
      <option value="DESC" {if isset($ud2)}{$ud2}{/if}>{#in_t#} {#Descending#}</option>
      <option value="ASC" {if isset($ud1)}{$ud1}{/if}>{#in_t#} {#Ascending#}</option>
    </select>

    <select class="form-control" name="pp">
      {$pp_l}
    </select>

    <span class="input-group-append">
      <input class="btn btn-primary" name="Submit" type="submit" value="{#GlobalShow#}" />
    </span>
  </div>
</form>

{if $pagenav}
    <div class="wrapper">
      {$pagenav}
    </div>
{/if}

{include file="$incpath/forums/footer.tpl"}
