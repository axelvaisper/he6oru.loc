{if $externGals}
    <div class="wrapper">
      <div class="h3 title">{#Gallery_extern#}</div>
      <div class="box-content flex-line">
        {foreach from=$externGals item=gex}
            {if $gex->ICount > 0}
                <div data-toggle="tooltip" title="{$gex->GalName|tooltip:100}">
                  <a href="{$gex->Link}"><img class="img-thumbnail height-6 m-1" src="{$gex->Img}" alt="{$gex->GalName|sanitize}" /></a>
                </div>
            {/if}
        {/foreach}
      </div>
    </div>
{/if}
