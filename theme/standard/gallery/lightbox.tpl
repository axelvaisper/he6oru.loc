<script>
<!-- //
$(function() {
    $('.img-popup').colorbox({
        photo: true,
        transition: "elastic",
        maxHeight: "98%",
        maxWidth: "98%",
        slideshow: true,
        slideshowAuto: false,
        slideshowSpeed: 2500,
        current: "{#GlobalImage#} {ldelim}current{rdelim} {#PageNavi_From#} {ldelim}total{rdelim}",
        slideshowStart: "{#GlobalStart#}",
        slideshowStop: "{#GlobalStop#}",
        previous: "{#GlobalBack#}",
        next: "{#GlobalNext#}",
        close: "{#GlobalGlose#}"
    });
});
//-->
</script>

<div class="wrapper">
<h1 class="title">{$Gallery_inf->TitleGalName|sanitize}</h1>

  <form  method="post" action="index.php">
    <div class="input-group">
      <select class="form-control" name="ascdesc">
        <option value="desc" {if isset($smarty.request.ascdesc) && $smarty.request.ascdesc == "desc"}selected="selected"{/if}>{#NewestFirst#}</option>
        <option value="asc" {if isset($smarty.request.ascdesc) && $smarty.request.ascdesc == "asc"}selected="selected"{/if}>{#OldestFirst#}</option>
      </select>
      <select class="form-control" name="pp">
        <option value="{$Galsettings->Bilder_Seite}" {if isset($smarty.request.pp) && $smarty.request.pp == $Galsettings->Bilder_Seite}selected="selected"{/if}>{$Galsettings->Bilder_Seite} {#Gallery_ImagesPP#}</option>
        {section name=pp loop=95 step=5}
            <option value="{$smarty.section.pp.index+10}" {if isset($smarty.request.pp) && $smarty.request.pp == $smarty.section.pp.index+10}selected="selected"{/if}>{$smarty.section.pp.index+10} {#Gallery_ImagesPP#}</option>
        {/section}
      </select>
      <span class="input-group-append">
        <input class="btn btn-primary" type="submit" value="{#Gallery_PPButton#}" />
      </span>
    </div>

    <input type="hidden" name="p" value="gallery" />
    <input type="hidden" name="action" value="showgallery" />
    <input type="hidden" name="id" value="{$smarty.request.id}" />
    <input type="hidden" name="categ" value="{$smarty.request.categ}" />
    <input type="hidden" name="name" value="xxx" />
    <input type="hidden" name="page"  value="1" />
    {if isset($smarty.request.favorites) && $smarty.request.favorites == 1}
        <input type="hidden" name="favorites"  value="1" />
    {/if}
  </form>

  {if $Gallery_inf->GalText}
      <div class="box-content">
        {$Gallery_inf->GalText}
      </div>
  {/if}

  {if $subGalleries}
      <div class="box-content">
        <div class="row">
          {assign var=lcc value=0 name=categ}
          {foreach from=$subGalleries item=subgallery name=subgalleries}
              <div class="col-md-6 my-1">
                <a class="font-weight-bold" href="{$subgallery->Link}">
                  <i class="icon-folder-open"></i>{$subgallery->SubGalName|sanitize}
                </a>
              </div>
              {assign var=lcc value=$lcc+1}
              {if $lcc % 2 == 0 && !$smarty.foreach.categ.last}
              </div>
              <div class="row">
              {/if}
          {/foreach}
        </div>
      </div>
  {/if}

  <div class="box-content">
    {if empty($items)}
        {#Gallery_NoImages#}
    {else}
        <div class="row spacer">
          {assign var=c value=0}
          {foreach from=$items item=gal name=BilderArray}
              {assign var=c value=$c+1}
              <div class="col d-flex justify-content-center">
                <a class="img-popup" rel="group" href="{$gal->Thumbnail_Gross}">
                  <img class="img-fluid" data-toggle="tooltip" title="{$gal->ImageName}" src="{$gal->Thumbnail}" alt="{$gal->ImageName}" />
                </a>
              </div>
              {if $c % $Galsettings->Bilder_Zeile == 0 && !$smarty.foreach.BilderArray.last}
              </div>
              <div class="row spacer">
              {/if}
          {/foreach}
        </div>

        <div class="listing-foot flex-line justify-content-center">
          <span class="mr-4" data-toggle="tooltip" title="{#GlobalAutor#}">
            <i class="icon-user"></i>{$Gallery_inf->AutorLink}
          </span>
          <span class="mr-4" data-toggle="tooltip" title="{#Added#}">
            <i class="icon-calendar"></i>{$Gallery_inf->Datum|date_format:$lang.DateFormatSimple}
          </span>
          <span class="mr-4" data-toggle="tooltip" title="{#Images#}">
            <i class="icon-picture"></i>{$Gallery_inf->Images}
          </span>
        </div>
    {/if}
  </div>

  {if !empty($GalNavi)}
      <div class="wrapper">
        {$GalNavi}
      </div>
  {/if}
</div>
