$(function () {
    var backTop = $('<a href="#" class="back-to-top"></a>');
    $('body').append(backTop);
    var backToTop = function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 100) {
            backTop.fadeIn();
        } else {
            backTop.fadeOut();
        }
    };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    backTop.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 700);
    });
});

function showModal(selector, time) {
    $(function () {
        selector = $(selector);
        if (selector.length) {
            selector.modal('show');
            if (time > 0) {
                setTimeout(function () {
                    selector.modal('hide');
                }, time);
            }
        }

    });
}

function openWindow(seite, name, w, h, scroll) {
    if (typeof w === 'undefined' || w === '') {
        w = screen.width;
    }
    if (typeof h === 'undefined' || h === '') {
        h = screen.height;
    }
    var left = screen.width ? (screen.width - w) / 2 : 0;
    var top = screen.height ? (screen.height - h) / 2 : 0;
    var settings = 'height=' + h + ',width=' + w + ',top=' + top + ',left=' + left + ',scrollbars=' + scroll + ',resizable';
    window.open(seite, name, settings);
}
function multiCheck() {
    var obj = document.kform;
    for (var i = 0; i < obj.elements.length; i++) {
        var e = obj.elements[i];
        if (e.name !== 'allbox' && e.type === 'checkbox' && !e.disabled) {
            e.checked = obj.allbox.checked;
        }
    }
}
function toggleContent(click, target) {
    $(function () {
        var timer = 0;
        $('#' + target).css({
            'position': 'absolute',
            'left': $('#' + click).offset().left + 'px',
            'top': ($('#' + click).offset().top + $('#' + click).height()) + 'px'
        }).slideToggle(300);
        $(document).on('click', function (e) {
            if ($(e.target).closest('#' + click + ', #' + target).length === 0) {
                $('#' + target).slideUp(300);
                e.stopPropagation();
            }
        });
        $('#' + click + ', #' + target).mouseover(function () {
            clearTimeout(timer);
        }).mouseout(function () {
            clearTimeout(timer);
            timer = setTimeout(function () {
                $('#' + target).slideUp(300);
            }, 1000);
        });
    });
}
function toggleSpoiler(elem) {
    $(function () {
        $(elem).next().slideToggle();
        $(elem).children('i').toggleClass('icon-plus-squared-alt').toggleClass('icon-minus-squared-alt');
    });
}
function closeWindow(reload) {
    $(function () {
        parent.$.colorbox.close();
        if (reload === true) {
            parent.location.href = parent.location;
        }
    });
}
function newWindow(url, width, height) {
    $(function () {
        $.colorbox({
            href: url,
            width: width + 'px',
            height: height + 'px',
            iframe: true
        });
    });
}
