<script>
<!-- //
$(function() {
    $('.linkextern').on('click', function() {
        var options = {
            target: '#plick',
            url: 'index.php?action=updatehitcount&p=manufacturer&id={$res->Id}',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>

<div id="plick"></div>
<div class="wrapper">
  <h1 class="title"><a class="linkextern" rel="nofollow" href="{$res->Homepage}" target="_blank">{$res->Name|sanitize}</a></h1>
  <div class="wrapper clearfix">
    {if !empty($res->Bild)}
        <img class="img-fluid img-right" src="uploads/manufacturer/{$res->Bild}" alt="{$res->Name|sanitize}" />
    {/if}
    {$res->Beschreibung}
  </div>

  <div class="h3 title">{#Info#}</div>
  <div class="box-content">
    {if $res->NameLang}
        <div class="row">
          <div class="col-3">{#Manufacturer_fullname#}</div>
          <div class="col-9">{$res->NameLang|sanitize}</div>
        </div>
    {/if}
    {if $res->Gruendung}
        <div class="row">
          <div class="col-3">{#Manufacturer_gr_year#}</div>
          <div class="col-9">{$res->Gruendung|sanitize}</div>
        </div>
    {/if}
    {if $res->GruendungLand}
        <div class="row">
          <div class="col-3">{#Country#}</div>
          <div class="col-9">{$res->GruendungLand|sanitize}</div>
        </div>
    {/if}
    <div class="row">
      <div class="col-3">{#Manufacturer_home#}</div>
      <div class="col-9"><a class="linkextern" rel="nofollow" href="{$res->Homepage}" target="_blank">{$res->Homepage|sanitize}</a></div>
    </div>
    {if $res->Personen >= 1}
        <div class="row">
          <div class="col-3">{#Manufacturer_worker#}</div>
          <div class="col-9">{$res->Personen|sanitize}</div>
        </div>
    {/if}
    {if $res->Adresse}
        <div class="row">
          <div class="col-3">{#Imprint#}</div>
          <div class="col-9">{$res->Adresse}</div>
        </div>
    {/if}
    {if $res->Telefonkontakt}
        <div class="row">
          <div class="col-3">{#Phone#}</div>
          <div class="col-9">{$res->Telefonkontakt|sanitize}</div>
        </div>
    {/if}
  </div>

  {if !empty($Products)}
      <div class="h3 title">{#Manufacturer_productsFrom#} {$res->Name|sanitize}</div>
      {foreach from=$Products item=prod}
          <div class="box-content">
            <h4>
              <a href="index.php?p=shop&amp;area={$area}&amp;action=showproduct&amp;id={$prod->Id}&amp;cid={$prod->Kategorie}&amp;pname={$prod->Titel|translit}">{$prod->Titel|sanitize}</a>
            </h4>
            <div class="clearfix">
              {if !empty($prod->Bild)}
                  <a class="img-fluid listing-img-left" data-toggle="tooltip" title="{$prod->Titel|sanitize}" href="index.php?p=shop&amp;area={$area}&amp;action=showproduct&amp;id={$prod->Id}&amp;cid={$prod->Kategorie}&amp;pname={$prod->Titel|translit}">
                    <img src="{$prod->Bild}" alt="" />
                  </a>
              {/if}
              <div class="text-justify">
                {$prod->Beschreibung|truncate:200}
              </div>
            </div>
          </div>
      {/foreach}
  {/if}
</div>
