{if !empty($SiteNavigation)}
    <h4 class="footer-title">{$navi_title|default:$lang.Title_Navi}</h4>
    <ul class="footer-list-menu">
      {foreach from=$SiteNavigation item=navi}
          <li class="footer-list-item w-50">
            <a title="{$navi->AltTitle|sanitize}" target="{$navi->target|default:'_self'}" href="{$navi->document|escape:'html'}">{$navi->title|sanitize}</a>
          </li>
      {/foreach}
    </ul>
{/if}
