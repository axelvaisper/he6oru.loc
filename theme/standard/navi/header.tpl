{if !empty($SiteNavigation)}
<script>
<!-- //
$(function () {
    $('.header-menu a.dropdown-toggle').on('click', function () {
        var el = $(this);
        if (el.length && el.attr('href')) {
            location.href = el.attr('href');
        }
    });
});
//-->
</script>

    <div class="header-menu">
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php?area={$area}">
          <img src="{$imgpath}/page/logo.png" height="30" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-navbar" aria-controls="header-navbar" aria-expanded="false">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="header-navbar">
          <ul class="navbar-nav mr-auto">
            {foreach from=$SiteNavigation item=navi}
                {if empty($navi->sub_navi)}
                    <li class="nav-item{if !empty($navi->active) || $navi->document == $document} active{/if}">
                      <a class="nav-link" title="{$navi->AltTitle|sanitize}" href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}">
                        {$navi->title|sanitize}
                      </a>
                    </li>
                {else}
                    <li class="nav-item dropdown{if !empty($navi->active) || $navi->document == $document} active{/if}">
                      <a class="nav-link dropdown-toggle" href="{$navi->document|escape:'html'}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        {$navi->title|sanitize}
                      </a>
                      <div class="dropdown-menu">
                        {foreach from=$navi->sub_navi item=sub_navi}
                            <a class="dropdown-item{if !empty($sub_navi->active) || $sub_navi->document == $document} active{/if}" title="{$sub_navi->AltTitle|sanitize}" href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                              {$sub_navi->title|sanitize}
                            </a>
                        {/foreach}
                      </div>
                    </li>
                {/if}
            {/foreach}
          </ul>
        </div>
      </nav>
    </div>
{/if}
