<h1 class="title">{$navi_title|default:$lang.Sitemap}</h1>
<div class="box-content">
  <ul>
    <li><a href="index.php?p=sitemap&amp;action=full&amp;area={$area}">{#SitemapFull#}</a></li>
      {foreach from=$SiteNavigation item=navi}
      <li>
        <a target="{$navi->target|default:'_self'}" href="{$navi->document|escape:"html"}">{$navi->title}</a>
        {if !count($navi->sub_navi)}
        </li>
      {/if}
      {if count($navi->sub_navi)}
          <ul>
            {foreach from=$navi->sub_navi item=sub_navi}
                <li><a target="{$sub_navi->target|default:'_self'}" href="{$sub_navi->document|escape:"html"}">{$sub_navi->title}</a>
                  {if !count($sub_navi->sub_navi)}
                  </li>
                {/if}
                {if count($sub_navi->sub_navi)}
                    <ul>
                      {foreach from=$sub_navi->sub_navi item=sub_sub_navi}
                          <li><a target="{$sub_sub_navi->target|default:'_self'}" href="{$sub_sub_navi->document|escape:"html"}">{$sub_sub_navi->title}</a></li>
                          {/foreach}
                    </ul>
                    </li>
                {/if}
            {/foreach}
          </ul>
          </li>
      {/if}
    {/foreach}
  </ul>
</div>
