<div class="wrapper">
  <h1 class="title clearfix">
    {#Newsarchive#}
    <div class="float-right">
      <a  target="_blank" href="{$rss_newslink}">
        <i class="icon-rss-squared" data-toggle="tooltip" title="{#RSSAbo#}"></i>
      </a>
    </div>
  </h1>

  {include file="$incpath/news/categ.tpl"}
  {if $newsitems}
      {foreach from=$newsitems item=news}
          <div class="box-content">
            <h3 class="listing-head">
              <a title="{$news.Titel|sanitize}" href="index.php?p=news&amp;area={$news.Sektion}&amp;newsid={$news.Id}&amp;name={$news.LinkTitle|translit}">{$news.Titel|sanitize}</a>
            </h3>

            <div class="clearfix">
              {if !empty($news.Bild)}
                  {assign var=length value=220}
                  <a href="index.php?p=news&amp;area={$news.Sektion}&amp;newsid={$news.Id}&amp;name={$news.LinkTitle|translit}">
                    <img class="img-fluid {if $news.BildAusrichtung == 'right'}listing-img-right{else}listing-img-left{/if}" src="{$news.Thumb}" alt="{$news.Titel|sanitize}" />
                  </a>
              {/if}

              <div class="text-justify">
                {if !empty($news.Intro)}
                    {$news.Intro|html_truncate:$length}
                {else}
                    {$news.News|html_truncate:$length}
                {/if}
              </div>
            </div>

            <div class="listing-foot flex-line justify-content-center">
              <span class="mr-4" data-toggle="tooltip" title="{#GlobalAutor#}">
                <a href="index.php?p=user&amp;id={$news.Autor}&amp;area={$area}"><i class="icon-user"></i>{$news.User}</a>
              </span>
              <span class="mr-4" data-toggle="tooltip" title="{#Added#}"><i class="icon-calendar"></i>{$news.ZeitStart|date_format:$lang.DateFormat}</span>
              <span class="mr-4" data-toggle="tooltip" title="{#Global_Hits#}"><i class="icon-eye"></i>{$news.Hits}</span>
              <a href="index.php?p=news&amp;area={$news.Sektion}&amp;newsid={$news.Id}&amp;name={$news.LinkTitle|translit}">
                <i class="icon-right-open"></i>{#ReadAll#}
              </a>
            </div>
          </div>
      {/foreach}
  {else}
      <div class="box-content">
        {#NoNews#}
      </div>
  {/if}
</div>

{if !empty($pages)}
    <div class="wrapper">
      {$pages}
    </div>
{/if}

{include file="$incpath/news/search.tpl"}
