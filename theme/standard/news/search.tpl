<div class="wrapper">
  <div class="h3 title">{#Search#}</div>
  <div class="box-content">
    <form method="post" action="index.php?p=newsarchive&amp;area={$area}">
      <div class="row form-group">
        <div class="col-md-3 col-sm-12">
          <label class="col-form-label" for="news-text">{#SearchT#}</label>
        </div>
        <div class="col-md-9 col-sm-12">
          <input class="form-control" id="news-text" type="text" name="q_news" value="{$smarty.request.q_news|escape:html}" />
        </div>
      </div>

      <div class="row form-group">
        <div class="col-md-3 col-sm-12">
          <label class="col-form-label" for="news-catid">{#Global_Categ#}</label>
        </div>
        <div class="col-md-9 col-sm-12">
          <select class="form-control" id="news-catid" name="catid">
            <option value="0">{#AllCategs#}</option>
            {foreach from=$dropdown item=dd}
                <option value="{$dd->Id}" {if isset($smarty.request.catid) && $smarty.request.catid == $dd->Id}selected="selected"{/if}>{$dd->visible_title}</option>
            {/foreach}
          </select>
        </div>
      </div>

      <div class="row form-group">
        <div class="col-md-3 col-sm-12">
          <label class="col-form-label" for="news-limit">{#Results#}</label>
        </div>
        <div class="col-md-9 col-sm-12">
          <input class="form-control" id="news-limit" name="limit" type="number" value="{$news_limit}" size="2" maxlength="2" />
        </div>
      </div>

      <div class="text-center">
        <input class="btn btn-primary btn-block-sm" type="submit" value="{#StartSearch#}" />
      </div>
      <input type="hidden" name="st" value="or" />
      <input type="hidden" name="page" value="1" />
    </form>
  </div>
</div>
