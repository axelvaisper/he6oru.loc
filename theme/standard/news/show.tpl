<div class="wrapper">
  <h1 class="title">{$row.Titel|sanitize}</h1>
  <div class="wrapper clearfix">
    {if !empty($row.Bild) && $smarty.request.artpage < 2}
        <img class="img-fluid {if $row.BildAusrichtung == 'left'}img-left{else}img-right{/if}" src="uploads/news/{$row.Bild}" alt="{$row.Titel|sanitize}" />
    {/if}
    {if $row.Intro}
        <p class="font-weight-bold">{$row.Intro}</p>
    {/if}
    {$row.News}
  </div>

  <div class="flex-line">
    <span class="mr-3">{#GlobalAutor#}: <a href="index.php?p=user&amp;id={$row.Autor}&amp;area={$area}">{$row.User}</a></span>
    <span class="mr-3">{#Date#}: {$row.ZeitStart|date_format:$lang.DateFormatSimple}</span>
  </div>
</div>

{if !empty($article_pages)}
    <div class="wrapper">
      {$article_pages}
    </div>
{/if}

{if $row.Bewertung == 1}
    <div class="wrapper">
      <div class="flex-line justify-content-center">
        <div class="mr-2">{#Rating_Rating#}</div>
        <div class="rating-star" data-rating="{$row.Wertung}"></div>
      </div>
    </div>
{/if}

{if $row.Bewertung == 1}
    {$RatingForm}
{/if}
{$IncludedGalleries}
{$IncludedNews}
{$IncludedArticles}
{$IncludedContent}
{$GetComments}
