<h1 class="title">{#Newsletter#}</h1>
<div class="box-content">
  {if !empty($error)}
      <div class="box-error">
        <div class="font-weight-bold">{#Newsletter_e_inf#}</div>
        {foreach from=$email_error item=e}
            <div>{$e}</div>
        {/foreach}
      </div>
  {else}
      <div class="font-weight-bold">{#Newsletter_okT#}</div>
      {#Newsletter_okfinal#}
  {/if}
</div>
