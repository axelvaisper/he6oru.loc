<h1 class="title text-truncate">{$title_html}</h1>
<div>
  {$text}
</div>
<form>
  <div class="text-center">
    <input class="btn btn-primary" type="button" onclick="print();" value="{#Print#}" />
    <input class="btn btn-secondary" type="button" onclick="closeWindow();" value="{#WinClose#}" />
  </div>
</form>
