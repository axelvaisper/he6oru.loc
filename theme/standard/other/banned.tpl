<h1 class="title">{#Info#}</h1>
<div class="box-content">


  {foreach from=$banned item=bn}
      {if $bn->reson}
          <div class="row">
            <div class="col-3 font-weight-bold">{#BanReson#}:</div>
            <div class="col-9">{$bn->Reson|sanitize}</div>
          </div>
      {/if}
      {if $bn->TimeStart}
          <div class="row">
            <div class="col-3 font-weight-bold">{#BanDate#}:</div>
            <div class="col-9">{$bn->TimeStart|date_format:'%d-%m-%Y, %H:%M:%S'}</div>
          </div>
      {/if}
      {if $bn->TimeEnd}
          <div class="row">
            <div class="col-3 font-weight-bold">{#BanDateEnd#}:</div>
            <div class="col-9">{$bn->TimeEnd|date_format:'%d-%m-%Y, %H:%M:%S'}</div>
          </div>
      {/if}
      {if $bn->Type}
          <div class="row">
            <div class="col-3 font-weight-bold">{#BanType#}:</div>
            <div class="col-9">{if $bn->Type == 'autobann'}{#BanAuto#}{else}{#BanAdmin#}{/if}</div>
          </div>
      {/if}
      {if $bn->Aktiv}
          <div class="row">
            <div class="col-3 font-weight-bold">{#BanStatus#}:</div>
            <div class="col-9">{if $bn->Aktiv == 1}{#BanAkt#}{else}{#BanNoAkt#}{/if}</div>
          </div>
      {/if}
      {if $bn->User_id}
          <div class="row">
            <div class="col-3 font-weight-bold">{#BanIdUser#}:</div>
            <div class="col-9">{$bn->User_id}</div>
          </div>
      {/if}
      {if $bn->Name}
          <div class="row">
            <div class="col-3 font-weight-bold">{#Username#}:</div>
            <div class="col-9">{$bn->Name|sanitize}</div>
          </div>
      {/if}
      {if $bn->Email}
          <div class="row">
            <div class="col-3 font-weight-bold">{#Email#}:</div>
            <div class="col-9">{$bn->Email}</div>
          </div>
      {/if}
      {if $bn->Ip}
          <div class="row">
            <div class="col-3 font-weight-bold">{#BanIp#}:</div>
            <div class="col-9">{$bn->Ip}</div>
          </div>
      {/if}
  {/foreach}
</div>
