{if $loggedin && get_active('social_bookmarks')}
    <div class="wrapper panel">
      <div class="panel-head">{#Bookmarks#}</div>
      <div class="panel-main">
        <form action="index.php?p=bookmark" method="post">
          <div class="form-group">
            <input class="form-control" name="document_name" type="text" value="" placeholder="{#TheName#}" required="required" />
          </div>
          <div class="form-group">
            <input class="btn btn-primary btn-block" type="submit" value="{#Global_Add#}" />
          </div>
          <input type="hidden" name="document" value="{page_link|base64encode}" />
        </form>

        {if $bookmarks}
            <form action="index.php?p=bookmark" method="post">
              <div class="row no-gutters form-group">
                {foreach from=$bookmarks item=bm}
                    <div class="col-11 text-truncate">
                      <a class="text-elepsis" href="{$bm->document}">{$bm->doc_name|sanitize}</a>
                    </div>
                    <div class="col-1">
                      <div class="form-group form-check">
                        <input class="form-check-input" id="del_bookmark[{$bm->id}]" name="del_bookmark[{$bm->id}]" value="{$bm->id}" type="checkbox" />
                        <label class="sr-only" for="del_bookmark[{$bm->id}]">{#PassCookieHelp#}</label>
                      </div>
                    </div>
                {/foreach}
              </div>
              <input class="btn btn-primary btn-block" type="submit" value="{#Delete#}" />
              <input type="hidden" name="action" value="delete" />
              <input type="hidden" name="backurl" value="{page_link|base64encode}" />
            </form>
        {/if}
      </div>
    </div>
{/if}
