{if $secure_active == 1}
{if empty($secure_uniqid)}{assign var=secure_uniqid value="$secure_default"}{/if}
<script>
<!-- //
$(function() {
    $('#secure_reload_{$secure_uniqid}').on('click', function() {
        $.ajax({
            url: '{$baseurl}/lib/secure.php?action=reload&secure_uniqid={$secure_uniqid}&key=' + Math.random(),
            success: function (data) {
                $('#secure_info_{$secure_uniqid}').html('{#Validate_required#}');
                $('#secure_image_{$secure_uniqid}').html(data);
                $('#{$secure_input_{$secure_uniqid}}').val('');
            }
        });
    });
    $('#{$secure_input_{$secure_uniqid}}').on('keyup focusout', function() {
        $.ajax({
            url: '{$baseurl}/lib/secure.php?action=validate&secure_uniqid={$secure_uniqid}&{$secure_input_{$secure_uniqid}}=' + $(this).val() + '&key=' + Math.random(),
            success: function (data) {
                $('#secure_info_{$secure_uniqid}').html(data === 'true' ? '<i class="icon-ok text-success"></i>' : '{#Validate_wrong#}');
            }
        });
    });
});
//-->
</script>

<div class="form-group">
  <label for="{$secure_input_{$secure_uniqid}}">{#SecureText#}</label>
  <div class="d-flex">
    <div class="captcha">
      <a id="secure_reload_{$secure_uniqid}" href="javascript:void(0);">
        <i data-toggle="tooltip" title="{#ReloadCode#}" class="icon-cw size-xl"></i>
      </a>
    </div>
    <div class="captcha">{#Secure#}</div>
    <div class="captcha captcha-width" id="secure_image_{$secure_uniqid}">{$secure_image_{$secure_uniqid}}</div>
    <div class="captcha captcha-width">
      <input class="d-none" name="scode" value="" type="text" />
      <input class="form-control" id="{$secure_input_{$secure_uniqid}}" name="{$secure_input_{$secure_uniqid}}" value="" type="text" />
    </div>
    <div class="captcha">
      <span class="text-danger" id="secure_info_{$secure_uniqid}"></span>
      <input name="secure_uniqid" value="{$secure_uniqid}" type="hidden" />
    </div>
  </div>
</div>
{/if}
