<head>
<meta charset="{$charset}" />
<title>{$pagetitle}</title>
<meta name="keywords" content="{$keywords}" />
<meta name="description" content="{$description}" />
<meta name="robots" content="{$robots}" />
<meta name="publisher" content="{$settings.Seitenbetreiber}" />
<meta name="generator" content="{#meta_generator#}" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
{if $settings.meta_google == 1 && !empty($settings.code_google)}
<meta name="google-site-verification" content="{$settings.code_google}" />
{/if}
{if $settings.meta_yandex == 1 && !empty($settings.code_yandex)}
<meta name="yandex-verification" content="{$settings.code_yandex}" />
{/if}
<link rel="shortcut icon" href="{$baseurl}/favicon.ico" />
{if !empty($canonical)}
<link rel="canonical" href="{$baseurl}/{$canonical}" />
{/if}
<link type="application/atom+xml" rel="alternate" title="{$settings.Seitenname|sanitize}" href="{$baseurl}/index.php?p=rss&amp;area={$area}" />
{if get_active('News')}
<link type="application/atom+xml" rel="alternate" title="{#Newsarchive#}" href="{$baseurl}/index.php?p=rss&amp;area={$area}&amp;action=news" />
{/if}
{if get_active('articles')}
<link type="application/atom+xml" rel="alternate" title="{#Gaming_articles#}" href="{$baseurl}/index.php?p=rss&amp;area={$area}&amp;action=articles" />
{/if}
{if get_active('forums')}
<link type="application/atom+xml" rel="alternate" title="{#Forums_Title#}" href="{$baseurl}/index.php?p=rss&amp;area={$area}&amp;action=forum" />
{/if}

{style file="{$themepath}/lib/bootstrap/css/bootstrap.min.css" position='head' priority='1000'}
{if get_active('forums')}
{style file="{$csspath}/forum.css" position='head' priority='1000'}
{/if}
{if get_active('calendar')}
{style file="{$csspath}/calendar.css" position='head' priority='1000'}
{/if}
{if get_active('shop')}
{style file="{$csspath}/shop.css" position='head' priority='1000'}
{/if}
{style file="{$csspath}/style.css" position='head' priority='1000'}
{style file="{$csspath}/colorbox.css" position='head' priority='800'}
{style file="{$themepath}/lib/fontello/css/fontello.css" position='head' priority='700'}

{script file="{$themepath}/lib/js/jquery-3.3.1.min.js" position='head' priority='1000'}
{script file="{$themepath}/lib/bootstrap/js/bootstrap.min.js" position='head' priority='1000'}
{script file="{$themepath}/lib/bootstrap/js/bootstrap.bundle.min.js" position='head' priority='1000'}

{script file="{$jspath}/jpatch.js" position='head' priority='1000'}
{script file="{$jspath}/jcolorbox.js" position='head' priority='800'}
{script file="{$jspath}/jcookie.js" position='head' priority='800'}
{script file="{$jspath}/jform.js" position='head' priority='800'}
{script file="{$jspath}/jtextcopy.js" position='head' priority='800'}
{script file="{$themepath}/lib/js/rating.js" position='800'}
{script file="{$themepath}/lib/js/main.js" position='head' priority='800'}

{result type='style' format='file' position='head'}  {* вывод файлов стилей *}
{result type='style' format='code' position='head'}  {* вывод кода стилей *}
{result type='script' format='file' position='head'} {* вывод файлов скриптов *}
{result type='script' format='code' position='head'} {* вывод кода скриптов *}

<script>
<!-- //
$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('.rating-star').rating();
    $('.colorbox').colorbox({
        height: '98%',
        width: '90%',
        iframe: true
    });
    $('.colorbox-sm').colorbox({
        height: '95%',
        width: '80%',
        iframe: true
    });
    $('body').textcopy({
        text: '{#MoreDetails#}'
    });
});
//-->
</script>

{result type='code' format='code' position='head'}   {* вывод кода *}
</head>
