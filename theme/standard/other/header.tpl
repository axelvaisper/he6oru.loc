<header class="header">
  <div class="container">
    <div class="row">
      <div class="col-md">

      </div>
      <div class="col-md-auto">
        {include file="$incpath/other/iconpanel.tpl"}
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        {navigation id=1 tpl='header.tpl'}
      </div>
    </div>

    {if !empty($breadcrumb)}
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            {foreach from=$breadcrumb item=item}
                <li class="breadcrumb-item">
                  {$item}
                </li>
            {/foreach}
          </ol>
        </nav>
    {/if}
  </div>
</header>
