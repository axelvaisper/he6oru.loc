{if permission('artvote')}
<script>
<!-- //
$(function() {
    $('#rating').submit(function () {
        var options = {
            target: '#rating_target',
            url: '{$RatingUrl}', timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return false;
    });
});
//-->
</script>

<div class="wrapper">
  <div class="h3 title">{#RateThis#}</div>
  <div class="box-content">
    <form class="form-inline" id="rating" method="post" action="">
      <div class="form-group mr-3">
        <div class="rating-star" data-rating="3" data-rating-active="true" data-rating-input="starrate"></div>
      </div>
      <div class="form-group mr-3">
        <input class="btn btn-primary" type="submit" value="{#RateThis#}" />
      </div>
      <span id="rating_target"></span>
    </form>
  </div>
</div>
{/if}
