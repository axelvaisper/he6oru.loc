{if $DisplayStartText != '0'}
    {$DisplayStartText}
{/if}
{if $OnlyStartText != 1}
    {$topcontent}
    {$toparticle}
    {$topnews}
    {$lastarticles}
    {$news}
    {$NewShopProducts}
    {$NewForumPosts}
    {$NewForumThreads}
    {$NewGalleries}
    {$NewProducts}
    {$NewCheats}
    {if get_active('new_links_downloads')}
        {$NewDownloads}
        {$NewLinks}
    {/if}
{/if}
