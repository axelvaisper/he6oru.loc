<!doctype html>
<html lang="{$langcode}">
  {include file="$incpath/other/head.tpl"}
  <body>
    {result type='script' format='file' position='body_start'} {* вывод файлов скриптов *}
    {result type='script' format='code' position='body_start'} {* вывод кода скриптов *}
    {result type='code' format='code' position='body_start'}   {* вывод кода *}

    {include file="$incpath/other/header.tpl"}

    <main class="main forum">
      <div class="container">
        {$content}
        {include file="$incpath/other/outlinks.tpl"}
    </main>

    {include file="$incpath/other/footer.tpl"}
    {include file="$incpath/other/google.tpl"}

    {result type='code' format='code' position='body_end'}   {* вывод кода *}
    {result type='script' format='file' position='body_end'} {* вывод файлов скриптов *}
    {result type='script' format='code' position='body_end'} {* вывод кода скриптов *}
  </body>
</html>
