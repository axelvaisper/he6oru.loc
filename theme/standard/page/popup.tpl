<!doctype html>
<html lang="{$langcode}">
  {include file="$incpath/other/head.tpl"}

  <main class="box-popup">
    {$content}
  </main>

  {include file="$incpath/other/google.tpl"}
</body>
</html>
