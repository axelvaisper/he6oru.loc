<!doctype html>
<html lang="{$langcode}">
  {include file="$incpath/other/head.tpl"}
  <body>
    {result type='script' format='file' position='body_start'} {* вывод файлов скриптов *}
    {result type='script' format='code' position='body_start'} {* вывод кода скриптов *}
    {result type='code' format='code' position='body_start'}   {* вывод кода *}

    {include file="$incpath/other/header.tpl"}

    <main class="main">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            {$shop_search_small}
            <div class="d-none d-md-block">
              {$shop_navigation}
              {include file="$incpath/shop/basket_saved_small.tpl"}
              {$curreny_selector}
              {$ShopInfoPanel}
              {$small_topseller}
              {$status_legend}
              {$payment_images}
              {include file="$incpath/shop/small_shipper.tpl"}
              {include file="$incpath/other/outlinks.tpl"}
            </div>
          </div>

          <div class="col-md-9">
            {$content}
          </div>
        </div>
      </div>
    </main>

    {include file="$incpath/other/footer.tpl"}
    {include file="$incpath/other/google.tpl"}

    {result type='code' format='code' position='body_end'}   {* вывод кода *}
    {result type='script' format='file' position='body_end'} {* вывод файлов скриптов *}
    {result type='script' format='code' position='body_end'} {* вывод кода скриптов *}
  </body>
</html>
