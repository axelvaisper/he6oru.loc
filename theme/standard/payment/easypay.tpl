{include file="$incpath/shop/steps.tpl"}

{if empty($payment_error)}
<script>
<!-- //
$(window).load(function () {
    setTimeout(function () {
        $('#easypay_payment_form').submit();
    }, 2500);
});
//-->
</script>

<div class="h3 title">{#Shop_thankyou_title#}</div>
<div class="box-content">
  {if !empty($payment_data.Icon)}
      <div class="wrapper text-center">
        <img src="uploads/shop/payment_icons/{$payment_data.Icon}" alt="" />
      </div>
  {/if}

  {if !empty($payment_data.Text)}
      <div class="wrapper">
        {$payment_data.Text}
      </div>
  {/if}

  {if !empty($payment_data.TextLang)}
      {$payment_data.TextLang}
  {/if}

  <form method="post" method="post" id="easypay_payment_form" action="https://ssl.easypay.by/weborder/">
    <input type="hidden" name="EP_MerNo" value="{$EP_MerNo}" />
    <input type="hidden" name="EP_Expires" value="{$EP_Expires}" />
    <input type="hidden" name="EP_Debug" value="{$EP_Debug}" />
    <input type="hidden" name="EP_Sum" value="{$EP_Sum}" />
    <input type="hidden" name="EP_OrderNo" value="{$EP_OrderNo}" />
    <input type="hidden" name="EP_OrderInfo" value="{$EP_OrderInfo}" />
    <input type="hidden" name="EP_Hash" value="{$EP_Hash}" />
    <input type="hidden" name="EP_URL_Type" value="get" />
    <input type="hidden" name="EP_Success_URL" value="{$baseurl}" />
    <input type="hidden" name="EP_Cancel_URL"	value="{$baseurl}" />
  </form>
</div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
