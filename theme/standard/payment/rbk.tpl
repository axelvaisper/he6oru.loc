{include file="$incpath/shop/steps.tpl"}

{if empty($payment_error)}
<script>
<!-- //
$(window).load(function() {
    setTimeout(function() {
        $('#process').submit();
    }, 2500);
});
//-->
</script>

<div class="h3 title">{#Shop_thankyou_title#}</div>
<div class="box-content">
  {if !empty($payment_data.Icon)}
      <div class="wrapper text-center">
        <img src="uploads/shop/payment_icons/{$payment_data.Icon}" alt="" />
      </div>
  {/if}

  {if !empty($payment_data.Text)}
      <div class="wrapper">
        {$payment_data.Text}
      </div>
  {/if}

  {if !empty($payment_data.TextLang)}
      {$payment_data.TextLang}
  {/if}

  <form method="post" name="process" id="process" action="https://rbkmoney.ru/acceptpurchase.aspx">
    <input type="hidden" name="eshopId" value="{$payment_data.IdSeller}" />
    <input type="hidden" name="orderId" value="{$smarty.session.id_num_order}" />
    <input type="hidden" name="serviceName" value="{$inf_payment}" />
    <input type="hidden" name="recipientAmount" value="{$smarty.session.price_final}" />
    <input type="hidden" name="recipientCurrency" value="{$payment_data.Testmodus}" />
    <input type="hidden" name="user_email" value="{$smarty.session.price_final}" />
    <input type="hidden" name="version" value="2" />
    <input type="hidden" name="successUrl" value="{$baseurl}/index.php?payment=rb&p=shop&action=callback&reply=success" />
    <input type="hidden" name="failUrl" value="{$baseurl}/index.php?payment=rb&p=shop&action=callback&reply=error" />
    <input type="hidden" name="userField_1" value="{$smarty.session.order_number}" />
    <input type="hidden" name="userField_2" value="{$payment_hash}" />
  </form>
</div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
