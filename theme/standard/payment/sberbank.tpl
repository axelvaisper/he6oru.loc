<script>
<!-- //
$(function() {
    $('.popup-print').colorbox({ height: "95%", width: "80%", iframe: true });
});
//-->
</script>

{include file="$incpath/shop/steps.tpl"}

{if $payment_error != 1}
    <div class="h3 title">{#Shop_thankyou_title#}</div>
    <div class="box-content">
      <div class="wrapper">
        Квитанция оформлена
      </div>
      <div class="spacer">
        <a class="popup-print" title="Распечатать квитанцию" href="index.php?p=misc&do=viewpayorder&oid={$smarty.session.id_num_order}">
          <i class=icon-print></i>Распечатать квитанцию
        </a>
      </div>
      <div class="spacer">
        <a title="{#Shop_go_myorders#}" href="index.php?p=shop&amp;action=myorders">
          <i class=icon-basket></i>{#Shop_go_myorders#}
        </a>
      </div>
      {include file="$incpath/payment/pd4.tpl"}
    </div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
