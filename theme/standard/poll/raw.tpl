{if $PollAllreadySmall == 1}
    {if isset($Extern)}
        <div class="h2">{$PollTitleSmall|sanitize}</div>
    {else}
        <div class="font-weight-bold mb-2">{$PollTitleSmall|sanitize}</div>
    {/if}
    <div class="spacer">
      {foreach from=$PollResultsSmall item=pas}
          {if $pas->Perc == 1}
              {assign var=PollVar value=0}
          {else}
              {assign var=PollVar value=$pas->Perc|replace:',':'.'}
          {/if}
          <div>{$pas->Frage|sanitize}</div>
          <div class="progress">
            <div class="progress-bar progress-bar-striped" role="progressbar" style="width:{$PollVar|default:0}%;background-color:{$pas->Farbe}" aria-valuenow="{$PollVar|default:0}" aria-valuemin="0" aria-valuemax="100">
              {if !empty($PollVar)}
                  {$PollVar|default:0}%
              {else}
                  <span style="color:{$pas->Farbe}">{$PollVar|default:0}%</span>
              {/if}
            </div>
          </div>
      {/foreach}
    </div>
{else}

<script>
<!-- //
$(function () {
    var options = {
        target: '#pollform{$Extern}',
        timeout: 3000
    };
    $('#poll_form_{$Extern}').submit(function() {
        $(this).ajaxSubmit(options);
        return false;
    });
});
//-->
</script>

  <form id="poll_form_{$Extern}" method="post" action="{$baseurl}/index.php?vote=1&amp;p=poll&amp;action=smallpoll{if isset($Extern)}&amp;intern=1{/if}" name="poll_form{$Extern}">
    {if isset($Extern)}
        <div class="h2">{$PollTitleSmall|sanitize}</div>
    {else}
        <div class="font-weight-bold mb-3">{$PollTitleSmall|sanitize}</div>
    {/if}

    {foreach from=$PollAnswersSmall item=pas}
        <div class="form-group form-check">
          {if $PollRes->Multi == 1}
              <input class="form-check-input" id="polloption{$pas->Id}" name="polloption[{$pas->Id}]" value="{$pas->Id}" type="checkbox" />
          {else}
              <input class="form-check-input" id="polloption{$pas->Id}" name="polloption" value="{$pas->Id}" type="radio" />
          {/if}
          <label class="form-check-label" for="polloption{$pas->Id}">{$pas->Frage|sanitize}</label>
        </div>
    {/foreach}
    {$out}

    <div class="form-group">
      {if $PollPermSmall == 1}
          <input class="btn btn-primary btn-block" type="submit" value="{#Poll_button#}" />
      {else}
          <div class="small text-center">{#Poll_noAccess#}</div>
      {/if}
    </div>
  </form>
{/if}
{if !isset($Extern) && $PollRes->Kommentare == 1}
    <a class="d-block" href="{$baseurl}/index.php?p=poll&amp;area={$area}#comments">{#Comments#}</a>
{/if}
<a class="d-block" href="{$baseurl}/index.php?p=poll&amp;action=archive&amp;area={$area}">{#Poll_Archive#}</a>
