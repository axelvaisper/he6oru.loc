{if !empty($NewProductEntries)}
    <div>
      <div class="h3 title">{#Products_new#}</div>
      {foreach from=$NewProductEntries item=e}
          <div class="box-content">
            <h3 class="listing-head text-truncate">
              <a title="{$e->Name|sanitize}" href="index.php?p=products&amp;area={$area}&amp;action=showproduct&amp;id={$e->Id}&amp;name={$e->Name|translit}">{$e->Name|sanitize}</a>
            </h3>
            <div class="clearfix">
              {if !empty($e->Bild)}
                  <a href="index.php?p=products&amp;area={$area}&amp;action=showproduct&amp;id={$e->Id}&amp;name={$e->Name|translit}">
                    <img class="img-fluid listing-img-right" src="uploads/products/{$e->Bild}" alt="{$e->Name|sanitize}" />
                  </a>
              {/if}
              <div class="text-justify">
                {$e->Beschreibung|truncate:500}
              </div>
            </div>
          </div>
      {/foreach}
    </div>
{/if}
