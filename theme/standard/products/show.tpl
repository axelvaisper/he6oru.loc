<div class="wrapper">
  <h1 class="title">{$res.Name|sanitize}</h1>
  <div class="wrapper clearfix">
    {if !empty($res.Bild) && $smarty.request.artpage < 2}
        <img class="img-fluid img-right" src="uploads/products/{$res.Bild}" alt="{$res.Name|sanitize}" />
    {/if}
    {$res.Inhalt}
  </div>
</div>

{if $article_pages}
    <div class="wrapper">
      {$article_pages}
    </div>
{/if}

<div class="wrapper">
  <div class="h3 title">{#Info#}</div>
  <div class="box-content">
    <div class="row">
      <div class="col-4">{#Added#}</div>
      <div class="col-8">
        {$res.Datum|date_format:$lang.DateFormatSimple}
      </div>
    </div>
    {if $res.ManLink}
        <div class="row">
          <div class="col-4">{#Manufacturer#}</div>
          <div class="col-8">{$res.ManLink}</div>
        </div>
    {/if}
    {if $res.PubLink}
        <div class="row">
          <div class="col-4">{#Products_publisher#}</div>
          <div class="col-8">{$res.PubLink}</div>
        </div>
    {/if}
    {if $res.Genre}
        <div class="row">
          <div class="col-4">{#Global_Categ#}</div>
          <div class="col-8">{$res.Genre}</div>
        </div>
    {/if}
    {if $res.Datum_Veroffentlichung > 1}
        <div class="row">
          <div class="col-4">{#Date#}</div>
          <div class="col-8">{$res.Datum_Veroffentlichung|date_format:$lang.DateFormatSimple}</div>
        </div>
    {/if}
    {if !empty($res.Preis)}
        <div class="row">
          <div class="col-4">{#Products_price#}</div>
          <div class="col-8">{$res.Preis|sanitize}</div>
        </div>
    {/if}
    {if $res.Adresse}
        <div class="row">
          <div class="col-4">{#Imprint#}</div>
          <div class="col-8">{$res.Adresse}</div>
        </div>
    {/if}
    {if $res.Shopurl}
        <div class="row">
          <div class="col-4">{#Products_buyat#}</div>
          <div class="col-8"><a href="{$res.Shopurl|sanitize}" target="_blank">{$res.Shop|sanitize}</a></div>
        </div>
    {/if}
    {if $ProductLinks}
        <div class="row">
          <div class="col-4">{#Products_links#}</div>
          <div class="col-8">
            {foreach from=$ProductLinks item=a}
                <a class="d-block" href="{$a->Link}" target="_blank">{$a->Name}</a>
            {/foreach}
          </div>
        </div>
    {/if}
    {if $res.Wertung && !empty($RatingUrl)}
        <div class="row">
          <div class="col-4">{#Rating_Rating#}</div>
          <div class="col-8 flex-line">
            <div class="rating-star" data-rating="{$res.Wertung}"></div>
          </div>
        </div>
    {/if}
  </div>
</div>

{$IncludedGalleries}
{$RatingForm}
{$GetComments}
