<style type="text/css">
  .dl_a_1 { background: #fdc; border-color: #e88; }
  .dl_b_1 { background: #fed; border-color: #e99; }
  .dl_a_2 { background: #ffb; border-color: #eea; }
  .dl_b_2 { background: #ffd; border-color: #dd8; }
  .dl_a_3 { background: #fbfbfb; border-color: #ddd; }
  .dl_b_3 { background: #f6f6f6; border-color: #ccc; }
  .dl_a_4 { background: #e7ffff; border-color: #cee; }
  .dl_b_4 { background: #dff; border-color: #bee; }
  .dl_a_5 { background: #e7eeff; border-color: #cde; }
  .dl_b_5 { background: #dde7ff; border-color: #cde; }
</style>

<h1 class="title">{#Roadmap#}</h1>
<div class="box-content">
  <h4>{$name}</h4>

  <div class="form-group">
    <select class="form-control" id="closed" name="closed" onchange="location.href = 'index.php?p=roadmap&amp;action=display&amp;rid={$smarty.request.rid}&amp;closed={if $smarty.request.closed == 1}0{else}1{/if}&amp;area={$area}&amp;name={$name|translit}';">
      <option value="1" {if $smarty.request.closed == 1}selected{/if}>{#ClosedTickets#}</option>
      <option value="0" {if $smarty.request.closed == 0}selected{/if}>{#OpenTickets#}</option>
    </select>
  </div>

  <table class="table">
    <thead>
      <tr>
        <th>{#Description#}</th>
        <th class="text-center">{#Date#}</th>
        <th class="text-center">{#GlobalAutor#}</th>
        <th class="text-center">{#Prio#}</th>
      </tr>
    </thead>
    {foreach from=$items item=item}
        <tr class="dl_{cycle name="1" values="a,b"}_{$item.pr}">
          <td>{$item.Beschreibung}</td>
          <td class="text-center">{$item.Datum|date_format: '%d.%m.%Y'}</td>
          <td class="text-center">{$item.Benutzer}</td>
          <td class="text-center">{$item.prio}</td>
        </tr>
    {/foreach}
  </table>
</div>
