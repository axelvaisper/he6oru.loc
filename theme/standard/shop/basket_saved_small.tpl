{if isset($Baskets)}
    <div class="wrapper panel">
      <div class="panel-head">{#Shop_savedBasketInf#}</div>
      <div class="panel-main">
        <div class="font-weight-bold wrapper">
          {if $Bcc->Bcount > 1}
              {#Shop_savedBasketMsgInf1#}
          {else}
              {#Shop_savedBasketMsgInf2#}
          {/if}
        </div>
        <a href="index.php?p=shop&amp;action=showsavedbaskets">{#Shop_savedBasketLink1#}</a>
      </div>
    </div>
{/if}
