{if (isset($basket_products_price) && $basket_products_price > 0) || (isset($basket_products_all) && $basket_products_all >= 1)}
  {if $smarty.request.subaction != 'step4' && $smarty.request.subaction != 'final'}
    {*
      <div class="shop-basket-count">{$basket_products|default:0}</div>
    *}
    <div class="shop-basket-count" data-toggle="tooltip" title="{#Shop_articleSumm#} {$basket_products_price|numformat} {$currency_symbol}">
      {$basket_products_all|default:0}
    </div>
  {/if}
{/if}
