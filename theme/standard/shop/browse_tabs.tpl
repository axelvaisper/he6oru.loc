<div class="row justify-content-between">
  {foreach from=$tab_items item=p name=pro}
      <div class="col-md text-center">
        <div class="wrapper">
          <a title="{$p.Titel|sanitize}" href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
            <img class="img-fluid height-4" src="{$p.Bild_Klein}" alt="{$p.Titel|sanitize}" />
          </a>
        </div>

        {if $shopsettings->PreiseGaeste == 1 || $loggedin}
            <div class="text-truncate2">
              <a title="{$p.Titel|sanitize}" href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                {$p.Titel|truncate:20|sanitize}
              </a>
            </div>
            {if $p.Preis > 0}
                <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
            {else}
                <span class="shop-price">{#Zvonite#}</span>
            {/if}
        {else}
            {#Shop_prices_justforUsers#}
        {/if}
      </div>
  {/foreach}
</div>
