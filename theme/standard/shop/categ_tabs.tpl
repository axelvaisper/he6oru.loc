{if $smarty.request.s != 1 && ($sub_categs || $newin_shop || $offers_shop  || $topseller_shop)}

    {assign var='active' value=0}
    <div class="wrapper">
      <ul class="nav nav-tabs" role="tablist">
        {if $sub_categs}
            <li class="nav-item">
              {if $active == 0}
                  {assign var='active' value=1}
                  <a class="nav-link active" id="categs-tab" data-toggle="tab" href="#categs" role="tab" aria-controls="categs" aria-selected="true">
                  {else}
                      <a class="nav-link" id="categs-tab" data-toggle="tab" href="#categs" role="tab" aria-controls="categs" aria-selected="false">
                      {/if}
                      {#AllCategs#}
                  </a>
            </li>
        {/if}
        {if $newin_shop}
            <li class="nav-item">
              {if $active == 0}
                  {assign var='active' value=2}
                  <a class="nav-link active" id="newin-tab" data-toggle="tab" href="#newin" role="tab" aria-controls="newin" aria-selected="true">
                  {else}
                      <a class="nav-link" id="newin-tab" data-toggle="tab" href="#newin" role="tab" aria-controls="newin" aria-selected="false">
                      {/if}
                      {#Shop_NewProducts#}
                  </a>
            </li>
        {/if}
        {if $offers_shop}
            <li class="nav-item">
              {if $active == 0}
                  {assign var='active' value=3}
                  <a class="nav-link active" id="offers-tab" data-toggle="tab" href="#offers" role="tab" aria-controls="offers" aria-selected="true">
                  {else}
                      <a class="nav-link" id="offers-tab" data-toggle="tab" href="#offers" role="tab" aria-controls="offers" aria-selected="false">
                      {/if}
                      {#Shop_Offers#}
                  </a>
            </li>
        {/if}
        {if $topseller_shop}
            <li class="nav-item">
              {if $active == 0}
                  {assign var='active' value=4}
                  <a class="nav-link active" id="topseller-tab" data-toggle="tab" href="#topseller" role="tab" aria-controls="topseller" aria-selected="true">
                  {else}
                      <a class="nav-link" id="topseller-tab" data-toggle="tab" href="#topseller" role="tab" aria-controls="topseller" aria-selected="false">
                      {/if}
                      {#Shop_Topseller#}
                  </a>
            </li>
        {/if}
      </ul>

      <div class="box-content">
        <div class="tab-content">
          {if $sub_categs}
              <div class="tab-pane fade{if $active == 1} show active{/if}" id="categs" role="tabpanel" aria-labelledby="categs-tab">
                {include file="$incpath/shop/browse_tabs_categ.tpl"}
              </div>
          {/if}
          {if $newin_shop}
              <div class="tab-pane fade{if $active == 2} show active{/if}" id="newin" role="tabpanel" aria-labelledby="newin-tab">
                {$newin_shop}
              </div>
          {/if}
          {if $offers_shop}
              <div class="tab-pane fade{if $active == 3} show active{/if}" id="offers" role="tabpanel" aria-labelledby="offers-tab">
                {$offers_shop}
              </div>
          {/if}
          {if $topseller_shop}
              <div class="tab-pane fade{if $active == 4} show active{/if}" id="topseller" role="tabpanel" aria-labelledby="topseller-tab">
                {$topseller_shop}
              </div>
          {/if}
        </div>
      </div>
    </div>
{/if}
