{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#ajaxcp').validate({
        rules: {
          coupon: { required: true }
        },
        messages: {
            coupon: { required: '' }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                target: '#ajaxcpresult',
                timeout: 6000,
                clearForm: false,
                resetForm: false
            });
        }
    });
});
//-->
</script>

<div class="wrapper">{#ShopCouponQuest#}</div>

{if $c_error}
    <div class="box-error">
      {$c_error}
    </div>
{/if}

<form autocomplete="off" id="ajaxcp" method="post" name="ajaxcp" action="{$baseurl}/index.php?action=ajaxcoupon&p=shop">
  <div class="input-group form-group">
    <input class="form-control" name="coupon" value="" type="text">
    <span class="input-group-append">
      <input class="btn btn-primary" value="{#ShopCouponQuestB#}" type="submit">
    </span>
  </div>
</form>
