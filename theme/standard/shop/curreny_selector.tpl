{if !isset($smarty.request.action) || isset($smarty.request.action) && ($smarty.request.action != 'shoporder' || $smarty.request.action != 'showbasket')}
    {if $curr_change == 1 && ($cu_array.Waehrung_2 || $cu_array.Waehrung_3)}
        <div class="wrapper panel">
          <div class="panel-head">{#Shop_changeCurrency#}</div>
          <div class="panel-main text-center">
            <div class="row wrapper">
              <div class="col">
                {if $cu_array.Waehrung_1}
                    {if (isset($smarty.request.currency) && $smarty.request.currency == 1) || (isset($smarty.session.currency) && $smarty.session.currency == 1) || empty($smarty.session.currency)}
                        <span class="font-weight-bold">{$cu_array.Waehrung_1}</span>
                    {else}
                        <a href="index.php?p=shop&amp;currency=1">{$cu_array.Waehrung_1}</a>
                    {/if}
                {/if}
              </div>
              <div class="col text-center">
                {if $cu_array.Waehrung_2}
                    {if (isset($smarty.request.currency) && $smarty.request.currency == 2) || (isset($smarty.session.currency) && $smarty.session.currency == 2)}
                        <span class="font-weight-bold">{$cu_array.Waehrung_2}</span>
                    {else}
                        <a href="index.php?p=shop&amp;currency=2">{$cu_array.Waehrung_2}</a>
                    {/if}
                {/if}
              </div>
              <div class="col text-center">
                {if $cu_array.Waehrung_3}
                    {if (isset($smarty.request.currency) && $smarty.request.currency == 3) || (isset($smarty.session.currency) && $smarty.session.currency == 3)}
                        <span class="font-weight-bold">{$cu_array.Waehrung_3}</span>
                    {else}
                        <a href="index.php?p=shop&amp;currency=3">{$cu_array.Waehrung_3}</a>
                    {/if}
                {/if}
              </div>
            </div>

            {assign var='foo' value='1'}
            {if $cu_array.Waehrung_2}
                <div>1 {$cu_array.WaehrungSymbol_2} = {($foo/$cu_array.Multiplikator_2)|numformat} {$cu_array.WaehrungSymbol_1}</div>
            {/if}
            {if $cu_array.Waehrung_3}
                <div>1 {$cu_array.WaehrungSymbol_3} = {($foo/$cu_array.Multiplikator_3)|numformat} {$cu_array.WaehrungSymbol_1}</div>
            {/if}
          </div>
        </div>
    {/if}
{/if}
