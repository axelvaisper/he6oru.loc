<div class="wrapper panel">
  <div class="panel-head">{#Info#}</div>
  <div class="panel-main d-flex flex-column">
    <a href="index.php?exts=1&amp;s=1&amp;area={$smarty.session.area}&amp;p=shop&amp;action=showproducts">{#Arrow#}{#ExtendedSearch#}</a>
    <a href="index.php?p=shop&amp;action=privacy">{#Arrow#}{#Shop_infopageDataInf#}</a>
    <a href="index.php?p=shop&amp;action=agb">{#Arrow#}{#Shop_infopageAgb#}</a>
    <a href="index.php?p=shop&amp;action=refusal">{#Arrow#}{#Shop_f_rcall_inf#}</a>
    <a href="index.php?p=shop&amp;action=shippingcost">{#Arrow#}{#Shop_shipping_cost#}</a>
    <a href="index.php?p=shop&amp;action=showproducts&amp;page=1&amp;offers=1{if !empty($smarty.request.cid)}&amp;cid={$smarty.request.cid}{/if}&amp;limit={$smarty.request.limit|default:$plim}">{#Arrow#}{#Shop_Offers#}</a>
    <a href="index.php?p=shop&amp;action=showproducts&amp;page=1&amp;topseller=1{if !empty($smarty.request.cid)}&amp;cid={$smarty.request.cid}{/if}&amp;limit={$smarty.request.limit|default:$plim}">{#Arrow#}{#Shop_Topseller#}</a>
    <a href="index.php?p=shop&amp;action=showproducts&amp;page=1&amp;lowamount=1{if !empty($smarty.request.cid)}&amp;cid={$smarty.request.cid}{/if}&amp;limit={$smarty.request.limit|default:$plim}">{#Arrow#}{#Shop_lowProducts#}</a>
    <a href="index.php?p=shop&amp;action=prais">{#Arrow#}{#Shop_Preis#}</a>
  </div>
</div>
