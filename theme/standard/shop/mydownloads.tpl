<div class="h1 title">{#LoginExternVd#}</div>
<div class="box-content">
  {if $smarty.get.sub != 'showfile'}
      <div class="wrapper">
        {#Shop_go_mydownloadsInf#}
      </div>
  {/if}
  {if isset($smarty.get.sub) && $smarty.get.sub == 'showfile'}
      <div class="wrapper">
        {#Shop_go_mydownloadsInf2#}
      </div>
  {/if}
  {if $error}
      <div class="box-error">
        <div class="font-weight-bold">{#Error#}</div>
        {foreach from=$error item=e}
            <div>{$e}</div>
        {/foreach}
      </div>
  {/if}

  {if isset($smarty.get.sub) && $smarty.get.sub == 'showfile'}
      <form method="post" action="index.php">

        <div class="row form-group">
          <div class="col-3">{#Shop_mydownloads_filename#}</div>
          <div class="col">{$file->Titel|sanitize}</div>
        </div>

        <div class="row form-group">
          <div class="col-3">{#Shop_mydownloads_filename#}</div>
          <div class="col">{$file->Datei}</div>
        </div>
        {if $file->Beschreibung}
            <div class="row form-group">
              <div class="col-3">{#Description#}</div>
              <div class="col">{$file->Beschreibung}</div>
            </div>
        {/if}
        {if $file->Gesperrt == 1}
            <div class="row form-group">
              <div class="col-3">{#Shop_mydownloads_filehint#}</div>
              <div class="col">
                {#Shop_mydownloads_loecked#}
                <div class="small">{$file->GesperrtGrund}</div>
              </div>
            </div>
        {else}
            {if !empty($file->KommentarAdmin)}
                <div class="row form-group">
                  <div class="col-3">{#Global_Comment#}</div>
                  <div class="col">{$file->KommentarAdmin}</div>
                </div>
            {/if}
            <div class="row form-group">
              <div class="col-3">
                <label for="KommentarBenutzer">{#Shop_mydownloads_filecommntuser#}</label>
              </div>
              <div class="col">
                <textarea class="form-control" name="KommentarBenutzer" rows="3" id="KommentarBenutzer">{$file->KommentarBenutzer|sanitize}</textarea>
              </div>
            </div>
            {if $file->UrlLizenz_Pflicht == 1}
                <div class="row form-group">
                  <div class="col-3">
                    <label for="UrlLizenz">
                      {#Shop_mydownloads_fileurl#}
                      <div class="small">{#Shop_mydownloads_fileurl_hint#}</div>
                    </label>
                  </div>
                  <div class="col">
                    <input class="form-control" type="text" name="UrlLizenz" id="UrlLizenz" value="{$file->UrlLizenz|sanitize}" />
                  </div>
                </div>
            {/if}
            <div class="row form-group">
              <div class="col offset-3">
                <input id="agbok" name="agb_ok" type="checkbox" value="1" />
                <a href="index.php?p=shop&amp;action=agb" target="_blank">{#Shop_mydownloads_fileagbok#}</a>
              </div>
            </div>

            <div class="text-center">
              <input class="btn btn-primary btn-block-sm" type="submit" value="{#Shop_mydownloads_filedownload#}" />
            </div>
        {/if}

        <input type="hidden" name="p" value="shop" />
        <input type="hidden" name="action" value="mydownloads" />
        <input type="hidden" name="sub" value="getfile" />
        <input type="hidden" name="Id" value="{$smarty.get.Id}" />
        <input type="hidden" name="FileId" value="{$smarty.get.FileId}" />
        <input type="hidden" name="getId" value="{$smarty.get.getId}" />
        <input type="hidden" name="FileName" value="{$file->Titel|sanitize}" />
      </form>

  {else}

<script>
<!-- //
function request(id) {
    var text = '{#OrderRequestText#}';
    text = text.replace(/__ORDER__/gi, id);
    text = text.replace(/__USER__/gi, '{$smarty.session.cp_uname}');
    document.getElementById('request').style.display = '';
    document.getElementById('request_subject').value = '{#OrdersRequestSubject#} ' + id;
    document.getElementById('request_text').value = text;
}
//-->
</script>

      {foreach from=$downloads item=dl name=d}
          <div class="h4 title">
            <span style="" id="so_{$dl->Id}">
              <a href="javascript:void(0);" onclick="document.getElementById('{$dl->Id}').style.display = ''; document.getElementById('sc_{$dl->Id}').style.display = '';document.getElementById('so_{$dl->Id}').style.display = 'none';">
                <i class="icon-plus-squared-alt"></i>{$dl->ArtName}
              </a>
            </span>
            <span style="display: none" id="sc_{$dl->Id}">
              <a href="javascript:void(0);" onclick="document.getElementById('{$dl->Id}').style.display = 'none';document.getElementById('so_{$dl->Id}').style.display = '';document.getElementById('sc_{$dl->Id}').style.display = 'none';">
                <i class="icon-minus-squared-alt"></i>{$dl->ArtName}
              </a>
            </span>
          </div>

          <div id="{$dl->Id}" style="display: none">
            {if $dl->Lizenz}
                <div class="h4 wrapper">
                  {#Shop_yLicData#}: {$dl->Lizenz}
                </div>
            {/if}
            <div class="wrapper">
              <div class="row row-header font-weight-bold">
                <div class="col">{#Shop_mydownloads_filename#}</div>
                <div class="col">{#Shop_mydownloads_downloadable#}</div>
                <div class="col">{#GlobalSize#}</div>
              </div>
            </div>
            {if $dl->DataFiles}
                <div class="row row-header">
                  <div class="col">{#Shop_mydownloads_full#}</div>
                </div>
                {foreach from=$dl->DataFiles item=df}
                    <div class="row {cycle name='files' values='row-primary,row-secondary'}">
                      <div class="col">
                        {if $df->Abgelaufen == 1}
                            {$df->Titel}
                        {else}
                            <a data-ttggle="tooltip" title="{$df->Beschreibung|tooltip}" href="index.php?p=shop&amp;action=mydownloads&amp;sub=showfile&amp;Id={$df->Id}&amp;FileId={$dl->ArtikelId}&amp;getId={$df->Id}">{$df->Titel}</a>
                        {/if}
                      </div>
                      <div class="col">{$dl->DownloadBis|date_format:'%d.%m.%Y'}</div>
                      <div class="col">{$df->size}</div>
                    </div>
                {/foreach}
            {/if}

            {if $dl->DataFilesUpdates}
                <div class="row row-header">
                  <div class="col">{#Shop_mydownloads_update#}</div>
                </div>
                {foreach from=$dl->DataFilesUpdates item=df}
                    <div class="row {cycle name='updates' values='row-primary,row-secondary'}">
                      <div class="col">
                        {if $df->Abgelaufen == 1}
                            {$df->Titel}
                        {else}
                            <a data-ttggle="tooltip" title="{$df->Beschreibung|tooltip}"  href="index.php?p=shop&amp;action=mydownloads&amp;sub=showfile&amp;Id={$df->Id}&amp;FileId={$dl->ArtikelId}&amp;getId={$df->Id}">{$df->Titel}</a>
                        {/if}
                      </div>
                      <div class="col">{$dl->DownloadBis|date_format:'%d.%m.%Y'}</div>
                      <div class="col">{$df->size}</div>
                    </div>
                {/foreach}
            {/if}

            {if $dl->DataFilesBugfixes}
                <div class="row row-header">
                  <div class="col">{#Shop_mydownloads_bugfix#}</div>
                </div>
                {foreach from=$dl->DataFilesBugfixes item=df}
                    <div class="row {cycle name='bigfix' values='row-primary,row-secondary'}">
                      <div class="col">
                        <a data-ttggle="tooltip" title="{$df->Beschreibung|tooltip}" href="index.php?p=shop&amp;action=mydownloads&amp;sub=showfile&amp;Id={$df->Id}&amp;FileId={$dl->ArtikelId}&amp;getId={$df->Id}">
                          {$df->Titel}
                        </a>
                      </div>
                      <div class="col">{$dl->DownloadBis|date_format:$lang.DateFormat}</div>
                      <div class="col">{$df->size}</div>
                    </div>
                {/foreach}
            {/if}

            {if $dl->DataFilesOther}
                <div class="row row-header">
                  <div class="col">{#ActionOther#}</div>
                </div>
                {foreach from=$dl->DataFilesOther item=df}
                    <div class="row {cycle name='other' values='row-primary,row-secondary'}">
                      <div class="col">
                        <a data-ttggle="tooltip" title="{$df->Beschreibung|tooltip}" href="index.php?p=shop&amp;action=mydownloads&amp;sub=showfile&amp;Id={$df->Id}&amp;FileId={$dl->ArtikelId}&amp;getId={$df->Id}">
                          {$df->Titel}
                        </a>
                      </div>
                      <div class="col">{#Shop_mydownloads_infi#}</div>
                      <div class="col">{$df->size}</div>
                    </div>
                {/foreach}
            {/if}
          </div>
      {/foreach}
  {/if}
</div>
