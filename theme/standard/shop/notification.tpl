{if empty($dialog)}
    {assign var=dialog value='default'}
{/if}

<script>
<!-- //
$(function() {
    $('#to-basket-{$dialog}').on('click', function() {
        {if isset($smarty.request.blanc) && $smarty.request.blanc == 1}parent.{/if}document.location = 'index.php?action=showbasket&p=shop';
    });
});
//-->
</script>

<div class="modal fade" id="basket-{$dialog}" tabindex="-1" role="dialog" aria-labelledby="basket-title-{$dialog}" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title h4" id="basket-title-{$dialog}">{#Notification#}</div>
        <button type="button" class="close" data-dismiss="modal" aria-label="{#WinClose#}">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center h5">{#Shop_ProdAddedToBasket#}</div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{#WinClose#}</button>
        <button type="button" class="btn btn-primary" id="to-basket-{$dialog}">{#Shop_go_basket#}</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="favorite-{$dialog}" tabindex="-1" role="dialog" aria-labelledby="favorite-title-{$dialog}" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title h4" id="favorite-title-{$dialog}">{#Notification#}</div>
        <button type="button" class="close" data-dismiss="modal" aria-label="{#WinClose#}">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center h5">{#Shop_ProdAddedToList#}</div>
    </div>
  </div>
</div>
