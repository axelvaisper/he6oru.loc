<div class="box-content">
  <div class="wrapper">
    {#Shop_priceAlertInf#}
  </div>
  {if !empty($error)}
      <div class="box-error">
        {foreach from=$error item=err}
            <div>{$err}</div>
        {/foreach}
      </div>
  {/if}
  <form method="post" action="#pricealert">

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="alert-mail">{#Email#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <input class="form-control" id="alert-mail" name="pricealert_email" type="email" value="{$smarty.post.pricealert_email|sanitize}" required="required" />
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="alert-price">{#Shop_priceAlertYPrice#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <input class="form-control" id="alert-price" name="pricealert_newprice" type="text" value="{$smarty.post.pricealert_newprice|sanitize}" required="required" />
      </div>
    </div>

    <div class="text-center">
      <input class="btn btn-primary btn-block-sm" type="submit" value="{#ButtonSend#}" />
    </div>

    <input type="hidden" name="pricealert_send" value="1" />
    <input type="hidden" name="red" value="{$red}" />
  </form>
</div>
