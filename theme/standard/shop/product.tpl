{if $p.Fsk18 == 1 && $fsk_user != 1}
    {assign var="not_possible_to_buy" value=1}
{/if}

{if isset($notfound) && $notfound == 1}
    <div class="box-content">
      <div class="box-error">
        {#Shop_errorProduct#}
      </div>
    </div>
{else}

{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file = "$incpath/other/jsvalidate.tpl"}
$(function() {
    $('.img-popup').colorbox({
        photo: true,
        maxHeight: "98%",
        maxWidth: "98%",
        slideshow: true,
        slideshowAuto: false,
        slideshowSpeed: 2500,
        current: "{#GlobalImage#} {ldelim}current{rdelim} {#PageNavi_From#} {ldelim}total{rdelim}",
        slideshowStart: "{#GlobalStart#}",
        slideshowStop: "{#GlobalStop#}",
        previous: "{#GlobalBack#}",
        next: "{#GlobalNext#}",
        close: "{#GlobalGlose#}"
    });
    $('.img-popup-more').colorbox({ height: '95%', width: '80%', iframe: true });
    $('#tobasket').validate({
        rules: {
        {if !empty($p.Frei_1) && $p.Frei_1_Pflicht == 1}
        free_1: { required: true },
        {/if}
        {if !empty($p.Frei_2) && $p.Frei_2_Pflicht == 1}
        free_2: { required: true },
        {/if}
        {if !empty($p.Frei_3) && $p.Frei_3_Pflicht == 1}
        free_3: { required: true },
        {/if}
        amount: { required: true, number: true }
        },
        messages: { },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                {if empty($smarty.request.blanc)}
                target: '#ajaxbasket',
                {/if}
                timeout: 6000,
                success: showResponse,
                clearForm: false,
                resetForm: false
            });
        }
    });
});

function showResponse() {
    if ($('#to_mylist').val() == 1) {
        showModal($('#favorite-product'), 3000);
    } else {
        showModal($('#basket-product'), 10000);
    }
    $('#to_mylist').val(0)
}
//-->
</script>

    {include file="$incpath/shop/notification.tpl" dialog='product'}

    <form method="post" name="product_request_form" action="{page_link}#product_request">
      <input type="hidden" name="subaction" value="product_request" />
    </form>

    <h1 class="title text-truncate">{$p.Titel|sanitize}</h1>
    <div>
      <form method="post" id="tobasket" action="index.php?p=shop&amp;area={$area}">

        <div class="row">
          <div class="col-md-2 col-sm-12 wrapper">
            <div class="row justify-content-center">

              <div class="col-auto">
                <div class="wrapper">
                  {if empty($p.NoBild)}
                      <a class="img-popup" rel="poppim" href="{$p.BildPopLink}">
                        <img class="img-fluid" src="{$p.Bild}" alt="{$p.Titel|sanitize}" />
                      </a>
                  {else}
                      <img src="{$p.Bild}" alt="{$p.Titel|sanitize}" />
                  {/if}
                </div>
              </div>

              <div class="col-auto">
                {if $images}
                    <div class="row">
                      {assign var=icount value=0}
                      {assign var=showlink value=0}
                      {foreach from=$images item=im name=categ}
                          {assign var=icount value=$icount+1}
                          {if $icount < 5}
                              <div class="col spacer">
                                <a class="img-popup" rel="poppim" href="{$im.Bild_GrossLink}">
                                  <img class="img-fluid" src="{$im.Bild}" alt="" />
                                </a>
                              </div>
                              {if $icount % 2 == 0 && !$smarty.foreach.categ.last}
                              </div>
                              <div class="row">
                              {/if}
                          {else}
                              {assign var=showlink value=1}
                              <div style="display:none">
                                <a class="img-popup" rel="poppim" href="{$im.Bild_GrossLink}">
                                  <img class="img-fluid" src="{$im.Bild}" alt="" />
                                </a>
                              </div>
                          {/if}
                      {/foreach}
                    </div>

                    {if $showlink == 1}
                        <div class="spacer text-center">
                          <a class="img-popup-more" href="index.php?p=misc&do=shopimgages&prodid={$p.Id}">
                            <div class="small">{#Shop_moreImages#}</div>
                          </a>
                        </div>
                    {/if}
                {/if}
              </div>

              {if $p.Fsk18 == 1}
                  <div class="col-auto">
                    <div class="spacer text-center">
                      <img class="img-fluid" src="{$imgpath_page}special.png" alt="{#Shop_isFSKWarning#}" />
                      <div class="small">{#Shop_isFSKWarning#}</div>
                    </div>
                  </div>
              {/if}
            </div>
          </div>

          <div class="col-md col-sm-12">
            <div class="row no-gutters">
              <div class="col-md col-sm-12 wrapper">
                <input type="hidden" value="{$p.Preis|jsnum}" id="price_hidden" name="price_h" />
                {#Shop_ArticleNumber#}: {$p.Artikelnummer}

                {include file="$incpath/shop/product_price.tpl"}

                {#Shop_Availablility#}: {$p.VIcon}
                <div>
                  {if $not_on_store == 1}
                      {$p.VMsg|sanitize}
                  {else}
                      <div>
                        {#Shop_shipping_timeinf#}:
                        {if $order_for_you == 1}
                            {$available_array.3->Name|sanitize}
                        {else}
                            {$p.Lieferzeit|sanitize}
                        {/if}
                      </div>

                      {if $p.Lagerbestand > 0}
                          {if $low_amount == 1}
                              <div class="text-danger">
                                {#Shop_lowAmount#}
                                <div>
                                  {$lang.ShopLowWarnInf|replace:'__COUNT__':$p.Lagerbestand}
                                </div>
                              </div>
                          {elseif $shopsettings->Zeige_Lagerbestand == 1}
                              {#Shop_av_storeAv#}: {$p.Lagerbestand}
                          {/if}
                      {/if}
                  {/if}
                </div>
              </div>

              <div class="col-md-auto col-sm-12 wrapper">
                {if $shipping_free == 1}
                    <div class="shop-sticker">
                      {#Shop_freeshipping#}
                    </div>
                {/if}
                {if $p.diffpro > 0}
                    <div class="shop-sticker">
                      {#Shop_Billiger#}{$p.diffpro|numformat}%
                    </div>
                {/if}
                {if get_active('shop_merge')}
                    <div class="spacer">
                      <a class="colorbox" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1&amp;prodid={$p.Id}&amp;cid={$p.Kategorie}">
                        <i class="icon-window"></i>{#Merge#}
                      </a>
                    </div>
                {/if}
                <div class="spacer">
                  <a href="#product_request" onclick="document.forms['product_request_form'].submit();
                          return false;">
                    <i class="icon-chat"></i>{#Shop_prod_request_link#}
                  </a>
                </div>
                {if get_active('shop_preisalarm') && $shopsettings->PreiseGaeste == 1 || $loggedin}
                    <div class="spacer">
                      <a href="#pricealert">
                        <i class="icon-plus"></i>{#Shop_priceAlert#}
                      </a>
                    </div>
                {/if}
                {if $shop_bewertung == 1}
                    <div class="spacer">
                      <a href="#vote">
                        <i class="icon-thumbs-up"></i>{#Shop_prod_votes_link#}
                      </a>
                    </div>
                {/if}
                {if isset($shop_cheaper)}
                    <div class="spacer">
                      <a id="cheaper_link" href="#" title="{#cheaper_name#}">
                        <i class="icon-warning-empty"></i>{#cheaper_name#}
                      </a>
                    </div>
                {/if}
              </div>
            </div>

            {if $not_possible_to_buy == 1}
                <div class="box-content">
                  {$shopsettings->Fsk18}
                </div>
            {/if}

            {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                {include file="$incpath/shop/product_vars.tpl"}
                {include file="$incpath/shop/product_config.tpl"}
                {include file="$incpath/shop/product_amount_submit.tpl"}
            {/if}

          </div>
        </div>
      </form>
    </div>

    {$shop_cheaper}

    <div>
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="opt-1-tab" data-toggle="tab" href="#opt-1" role="tab" aria-controls="opt-1" aria-selected="true">
            {#buttonDetails#}
          </a>
        </li>
        {if $Zub_a_products_array}
            <li class="nav-item">
              <a class="nav-link" id="opt-2-tab" data-toggle="tab" href="#opt-2" role="tab" aria-controls="opt-2" aria-selected="false">
                {$tabs->TAB1|sanitize}
              </a>
            </li>
        {/if}
        {if $Zub_b_products_array}
            <li class="nav-item">
              <a class="nav-link" id="opt-3-tab" data-toggle="tab" href="#opt-3" role="tab" aria-controls="opt-3" aria-selected="false">
                {$tabs->TAB2|sanitize}
              </a>
            </li>
        {/if}
        {if $Zub_c_products_array}
            <li class="nav-item">
              <a class="nav-link" id="opt-4-tab" data-toggle="tab" href="#opt-4" role="tab" aria-controls="opt-4" aria-selected="true">
                {$tabs->TAB3|sanitize}
              </a>
            </li>
        {/if}
        {if $shopsettings->similar_product == 1 && $Zub_d_products_array}
            <li class="nav-item">
              <a class="nav-link" id="opt-5-tab" data-toggle="tab" href="#opt-5" role="tab" aria-controls="opt-5" aria-selected="false">
                {#Shop_detailSimilar#}
              </a>
            </li>
        {/if}
        {if $prod_downloads}
            <li class="nav-item">
              <a class="nav-link" id="opt-6-tab" data-toggle="tab" href="#opt-6" role="tab" aria-controls="opt-6" aria-selected="false">
                {#Shop_Downloads#}
              </a>
            </li>
        {/if}
      </ul>

      <div class="tab-content">
        <div class="tab-pane fade show active" id="opt-1" role="tabpanel" aria-labelledby="opt-1-tab">
          {include file="$incpath/shop/products_details.tpl"}
        </div>
        {if $Zub_a_products_array}
            <div class="tab-pane fade" id="opt-2" role="tabpanel" aria-labelledby="opt-2-tab">
              {$Zub_a_products}
            </div>
        {/if}
        {if $Zub_b_products_array}
            <div class="tab-pane fade" id="opt-3" role="tabpanel" aria-labelledby="opt-3-tab">
              {$Zub_b_products}
            </div>
        {/if}
        {if $Zub_c_products_array}
            <div class="tab-pane fade" id="opt-4" role="tabpanel" aria-labelledby="opt-4-tab">
              {$Zub_c_products}
            </div>
        {/if}
        {if $shopsettings->similar_product == 1 && $Zub_d_products_array}
            <div class="tab-pane fade" id="opt-5" role="tabpanel" aria-labelledby="opt-5-tab">
              {$Zub_d_products}
            </div>
        {/if}
        {if $prod_downloads}
            <div class="tab-pane fade" id="opt-6" role="tabpanel" aria-labelledby="opt-6-tab">
              <div class="box-content">
                {foreach from=$prod_downloads item=pdd}
                    <div class="row">
                      <div class="col-auto">
                        <a  class="img-fluid" href="{$baseurl}/uploads/shop/product_downloads/{$pdd->Datei}">
                          <img src="{$imgpath}/filetypes/{$pdd->Icon}" alt="" />
                        </a>
                      </div>

                      <div class="col">
                        <a href="{$baseurl}/uploads/shop/product_downloads/{$pdd->Datei}">
                          {$pdd->DlName}
                        </a>
                        {$pdd->Beschreibung}
                      </div>

                      <div class="col">{$pdd->Size}</div>
                    </div>
                {/foreach}
              </div>
            </div>
        {/if}
      </div>
    </div>

    {if $smarty.request.subaction == 'product_request' || $shopsettings->AnfrageForm == 1}

    {script file="$jspath/jvalidate.js" position='head'}
    <script>
    <!-- //
    {include file = "$incpath/other/jsvalidate.tpl"}
    $(function () {
        $('#product_request').validate({
            rules: {
                product_request_email: { required: true, email: true },
                product_request_name: { required: true },
                product_request_text: { required: true, minlength: 10 }
            },
            submitHandler: function () {
                document.forms['product_request'].submit();
            }
        });
    });
    //-->
    </script>

        <div class="h3 title">{#Shop_prod_request#}</div>
        <div class="box-content" name="product_request">
          {if isset($msg_send) && $msg_send == 1}
              {#Shop_prod_request_thankyou#}
          {else}
              {if !empty($error)}
                  <div class="box-error">
                    {foreach from=$error item=err}
                        <div>{$err}</div>
                    {/foreach}
                  </div>
              {/if}

              <form name="product_request" id="product_request" method="post" action="{page_link}#product_request">
                <div class="row form-group">
                  <div class="col-md-3 col-sm-12">
                    <label class="col-form-label" for="request-email">{#SendEmail_Email#}</label>
                  </div>
                  <div class="col-md-9 col-sm-12">
                    <input class="form-control" id="request-email" name="product_request_email" value="{$smarty.request.product_request_email|default:$smarty.session.login_email|sanitize}" type="email" />
                  </div>
                </div>

                <div class="row form-group">
                  <div class="col-md-3 col-sm-12">
                    <label class="col-form-label" for="request-name">{#Contact_myName#}</label>
                  </div>
                  <div class="col-md-9 col-sm-12">
                    <input class="form-control" id="request-name" name="product_request_name" value="{$smarty.request.product_request_name|default:$whole_name|sanitize}" type="text" />
                  </div>
                </div>

                <div class="row form-group">
                  <div class="col-md-3 col-sm-12">
                    <label class="col-form-label" for="request-text">{#GlobalMessage#}</label>
                  </div>
                  <div class="col-md-9 col-sm-12">
                    <textarea class="form-control" id="request-text" name="product_request_text"rows="3">{$smarty.request.product_request_text|sanitize}</textarea>
                  </div>
                </div>

                {include file="$incpath/other/captcha.tpl"}

                <div class="text-center">
                  <input class="btn btn-primary btn-block-sm" type="submit" value="{#ButtonSend#}" />
                </div>

                <input type="hidden" name="subaction" value="product_request" />
                <input type="hidden" name="id" value="{$smarty.request.id}" />
                <input type="hidden" name="cid" value="{$smarty.request.cid}" />
                <input type="hidden" name="red" value="{$red}" />
                <input type="hidden" name="sub" value="product_request" />
                <input type="hidden" name="prod_name" value="{$p.Titel|sanitize}" />
              </form>
          {/if}
        </div>
    {/if}

    {if get_active('shop_preisalarm') && ($shopsettings->PreiseGaeste == 1 || $loggedin)}
        <div class="wrapper" id="pricealert">
          <div class="h3 title">{#Shop_priceAlert#}</div>
          {$price_alert}
        </div>
    {/if}

    {if $shop_bewertung == 1}
        <div class="wrapper" id="vote">
          <div class="h3 title">{#Shop_prod_vote_votesall#}</div>
          {if $votes}
              {foreach from=$votes item=v}
                  <div class="box-content">
                    <div class="row">
                      <div class="col-2">{#Date#}:</div>
                      <div class="col">{$v->Datum|date_format:$lang_settings.Zeitformat}</div>
                    </div>

                    <div class="row">
                      <div class="col-2">{#GlobalAutor#}:</div>
                      <div class="col">{$v->Benutzer}</div>
                    </div>

                    <div class="row">
                      <div class="col-2">{#Shop_prod_vote_auttext#}</div>
                      <div class="col">{$v->Bewertung}</div>
                    </div>

                    <div class="row">
                      <div class="col-2">{#Shop_prod_vote_points#}</div>
                      <div class="col">
                        <div class="rating-star" data-rating="{$v->Bewertung_Punkte}"></div>
                      </div>
                    </div>
                  </div>
              {/foreach}
          {else}
              <div class="box-content">
                {#Shop_prod_vote_novotes#}
              </div>
          {/if}

          <div class="box-content">
            {if !permission('shop_vote')}
                {#Shop_prod_vote_login#}
            {else}

{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function () {
    $('#prod_vote_form').validate({
        rules: {
            prod_vote_text: { required: true, minlength: 10 }
        },
        submitHandler: function () {
            document.forms['prod_vote_form'].submit();
        }
    });
});
//-->
</script>

                {assign var=secure_uniqid value="two"}
                <a name="vote_form"></a>
                {if !empty($error{$secure_uniqid})}
                    <div class="box-error">
                      {foreach from=$error{$secure_uniqid} item=err}
                          <div>{$err}</div>
                      {/foreach}
                    </div>
                {/if}
                <form name="prod_vote_form" id="prod_vote_form" method="post" action="{page_link}#vote_form">
                  <input type="hidden" name="id" value="{$smarty.request.id}" />
                  <input type="hidden" name="red" value="{$red}" />
                  <input type="hidden" name="sub" value="prod_vote" />
                  <input type="hidden" name="prod_name" value="{$p.Titel|sanitize}" />

                  <div class="row form-group">
                    <div class="col-md-3 col-sm-12">
                      <label class="col-form-label" for="prod-vote-text">{#GlobalMessage#}</label>
                    </div>
                    <div class="col-md-9 col-sm-12">
                      <textarea class="form-control" id="prod-vote-text" name="prod_vote_text"rows="3">{$smarty.request.prod_vote_text|sanitize}</textarea>
                    </div>
                  </div>

                  <div class="row form-group">
                    <div class="col-md-3 col-sm-12">{#Shop_prod_vote_points#}</div>
                    <div class="col-md-9 col-sm-12">
                      <div class="rating-star" data-rating="4" data-rating-active="true" data-rating-input="prod_vote_points"></div>
                    </div>
                  </div>

                  {include file="$incpath/other/captcha.tpl"}

                  <div class="text-center">
                    <input class="btn btn-primary btn-block-sm" type="submit" value="{#RateThis#}" />
                  </div>
                </form>
            {/if}
          </div>
        </div>
    {/if}
{/if}

<div class="wrapper">
  <div class="h3 title">{#Shop_detailLastSeen#}</div>
  {$small_seen_products}
</div>

{if $shopsettings->vat_info_product == 1}
    {include file="$incpath/shop/vat_info.tpl"}
{/if}
