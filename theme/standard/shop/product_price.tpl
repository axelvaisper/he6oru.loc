{if $shopsettings->PreiseGaeste == 1 || $loggedin}
    {if $p.Preis_Liste > 0}
        {#Shop_instead#}
        <span class="shop-price-old">
          <span id="price_list">{$p.Preis_Liste|numformat}</span> {$currency_symbol}
        </span>
        <span id="you_saved" style="display: none"></span>
        <div class="small">
          {#Shop_usave#} {$p.diff|numformat} {$currency_symbol} ({$p.diffpro|numformat}%)
        </div>
    {/if}

    <div class="h5">
      {if $shopsettings->PreiseGaeste == 1 || $loggedin}
          {if $p.Preis > 0}
              <span id="new_price" class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
          {else}
              <span class="shop-price">{#Zvonite#}</span>
          {/if}
      {else}
          {#Shop_prices_justforUsers#}
      {/if}
    </div>

    {if $p.Preis > 0}
        {include file="$incpath/shop/tax_inf_small.tpl"}
    {/if}

    {if $price_onlynetto != 1}
        {include file="$incpath/shop/price_detail_netto.tpl"}
    {elseif $price_onlynetto == 1 && !empty($p.price_ust_ex)}
        {include file="$incpath/shop/price_detail.tpl"}
    {else}
        {include file="$incpath/shop/price_detail_novat.tpl"}
    {/if}

    {include file="$incpath/shop/product_volumes.tpl"}
{else}
    {#Shop_prices_justforUsers#}
{/if}
