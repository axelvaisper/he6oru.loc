{if !empty($price_group)}
    <div class="spacer">
      <div class="small font-weight-bold">
        {#Shop_Price_Group#}
      </div>
      {foreach from=$price_group item=pg}
          <div class="small">
            {$pg->Name}: {$pg->Rabatt}% {#Shop_dicount_d#}, {#Shop_f_price_s#} - {$pg->price|numformat} {$currency_symbol}
          </div>
      {/foreach}
    </div>
{/if}

{if $st_prices}
    <div class="spacer">
      <div class="small font-weight-bold">
        {#Shop_volume_discounts_d#}
      </div>
      {foreach from=$st_prices item=st}
          <div class="small">
            {if $smarty.foreach.sta.last}
                {#Shop_discount_endf#} {$st->Von}
            {else}
                {$st->Von} - {$st->Bis}
            {/if}
            {#Shop_pieces#}: {$st->Wert|numf} {if $st->Operand == 'pro'}%{else}{$currency_symbol}{/if} {#Shop_dicount_d#}
          </div>
      {/foreach}
    </div>
{/if}
