<script>
<!-- //
$(function() {
    var options = { target: '#ajaxbasket', timeout: 3000 };
    $('.ajax_products').submit(function() {
        var id = '#mylist_' + $(this).attr('id');
        if ($(id).val() == 1) {
            showModal($('#favorite-products'), 3000);
        } else {
            showModal($('#basket-products'), 10000);
        }
        $(this).ajaxSubmit(options);
        $(id).val(0);
        return false;
    });
});
//-->
</script>

{include file="$incpath/shop/notification.tpl" dialog='products'}

{if $shopsettings->TopNewOffersPos == 'top'}
    {include file="$incpath/shop/categ_tabs.tpl"}
{/if}

{if $smarty.request.s == 1}
    {include file="$incpath/shop/search_extended.tpl"}
{/if}

{if $cat_desc}
    {$cat_desc}
{/if}

<div class="h3 title">{#Shop_productOverview#}</div>
{include file="$incpath/shop/products_headernavi.tpl" position="top"}
{if !$products}
    <div class="box-content">
      {if !empty($smarty.request.shop_q)}
          {#Shop_searchNull#}
      {else}
          {#Shop_noProducts#}
      {/if}
    </div>
{else}
    <div class="box-content">
      <div class="row no-gutters">
        {assign var=pcc value=0}
        {foreach from=$products item=p name=pro}
            <div class="col-md-6 spacer" id="prod_anchor_{$p.Id}">
              <form method="post" name="products_{$p.Id}" id="ajax_{$p.Id}" class="ajax_products" action="{if empty($p.Vars)}index.php?p=shop&amp;area={$area}{else}index.php?p=shop&amp;area={$area}&amp;action=showproduct&amp;id={$p.Id}{/if}">

                <div class="mx-1 box-block">
                  <div class="">
                    <div class="wrapper text-center" data-toggle="tooltip" title="{$p.Beschreibung|tooltip:500}">
                      <h4 class="text-truncate">
                        {if $shopsettings->popup_product == 1}
                            <a class="colorbox" href="{$p.ProdLink}&amp;blanc=1">{$p.Titel|sanitize}</a>
                        {else}
                            <a href="{$p.ProdLink}">{$p.Titel|sanitize}</a>
                        {/if}
                      </h4>

                      {if $shopsettings->popup_product == 1}
                          <a class="colorbox" href="{$p.ProdLink}&amp;blanc=1">
                            <img class="img-fluid height-6" src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" />
                          </a>
                      {else}
                          <a href="{$p.ProdLink}">
                            <img class="img-fluid height-6" src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" />
                          </a>
                      {/if}
                    </div>

                    <div class="d-flex flex-column">
                      {if $p.Lieferzeit && $p.Lagerbestand>0 && $shopsettings->Zeige_Lieferzeit == 1}
                          <div>
                            {#Shop_shipping_timeinf#} {$p.Lieferzeit|sanitize}
                          </div>
                      {/if}

                      {if $p.Lieferzeit && $shopsettings->Zeige_Verfuegbarkeit == 1}
                          <div>
                            {#Shop_Availablility#}: {$p.VIcon}
                          </div>
                      {/if}

                      {if $p.Fsk18 == 1}
                          <img class="img-fluid" src="{$imgpath_page}special.png" align="right" alt="{#Shop_isFSKWarning#}" />
                      {/if}

                      {if $shopsettings->Zeige_Text == 1 && !empty($p.Beschreibung)}
                          <div class="small height-4">
                            {$p.Beschreibung|striptags|truncate:$shopsettings->Prodtext_Laenge|sanitize}
                          </div>
                      {/if}

                      {if $shopsettings->popup_product == 1}
                          <a class="colorbox" href="{$p.ProdLink}&amp;blanc=1">{#Arrow#}{#MoreDetails#}</a>
                      {else}
                          <a href="{$p.ProdLink}">
                            {#Arrow#}{#MoreDetails#}
                          </a>
                      {/if}

                      {if get_active('shop_merge')}
                          <a class="colorbox" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1&amp;prodid={$p.Id}&amp;cid={$p.Kategorie}">
                            {#Arrow#}{#Merge#}
                          </a>
                      {/if}
                    </div>

                    <div class="text-center">
                      {if $shopsettings->PreiseGaeste == 1 || !$loggedin}
                          <div class="small">
                            {if $p.Preis_Liste != $p.Preis}
                                {#Shop_instead#}
                                <span class="shop-price-old">{$p.Preis_Liste|numformat} {$currency_symbol}</span>
                            {/if}
                          </div>
                          {if !empty($p.Vars)}
                              {#Shop_priceFrom#}
                          {/if}
                          {if $p.Preis > 0}
                              <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                              <div class="small">
                                {include file="$incpath/shop/tax_inf_small.tpl"}
                              </div>
                          {else}
                              <span class="shop-price">{#Zvonite#}</span>
                          {/if}
                      {else}
                          {#Shop_prices_justforUsers#}
                      {/if}
                    </div>
                  </div>

                  <div class="text-center">
                    {if $p.Fsk18 == 1 && $fsk_user != 1}
                        {if $shopsettings->popup_product == 1}
                            <button class="btn btn-secondary btn-block-sm spacer" type="button" onclick="newWindow('{$p.ProdLink}&amp;blanc=1', '90%', '97%');">{#buttonDetails#}</button>
                        {else}
                            <button class="btn btn-secondary btn-block-sm spacer" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
                        {/if}
                    {else}
                        {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                            <input type="hidden" name="action" value="to_cart" />
                            <input type="hidden" name="redir" value="{page_link}#prod_anchor_{$p.Id}" />
                            <input type="hidden" name="product_id" value="{$p.Id}" />
                            <input type="hidden" name="mylist" id="mylist_ajax_{$p.Id}" value="0" />
                            <input type="hidden" name="ajax" value="1" />
                            <noscript>
                            <input type="hidden" name="ajax" value="0" />
                            </noscript>
                            <button class="btn btn-primary btn-block-sm spacer" type="submit">{#Shop_toBasket#}</button>
                            <button class="btn btn-secondary btn-block-sm spacer" onclick="document.getElementById('mylist_ajax_{$p.Id}').value = '1';" type="submit">{#Shop_WishList#}</button>
                        {else}
                            <input type="hidden" name="parent" value="{$p.Parent}" />
                            <input type="hidden" name="navop" value="{$p.Navop}" />
                            <input type="hidden" name="cid" value="{$p.Kategorie}" />
                            {if $shopsettings->popup_product == 1}
                                <button class="btn btn-secondary btn-block-sm spacer" type="button" onclick="newWindow('{$p.ProdLink}&amp;blanc=1', '90%', '97%');">{#buttonDetails#}</button>
                            {else}
                                <button class="btn btn-secondary btn-block-sm spacer" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
                            {/if}
                        {/if}
                    {/if}
                  </div>
                </div>

              </form>
            </div>
            {assign var=pcc value=$pcc+1}
            {if $pcc % 2 == 0 && !$smarty.foreach.pro.last}
            </div>
            <div class="row no-gutters">
            {/if}
        {/foreach}
      </div>
    </div>
    {include file="$incpath/shop/products_headernavi.tpl" position="bottom"}

    {if $shopsettings->TopNewOffersPos == 'bottom'}
        {include file="$incpath/shop/categ_tabs.tpl"}
    {/if}
{/if}

{if $smarty.request.s != '1'}
    {include file="$incpath/shop/products_navi_bottom.tpl"}
{/if}

{if $shopsettings->seen_cat == 1}
    {$small_seen_products}
{/if}

{if $shopsettings->vat_info_cat == 1}
    {include file="$incpath/shop/vat_info.tpl"}
{/if}
