{script file="$jspath/jsuggest.js" position='head'}
<script>
<!-- //
$(function () {
    $('#qs').suggest('{$baseurl}/lib/ajax.php?action=shop&key=' + Math.random(), {
        onSelect: function () {
            document.forms['shopssform'].submit();
        }
    });
});
//-->
</script>

<div class="wrapper panel">
  <div class="panel-head">{#Search#}</div>
  <div class="panel-main">
    <form method="post" id="shopssform" name="shopssform" action="{$shop_search_small_action}">
      <input type="hidden" name="cid" value="0" />
      <input type="hidden" name="man" value="0" />
      <div class="input-group form-group">
        <input class="form-control" name="shop_q" id="qs" type="text" value="{if  isset($smarty.request.shop_q) && $smarty.request.shop_q != 'empty'}{$smarty.request.shop_q|escape:html}{/if}" />
        <span class="input-group-append">
          <input class="btn btn-primary" type="submit" value="{#Search#}" />
        </span>
      </div>
      <input type="hidden" name="p" value="shop" />
      <input type="hidden" name="action" value="showproducts" />
    </form>

    <a class="d-block" href="{$baseurl}/index.php?exts=1&amp;s=1&amp;area={$smarty.session.area}&amp;p=shop&amp;action=showproducts">{#ExtendedSearch#}</a>
    <a class="d-block" href="{$baseurl}/index.php?p=shop&amp;action=mylist">{#Shop_mylist#}</a>
    {if $loggedin}
        <a class="d-block colorbox" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1">{#Shop_mergeListsMy#}</a>
    {/if}
  </div>
</div>
