<div class="h1 title">{#Shop_detailLastSeen#}</div>

{if $send == 1}
    <div class="box-content">
      <div class="wrapper">
        {#Shop_tellOk#}
      </div>
    </div>
{/if}
{if $nocheckbox == 1}
    <div class="box-content">
      <div class="box-error">
        {#Shop_noSelection#}
      </div>
    </div>
{/if}

<div class="box-content">
  {if $is_seen_products != 1}
      {#Shop_noSeenProducts#}
  {else}

<script>
<!-- //
function show_tell_form() {
    if (document.getElementById('seen_tell').selected == true) {
        document.getElementById('seen_tell_form').style.display = '';
    } else {
        document.getElementById('seen_tell_form').style.display = 'none';
    }
}
function check_shop_tellform() {
    if (document.getElementById('seen_tell').selected == true) {
        if (document.getElementById('pte').value == '' || document.getElementById('pte').value.indexOf('@') == -1) {
            alert('{#Shop_tellNoRE#}');
            document.getElementById('pte').focus();
            return false;
        }
        if (document.getElementById('ptn').value == '') {
            alert('{#Shop_tellNoNA#}');
            document.getElementById('ptn').focus();
            return false;
        }
        if (document.getElementById('ptr').value == '' || document.getElementById('ptr').value.indexOf('@') == -1) {
            alert('{#Comment_NoEmail#}');
            document.getElementById('ptr').focus();
            return false;
        }
        if (document.getElementById('ptrn').value == '') {
            alert('{#Comment_NoAuthor#}');
            document.getElementById('ptrn').focus();
            return false;
        }
    }
}
//-->
</script>

      <form method="post" action="index.php?p=shop&amp;area={$area}&amp;action=showseenproducts" onsubmit="return check_shop_tellform();">
        {foreach from=$seen_products_array item=p name=pro}
            <div class="row wrapper">

              <div class="col-auto">
                <input name="prodid[{$p.Id}]" type="checkbox" value="{$p.Kategorie}" />
              </div>

              <div class="col-auto" data-toggle="tooltip" title="{$p.Beschreibung|tooltip:500}">
                <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                  <img class="img-fluid" src="{$p.Bild_Klein}" alt="{$p.Titel|sanitize}" />
                </a>
              </div>

              <div class="col">
                <a title="{$p.Titel|sanitize}" href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                  {$p.Titel|sanitize}
                </a>
                <div class="small">{$p.Kategorie_Name|sanitize}</div>
                {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                    {if $p.Preis > 0}
                        <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                        {if $no_nettodisplay != 1}
                            {if $price_onlynetto != 1}
                                <span class="small">({#Shop_icludes#} {$p.product_ust}% {#Shop_vat#})</span>
                            {/if}
                            {if $price_onlynetto == 1 && !empty($p.price_ust_ex)}
                                <span class="small">({#Shop_exclVat#} {$p.product_ust}% {#Shop_vat#})</span>
                            {/if}
                        {/if}
                    {else}
                        <span class="shop-price">{#Zvonite#}</span>
                    {/if}
                {else}
                    {#Shop_prices_justforUsers#}
                {/if}
              </div>

            </div>
        {/foreach}

        <div class="row form-group">
          <div class="col-md-3 col-sm-12">
            <label class="col-form-label" for="seen-select">{#Shop_selectedProducts#}</label>
          </div>
          <div class="col-md-9 col-sm-12">
            <select class="form-control" id="seen-select" name="subaction" onchange="show_tell_form();">
              <option value="del">{#Delete#}</option>
              <option value="merge">{#AddMerge#}</option>
              <option id="seen_tell" value="sendfriend">{#Shop_tellFriend#}</option>
            </select>
          </div>
        </div>

        <div id="seen_tell_form" style="display:none">
          <div class="row form-group">
            <div class="col-md-3 col-sm-12">
              <label class="col-form-label" for="pte">{#Shop_tellReciever#}</label>
            </div>
            <div class="col-md-9 col-sm-12">
              <input class="form-control" id="pte" name="prod_tell_email" type="email" value="{$smarty.request.prod_tell_email|escape:html}" />
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-3 col-sm-12">
              <label class="col-form-label" for="ptn">{#Shop_tellRecieverName#}</label>
            </div>
            <div class="col-md-9 col-sm-12">
              <input class="form-control" id="ptn" name="prod_tell_name" type="text" value="{$smarty.request.prod_tell_name|escape:html}" />
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-3 col-sm-12">
              <label class="col-form-label" for="ptr">{#SendEmail_Email#}</label>
            </div>
            <div class="col-md-9 col-sm-12">
              <input class="form-control" id="ptr" name="prod_tell_remail" type="email" value="{$smarty.request.prod_tell_remail|escape:html|default:$smarty.session.login_email}" />
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-3 col-sm-12">
              <label class="col-form-label" for="ptrn">{#Contact_myName#}</label>
            </div>
            <div class="col-md-9 col-sm-12">
              <input class="form-control" id="ptrn" name="prod_tell_rname" type="text" value="{$smarty.request.prod_tell_rname|escape:html|default:$smarty.session.user_name}" />
            </div>
          </div>
        </div>

        <div class="text-center">
          <input class="btn btn-primary btn-block-sm" type="submit" value="{#ButtonSend#}" />
        </div>
      </form>
  {/if}
</div>
