<div class="wrapper">
  <form method="post" action="index.php">
    <input type="hidden" name="p" value="shop" />
    <input type="hidden" name="area" value="{$area}" />
    <input type="hidden" name="action" value="shoporder" />
    <input type="hidden" name="subaction" value="final" />
    {if !$loggedin}
        <input type="hidden" name="order" value="guest" />
    {/if}

    <div class="box-content">
      <div class="form-check">
        <input class="form-check-input" id="ok-agb" name="agb_ok[]" type="checkbox" value="1" {if $agb_accept_checked == 1}checked="checked"{/if} />
        <label class="form-check-label{if $agb_error == 1} text-danger{/if}" for="ok-agb">{$INF_MSG}</label>
      </div>
    </div>

    {if $GefundenDurch}
        <div class="box-content">
          <div class="row">
            <div class="col-md-5 col-sm-12">
              <label class="col-form-label" for="form-refer">{#Shop_referedby#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              <select class="form-control" id="form-refer" name="GefundenDurch">
                {foreach from=$GefundenDurch item=gd}
                    <option value="{$gd|sanitize}">{$gd|sanitize}</option>
                {/foreach}
              </select>
            </div>
          </div>
        </div>
    {/if}

    <div class="text-center">
      <input class="btn btn-primary" type="submit" value="{#Shop_sendorder_button#}" />
    </div>
  </form>
</div>
