<div class="shop-sidebar-menu">
  <div class="wrapper panel">
    <div class="panel-head">{#Products#}</div>
    <div class="panel-main">

      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link{if isset($smarty.request.start) && $smarty.request.start == 1} active{/if}" href="index.php?p=shop&amp;start=1">
            <img src="{$imgpath}/shop/navi_img.png" alt="" /> {#Shop_starterPage#}
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link{if $smarty.request.t == 'liste'} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid=0&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t=liste">
            <img src="{$imgpath}/shop/navi_img.png" alt="" /> {#Shop_starterShowAll#}
          </a>
        </li>

        {assign var=cid value=$smarty.request.cid}
        {foreach from=$MyShopNavi item=sn}
            <li class="nav-item">
              <a class="nav-link{if $cid == $sn->Id || in_array($sn->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
                {$sn->Icon} {$sn->Entry|sanitize}
              </a>

              {if !empty($sn->Sub1) && count($sn->Sub1)}
                  <div class="collapse{if $cid == $sn->Id || in_array($sn->Id, $navi_current)} show{/if}">
                    <ul class="nav flex-column">
                      {foreach from=$sn->Sub1 item=sub1}
                          <li class="nav-item ml-3">
                            <a class="nav-link ml-3{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                              {$sub1->Entry|sanitize}
                            </a>

                            {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                                <div class="collapse{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} show{/if}">
                                  <ul class="nav flex-column">
                                    {foreach from=$sub1->Sub2 item=sub2}
                                        <li class="nav-item ml-3">
                                          <a class="nav-link ml-3{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                                            {$sub2->Name|sanitize}
                                          </a>

                                          {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                                              <div class="collapse{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} show{/if}">
                                                <ul class="nav flex-column">
                                                  {foreach from=$sub2->Sub3 item=sub3}
                                                      <li class="nav-item ml-3">
                                                        <a class="nav-link ml-3{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                                          {$sub3->Name|sanitize}
                                                        </a>

                                                        {if !empty($sub3->Sub4) && count($sub3->Sub4)}
                                                            <div class="collapse{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} show{/if}">
                                                              <ul class="nav flex-column">
                                                                {foreach from=$sub3->Sub4 item=sub4}
                                                                    <li class="nav-item ml-3">
                                                                      <a class="nav-link ml-3{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                                                        {$sub4->Name|sanitize}
                                                                      </a>
                                                                    </li>
                                                                {/foreach}
                                                              </ul>
                                                            </div>
                                                        {/if}

                                                      </li>
                                                  {/foreach}
                                                </ul>
                                              </div>
                                          {/if}

                                        </li>
                                    {/foreach}
                                  </ul>
                                </div>
                            {/if}

                          </li>
                      {/foreach}
                    </ul>
                  </div>
              {/if}

            </li>
        {/foreach}
      </ul>

    </div>
  </div>
</div>
