{if $small_topseller_array}
    <div class="wrapper panel">
      <div class="panel-head">{#Shop_Topseller#}</div>
      <div class="panel-main">
        {foreach from=$small_topseller_array item=p name=pro}
            <div class="row no-gutters wrapper" data-toggle="tooltip" title="{$p.Beschreibung|tooltip:500}">
              <div class="col-auto">
                <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                  <img class="img-fluid mr-2" src="{$p.Bild_Klein}" alt="" />
                </a>
              </div>
              <div class="col">
                <h5>
                  <a class="small" href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">{$p.Titel|sanitize}</a>
                </h5>
                {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                    {if $p.Preis > 0}
                        <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                    {else}
                        <span class="shop-price">{#Zvonite#}</span>
                    {/if}
                {else}
                    {#Shop_prices_justforUsers#}
                {/if}
              </div>
            </div>
        {/foreach}

        <a href="index.php?p=shop&amp;action=showproducts&amp;page=1&amp;topseller=1{if !empty($smarty.request.cid)}&amp;cid={$smarty.request.cid}{/if}">
          {if !empty($smarty.request.cid)}
              {#Shop_topsellerAllthisCateg#}
          {else}
              {#Shop_topsellerAll#}
          {/if}
        </a>
      </div>
    </div>
{/if}
