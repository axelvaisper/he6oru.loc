{if $Zub_a_products_array}
<script>
<!-- //
$(function() {
    var options = { target: '#ajaxbasket', timeout: 3000 };
    $('.zuba_products').submit(function() {
        var id = '#mylist_' + $(this).attr('id');
        if ($(id).val() == 1) {
            showModal($('#favorite-zuba'), 3000);
        } else {
            showModal($('#basket-zuba'), 10000);
        }
        $(this).ajaxSubmit(options);
        $(id).val(0);
        return false;
    });
});
//-->
</script>

{include file="$incpath/shop/notification.tpl" dialog='zuba'}

<div class="box-content">
  {foreach from=$Zub_a_products_array item=p}
      <div class="row wrapper">

        <div class="col-1" data-toggle="tooltip" title="{$p.Beschreibung|tooltip:500}">
          <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
            <img class="img-fluid" src="{$p.Bild_Klein}" alt="" />
          </a>
        </div>

        <div class="col" data-toggle="tooltip" title="{$p.Beschreibung|tooltip:500}">
          <h5 class="text-truncate">
            <a  href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">{$p.Titel|sanitize}</a>
          </h5>
          {if $shopsettings->PreiseGaeste == 1 || $loggedin}
              {if $p.Preis > 0}
                  <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
              {else}
                  <span class="shop-price">{#Zvonite#}</span>
              {/if}
              {if $p.Fsk18 == 1 && $fsk_user != 1}
                  <div class="small">{#Shop_isFSKWarning#}</div>
              {/if}
          {else}
              {#Shop_prices_justforUsers#}
          {/if}
        </div>

        <div class="col-auto">

          {if $shopsettings->PreiseGaeste == 1 || $loggedin}
              {if $p.Fsk18 == 1 && $fsk_user != 1}
                  <form method="post" action="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                    <button class="btn btn-secondary btn-sm" type="submit">{#buttonDetails#}</button>
                  </form>
              {else}
                  {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                      <form method="post" class="zuba_products" id="zuba_{$p.Id}" action="{if empty($p.Vars)}index.php?p=shop{else}index.php?p=shop&amp;action=showproduct&amp;id={$p.Id}{/if}">
                        <input type="hidden" name="amount"  value="{if $p.MinBestellung == 0}1{else}{$p.MinBestellung}{/if}" maxlength="2" />
                        <input type="hidden" name="action" value="to_cart" />
                        <input type="hidden" name="redir" value="{page_link}#prod_anchor_{$p.Id}" />
                        <input type="hidden" name="product_id" value="{$p.Id}" />
                        <input type="hidden" name="mylist" id="mylist_zuba_{$p.Id}" value="0" />
                        <input type="hidden" name="ajax" value="1" />
                        <noscript>
                        <input type="hidden" name="ajax" value="0" />
                        </noscript>
                        <button class="btn btn-primary btn-sm" type="submit">{#Shop_toBasket#}</button>
                        <button class="btn btn-secondary btn-sm" onclick="document.getElementById('mylist_zuba_{$p.Id}').value = '1';" type="submit">{#Shop_WishList#}</button>
                      </form>
                  {else}
                      <form method="post" action="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                        <button class="btn btn-primary btn-sm" type="submit">{#buttonDetails#}</button>
                      </form>
                  {/if}
              {/if}
          {else}
              {#Shop_prices_justforUsers#}
          {/if}

        </div>
      </div>
  {/foreach}
</div>
{/if}
