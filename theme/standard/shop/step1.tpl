{include file="$incpath/shop/steps.tpl"}

<div class="wrapper">
  <form method="post" action="index.php">
    <div class="h3 title text-truncate">{#TypeClient#}</div>
    <div class="box-content">

      <div class="form-check form-group">
        <input class="form-check-input" id="type_1" name="type_client" value="1" {if !$smarty.session.ship_ok || $smarty.session.type_client == 1}checked="checked"{/if} type="radio" />
        <label class="form-check-label" for="type_1">{#TypeClientCl#}</label>
      </div>

      <div class="form-check form-group">
        <input class="form-check-input" id="type_2" name="type_client" value="2" {if $smarty.session.type_client == 2}checked="checked"{/if} type="radio" />
        <label class="form-check-label" for="type_2">{#Profile_company#}</label>
      </div>

      <div class="form-check form-group">
        <input class="form-check-input" id="type_3" name="type_client" value="3" {if $smarty.session.type_client == 3}checked="checked"{/if} type="radio" />
        <label class="form-check-label" for="type_3">{#TypeClientIp#}</label>
      </div>

    </div>

    <div class="h3 title text-truncate">{#ShipOk#}</div>
    <div class="box-content">

      <div class="form-check form-group">
        <input class="form-check-input" id="ship_1" name="ship_ok" value="1" {if !$smarty.session.ship_ok || $smarty.session.ship_ok == 1}checked="checked"{/if} type="radio" />
        <label class="form-check-label" for="ship_1">{#Yes#}</label>
      </div>

      <div class="form-check form-group">
        <input class="form-check-input" id="ship_2" name="ship_ok" value="2" {if $smarty.session.ship_ok == 2}checked="checked"{/if} type="radio" />
        <label class="form-check-label" for="ship_2">{#No#}</label>
      </div>

    </div>

    <div class="text-center">
      <input class="btn btn-primary btn-block-sm" type="submit" value="{#Shop_nextStep#}" />
    </div>

    <input type="hidden" name="p" value="shop" />
    <input type="hidden" name="area" value="{$area}" />
    <input type="hidden" name="action" value="shoporder" />
    <input type="hidden" name="subaction" value="step2" />
  </form>
</div>
