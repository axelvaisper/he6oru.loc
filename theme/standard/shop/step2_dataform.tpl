{if $status_error}
    <div class="h3 title">{#Error#}</div>
    <div class="box-content">
      <div class="box-error">
        {if $status_error == 'to_much'}
            {#Shop_basket_summ_tohigh#} {$best_max|numformat} {$currency_symbol}.
        {else}
            {#Shop_basket_summ_tolow#} {$best_min|numformat} {$currency_symbol}.
        {/if}
      </div>
    </div>
{else}
    <div class="wrapper">
      {if $shop_coupons == 1 && $smarty.session.price > 0}
          <div class="h3 title">{#Shop_o_coupon#}</div>
          <div class="box-content" id="ajaxcpresult">
            {if $smarty.session.coupon_code}
                {include file="$incpath/shop/coupon_del.tpl"}
            {else}
                {include file="$incpath/shop/coupon_insert.tpl"}
            {/if}
          </div>
      {/if}

      {if $r_errors}
          <div class="h3 title">{#Error#}</div>
          <div class="box-content">
            <div class="box-error">
              {foreach from=$r_errors item=error}
                  <div>{$error}</div>
              {/foreach}
            </div>
          </div>
      {/if}

      <form method="post" action="index.php">

        <div class="h3 title">{#Shop_f_billingadress#}</div>
        <div class="box-content">
          {if $smarty.session.ship_ok == 1 || $settings.Reg_DataPflicht == 1}
              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_DataPflichtFill == 1} required{/if}" for="l_r_nachname">{#LastName#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_nachname" type="text" id="l_r_nachname" value="{if empty($smarty.post.r_nachname)}{$smarty.session.benutzer_nachname|default:$smarty.session.r_nachname}{else}{$smarty.post.r_nachname|escape:html|default:$smarty.session.r_nachname}{/if}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_DataPflichtFill == 1} required{/if}" for="l_r_vorname">{#GlobalName#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_vorname" type="text" id="l_r_vorname" value="{if empty($smarty.post.r_vorname)}{$smarty.session.benutzer_vorname|default:$smarty.session.r_vorname}{else}{$smarty.post.r_vorname|escape:html|default:$smarty.session.r_vorname}{/if}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label required" for="l_r_middlename">{#Profile_MiddleName#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_middlename" type="text" id="l_r_middlename" value="{if empty($smarty.post.r_middlename)}{$smarty.session.benutzer_middlename|default:$smarty.session.r_middlename}{else}{$smarty.post.r_middlename|escape:html|default:$smarty.session.r_middlename}{/if}" />
                </div>
              </div>
          {/if}
          <div class="row form-group">
            <div class="col-md-4 col-sm-12">
              <label class="col-form-label required" for="r_email">{#Email#}</label>
            </div>
            <div class="col-md-8 col-sm-12">
              <input class="form-control" name="r_email" type="email" id="l_r_email" value="{$smarty.post.r_email|escape:html|default:$smarty.session.r_email}" />
            </div>
          </div>
          {if $settings.Reg_Fon == 1 || $shopsettings->Telefon_Pflicht}
              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label{if $shopsettings->Telefon_Pflicht} required{/if}" for="l_r_telefon">{#Phone#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_telefon" type="text" id="l_r_telefon" value="{if empty($smarty.post.r_telefon)}{$smarty.session.benutzer_fon|default:$smarty.session.r_telefon}{else}{$smarty.post.r_telefon|escape:html|default:$smarty.session.r_telefon}{/if}" />
                </div>
              </div>
          {/if}
          {if $settings.Reg_Fax == 1}
              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label" for="l_r_fax">{#Fax#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_fax" type="text" id="l_r_fax" value="{if empty($smarty.post.r_fax)}{$smarty.session.benutzer_fax|default:$smarty.session.r_fax}{else}{$smarty.post.r_fax|escape:html|default:$smarty.session.r_fax}{/if}" />
                </div>
              </div>
          {/if}
          <div class="row form-group">
            <div class="col-md-4 col-sm-12">
              <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_AddressFill == 1} required{/if}" for="l_r_land">{#Country#}</label>
            </div>
            <div class="col-md-8 col-sm-12">
              <select class="form-control" name="r_land" id="l_r_land">
                {foreach from=$countries item=c}
                    <option value="{$c.Code}" {if $smarty.request.save == 1}{if $smarty.request.r_land == $c.Code}selected="selected"{/if}{else}{if $smarty.session.r_land == $c.Code}selected="selected"{else}{if $smarty.request.save != '1' && $shop_country == $c.Code && !$smarty.session.r_land}selected="selected"{elseif $smarty.request.r_land == $c.Code}selected="selected"{/if}{/if}{/if}>
                      {$c.Name}
                    </option>
                {/foreach}
              </select>
            </div>
          </div>
          {if $smarty.session.ship_ok == 1 || $settings.Reg_Address == 1}
              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_AddressFill == 1} required{/if}" for="l_r_plz">{#Profile_Zip#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_plz" type="text" id="l_r_plz" value="{if empty($smarty.post.r_plz)}{$smarty.session.benutzer_plz|default:$smarty.session.r_plz}{else}{$smarty.post.r_plz|escape:html|default:$smarty.session.r_plz}{/if}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_AddressFill == 1} required{/if}" for="l_r_ort">{#Town#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_ort" type="text" id="l_r_ort" value="{if empty($smarty.post.r_ort)}{$smarty.session.benutzer_ort|default:$smarty.session.r_ort}{else}{$smarty.post.r_ort|escape:html|default:$smarty.session.r_ort}{/if}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_AddressFill == 1} required{/if}" for="l_r_strasse">{#Profile_Street#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_strasse" type="text" id="l_r_strasse" value="{if empty($smarty.post.r_strasse)}{$smarty.session.benutzer_strasse|default:$smarty.session.r_strasse}{else}{$smarty.post.r_strasse|escape:html|default:$smarty.session.r_strasse}{/if}" />
                </div>
              </div>
          {/if}
          {if $settings.Reg_Firma == 1 && $smarty.session.type_client != 1}
              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label" for="l_r_firma">{#Profile_company#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_firma" type="text" id="l_r_firma" value="{if empty($smarty.post.r_firma)}{$smarty.session.benutzer_firma|default:$smarty.session.r_firma}{else}{$smarty.post.r_firma|escape:html|default:$smarty.session.r_firma}{/if}" />
                </div>
              </div>
          {/if}
          {if $settings.Reg_Ust == 1 && $smarty.session.type_client != 1}
              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label" for="l_r_ustid">{#Profile_vatnum#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <input class="form-control" name="r_ustid" type="text" id="l_r_ustid" value="{if empty($smarty.post.r_ustid)}{$smarty.session.benutzer_ustid|default:$smarty.session.r_ustid}{else}{$smarty.post.r_ustid|escape:html|default:$smarty.session.r_ustid}{/if}" />
                </div>
              </div>
          {/if}
          {if $settings.Reg_Bank == 1 && $smarty.session.type_client != 1}
              <div class="row form-group">
                <div class="col-md-4 col-sm-12">
                  <label class="col-form-label" for="l_r_bankname">{#Profile_vatnum#}</label>
                </div>
                <div class="col-md-8 col-sm-12">
                  <textarea class="form-control" name="r_bankname" rows="3" id="l_r_bankname">{$smarty.post.r_bankname|escape:html|default:$smarty.session.r_bankname}</textarea>
                </div>
              </div>
          {/if}
          <div class="row form-group">
            <div class="col-md-4 col-sm-12">
              <label class="col-form-label" for="l_r_nachricht">{#Shop_f_ordermessage#}</label>
            </div>
            <div class="col-md-8 col-sm-12">
              <textarea class="form-control" name="r_nachricht" rows="3" id="l_r_nachricht">{$smarty.post.r_nachricht|escape: html|default:$smarty.session.r_nachricht}</textarea>
            </div>
          </div>
          <div class="spacer">{#Shop_fillout_required#}</div>
        </div>

        {if $smarty.session.ship_ok == 1}
            <div class="h3 title" id="diff_adress">{#Shop_f_same_sa#}</div>
            <div class="box-content">

              <div class="wrapper">
                <div class="form-check form-group">
                  <input class="form-check-input" id="l_r_diff" onclick="document.getElementById('liefer_andere_div').style.display = 'none';" type="radio" name="diff_rl" value="liefer_gleich" {if $smarty.request.diff_rl == 'liefer_gleich' || empty($smarty.request.diff_rl) || $smarty.session.diff_rl == 'liefer_gleich'}checked="checked"{/if} />
                  <label class="form-check-label" for="l_r_diff">{#TypeClientCl#}</label>
                </div>

                <div class="form-check form-group">
                  <input class="form-check-input" id="l_r_diff2" onclick="document.getElementById('liefer_andere_div').style.display = '';" type="radio" name="diff_rl" value="liefer_andere" {if $smarty.request.diff_rl == 'liefer_andere' || $smarty.session.diff_rl == 'liefer_andere'}checked="checked"{/if} />
                  <label class="form-check-label" for="l_r_diff2">{#Shop_f_other_sa#}</label>
                </div>
              </div>

              <div id="liefer_andere_div" {if $smarty.request.diff_rl == 'liefer_andere' || $smarty.session.diff_rl == 'liefer_andere'}{else}style="display: none"{/if}>
                {if $smarty.session.ship_ok == 1 || $settings.Reg_DataPflicht == 1}
                    <div class="row form-group">
                      <div class="col-md-4 col-sm-12">
                        <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_DataPflichtFill == 1} required{/if}" for="l_l_nachname">{#LastName#}</label>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <input class="form-control" name="l_nachname" type="text" id="l_l_nachname" value="{$smarty.post.l_nachname|escape:html|default:$smarty.session.l_nachname}" />
                      </div>
                    </div>

                    <div class="row form-group">
                      <div class="col-md-4 col-sm-12">
                        <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_DataPflichtFill == 1} required{/if}" for="l_l_vorname">{#GlobalName#}</label>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <input class="form-control" name="l_vorname" type="text" id="l_l_vorname" value="{$smarty.post.l_vorname|escape:html|default:$smarty.session.l_vorname}" />
                      </div>
                    </div>

                    <div class="row form-group">
                      <div class="col-md-4 col-sm-12">
                        <label class="col-form-label" for="l_l_middlename">{#Profile_MiddleName#}</label>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <input class="form-control" name="l_middlename" type="text" id="l_l_middlename" value="{$smarty.post.l_middlename|escape:html|default:$smarty.session.l_middlename}" />
                      </div>
                    </div>
                {/if}
                {if $settings.Reg_Fon == 1 || $shopsettings->Telefon_Pflicht}
                    <div class="row form-group">
                      <div class="col-md-4 col-sm-12">
                        <label class="col-form-label{if $shopsettings->Telefon_Pflicht} required{/if}" for="l_l_telefon">{#Phone#}</label>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <input class="form-control" name="l_telefon" type="text" id="l_l_telefon" value="{$smarty.post.l_telefon|escape:html|default:$smarty.session.l_telefon}" />
                      </div>
                    </div>
                {/if}
                {if $settings.Reg_Fax == 1}
                    <div class="row form-group">
                      <div class="col-md-4 col-sm-12">
                        <label class="col-form-label{if $shopsettings->Telefon_Pflicht} required{/if}" for="l_l_fax">{#Fax#}</label>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <input class="form-control" name="l_fax" type="text" id="l_l_fax" value="{$smarty.post.l_fax|escape:html|default:$smarty.session.l_fax}" />
                      </div>
                    </div>
                {/if}
                {if $settings.Reg_Firma == 1 && $smarty.session.type_client != 1}
                    <div class="row form-group">
                      <div class="col-md-4 col-sm-12">
                        <label class="col-form-label" for="l_l_firma">{#Profile_company#}</label>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <input class="form-control" name="l_firma" type="text" id="l_l_firma" value="{$smarty.post.l_firma|escape:html|default:$smarty.session.l_firma}" />
                      </div>
                    </div>
                {/if}
                <div class="row form-group">
                  <div class="col-md-4 col-sm-12">
                    <label class="col-form-label" for="l_l_land">{#Country#}</label>
                  </div>
                  <div class="col-md-8 col-sm-12">
                    <select class="form-control" name="l_land" id="l_l_land">
                      {foreach from=$countries item=c}
                          <option value="{$c.Code}" {if $smarty.session.l_land == $c.Code}selected="selected"{else}{if $smarty.request.save != '1' && $shop_country == $c.Code && !$smarty.session.l_land}selected="selected"{elseif $smarty.request.l_land == $c.Code}selected="selected"{/if}{/if}>
                            {$c.Name}
                          </option>
                      {/foreach}
                    </select>
                  </div>
                </div>

                {if $smarty.session.ship_ok == 1 || $settings.Reg_Address == 1}
                    <div class="row form-group">
                      <div class="col-md-4 col-sm-12">
                        <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_AddressFill == 1} required{/if}" for="l_l_plz">{#Profile_Zip#}</label>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <input class="form-control" name="l_plz" type="text" id="l_l_plz" value="{$smarty.post.l_plz|escape:html|default:$smarty.session.l_plz}" />
                      </div>
                    </div>

                    <div class="row form-group">
                      <div class="col-md-4 col-sm-12">
                        <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_AddressFill == 1} required{/if}" for="l_l_ort">{#Town#}</label>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <input class="form-control" name="l_ort" type="text" id="l_l_ort" value="{$smarty.post.l_ort|escape:html|default:$smarty.session.l_ort}" />
                      </div>
                    </div>


                    <div class="row form-group">
                      <div class="col-md-4 col-sm-12">
                        <label class="col-form-label{if $smarty.session.ship_ok == 1 || $settings.Reg_AddressFill == 1} required{/if}" for="l_l_strasse">{#Profile_Street#}</label>
                      </div>
                      <div class="col-md-8 col-sm-12">
                        <input class="form-control" name="l_strasse" type="text" id="l_l_strasse" value="{$smarty.post.l_strasse|escape:html|default:$smarty.session.l_strasse}" />
                      </div>
                    </div>
                {/if}

                <div class="spacer">{#Shop_fillout_required#}</div>
              </div>
            </div>
        {/if}

        <div class="text-center">
          <input class="btn btn-primary btn-block-sm" type="submit" value="{#Shop_nextStep#}" />
        </div>

        <input type="hidden" name="p" value="shop" />
        <input type="hidden" name="area" value="{$area}" />
        <input type="hidden" name="action" value="shoporder" />
        <input type="hidden" name="subaction" value="step2" />
        {if $guest_order == 1}
            <input type="hidden" name="order" value="guest" />
        {/if}
        <input type="hidden" name="save" value="1" />
      </form>
    </div>
{/if}
