<div class="wrapper">
  <div class="row no-gutters align-items-stretch">

    <div class="col">
      <form method="post" name="step1_f" action="index.php">
        <input type="hidden" name="p" value="shop" />
        <input type="hidden" name="area" value="{$area}" />
        <input type="hidden" name="action" value="shoporder" />
        <input type="hidden" name="subaction" value="step1" />
        {if isset($smarty.request.order) && $smarty.request.order == 'guest'}
            <input type="hidden" name="order" value="guest" />
        {/if}
      </form>
      {if $smarty.session.shopstep>=1}
          <a href="javascript:document.forms['step1_f'].submit();">
          {/if}
          <div class="shop-steps{if $smarty.session.shopstep == 1} active{/if}">
            1. {#Shop_step_1#}
            <div class="small">{#Shop_step_1_inf#}</div>
          </div>
          {if $smarty.session.shopstep>=1}
          </a>
      {/if}
    </div>

    <div class="col">
      <form method="post" name="step2_f" action="index.php">
        <input type="hidden" name="p" value="shop" />
        <input type="hidden" name="area" value="{$area}" />
        <input type="hidden" name="action" value="shoporder" />
        <input type="hidden" name="subaction" value="step2" />
        {if isset($smarty.request.order) && $smarty.request.order == 'guest'}
            <input type="hidden" name="order" value="guest" />
        {/if}
      </form>
      {if $smarty.session.shopstep>=2}
          <a href="javascript:document.forms['step2_f'].submit();">
          {/if}
          <div class="shop-steps{if $smarty.session.shopstep == 2} active{/if}">
            2. {#Shop_step_2#}
            <div class="small">{#Shop_step_2_inf#}</div>
          </div>
          {if $smarty.session.shopstep>=2}
          </a>
      {/if}
    </div>

    <div class="col">
      <form method="post" name="step3_f" action="index.php">
        <input type="hidden" name="p" value="shop" />
        <input type="hidden" name="area" value="{$area}" />
        <input type="hidden" name="action" value="shoporder" />
        <input type="hidden" name="subaction" value="step3" />
        <input type="hidden" name="payment_id" value="{$smarty.request.payment_id}" />
        <input type="hidden" name="versand_id" value="{$smarty.request.versand_id}" />
        {if isset($smarty.request.order) && $smarty.request.order == 'guest'}
            <input type="hidden" name="order" value="guest" />
        {/if}
      </form>
      {if $smarty.session.shopstep>=3}
          <a href="javascript:document.forms['step3_f'].submit();">
          {/if}
          <div class="shop-steps{if $smarty.session.shopstep == 3} active{/if}">
            3. {#Shop_step_3#}
            <div class="small">{#Shop_step_3_inf#}</div>
          </div>
          {if $smarty.session.shopstep>=3}
          </a>
      {/if}
    </div>

    <div class="col">
      <div class="shop-steps{if $smarty.session.shopstep == 4} active{/if}">
        4. {#Shop_step_4#}
        <div class="small">{#Shop_step_4_inf#}</div>
      </div>
    </div>

    <div class="col">
      <div class="shop-steps{if $smarty.session.shopstep == 'final'} active{/if}">
        5. {#Shop_step_5#}
        <div class="small">{#Shop_step_5_inf#}</div>
      </div>
    </div>

  </div>
</div>
