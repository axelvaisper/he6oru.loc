<h1 class="title">{#ChangePassTitle#}</h1>
<div class="box-content">
  {if $not_logged == 1}
      {#NotLoggedInPass#}
  {else}
      {if $register_ok == 1}
          {#ChangePass_Ok#}
      {else}
          {if $error}
              <div class="box-error">
                <div class="font-weight-bold">{#Error#}</div>
                {foreach from=$error item=reg_error}
                    <div>{$reg_error}</div>
                {/foreach}
              </div>
          {/if}
          <form method="post" action="{page_link}">

            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label" for="form-oldpass">{#ChangePass_Ap#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="form-control" id="form-oldpass" name="oldpass" type="password" value="{$smarty.post.oldpass|sanitize}" />
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label" for="form-newpass">{#ChangePass_Np#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="form-control" id="form-newpass" name="newpass" type="password" value="{$smarty.post.newpass|sanitize}" />
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label" for="form-newpass2">{#ChangePass_Np2#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="form-control" id="form-newpass2" name="newpass2" type="password" value="{$smarty.post.newpass2|sanitize}" />
              </div>
            </div>

            <div class="text-center">
              <input class="btn btn-primary btn-block-sm" type="submit" value="{#LoginExternCp#}" />
            </div>

            <input type="hidden" name="send" value="1" />
          </form>
      {/if}
  {/if}
</div>
