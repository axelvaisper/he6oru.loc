<script>
<!-- //
function switchImage(data) {
    document.getElementById('img').innerHTML = '<img class="img-fluid" src="' + data + '" alt="" />';
}
//-->
</script>

<div class="h1 title text-truncate">{$row.Name|sanitize}</div>
<div class="wrapper">
  <div class="d-flex flex-wrap justify-content-center">
    {foreach from=$images item=im}
        <a href="{$im->Normal}" onclick="switchImage('{$im->Normal}');return false;">
          <img class="img-thumbnail height-4" src="{$im->Bild}" alt="{$im->Name|sanitize}" />
        </a>
    {/foreach}
  </div>
</div>
<div class="wrapper text-center">
  <span id="img">
    <img class="img-fluid" src="{$row.image}" alt="" />
  </span>
</div>

<div class="text-center">
  <button class="btn btn-secondary" onclick="closeWindow();">{#WinClose#}</button>
</div>
