{if !isset($smarty.request.subaction) || ($smarty.request.subaction != 'step2' && $smarty.request.subaction != 'step3' && $smarty.request.subaction != 'step4')}
<script>
<!-- //
$(function() {
    $('#login-form-send').submit(function () {
        $(this).ajaxSubmit({
            target: '#login-form-user', timeout: 3000
        });
        $('#ajl').hide();
        $('#ajlw').show();
        return false;
    });
});
//-->
</script>

    <div class="wrapper panel">
      <div class="panel-head">{#Login#}</div>
      <div class="panel-main">

        <div id="login-form-user">
        {include file="$incpath/user/login_raw.tpl"}
        </div>

        <div class="loader" id="ajlw"></div>

        <div id="ajl">
          <form method="post" action="{$baseurl}/index.php?p=userlogin&amp;action=ajaxlogin" id="login-form-send">
            <div class="form-group">
              <label for="login-email">{#LoginMailUname#}</label>
              <input type="text" class="form-control" name="login_email" id="login-email" placeholder="{#LoginMailUname#}" required="required" />
            </div>

            <div class="form-group">
              <label for="login-pass">{#Pass#}</label>
              <input type="password" class="form-control" name="login_pass" id="login-pass" placeholder="{#Pass#}" required="required" />
            </div>

            <div class="form-group form-check">
              <input class="form-check-input" name="staylogged" id="login-cookie" type="checkbox" />
              <label class="form-check-label" for="login-cookie">{#PassCookieT#}</label>
            </div>

            <div class="form-group">
              <input class="btn btn-primary btn-block" type="submit" value="{#Login_Button#}" />
            </div>

            <input type="hidden" name="p" value="userlogin" />
            <input type="hidden" name="action" value="ajaxlogin" />
            <input type="hidden" name="area" value="{$area}" />
            <input type="hidden" name="backurl" value="{page_link|base64encode}" />

            {if get_active('Register')}
            <a class="d-block" href="index.php?p=register&amp;lang={$langcode}&amp;area={$area}">{#RegNew#}</a>
            {/if}
            <a class="d-block" href="index.php?p=pwlost">{#PassLost#}</a>
        </div>
        </form>
      </div>
    </div>
{/if}
