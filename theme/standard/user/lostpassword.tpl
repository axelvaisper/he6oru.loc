<script>
<!-- //
$(function () {
    $('#getnew').submit(function () {
        var options = {
            url: 'index.php?action=getnew&p=pwlost',
            target: '#msggetnew',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return false;
    });

    $('#activate').submit(function () {
        var options = {
            url: 'index.php?action=activate&p=pwlost',
            target: '#msgactivate',
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return false;
    });
});
//-->
</script>

<div class="h3 title">{#PassLost#}</div>
<div class="box-content">
  <div class="wrapper">
    {#PassLostKodeText#}
  </div>

  <div id="msggetnew"></div>
  <form method="post" name="getnew" id="getnew">
    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="lost-mail">{#Email#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <input class="form-control" id="lost-mail" name="mail" value="" type="email" required="required" />
      </div>
    </div>

    <div class="text-center">
      <input class="btn btn-primary btn-block-sm" type="submit" value="{#SendKod#}" />
    </div>
  </form>
</div>

<div class="h3 title">{#ZagolovokKod#}</div>
<div class="box-content">
  <div class="wrapper">
    {#EnterKodText#}
  </div>

  <div id="msgactivate"></div>
  <form method="post" name="activate" id="activate">
    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="confirm-mail">{#Email#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <input class="form-control" id="confirm-mail" name="mail" type="email" value="{$smarty.request.email|sanitize}" required="required" />
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="confirm-code">{#PassKod#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <input class="form-control" id="confirm-code" name="pass" type="text" value="{$smarty.request.pass|sanitize}" required="required" />
      </div>
    </div>

    <div class="text-center">
      <input class="btn btn-primary btn-block-sm" type="submit" value="{#SendPassKod#}" />
    </div>

  </form>
</div>
