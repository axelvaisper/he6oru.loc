<script src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>

<h1 class="title text-truncate">{$title_html}</h1>
{if $skype}
    <div class="wrapper h4 text-center">
      {$skype|sanitize}
    </div>
    <div class="wrapper d-flex flex-column">
      <a href="skype: {$skype|sanitize}?voicemail" onclick="return skypeCheck();">
        {#Arrow#}{#Profile_ScypeVoicemail#}
      </a>
      <a href="skype:{$skype|sanitize}?userinfo" onclick="return skypeCheck();">
        {#Arrow#}{#Profile_ScypeProfile#}
      </a>
      <a href="skype:{$skype|sanitize}?call" onclick="return skypeCheck();">
        {#Arrow#}{#Profile_ScypeCall#}
      </a>
      <a href="skype:{$skype|sanitize}?add" onclick="return skypeCheck();">
        {#Arrow#}{#Profile_ScypeAdd#}
      </a>
      <a href="skype:{$skype|sanitize}?chat" onclick="return skypeCheck();">
        {#Arrow#}{#SendEmail_Send#}
      </a>
      <a href="skype:{$skype|sanitize}?sendfile" onclick="return skypeCheck();">
        {#Arrow#}{#Profile_ScypeSendFile#}
      </a>
    </div>
    <div class="wrapper">
      {#Profile_ICQInf#}
    </div>
{/if}
<div class="text-center">
  <input class="btn btn-primary" type="button" onclick="closeWindow();" value="{#WinClose#}" />
</div>
