<script>
<!-- //
$(function () {
    $('.user_pop').colorbox({
        height: "550px",
        width: "550px",
        iframe: true
    });
});
//-->
</script>

{include file="$incpath/forums/user_panel.tpl"}

<h1 class="title">{#MyAccount#} {$user.Benutzername|sanitize}</h1>
{if $user.Profil_public == 1 || $smarty.session.benutzer_id == $smarty.request.id}
    <div class="row">
      <div class="col-md-3 col-xs-12">

        <div class="h3 title">{#Info#}</div>
        <div class="box-content">
          <div class=wrapper>
            <div class="d-flex justify-content-center">
              {$user.Avatar}
            </div>
          </div>

          <div class="nav flex-column nav-pills" id="user-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active show" id="user-info-tab" data-toggle="pill" href="#user-info" role="tab" aria-controls="user-info" aria-selected="false">
              {#Profile_GenInfo#}
            </a>
            <a class="nav-link" id="user-contacts-tab" data-toggle="pill" href="#user-contacts" role="tab" aria-controls="user-contacts" aria-selected="false">
              {#Imprint#}
            </a>
            {if !empty($user)}
                <a class="nav-link" id="user-data-tab" data-toggle="pill" href="#user-data" role="tab" aria-controls="user-data" aria-selected="true">
                  {#PersonalData#}
                </a>
            {/if}
            <a class="nav-link" id="user-activity-tab" data-toggle="pill" href="#user-activity" role="tab" aria-controls="user-activity" aria-selected="false">
              {#UserActivity#}
            </a>
            <a class="nav-link" id="user-friends-tab" data-toggle="pill" href="#user-friends" role="tab" aria-controls="user-friends" aria-selected="false">
              {#Profile_Friends#}
            </a>
            <a class="nav-link" id="user-visits-tab" data-toggle="pill" href="#user-visits" role="tab" aria-controls="user-visits" aria-selected="false">
              {#UserVisits#}
            </a>
            <a class="nav-link" id="user-gallery-tab" data-toggle="pill" href="#user-gallery" role="tab" aria-controls="user-gallery" aria-selected="false">
              {#Gallery_Name#}
            </a>
            {if get_active('user_videos') && $user_videos}
                <a class="nav-link" id="user-videos-tab" data-toggle="pill" href="#user-videos" role="tab" aria-controls="user-videos" aria-selected="false">
                  {#Forums_UserVideos#}
                </a>
            {/if}
            <a class="nav-link" id="user-guestbook-tab" data-toggle="pill" href="#user-guestbook" role="tab" aria-controls="user-guestbook" aria-selected="false">
              {#Profile_GuestbookUser#}
            </a>
          </div>
        </div>
      </div>

      <div class="col-md-9 col-xs-12">
        <div class="tab-content" id="user-tabContent">
          <div class="tab-pane fade active show" id="user-info" role="tabpanel" aria-labelledby="user-info-tab">

            <div class="h3 title">{#Profile_GenInfo#}</div>
            <div class="box-content">
              <div class="row">
                <div class="col-4">{#Autoritets#}</div>
                <div class="col-8">
                  <div class="progress">
                    <div class="progress-bar progress-bar-striped" role="progressbar" style="width:{$autoritet_bar}%;" aria-valuenow="{$autoritet_bar}" aria-valuemin="0" aria-valuemax="100">
                      {if !empty({$autoritet_bar})}
                          {$autoritet_bar}%
                      {else}
                          <span class="text-danger">{$autoritet_bar}%</span>
                      {/if}
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-4">{#Profile_Regged#}</div>
                <div class="col-8">{$user.Regdatum|date_format:$lang.DateFormatExtended}</div>
              </div>

              <div class="row">
                <div class="col-4">{#Profile_CountProfile#}</div>
                <div class="col-8">{$user.Profil_Hits}</div>
              </div>

              {if $user.Rang}
                  <div class="row">
                    <div class="col-4">{#Profile_Rank#}</div>
                    <div class="col-8">
                      {if $user.Team == 1}
                          {$user.TeamName|sanitize}
                      {else}
                          {$user.Rang|sanitize}
                      {/if}
                    </div>
                  </div>
              {/if}

              <div class="row">
                <div class="col-4">{#Profile_LastAction#}</div>
                <div class="col-8">
                  {if $user.Zuletzt_Aktiv > 1}
                      {$user.Zuletzt_Aktiv|date_format:$lang.DateFormatExtended}
                  {else}
                      -
                  {/if}
                </div>
              </div>

              <div class="row">
                <div class="col-4">{#Forums_Postings#}</div>
                <div class="col-8">
                  {if $user.Beitraege >= 1}
                      <a href="index.php?p=forum&amp;action=print&amp;what=posting&amp;id={$user.Id}">
                        {$user.Beitraege}
                      </a>{else}
                      -
                  {/if}
                </div>
              </div>

              {if isset($user_thanks.num_post) && $user_thanks.num_post >= 1}
                  <div class="row">
                    <div class="col-4">{#ThanksMes#}</div>
                    <div class="col-8">{$user_thanks.num_post}</div>
                  </div>

                  <div class="row">
                    <div class="col-4">{#ThanksAll#}</div>
                    <div class="col-8">{$user_thanks.num_thanks}</div>
                  </div>

                  <div class="row">
                    <div class="col-4">{#ThanksUser#}</div>
                    <div class="col-8">{$user_thanks.num_user}</div>
                  </div>
              {/if}
            </div>

          </div>

          {if !empty($user)}
              <div class="tab-pane fade" id="user-data" role="tabpanel" aria-labelledby="user-data-tab">

                <div class="h3 title">{#PersonalData#}</div>
                <div class="box-content">

                  {if !empty($user.Status)}
                      <div class="row">
                        <div class="col-4">{#Profile_Status#}</div>
                        <div class="col-8">{$user.Status|sanitize}</div>
                      </div>
                  {/if}
                  {if $user.Geburtstag_public == 1 && $user.Geburtstag > 1}
                      <div class="row">
                        <div class="col-4">{#Birth#}</div>
                        <div class="col-8">{$user.Geburtstag|sanitize}</div>
                      </div>
                  {/if}
                  {if $user.Geschlecht != '-'}
                      <div class="row">
                        <div class="col-4">{#Profile_Gender#}</div>
                        <div class="col-8">
                          {if $user.Geschlecht == 'm'}
                              {#User_Male#}
                          {elseif $user.Geschlecht == 'f'}
                              {#User_Female#}
                          {else}
                              {#User_NoSettings#}
                          {/if}
                        </div>
                      </div>
                  {/if}
                  {if !empty($user.Ort) && $user.Ort_Public == 1}
                      <div class="row">
                        <div class="col-4">{#Town#}</div>
                        <div class="col-8">
                          <a data-toggle="tooltip" title="{#Profile_ShowGoogleMaps#}" href="http://maps.google.ru/maps?f=q&hl={$user.LandCode|sanitize}&q={$user.Postleitzahl|sanitize}+{$user.Ort|urlencode|sanitize}" target="_blank" rel="nofollow">
                            {$user.Ort|sanitize}
                          </a>
                        </div>
                      </div>
                  {/if}
                  {if !empty($user.Beruf)}
                      <div class="row">
                        <div class="col-4">{#Profile_Job#}</div>
                        <div class="col-8">{$user.Beruf|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Interessen)}
                      <div class="row">
                        <div class="col-4">{#Profile_Int#}</div>
                        <div class="col-8">{$user.Interessen|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Hobbys)}
                      <div class="row">
                        <div class="col-4">{#Profile_Hobbys#}</div>
                        <div class="col-8">{$user.Hobbys|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Essen)}
                      <div class="row">
                        <div class="col-4">{#Profile_Food#}</div>
                        <div class="col-8">{$user.Essen|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Musik)}
                      <div class="row">
                        <div class="col-4">{#Profile_Music#}</div>
                        <div class="col-8">{$user.Musik|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Films)}
                      <div class="row">
                        <div class="col-4">{#Profile_Films#}</div>
                        <div class="col-8">{$user.Films|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Tele)}
                      <div class="row">
                        <div class="col-4">{#Profile_Tele#}</div>
                        <div class="col-8">{$user.Tele|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Book)}
                      <div class="row">
                        <div class="col-4">{#Profile_Book#}</div>
                        <div class="col-8">{$user.Book|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Game)}
                      <div class="row">
                        <div class="col-4">{#Profile_Game#}</div>
                        <div class="col-8">{$user.Game|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Citat)}
                      <div class="row">
                        <div class="col-4">{#Profile_Citat#}</div>
                        <div class="col-8">{$user.Citat|sanitize|nl2br}</div>
                      </div>
                  {/if}
                  {if !empty($user.Other)}
                      <div class="row">
                        <div class="col-4">{#Profile_Other#}</div>
                        <div class="col-8">{$user.Other|sanitize|nl2br}</div>
                      </div>
                  {/if}
                </div>

              </div>
          {/if}

          <div class="tab-pane fade" id="user-activity" role="tabpanel" aria-labelledby="user-activity-tab">
            {$user_activity}
          </div>

          <div class="tab-pane fade" id="user-friends" role="tabpanel" aria-labelledby="user-friends-tab">
            {$user_friends}
          </div>

          <div class="tab-pane fade" id="user-visits" role="tabpanel" aria-labelledby="user-visits-tab">
            {$user_visits}
          </div>

          <div class="tab-pane fade" id="user-gallery" role="tabpanel" aria-labelledby="user-gallery-tab">
            {$user_gallery}
            {$user_gallery_profile}
          </div>

          {if get_active('user_videos') && $user_videos}
              <div class="tab-pane fade" id="user-videos" role="tabpanel" aria-labelledby="user-videos-tab">

                <div class="h3 title" id="uservideo">
                  {#Forums_UserVideos#}
                </div>
                <div class="box-content">
                  <div class="d-flex justify-content-center flex-wrap">
                    {foreach from=$user_videos item=u}
                        <div class="mx-2">
                          {$u->VideoData}
                          {if $u->Name}
                              <div class="text-center">{$u->Name}</div>
                          {/if}
                        </div>
                    {/foreach}
                  </div>
                </div>

              </div>
          {/if}

          <div class="tab-pane fade" id="user-guestbook" role="tabpanel" aria-labelledby="user-guestbook-tab">
            {include file="$incpath/user/guestbook.tpl"}
          </div>

          <div class="tab-pane fade" id="user-contacts" role="tabpanel" aria-labelledby="user-contacts-tab">

            <div class="h3 title">{#Imprint#}</div>
            <div class="box-content">
              {if !empty($user.Webseite)}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Web#}</div>
                    <div class="col-8">
                      <a rel="nofollow" href="{$user.Webseite|sanitize}" target="_blank">
                        <i class="icon-link size-xl" data-toggle="tooltip" title="{$user.Webseite|sanitize}"></i>
                      </a>
                    </div>
                  </div>
              {/if}
              {if $user.Email_User}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Profile_EmailContact#}</div>
                    <div class="col-8">{$user.Email_User}</div>
                  </div>
              {/if}
              {if $user.Pn_User}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Profile_SendPN#}</div>
                    <div class="col-8">{$user.Pn_User}</div>
                  </div>
              {/if}
              {if $user.Icq_User}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Profile_ICQ#}</div>
                    <div class="col-8">{$user.Icq_User}</div>
                  </div>
              {/if}
              {if $user.Skype_User}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Profile_ScypeOpt#}</div>
                    <div class="col-8">{$user.Skype_User}</div>
                  </div>
              {/if}
              {if !empty($user.msn)}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Profile_MSN#}</div>
                    <div class="col-8">{$user.msn|sanitize}</div>
                  </div>
              {/if}
              {if !empty($user.Vkontakte)}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Vkontakte#}</div>
                    <div class="col-8">
                      <a rel="nofollow" href="{$user.Vkontakte|sanitize}" target="_blank">
                        <i class="icon-vkontakte size-xl"></i>
                      </a>
                    </div>
                  </div>
              {/if}
              {if !empty($user.Odnoklassniki)}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Odnoklassniki#}</div>
                    <div class="col-8">
                      <a rel="nofollow" href="{$user.Odnoklassniki|sanitize}" target="_blank">
                        <i class="icon-odnoklassniki size-xl"></i>
                      </a>
                    </div>
                  </div>
              {/if}
              {if !empty($user.Mymail)}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Mymail#}</div>
                    <div class="col-8">
                      <a rel="nofollow" href="{$user.Mymail|sanitize}" target="_blank"> 
                        <i class="icon-user size-xl"></i>
                      </a>
                    </div>
                  </div>
              {/if}
              {if !empty($user.Google)}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Google#}</div>
                    <div class="col-8">
                      <a rel="nofollow" href="{$user.Google|sanitize}" target="_blank">
                        <i class="icon-gplus size-xl"></i>
                      </a>
                    </div>
                  </div>
              {/if}
              {if !empty($user.Facebook)}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Facebook#}</div>
                    <div class="col-8">
                      <a rel="nofollow" href="{$user.Facebook|sanitize}" target="_blank">
                        <i class="icon-facebook size-xl"></i>
                      </a>
                    </div>
                  </div>
              {/if}
              {if !empty($user.Twitter)}
                  {assign var=control value=1}
                  <div class="row">
                    <div class="col-4">{#Twitter#}</div>
                    <div class="col-8">
                      <a rel="nofollow" href="{$user.Twitter|sanitize}" target="_blank">
                      <i class="icon-twitter size-xl"></i>
                      </a>
                    </div>
                  </div>
              {/if}
              {if empty($control)}
                  {#ProfilContactEmpty#}
              {/if}
            </div>

          </div>
        </div>
      </div>
    </div>
{else}
    <div class="box-content">{#Profile_NotPublicThis#}</div>
{/if}
