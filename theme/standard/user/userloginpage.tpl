<h1 class="title">{#MyAccount#}</h1>
<div class="box-content">
  {if $loggedin}
      <div class="d-flex flex-column">
        {if isset($smarty.get.success) && $smarty.get.success == 1}
            {#LoginExternSuccess#}
        {/if}
        <div class="wrapper">
          {if get_active('shop')}
              {$welcome}, {$smarty.session.benutzer_vorname} {$smarty.session.benutzer_nachname}!
              {#LoginExternCustomerNr#}: {$smarty.session.benutzer_id}
          {else}
              {$welcome}, {$smarty.session.user_name}!
          {/if}
        </div>

        <div class="wrapper">
          {#LoginExternActions#}
        </div>

        {if get_active('pn')}
            <a href="index.php?p=pn">
              {#Arrow#}{#PN_PeronalMessages#}
            </a>
        {/if}
        {if get_active('shop')}
            <a href="index.php?p=shop&amp;action=myorders">
              {#Arrow#}{#Shop_go_myorders#}
            </a>
            <a href="index.php?p=shop&amp;action=mydownloads">
              {#Arrow#}{#LoginExternVd#}
            </a>
            <a href="index.php?p=shop&amp;action=mylist">
              {#Arrow#}{#Shop_mylist#}
            </a>
            <a class="colorbox" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1">
              {#Arrow#}{#Shop_mergeListsMy#}
            </a>
        {/if}
        {if get_active('calendar')}
            <a href="index.php?p=calendar&amp;month={$smarty.now|date_format:'m'}&amp;year={$smarty.now|date_format:'Y'}&amp;area={$area}&amp;show=private">
              {#Arrow#}{#UserCalendar#}
            </a>
        {/if}
        <a href="index.php?p=user&amp;id={$smarty.session.benutzer_id}&amp;area={$area}">
          {#Arrow#}{#LoginExternVp#}
        </a>
        <a href="index.php?p=useraction&amp;action=profile">
          {#Arrow#}{#LoginExternPc#}
        </a>
        <div class="wrapper">
          <a href="index.php?p=useraction&amp;action=changepass">
            {#Arrow#}{#LoginExternCp#}
          </a>
        </div>

        {if permission('adminpanel')}
            <a href="javascript:void(0);" onclick="openWindow('{$baseurl}/admin', 'admin', '', '', 1);">
              {#Arrow#}{#LoginExternAd#}
            </a>
            <a target="_blank" href="{$baseurl}/admin">
              {#Arrow#}{#LoginExternAd2#}
            </a>
        {/if}
        {if permission('deleteaccount') && $smarty.session.benutzer_id != 1}
            <a href="index.php?p=useraction&amp;action=deleteaccount">
              {#Arrow#}{#Arrow#}{#AccountDel#}
            </a>
        {/if}
        <form method="post" name="logout_form_user" action="index.php">
          <input type="hidden" name="p" value="userlogin" />
          <input type="hidden" name="action" value="logout" />
          <input type="hidden" name="area" value="{$area}" />
          <input type="hidden" name="backurl" value="{"index.php"|base64encode}" />
          <a onclick="return confirm('{#Confirm_Logout#}');" href="javascript: document.forms['logout_form_user'].submit();">
            {#Arrow#}{#Logout#}
          </a>
        </form>

      </div>
  {else}

<script>
<!-- //
$(function() {
    $('#login-button').on('click', function () {
        $('#login-content').hide();
        $('#login-loader').show();
    });
});
//-->
</script>

      <div id="login-content">
        {if isset($LoginError)}
            <div class="box-error">
              <div class="font-weight-bold">{#Error#}</div>
              {#WrongLoginData#}
            </div>
        {/if}

        <div class="wrapper">
          {#LoginExternHeader#}
        </div>
        <div class="wrapper">
          <form name="login" method="post" action="index.php?p=userlogin">
            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="form-login-mail">{#LoginMailUname#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <input class="form-control" id="form-login-mail" name="login_email" type="text" />
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="form-login-pass">{#Pass#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <input class="form-control" id="form-login-pass" name="login_pass" type="password" />
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-9 offset-md-3 col-sm-12">
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" name="staylogged" value="1" type="checkbox"{if $data.PnEmail == 0} checked="checked"{/if} /> {#PassCookieT#}
                  </label>
                </div>
              </div>
            </div>

            <input class="btn btn-primary btn-block-sm" id="login-button" type="submit" value="{#Login_Button#}" />

            <input type="hidden" name="p" value="userlogin" />
            <input type="hidden" name="action" value="newlogin" />
            <input type="hidden" name="area" value="{$area}" />
            <input type="hidden" name="backurl" value="{page_link|base64encode}" />
          </form>
        </div>

        <div class="d-flex flex-wrap text-nowrap">
          {if get_active('Register')}
              <a class="mr-3" href="index.php?p=register&amp;area={$area}">
                {#Arrow#}{#RegNew#}
              </a>
          {/if}
          <a href="index.php?p=pwlost">
            {#Arrow#}{#PassLost#}
          </a>
        </div>

      </div>
      <div class="loader" id="login-loader"></div>
  {/if}
</div>
