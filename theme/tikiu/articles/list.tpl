<div class="wrapper">
  <h1 class="title clearfix">
    {#Gaming_articles#}
    <div class="float-right">
      <a  target="_blank" href="{$rss_article_link}">
        <i class="icon-rss-squared" data-toggle="tooltip" title="{#RSSAbo#}"></i>
      </a>
    </div>
  </h1>

  {include file="$incpath/articles/categ.tpl"}
  {if $articlesitems}
      {foreach from=$articlesitems item=articles name=dn}
          <div class="box-content">
            <h3 class="listing-head">
              <a title="{$articles.Titel|sanitize}" href="index.php?p=articles&amp;area={$articles.Sektion}&amp;action=displayarticle&amp;id={$articles.Id}&amp;name={$articles.LinkTitle|translit}">{$articles.Titel|sanitize}</a>
            </h3>

            {if !empty($articles.Kennwort)}
                {#Content_LoginText#}
            {else}
                <div class="clearfix">
                  {if !empty($articles.Bild)}
                      <a href="index.php?p=articles&amp;area={$articles.Sektion}&amp;action=displayarticle&amp;id={$articles.Id}&amp;name={$articles.LinkTitle|translit}">
                        <img class="img-fluid {if $articles.Bildausrichtung == 'left'}listing-img-left{else}listing-img-right{/if}" src="uploads/articles/{$articles.Bild}" alt="{$articles.Titel|sanitize}" />
                      </a>
                  {/if}

                  {if $articles.Intro}
                      <div class="text-justify">
                        {$articles.Intro|sanitize}
                      </div>
                  {/if}
                  <div class="text-justify">
                    {$articles.News|html_truncate:500}
                  </div>
                </div>
            {/if}

            <div class="listing-foot flex-line justify-content-center">
              <span class="mr-4" data-toggle="tooltip" title="{#GlobalAutor#}">
                <a href="index.php?p=user&amp;id={$articles.Autor}&amp;area={$area}"><i class="icon-user"></i>{$articles.User}</a>
              </span>
              <span class="mr-4" data-toggle="tooltip" title="{#Added#}"><i class="icon-calendar"></i>{$articles.ZeitStart|date_format:$lang.DateFormatSimple}</span>
              <span class="mr-4" data-toggle="tooltip" title="{#Global_Hits#}"><i class="icon-eye"></i>{$articles.Hits}</span>
              <a href="index.php?p=articles&amp;area={$articles.Sektion}&amp;action=displayarticle&amp;id={$articles.Id}&amp;name={$articles.LinkTitle|translit}">
                <i class="icon-right-open"></i>{#ReadAll#}
              </a>
            </div>
          </div>
      {/foreach}
  {else}
      <div class="box-content">
        {#Gaming_articles_no#}
      </div>
  {/if}
</div>

{if !empty($pages)}
    <div class="wrapper">
      {$pages}
    </div>
{/if}

<div class="hidden-print">
  {include file="$incpath/articles/search.tpl"}
</div>

