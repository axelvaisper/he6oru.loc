<div class="wrapper">
  <h1 class="title">{$row.Titel|sanitize}</h1>
  <div class="wrapper clearfix">
    {if !empty($row.Bild) && $smarty.request.artpage < 2}
        <img class="img-fluid {if $row.BildAusrichtung == 'left'}img-left{else}img-right{/if}" src="uploads/articles/{$row.Bild}" alt="{$row.Titel|sanitize}" />
    {/if}
    {if $row.Intro}
        <p class="font-weight-bold">{$row.Intro}</p>
    {/if}
    {$row.News}
  </div>

  <div class="flex-line">
    <span class="mr-3">{#GlobalAutor#}: <a href="index.php?p=user&amp;id={$row.Autor}&amp;area={$area}">{$row.User}</a></span>
    <span class="mr-3">{#Date#}: {$row.ZeitStart|date_format:$lang.DateFormatSimple}</span>
  </div>
</div>

{if !empty($article_pages)}
    <div class="wrapper">
      {$article_pages}
    </div>
{/if}

{if $row.Bewertung == 1}
    <div class="wrapper">
      <div class="flex-line justify-content-center">
        <div class="mr-2">{#Rating_Rating#}</div>
        <div class="rating-star" data-rating="{$row.Wertung}"></div>
      </div>
    </div>
{/if}

{if $row.Typ == 'review' || $row.Typ == 'preview'}
    <div class="h3 title">{#Info#}</div>
    <div class="box-content">
      <div class="row">
        <div class="col-3">{#Gaming_articles_type#}:</div>
        <div class="col-9">{$row.TypName}</div>
      </div>
      {if $row.ManLink}
          <div class="row">
            <div class="col-3">{#Manufacturer#}:</div>
            <div class="col-9">{$row.ManLink}</div>
          </div>
      {/if}
      {if $row.PubLink}
          <div class="row">
            <div class="col-3">{#Products_publisher#}:</div>
            <div class="col-9">{$row.PubLink}</div>
          </div>
      {/if}
      {if !empty($row.Preis)}
          <div class="row">
            <div class="col-3">{#Products_price#}:</div>
            <div class="col-9">{$row.Preis|sanitize}</div>
          </div>
      {/if}
      {if !empty($row.Veroeffentlichung)}
          <div class="row">
            <div class="col-3">{#Gaming_articles_release#}:</div>
            <div class="col-9">{$row.Veroeffentlichung|sanitize}</div>
          </div>
      {/if}
      {if !empty($row.Genre)}
          <div class="row">
            <div class="col-3">{#Global_Categ#}:</div>
            <div class="col-9">{$row.Genre}</div>
          </div>
      {/if}
      {if !empty($row.Plattform)}
          <div class="row">
            <div class="col-3">{#Gaming_cheats_plattform#}:</div>
            <div class="col-9">{$row.Plattform}</div>
          </div>
      {/if}
      {if !empty($row.Shop) && empty($row.ShopArtikel)}
          <div class="row">
            <div class="col-3">{#Products_buyat#}:</div>
            <div class="col-9">
              <a data-toggle="tooltip" title="{$row.Shop|tooltip}" target="_blank" href="{$row.Shop|sanitize}">{#Gaming_articles_buyatName#}</a>
            </div>
          </div>
      {/if}
      {if !empty($row.ShopArtikel)}
          <div class="row">
            <div class="col-3">{#Products_buyat#}:</div>
            <div class="col-9">{$row.ShopArtikel}</div>
          </div>
      {/if}
      {if $LinksExtern}
          <div class="row">
            <div class="col-3">{#Links#}:</div>
            <div class="col-9">
              {foreach from=$LinksExtern item=a}
                  <a class="d-block" href="{$a->Link}" target="_blank">{$a->Name}</a>
              {/foreach}
            </div>
          </div>
      {/if}
      {if $DataVal && $row.Typ == 'review'}
          {foreach from=$DataVal item=dv}
              <div class="row align-items-center">
                <div class="col-3">{$dv->Name}:</div>
                <div class="col-9">
                  <div class="rating-star" data-rating="{$dv->Wert}"></div>
                </div>
              </div>
          {/foreach}
          <div class="row">
            <div class="col-3">{#Rating_Rating#}:</div>
            <div class="col-9">{$Ges|numf} %</div>
          </div>
      {/if}
    </div>

    {if !empty($row.Top)}
        <div class="h3 title">{#Gaming_articles_top#}</div>
        <div class="box-content">
          {foreach from=$row.Top item=top}
              <div>{$top|sanitize}</div>
          {/foreach}
        </div>
    {/if}
    {if !empty($row.Flop)}
        <div class="h3 title">{#Gaming_articles_flop#}</div>
        <div class="box-content">
          {foreach from=$row.Flop item=Flop}
              <div>{$Flop|sanitize}</div>
          {/foreach}
        </div>
    {/if}
    {if $row.Minimum}
        <div class="h3 title">{#Gaming_articles_sysmin#}</div>
        <div class="box-content">
          {foreach from=$row.Minimum item=minsys}
              <div>{$minsys|sanitize}</div>
          {/foreach}
        </div>
    {/if}
    {if $row.Optimum}
        <div class="h3 title">{#Gaming_articles_sysopt#}</div>
        <div class="box-content">
          {foreach from=$row.Optimum item=optsys}
              <div>{$optsys|sanitize}</div>
          {/foreach}
        </div>
    {/if}
{/if}

{if $row.Bewertung == 1}
    {$RatingForm}
{/if}

{$IncludedGalleries}
{$IncludedArticles}
{$IncludedNews}
{$IncludedContent}
{$GetComments}
