{if permission('calendar_event') || permission('calendar_event_new')}
    <div class="spacer text-center">
      <a class="btn btn-sm btn-primary" href="index.php?p=calendar&amp;area={$area}&amp;action=newevent&amp;month={$currentmonth|default:1}&amp;year={$Year}&amp;area={$area}&amp;show={$showtype}">
        <i class="icon-plus"></i> {#Calendar_newEvent#}
      </a>
      <a class="btn btn-sm btn-primary" href="index.php?p=calendar&amp;area={$area}&amp;action=myevents">
        {#Calendar_MyEvents#}
      </a>
    </div>
{/if}
