{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
{include file="$incpath/other/jsform.tpl"}
$(function () {
    $("#cf").validate({
        rules: {
            name: { required: true },
            text: { required: true }
        },
        messages: { },
        submitHandler: function () {
            document.forms['f'].submit();
        }
    });
});
//-->
</script>

<h1 class="title">
  {if isset($smarty.request.action) && $smarty.request.action == 'editevent'}
      {#Calendar_editEvent#}
  {else}
      {#Calendar_newEvent#}
  {/if}
</h1>

<div class="box-content" id="new">
  <form method="post" action="index.php?p=calendar" name="f" id="cf" onsubmit="closeCodes();">

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="cal-date">{#Date#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <div class="form-row">

          <div class="col">
            <select class="form-control" name="day" id="cal-date">
              {if !empty($smarty.request.day)}
                  {assign var=n_tag value=$smarty.request.day}
                  {assign var=n_monat value=$smarty.request.month}
              {else}
                  {assign var=n_tag value=$smarty.now|date_format:'%d'}
                  {assign var=n_monat value=$currentmonth}
              {/if}
              {assign var=n_jahr value=$smarty.request.year}
              {section name=day loop=31 start=0}
                  <option value="{$smarty.section.day.index+1}"{if $n_tag == $smarty.section.day.index+1} selected="selected"{/if}>{if $smarty.section.day.index+1 < 10}0{/if}{$smarty.section.day.index+1}</option>
              {/section}
            </select>
          </div>

          <div class="col">
            <select class="form-control" name="month">
              {foreach from=$month name=month item=m}
                  <option value="{$smarty.foreach.month.index+1}"{if $smarty.foreach.month.index+1 == $n_monat} selected="selected"{/if}>{$m}</option>
              {/foreach}
            </select>
          </div>

          <div class="col">
            <select class="form-control" name="year">
              {section name=year loop=$startYear+10 name=year start=$startYear}
                  <option value="{$smarty.section.year.index}"{if $n_jahr == $smarty.section.year.index} selected="selected"{/if}>{$smarty.section.year.index}</option>
              {/section}
            </select>
          </div>
        </div>
      </div>
    </div>


    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="cal-start">{#Calendar_Start#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        {if isset($smarty.request.action) && $smarty.request.action == 'editevent'}
            {assign var=startHour value=$row->Start|date_format:'%H'}
            {assign var=startMinute value=$row->Start|date_format:'%M'}
            {assign var=endHour value=$row->Ende|date_format:'%H'}
            {assign var=endMinute value=$row->Ende|date_format:'%M'}
        {else}
            {assign var=startHour value=8}
            {assign var=startMinute value=0}
            {assign var=endHour value=15}
            {assign var=endMinute value=0}
        {/if}
        <div class="form-row">

          <div class="col">
            <select class="form-control" name="s_std" id="cal-start">
              <option value="0">00</option>
              {section name=std loop=23 start=0 step=1}
                  <option value="{$smarty.section.std.index+1}"{if $smarty.section.std.index+1 == $startHour} selected="selected"{/if}>{if $smarty.section.std.index+1 < 10}0{/if}{$smarty.section.std.index+1}</option>
              {/section}
            </select>
          </div>

          <div class="col">
            <select class="form-control" name="s_min">
              <option value="0">00</option>
              {section name=min loop=59 start=0 step=1}
                  <option value="{$smarty.section.min.index+1}"{if $smarty.section.min.index+1 == $startMinute} selected="selected"{/if}>{if $smarty.section.min.index+1 < 10}0{/if}{$smarty.section.min.index+1}</option>
              {/section}
            </select>
          </div>
        </div>
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="cal-end">{#Calendar_End#}</label>
      </div>

      <div class="col-md-9 col-sm-12">
        <div class="form-row">

          <div class="col">
            <select class="form-control" name="e_std" id="cal-end">
              <option value="0">00</option>
              {section name=estd loop=23 start=0 step=1}
                  <option value="{$smarty.section.estd.index+1}"{if $smarty.section.estd.index+1 == $endHour} selected="selected"{/if}>{if $smarty.section.estd.index+1 < 10}0{/if}{$smarty.section.estd.index+1}</option>
              {/section}
            </select>
          </div>

          <div class="col">
            <select class="form-control" name="e_min">
              <option value="0">00</option>
              {section name=emin loop=59 start=0 step=1}
                  <option value="{$smarty.section.emin.index+1}"{if $smarty.section.emin.index+1 == $endMinute} selected="selected"{/if}>{if $smarty.section.emin.index+1 < 10}0{/if}{$smarty.section.emin.index+1}</option>
              {/section}
            </select>
          </div>
        </div>
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">{#Calendar_wholeDay#}</div>
      <div class="col-md-9 col-sm-12">
        <div class="form-check">
          <input class="form-check-input" id="cal-wday" name="s_wday" type="checkbox" value="1"{if isset($row->wd) && $row->wd == 1} checked="checked"{/if} />
          <label class="form-check-label" for="cal-wday">{#Yes#}</label>
        </div>
      </div>
    </div>

    {if isset($smarty.request.action) && $smarty.request.action == 'editevent'}
        <div class="row form-group">
          <div class="col-md-3 col-sm-12">{#GlobalOk#}</div>
          <div class="col-md-9 col-sm-12">
            <div class="form-check">
              <input class="form-check-input" id="cal-done" name="done" type="checkbox" value="1"{if isset($row->Erledigt) && $row->Erledigt == 1} checked="checked"{/if} />
              <label class="form-check-label" for="cal-done">{#Yes#}</label>
            </div>
          </div>
        </div>
    {/if}

    {if $smarty.request.action != 'editevent'}
        <div class="row form-group">
          <div class="col-md-3 col-sm-12">
            <label class="col-form-label" for="cal-days">{#Calendar_moreDays#}</label>
          </div>
          <div class="col-md-9 col-sm-12">
            <select class="form-control" name="days" id="cal-days">
              <option value="0" selected="selected">{#No#}</option>
              {section name=days loop=21 start=1}
                  <option value="{$smarty.section.days.index+1}">{$smarty.section.days.index+1} {#Calendar_eDays#}</option>
              {/section}
            </select>
          </div>
        </div>
    {/if}

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="cal-name">{#Title#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <input class="form-control" id="cal-name" name="name" type="text" size="50" value="{$row->Titel|sanitize}" required="required" />
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="msgform">{#Description#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        {include file="$incpath/comments/format.tpl"}
        <textarea class="form-control" name="text" cols="" rows="5" id="msgform">{$row->Beschreibung|sanitize}</textarea>
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="cal-weight">{#Calendar_weight#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <select class="form-control" name="weight" id="cal-weight">
          {assign var=plus value=0}
          {foreach from=$weight item=we}
              {assign var=plus value=$plus+1}
              <option value="{$plus}"{if $plus == $row->Gewicht} selected="selected"{/if}>{$we}</option>
          {/foreach}
        </select>
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="cal-type">{#Calendar_selCal#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        {if ($smarty.request.show == 'public' || empty($smarty.request.show))}
            <input name="show" type="hidden" id="show" value="public" />
        {else}
            <input name="show" type="hidden" id="show" value="private" />
        {/if}
        <select class="form-control" name="show" id="cal-type">
          {if permission('calendar_event_new')}
              <option value="public"{if $smarty.request.show == 'public'} selected="selected"{/if}>{#Calendar_public#}</option>
          {/if}
          {if permission('calendar_event')}
              <option value="private"{if $smarty.request.show == 'private'} selected="selected"{/if}>{#Calendar_private#}</option>
          {/if}
        </select>
      </div>
    </div>

    <div style="text-align: center">
      <input class="btn btn-primary btn-block-sm" name="Submit" type="submit" value="{#Save#}" />
    </div>

    <input name="newevent" type="hidden" value="1" />
    <input name="area" type="hidden" value="{$area}" />
    {if isset($smarty.request.action) && $smarty.request.action == 'newevent'}
        <input name="action" type="hidden" value="insertevent" />
    {else}
        <input name="month" type="hidden" id="month" value="{$smarty.request.month}" />
        <input name="year" type="hidden" id="year" value="{$smarty.request.year}" />
        <input name="day" type="hidden" id="day" value="{$smarty.request.day}" />
        <input name="action" type="hidden" value="editevent" />
        <input name="subaction" type="hidden" id="subaction" value="save" />
        <input name="id" type="hidden" id="id" value="{$smarty.request.id}" />
    {/if}

  </form>
</div>
