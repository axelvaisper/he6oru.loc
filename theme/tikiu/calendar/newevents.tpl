{if get_active('calendar') && $NewCalEvents}
    <div class="wrapper panel">
      <div class="panel-head">{#CalendarNewEvents#}</div>
      <div class="panel-main">
        {foreach from=$NewCalEvents item=nce name=nextevents}
            <div class="wrapper">
              <a data-toggle="tooltip" title="{$nce->Beschreibung|truncate:200}" href="{$nce->EventLink}">
                {$nce->Titel|sanitize}
              </a>
              <div>
                {$nce->Beschreibung|striptags|truncate:100|sanitize}
              </div>
              <div class="row">
                <div class="col-6">
                  {$nce->Start|date_format:'%d-%m-%Y'}
                </div>
                <div class="col-6 text-right">
                  <a href="{$nce->EventLink}">{#MoreDetails#}</a>
                </div>
              </div>
            </div>
        {/foreach}
      </div>
    </div>
{/if}
