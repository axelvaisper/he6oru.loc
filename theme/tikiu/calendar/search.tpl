<div class="h3 title">{#Calendar_search#}</div>
<div class=box-content>
  <form action="index.php" method="get">
    <div class="input-group">
      <input class="form-control" type="text" name="qc" value="{$smarty.request.qc|sanitize}" required="required" />
      <span class="input-group-append">
        <input class="btn btn-primary" type="submit" value="{#Calendar_search#}" />
      </span>
    </div>
    <input name="area" type="hidden" value="{$area}" />
    <input name="p" type="hidden" value="calendar" />
    <input name="action" type="hidden" value="search" />
  </form>
</div>
