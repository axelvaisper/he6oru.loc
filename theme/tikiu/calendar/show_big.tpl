<h1 class="title">{$header} ({if $smarty.request.show == 'private'}{#Calendar_private#}{else}{#Calendar_public#}{/if})</h1>

{include file="$incpath/calendar/actions.tpl"}

<div class="row wrapper">
  <div class="col-auto text-left">
    <a class="btn btn-link" href="index.php?p=calendar&amp;month={$mp}&amp;year={$yp}&amp;area={$area}&amp;show={$showtype}">
      <i class="icon-left-open"></i>{$text_prev_m}
    </a>
  </div>

  <div class="col text-center">
    <a href="index.php?p=calendar&amp;area={$area}&amp;action=displayyear&amp;show={$showtype}&amp;year={$Year}">{#Calendar_yearView#}</a>
    <span class="text-mutted">&nbsp;|&nbsp;</span>
    {#Calendar_weeks#}:
    {assign var=num_week value=0}
    {foreach from=$cal_data item=cd}
        {assign var=num_week value=$num_week+1}
        <a class="btn btn-link" href="index.php?p=calendar&amp;show={$showtype}&amp;action=week&amp;weekstart={$cd->StartWeek}&amp;weekend={$cd->EndWeek}&amp;area={$area}">{$num_week}</a>
    {/foreach}
  </div>

  <div class="col-auto text-right">
    <a class="btn btn-link" href="index.php?p=calendar&amp;month={$mn}&amp;year={$yn}&amp;area={$area}&amp;show={$showtype}">
      {$text_next_m}<i class="icon-right-open"></i>
    </a>
  </div>
</div>

<div class="calendarBorder">
  <table class="calendarTable">
    <tr>
      <td class="calendarHeaderBig">&nbsp;</td>
      {foreach from=$DayNamesArray item=day}
          <td class="calendarHeaderBig" title="{$day}">
            {$day|truncate:21:false}
          </td>
      {/foreach}
    </tr>
    {foreach from=$cal_data item=cd}
        <tr>
          <td class="text-center calendarBlanc">
            <a href="index.php?p=calendar&amp;show={$showtype}&amp;action=week&amp;weekstart={$cd->StartWeek}&amp;weekend={$cd->EndWeek}&amp;area={$area}">
              <i class="icon-right-open"></i>
              <i class="icon-right-open"></i>
              <i class="icon-right-open"></i>
            </a>
          </td>
          {foreach from=$cd->CalDataInner item=td}
              <td class="{$td->tdclass}" style="width: 14.28%">
                <div>
                  {$td->thelink}
                </div>
                {if $td->countitems > 2}
                    {if $td->packed_events_niy == 1}
                        <div class="calendarInactiveDay">{#Calendar_moreEvents#} ({$td->countitems})</div>
                    {else}
                        <a class="d-block calendarEventLink" href="{$td->packed_events_link}">{#Calendar_moreEvents#} ({$td->countitems})</a>
                    {/if}
                {else}
                    {foreach from=$td->Ereignisse item=sd name=Ereigniss}
                        {if $sd->is_not_inyear == 1}
                            <div class="calendarInactiveDay">
                              {if $sd->Gewicht == 1}
                                  <i class="icon-warning-empty"></i>
                              {/if}
                              <i class="icon-lightbulb"></i>
                              {$sd->Titel|truncate:20|sanitize}
                            </div>
                        {else}
                            <a class="d-block calendarEventLink"{if $sd->Erledigt == 1}style="text-decoration: line-through"{/if} href="{$sd->link_event_only}">
                              {if $sd->Gewicht == 1}
                                  <i class="icon-warning-empty"></i>
                              {/if}
                              <i class="icon-lightbulb"></i>
                              {$sd->Titel|truncate:20|sanitize}
                            </a>
                        {/if}
                    {/foreach}
                {/if}
                {if !empty($td->Geburtstage)}
                    <div>
                      {$td->Geburtstage}
                    </div>
                {/if}
              </td>
          {/foreach}
        </tr>
    {/foreach}
  </table>
</div>
