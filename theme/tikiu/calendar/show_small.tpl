{if get_active('calendar')}
    <div class="wrapper panel">
      <div class="panel-head">{#Calendar#}</div>
      <div class="panel-main" id="calraw">
        {include file="$incpath/calendar/raw.tpl"}
      </div>
    </div>
{/if}
