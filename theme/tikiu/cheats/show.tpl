<script>
<!-- //
$(function () {
    $('.linkextern').on('click', function () {
        var options = {
            target: '#plick',
            url: 'index.php?action=updatehitcount&p=cheats&id=' + $(this).data('id'),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>

<div id="plick"></div>
<div class="wrapper">
  <h1 class="title">{$cheat_res->Name|sanitize}</h1>
  <div class="wrapper clearfix">
    {if !empty($cheat_res->Bild)}
        <img class="img-fluid img-right" src="uploads/cheats/{$cheat_res->Bild}" alt="{$cheat_res->Name|sanitize}" />
    {/if}
    {$cheat_res->Beschreibung}
  </div>
</div>

<div class="wrapper">
  <div class="h3 title">{#Info#}</div>
  <div class="box-content">
    <div class="row">
      <div class="col-3">{#Added#}</div>
      <div class="col-9">
        {$cheat_res->DatumUpdate|date_format: $lang.DateFormatSimple}
      </div>
    </div>
    <div class="row">
      <div class="col-3">{#GlobalAutor#}</div>
      <div class="col-9">
        <a href="index.php?p=user&amp;id={$cheat_res->Benutzer}&amp;area={$area}">{$cheat_res->UserName}</a>
      </div>
    </div>
    {if $cheat_res->Pf}
        <div class="row">
          <div class="col-3">{#Gaming_cheats_plattform#}</div>
          <div class="col-9">{$cheat_res->Pf}</div>
        </div>
    {/if}
    {if !empty($cheat_res->Size)}
        <div class="row">
          <div class="col-3">{#Downloads_Size#}</div>
          <div class="col-9">{$cheat_res->Size}</div>
        </div>
        <div class="row">
          <div class="col-3">{#Downloads_Hits#}</div>
          <div class="col-9">{$cheat_res->DownloadHits}</div>
        </div>
    {/if}
    {if $cheat_res->Webseite}
        <div class="row">
          <div class="col-3">{#Web#}</div>
          <div class="col-9"><a rel="nofollow" target="_blank" href="{$cheat_res->Webseite}">{$cheat_res->Webseite}</a></div>
        </div>
    {/if}
    {if $cheat_res->Mf}
        <div class="row">
          <div class="col-3">{#Manufacturer#}</div>
          <div class="col-9">{$cheat_res->Mf}</div>
        </div>
    {/if}
    {if !empty($cheat_res->Sprache) && $CheatSettings->Flaggen == 1}
        <div class="row">
          <div class="col-3">{#Country#}</div>
          <div class="col-9"><img src="{$imgpath}/flags/{$cheat_res->Sprache}.png" alt="" /></div>
        </div>
    {/if}
    {if $CheatSettings->Wertung == 1}
        <div class="row">
          <div class="col-3">{#Rating_Rating#}</div>
          <div class="col-9 flex-line">
            <div class="rating-star" data-rating="{$cheat_res->Wertung}"></div>
          </div>
        </div>
    {/if}
  </div>
</div>

<div class="wrapper">
  <div class="h3 title">{#GlobalActions#}</div>
  <div class="box-content">
    {if permission('cheats_candownload')}
        {if !empty($cheat_res->Download)}
            <a class="d-block linkextern" data-id="{$cheat_res->Id}" rel="nofollow" {if empty($cheat_res->DownloadLink)}target="_blank"{/if} href="{if !empty($cheat_res->DownloadLink)}{$cheat_res->DownloadLink}{else}index.php?p=cheats&amp;action=getfile&amp;id={$cheat_res->Id}{/if}">
              <i class="icon-download"></i>{#DownloadsFile#}
            </a>
        {else}
            <div class="text-danger">
              <i class="icon-error"></i>{#FileNoLoad#}
            </div>
        {/if}
    {else}
        <div class="text-danger">
          <i class="icon-lock"></i>{#Downloads_NoPerm#}
        </div>
    {/if}
    {if $CheatSettings->Kommentare == 1}
        <a class="d-block" href="#comments">
          <i class="icon-chat"></i>{#Comments#}
        </a>
    {/if}
    {if $CheatSettings->DefektMelden == 1}
        {if !empty($cheat_res->DefektGemeldet)}
            <div class="text-danger">
              <i class="icon-warning-empty"></i>{#Links_ErrorSendBrokenImpos#}
            </div>
        {else}
            <a class="d-block" onclick="document.getElementById('broken-click').style.display = '';" href="javascript:void(0);">
              <i class="icon-warning-empty"></i>{#Links_ErrorSendBroken#}
            </a>
        {/if}
    {/if}
  </div>
</div>

{if $CheatSettings->DefektMelden == 1}
    {include file="$incpath/other/broken.tpl"}
{/if}

{if $alternatives && permission('cheats_candownload')}
    <div class="wrapper">
      <div class="h3 title">{#Links#}</div>
      <div class="box-content">
        {foreach from=$alternatives item=a}
            <a class="d-block linkextern" data-id="{$cheat_res->Id}" rel="nofollow" href="{$a->Link}" target="_blank">
              <i class="icon-link"></i>{$a->Name}
            </a>
        {/foreach}
      </div>
    </div>
{/if}

{$IncludedGalleries}
{$RatingForm}
{$GetComments}
