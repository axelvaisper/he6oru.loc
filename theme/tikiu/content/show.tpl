<h1 class="uk-text-center uk-margin-remove uk-padding-small">{$res.Titel|sanitize}</h1>
<div class="clearfix">
  {if !empty($res.Bild) && $smarty.request.artpage < 2}
      <img class="img-fluid {if $res.BildAusrichtung == 'left'}img-left{else}img-right{/if}" src="uploads/content/{$res.Bild}" alt="{$res.Titel|sanitize}" />
  {/if}
  {$res.Inhalt}
</div>

{if !empty($article_pages)}
    <div class="wrapper">
      {$article_pages}
    </div>
{/if}

{if $res.Bewertung == 1}
    <div class="flyu">
      <div >{#Rating_Rating#}</div>
      <div class="rating-star" data-rating="{$res.Wertung}"></div>
    </div>
{/if}

{if $res.Bewertung == 1}
    {$RatingForm}
{/if}
{$IncludedGalleries}
{$IncludedContent}
{$IncludedArticles}
{$IncludedNews}
{$GetComments}
