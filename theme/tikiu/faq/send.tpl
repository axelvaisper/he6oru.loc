<h1 class="title text-truncate">{$sname}</h1>
{if isset($smarty.request.faqsend) && $smarty.request.faqsend == 1 && empty($error)}
    <div class="text-center">
      <div class="wrapper">
        {#SendEmail_Ok#}
      </div>
      <button class="btn btn-primary" onclick="closeWindow();">{#WinClose#}</button>
    </div>
{else}

{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#cfaq').validate({
        rules: {
            {if !$loggedin}
            email: { required: true, email: true },
            {/if}
            body: { required: true, minlength: 10 }
        },
        submitHandler: function() {
            document.forms['faq'].submit();
        }
    });
});
//-->
</script>

{if !empty($error)}
    <div class="box-error">
      {foreach from=$error item=err}
          <div>{$err}</div>
      {/foreach}
    </div>
{/if}
<form name="faq" id="cfaq" action="index.php?p=faq&amp;action=mail" method="post">
  <div class="form-group">
    <label for="send-email">{#SendEmail_Email#}</label>
    <input class="form-control" id="send-email" name="email" type="email" value="{$smarty.request.email|default:$smarty.session.login_email|sanitize}" required="required" />
  </div>

  <div class="form-group">
    <label for="send-newcateg">{#New_Categ#}</label>
    <input class="form-control" id="send-newcateg" name="newcateg" type="text" value="{$smarty.request.newcateg|sanitize}" />
  </div>

  {if !empty($categs)}
      <div class="form-group">
        <label for="send-categ">{#Global_Categ#}</label>
        <select class="form-control" id="send-categ" name="faq_id">
          <option value="0">{#Global_Select_Categ#}</option>
          {foreach from=$categs item=dd}
              <option {if $dd->Id == $smarty.request.faq_id}selected="selected"{/if} value="{$dd->Id}">{$dd->visible_title} </option>
          {/foreach}
        </select>
      </div>
  {/if}

  <div class="form-group">
    <label for="send-body">{#Global_Guest#}</label>
    <textarea class="form-control" id="send-body" name="body" rows="5">{$smarty.request.body|escape:html}</textarea>
  </div>

  {include file="$incpath/other/captcha.tpl"}
  <div class="text-center">
    <input class="btn btn-primary" value="{#SendEmail_Send#}" type="submit">
    <input class="btn btn-secondary" onclick="closeWindow();" value="{#WinClose#}" type="button">
  </div>
  <input name="faqsend" type="hidden" value="1" />
</form>
{/if}
