{include file="$incpath/forums/user_panel.tpl"}
<div class="wrapper">
  {$navigation}
</div>

<h1 class="title">{#Forums_ChangeTypePost#}</h1>
<div class="box-content">
  <div class="wrapper">
    {#Forums_Label_choose_type#}
  </div>

  <form action="index.php?p=forum&amp;action=save_type" method="post">
    <div class="form-row">
      <div class="col-md-3 form-group">
        <select class="form-control" name="type">
          <option value="0" {if $topic->type == 0}selected{/if}>{#Forums_TypeNorm#}</option>
          <option value="1" {if $topic->type == 1}selected{/if}>{#Forums_Sticky#}</option>
          <option value="100" {if $topic->type == 100}selected{/if}>{#Forums_Announcement#}</option>
        </select>
      </div>

      <div class="col-md-auto">
        <input class="btn btn-primary btn-block-sm form-group" type="submit" value="{#Save#}" />
        <input class="btn btn-primary btn-block-sm form-group" type="button" value="{#Forums_JumpBack#}" onclick="history.go(-1);" />
      </div>
    </div>

    <input type="hidden" name="t_id" value="{$smarty.request.id}" />
    <input type="hidden" name="f_id" value="{$smarty.request.fid}" />
  </form>
</div>

{include file="$incpath/forums/footer.tpl"}
