<div class="popup_header">{#Forums_Complaint#}</div>
{if (isset($smarty.request.send) && $smarty.request.send == 1) && empty($error)}
  <table width="100%" cellpadding="4" cellspacing="1" class="box_inner">
    <tr>
      <td width="10%" class="row_first">
        <div align="center">
          {#SendEmail_Ok#}
          <br />
          <br />
          <input class="btn btn-secondary" type="button" onclick="closeWindow();" value="{#WinClose#}" />
        </div>
      </td>
    </tr>
  </table>
{else}

{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#cf').validate({
        rules: {
            {if !$loggedin}
            email: { required: true, email: true },
            {/if}
            body: { required: true, minlength: 10 }
        },
        submitHandler: function() {
            document.forms['fc'].submit();
        }
    });
});
//-->
</script>

<form name="fc" id="cf" action="index.php?p=forums&amp;action=complaint" method="post">
  <div class="popup_content padding5">
    <div class="popup_box">
      {if !empty($error)}
          <div class="box-error">
            {foreach from=$error item=err}
                <div>{$err}</div>
            {/foreach}
          </div>
      {/if}
      <br />
      <fieldset>
        <legend>{#GlobalMessage#}</legend>
        {$title|sanitize}
      </fieldset>
      <fieldset>
        <legend>{#SendEmail_Email#}</legend>
        <input name="email" type="text" style="width: 98%" value="{$smarty.request.email|default:$smarty.session.login_email|sanitize}" maxlength="35" />
      </fieldset>
      <fieldset>
        <legend>{#Forums_ComplaintText#}</legend>
        <textarea name="body" cols="" rows="8" style="width: 98%">{$smarty.request.body|escape: html}</textarea>
      </fieldset>
      {include file="$incpath/other/captcha.tpl"}
    </div>
  </div>
  <p align="center">
    <input class="btn btn-primary" type="submit" value="{#SendEmail_Send#}" />
    <input class="btn btn-secondary" type="button" onclick="closeWindow();" value="{#WinClose#}" />
    <input name="pid" type="hidden" value="{$smarty.request.pid|sanitize}" />
    <input name="fid" type="hidden" value="{$smarty.request.fid|sanitize}" />
    <input name="send" type="hidden" value="1" />
  </p>
</form>
{/if}
