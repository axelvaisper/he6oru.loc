{include file="$incpath/forums/user_panel.tpl"}

<h1 class="title">{#Help_General#}</h1>
{foreach from=$faq_categ item=fc}
    <div class="box-content">
      <div class="h3">{$fc->Name}</div>
      <ul>
        {foreach from=$fc->Items item=items}
            <li>
              <a href="index.php?p=forum&amp;action=help&amp;hid={$items->Id}&amp;sub={$items->FaqName|translit}">{$items->FaqName}</a>
            </li>
        {/foreach}
      </ul>
    </div>
{/foreach}
