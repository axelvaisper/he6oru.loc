{if $last_post_array}
    <div>
      <div class="h3 title">{#Forums_lastPosts#}</div>
      <div class="box-content">
        {foreach from=$last_post_array item=x}
            <div class="d-flex justify-content-between">
              <h5 class="text-truncate mr-2">
                <a href="{$x->LpLink}"><i class="icon-chat"></i>{$x->LpTitle|truncate:200|sanitize}</a>
              </h5>
              <div class="text-nowrap">
                <i class="icon-calendar"></i>{$x->Datum|date_format:$lang.DateFormat}
              </div>
            </div>
        {/foreach}
      </div>
    </div>
{/if}
