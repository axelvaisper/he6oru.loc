{include file="$incpath/forums/user_panel.tpl"}
<div class="wrapper">
  {$navigation}
</div>

<h1 class="title">{#Mess_Move#}</h1>
<div class="box-content">
  <div class="wrapper">
    {#Forums_SelectForumsToMove#}
  </div>

  <span class="font-weight-bold">{$item->title}</span> {#Forums_Label_moveitem#}

  <form action="index.php?p=forums&amp;action=move&amp;subaction=commit" method="post">
    <div class="form-row">
      <div class="col-md-3 form-group">
        <select class="form-control" name="dest">
          {if $smarty.request.item eq "c"}
              <option value="0"></option>
          {/if}
          {foreach from=$categories_dropdown item=category}
              <optgroup label="{$category->title}">
                {foreach from=$category->forums item=forum_dropdown}
                    {if $forum_dropdown->category_id == 0}
                        <option class="text-mutted" value="{$forum_dropdown->id}" disabled="disabled">{$forum_dropdown->visible_title} </option>
                    {else}
                        <option value="{$forum_dropdown->id}"{if isset($smarty.request.fid) && $smarty.request.fid == $forum_dropdown->id} selected="selected" {/if}>{$forum_dropdown->visible_title} </option>
                    {/if}
                {/foreach}
              </optgroup>
          {/foreach}
        </select>
      </div>

      <div class="col-md-auto">
        <input class="btn btn-primary btn-block-sm form-group" type="submit" value="{#ButtonSend#}" />
        <input class="btn btn-primary btn-block-sm form-group" type="button" value="{#Forums_JumpBack#}" onclick="history.go(-1);" />
      </div>
    </div>

    <input type="hidden" name="item" value="{$smarty.request.item}" />
    <input type="hidden" name="id" value="{$smarty.request.id}" />
    <input type="hidden" name="fid" value="{$smarty.request.fid}" />
  </form>
</div>

{include file="$incpath/forums/footer.tpl"}
