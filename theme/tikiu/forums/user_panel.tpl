<div class="box-content">
  <div class="row">
    <div class="col wrapper">
      {if !$loggedin}
          {$welcome}, {#Forums_Welcome_Guest#}
      {else}
          <div>
            {$welcome}, <span class="font-weight-bold">{$smarty.session.user_name}</span>!
          </div>
          {#Forums_Welcome2#}
      {/if}
    </div>
    <div class="col-auto">
      {if $loggedin}
          {include file="$incpath/forums/userpanel.tpl"}
      {else}
          {include file="$incpath/forums/login.tpl"}
      {/if}
    </div>
  </div>
</div>

<div class="box-content">
  <div class="d-flex flex-wrap justify-content-center text-nowrap">
    {if get_active('forums')}
        <a class="mx-2" href="index.php?p=showforums">
          <i class="icon-chat"></i>{#Forums_Title#}
        </a>
    {/if}
    {if permission('showuserpage')}
        <a class="mx-2" href="index.php?p=members&amp;area={$area}">
          <i class="icon-user"></i>{#Users#}
        </a>
    {/if}
    {if get_active('calendar')}
        <a class="mx-2" href="index.php?p=calendar&amp;month={$smarty.now|date_format:'m'}&amp;year={$smarty.now|date_format:'Y'}&amp;area={$area}&amp;show=public">
          <i class="icon-calendar"></i>{#Calendar#}
        </a>
    {/if}
    {if get_active('forums')}
        <a class="mx-2" href="index.php?p=forum&amp;action=help">
          <i class="icon-info-circled"></i>{#Help_General#}
        </a>
        <a class="mx-2" id="search_link" onclick="toggleContent('search_link', 'search_content');" href="javascript:void(0);">
          <i class="icon-search"></i>{#Search#}
        </a>
        <div class="p-2 status" id="search_content" style="display:none">
          <form action="index.php" method="get">
            <input type="hidden" name="p" value="forum" />
            <input type="hidden" name="action" value="xsearch" />
            <input type="hidden" name="user_name" value="" />
            <input type="hidden" name="search_post" value="1" />

            <div class="input-group">
              <input class="form-control" name="pattern" type="text" value="{$smarty.request.pattern|escape}" required="required" />
              <span class="input-group-append">
                <input class="btn btn-primary btn-sm" type="submit" value="{#Search#}" />
              </span>
            </div>

          </form>
          <a href="index.php?p=forum&amp;action=search_mask">{#ExtendedSearch#}</a>
        </div>
        <a class="mx-2" href="index.php?p=forum&amp;action=print&amp;what=lastposts">
          <i class="icon-warning-empty"></i>{#Forums_NewPostings#}
        </a>
        <a class="mx-2" href="index.php?p=forum&amp;action=show&amp;unit=h&amp;period=24">
          <img src="{$imgpath_forums}threads_last24.png" alt="" /> {#Forums_ShowLastActiveShort#}
        </a>
        <a class="mx-2" href="index.php?p=forum&amp;action=print&amp;what=topicsempty">
          <img src="{$imgpath_forums}threads_last24.png" alt="" /> {#Forums_ThreadsEmpty#}
        </a>
        {if $loggedin}
            <a class="mx-2" href="index.php?p=forum&amp;action=print&amp;what=subscription">
              <i class="icon-star-empty"></i>{#Forums_ShowAllAbos#}
            </a>
            <a class="mx-2" href="index.php?p=forum&amp;action=print&amp;what=posting&amp;id={$smarty.session.benutzer_id}">
              <img src="{$imgpath_forums}small_ownposts.png" alt="" /> {#Forums_ShowOwnPosts#}
            </a>
            <a class="mx-2" href="index.php?p=forum&amp;action=markread&amp;what=forum&amp;ReadAll=1">
              <i class="icon-ok"></i>{#Forums_MarkForumsRead#}
            </a>
        {/if}
    {/if}
  </div>
</div>
