<div class="row text-nowrap">
  <div class="col-auto">
    <form method="post" name="logout_form_up" action="index.php">
      <input type="hidden" name="p" value="userlogin" />
      <input type="hidden" name="action" value="logout" />
      <input type="hidden" name="backurl" value="{page_link|base64encode}" />
    </form>
    {if get_active('pn')}
        <a class="d-block" href="index.php?p=pn">
          <i class="icon-chat"></i>{#PN_inbox#} {newpn}
        </a>
    {/if}
    {if get_active('ignorelist')}
        <a class="d-block" href="index.php?p=forum&amp;action=ignorelist">
          <i class="icon-user-times"></i>{#Ignorelist#}
        </a>
    {/if}
    {if get_active('calendar')}
        <a class="d-block" href="index.php?p=calendar&amp;month={$smarty.now|date_format:'m'}&amp;year={$smarty.now|date_format:'Y'}&amp;area={$area}&amp;show=private">
          <i class="icon-calendar"></i>{#UserCalendar#}
        </a>
    {/if}
  </div>

  <div class="col-auto">
    <a class="d-block" onclick="return confirm('{#Confirm_Logout#}');" href="javascript: document.forms['logout_form_up'].submit();">
      <i class="icon-off"></i>{#Logout#}
    </a>
    <a class="d-block" href="index.php?p=useraction&amp;action=changepass">
      <i class="icon-lock"></i>{#ChangePass#}
    </a>
    <a class="d-block" href="index.php?p=useraction&amp;action=profile">
      <i class="icon-pencil"></i>{#Profile#}
    </a>
    {if permission('adminpanel')}
        <a class="d-block" href="admin/" target="_blank">
          <i class="icon-user-secret"></i>{#AdminLink#}
        </a>
    {/if}
  </div>
</div>
