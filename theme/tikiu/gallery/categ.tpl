{script file="$jspath/jsuggest.js" position='head'}
<script>
<!-- //
$(function () {
    $('#qs').suggest('index.php?p=gallery&action=categquicksearch&key=' + Math.random());
});
//-->
</script>

<div class="wrapper">
  <h1 class="title">{#Gallery_Name#}</h1>

  <form method="post" action="index.php?p=gallery&amp;area={$area}">
    <div class="input-group">
      <input class="form-control" type="text" name="q" id="qs" value="{$smarty.request.q|urldecode|sanitize|replace:'empty': ''}" />
      <select class="form-control" name="ascdesc">
        <option value="desc"{if $smarty.request.ascdesc == 'desc'} selected="selected"{/if}>{#desc_t#}</option>
        <option value="asc"{if $smarty.request.ascdesc == 'asc'} selected="selected"{/if}>{#asc_t#}</option>
      </select>
      <select class="form-control" name="searchtype">
        <option value="full"{if $smarty.request.searchtype == 'full'} selected="selected"{/if}>{#SearchFull#}</option>
        <option value="tags"{if $smarty.request.searchtype == 'tags'} selected="selected"{/if}>{#SearchTags#}</option>
      </select>
      <span class="input-group-append">
        <input class="btn btn-primary" type="submit" value="{#Search#}" />
      </span>
    </div>
    <input name="log" type="hidden" value="1" />
  </form>

  {foreach from=$galleries item=item}
      <div class="box-content">
        <h3 class="listing-head text-truncate">
          <a href="index.php?p=gallery&amp;action=showincluded&amp;categ={$item->Id}&amp;name={$item->Name|translit}&amp;area={$area}">{$item->Name|sanitize}</a>
        </h3>
        <div class="d-flex">
          <div class="mr-3" data-toggle="tooltip" title="{$item->Text|tooltip}">
            {if !empty($item->Bild)}
                <a href="index.php?p=gallery&amp;action=showincluded&amp;categ={$item->Id}&amp;name={$item->Name|translit}&amp;area={$area}">
                  <img class="img-thumbnail" src="uploads/galerie_icons/{$item->Bild}" alt="{$item->Name|translit}" />
                </a>
            {else}
                <img class="img-fluid" src="uploads/other/noimage.png" alt="" />
            {/if}
          </div>

          <div>
            {if !empty($item->Text)}
                <div class="spacer">
                  {$item->Text|html_truncate:200}
                </div>
            {/if}

            {if $item->subGalleries}
                <div class="spacer">
                  {foreach from=$item->subGalleries name=c item=child}
                      <a class="mr-2 text-nowrap" href="{$child->Link}">
                        <i class="icon-folder-open"></i>{$child->SubGalName|sanitize}
                      </a>
                  {/foreach}
                </div>
            {/if}
            {if $item->Tags}
                <div class="spacer">
                  {foreach from=$item->Tags item=ttags name=tag}
                      {if !empty($ttags)}
                          <a class="mr-2 text-nowrap" href="index.php?p=gallery&amp;q={$ttags|urlencode|tagchars}&amp;searchtype=tags{$def_sort_n}&amp;page=1&amp;area={$area}">
                            <i class="icon-tags"></i>{$ttags|tagchars}
                          </a>
                      {/if}
                  {/foreach}
                </div>
            {/if}
          </div>
        </div>
      </div>
  {/foreach}

  {if !empty($GalNavi)}
      <div class="wrapper">
        {$GalNavi}
      </div>
  {/if}

  {if $tagCloud}
      <div class="h3 title">{#Tagcloud#}</div>
      <div class="box-content d-flex flex-wrap align-items-center">
        {foreach from=$tagCloud item=tC}
            <div class="{$tC->Class} mr-3">
              <a href="index.php?p=gallery&amp;q={$tC->Name|urlencode|tagchars}&amp;searchtype=tags{$def_sort_n}&amp;page=1&amp;area={$area}">
                <i class="icon-tags"></i>{$tC->Name|tagchars}
              </a>
            </div>
        {/foreach}
      </div>
  {/if}
</div>
