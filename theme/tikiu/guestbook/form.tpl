{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
{include file="$incpath/other/jsform.tpl"}

$(function() {
{if !empty($error)}
    location.href = '#error';
{/if}
    $('#cf').validate({
        rules: {
            {if !$loggedin}
            Autor: { required: true },
            Email: { required: true, email: true },
            {/if}
            Webseite: { url: true },
            text: { required: true, minlength: 10 }
        },
        submitHandler: function() {
            document.forms['f'].submit();
        }
    });
});
//-->
</script>

<div class="h3 title">{#Guestbook_new#}</div>
<div class="box-content">
  {if !empty($error)}
      <div class="box-error" id="error">
        {foreach from=$error item=err}
            <div>{$err}</div>
        {/foreach}
      </div>
  {/if}

  <form method="post" action="" name="f" id="cf" onsubmit="closeCodes();">
    {if !$loggedin}
        <div class="row form-group">
          <div class="col-md-3 col-sm-12">
            <label class="col-form-label" for="g-autor">{#Contact_myName#}</label>
          </div>
          <div class="col-md-9 col-sm-12">
            <input class="form-control" id="g-autor" name="Autor" type="text" value="{$smarty.post.Autor|sanitize}" />
          </div>
        </div>
        <div class="row form-group">
          <div class="col-md-3 col-sm-12">
            <label class="col-form-label" for="g-mail">{#Comment_YourEmail#}</label>
          </div>
          <div class="col-md-9 col-sm-12">
            <input class="form-control" id="g-mail" name="Email" type="mail" value="{$smarty.post.Email|sanitize}" />
          </div>
        </div>
    {else}
        <input name="Autor" type="hidden" value="{$smarty.session.user_name}" />
        <input name="Email" type="hidden" value="{$smarty.session.login_email}" />
    {/if}

    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="g-site">{#Web#}{#GlobalOption#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <input class="form-control" id="g-site" name="Webseite" type="text" value="{$smarty.post.Webseite|sanitize}" />
      </div>
    </div>
    <div class="row form-group">
      <div class="col-md-3 col-sm-12">
        <label class="col-form-label" for="g-town">{#Town#}{#GlobalOption#}</label>
      </div>
      <div class="col-md-9 col-sm-12">
        <input class="form-control" id="g-town" name="Herkunft" type="text" value="{$smarty.post.Herkunft|sanitize}" />
      </div>
    </div>
    {if $settings.KommentarFormat == 1}
        <div class="row">
          <div class="col-md-12">
            {if $settings.SysCode_Smilies == 1}
                {$listemos}
            {/if}
            {include file="$incpath/comments/format.tpl"}
          </div>
        </div>
    {/if}
    <div class="row form-group">
      <div class="col-md-12">
        <label class="sr-only" for="msgform">{#GlobalMessage#}</label>
        <textarea class="form-control" id="msgform" name="text" rows="5">{$smarty.post.text|escape:html}</textarea>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        {include file="$incpath/other/captcha.tpl"}
      </div>
    </div>
    <div class="text-center">
      <input class="btn btn-primary" type="submit" onclick="closeCodes();" value="{#ButtonSend#}" />
      <input class="btn btn-secondary" type="button" onclick="closeCodes(); countComments({$settings.Kommentar_Laenge});" value="{#Comment_ButtonChecklength#}" />
    </div>
    <input type="hidden" name="Redir" value="{page_link}" />
    <input type="hidden" name="Eintrag" value="1" />
    <input type="hidden" name="id" value="{$smarty.request.id}" />
  </form>
</div>
