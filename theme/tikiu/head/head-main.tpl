<head>
<meta name="robots" content="{$robots}" />
<meta charset="{$charset}" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>{$pagetitle}</title>
<meta name="keywords" content="{$keywords}" />
<meta name="description" content="{$description}" />

<meta name="publisher" content="{$settings.Seitenbetreiber}" />
<meta name="generator" content="{#meta_generator#}" />
<link rel="shortcut icon" href="{$baseurl}/favicon.ico" />

{if $settings.meta_google == 1 && !empty($settings.code_google)}<meta name="google-site-verification" content="{$settings.code_google}" />{/if}

{if $settings.meta_yandex == 1 && !empty($settings.code_yandex)}<meta name="yandex-verification" content="{$settings.code_yandex}" />{/if}

{* <a rel="author" href="ССЫЛКА НА G+ АККАУНТ">ИМЯ ФАМИЛИЯ</a>  *}

<meta property="og:title" content="{$pagetitle}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{$baseurl}" />
<meta property="og:image" content="{$baseurl}/uploads/content/onas.jpg" />

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="{$pagetitle}">
<meta name="twitter:description" content="{$description}">
<meta name="twitter:image" content="{$baseurl}/uploads/content/onas.jpg">

{if !empty($canonical)}<link rel="canonical" href="{$baseurl}/{$canonical}" />{/if}

<link type="application/atom+xml" rel="alternate" title="{$settings.Seitenname|sanitize}" href="{$baseurl}/index.php?p=rss&amp;area={$area}" />

{if get_active('News')}<link type="application/atom+xml" rel="alternate" title="{#Newsarchive#}" href="{$baseurl}/index.php?p=rss&amp;area={$area}&amp;action=news" />{/if}

{if get_active('articles')}<link type="application/atom+xml" rel="alternate" title="{#Gaming_articles#}" href="{$baseurl}/index.php?p=rss&amp;area={$area}&amp;action=articles" />{/if}

{if get_active('forums')}<link type="application/atom+xml" rel="alternate" title="{#Forums_Title#}" href="{$baseurl}/index.php?p=rss&amp;area={$area}&amp;action=forum" />{/if}

{* СТИЛИ *}

{include file="$incpath/head/headcss.tpl"}
{* {style file="{$csspath}/bootstrap.min.css" position='head' priority='900'} *}
{* {style file="{$csspath}/bootstrap-grid.css" position='head' priority='900'} *}
{style file="{$csspath}/uikit.min.css" position='head' priority='1000'}
{style file="{$csspath}/uikit-custom.css" position='head' priority='1000'}
{style file="{$csspath}/style.css" position='head' priority='1000'}
{* {if get_active('forums')}    {style file="{$csspath}/forum.css"      position='head' priority='1000'}{/if} *}
{if get_active('calendar')}  {style file="{$csspath}/calendar.css"   position='head' priority='1000'}{/if}
{* {if get_active('shop')}      {style file="{$csspath}/shop.css"       position='head' priority='1000'}{/if} *}
{if get_active('partners')}  {style file="{$csspath}/partner.css"    position='head' priority='200'} {/if}
{if get_active('newsletter')}{style file="{$csspath}/newsletter.css" position='head' priority='200'} {/if}
{* {style file="{$csspath}/colorbox.css" position='head' priority='800'} *}
{* {style file="{$themepath}/font/fontello/css/fontello.css" position='head' priority='700'} *}

{* СКРИПТЫ *}

{script file="{$themepath}/js/jquery-3.3.1.min.js" position='head' priority='1000'}
{* {script file="{$themepath}/js/bootstrap.bundle.min.js" position='head' priority='1000'} *}
{script file="{$themepath}/js/uikit.min.js" position='head' priority='900'}
{script file="{$themepath}/js/uikit-icons.min.js" position='head' priority='800'}

{script file="{$themepath}/js/vk_openapi.js" position='head' priority='800'}

{script file="{$jspath}/jpatch.js" position='head' priority='1000'}
{* {script file="{$jspath}/jcolorbox.js" position='head' priority='800'} *}
{script file="{$jspath}/jcookie.js" position='head' priority='800'}
{script file="{$jspath}/jform.js" position='head' priority='800'}
{script file="{$jspath}/jtextcopy.js" position='head' priority='800'}
{if get_active('shop_bewertung')}{script file="{$themepath}/js/rating.js" position='800'}{/if}

{script file="{$themepath}/js/site.js" position='head' priority='800'}

{* <script src="//vk.com/js/api/openapi.js?154"></script> *}

{result type='style'  format='file' position='head'}
{result type='style'  format='code' position='head'}
{result type='script' format='file' position='head'}
{result type='script' format='code' position='head'}

<script>
<!-- //

  VK.Retargeting.Init('VK-RTRG-246785-DsTo');
  VK.Retargeting.Hit();

$(function() {

  {* аним.загрузки *}
  $('#preloader').fadeOut('slow', function () { $(this).remove(); });

  // $('[data-toggle="tooltip"]').tooltip();
  {if get_active('shop_bewertung')}$('.rating-star').rating();{/if}
  $('body').textcopy({ text: '{#MoreDetails#}' });

});
//-->
</script>

{result type='code' format='code' position='head'}
</head>
