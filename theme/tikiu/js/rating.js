/*
 ############################################################################
 # ********************  SX CONTENT MANAGEMENT SYSTEM  ******************** #
 # *   Copyright © 2009-2018 * Alexander Voloshin * All Rights Reserved   * #
 # ************************************************************************ #
 # *  http://www.sx-cms.ru  *  License GNU GPL v3  *    info@sx-cms.ru    * #
 # ************************************************************************ #
 ############################################################################
 */

(function () {
    'use strict';

    var defaults = {
        rating: 0,     // Число отображаемого рейтинга        | data-rating="..."
        active: false, // Активность выбора рейтинга          | data-rating-active="..."
        stars: 5,      // Количество отображаемых звезд       | data-rating-stars="..."
        input: ''      // Имя поля input для работы с формами | data-rating-input="..."
    };

    if (window.jQuery) {
        jQuery.fn.rating = function (options) {
            var ratings = [];
            var objects = this;
            if (objects.length) {
                for (var i = 0; i < objects.length; i++) {
                    ratings.push(new sxRating(objects[i], options));
                }
            }
        };
    }

    class sxRating {

        constructor(element, options) {
            this.element = $(element);
            this.options = $.extend({}, defaults, options);
            this.init();
        }

        init() {
            this.data();
            this.html();

            this.live(this.options.rating);

            if (this.options.active === true) {
                this.element.children().on('click', {self: this}, function (e) {
                    e.data.self.save($(this));
                });

                this.element.children().on('mouseenter', {self: this}, function (e) {
                    e.data.self.live($(this).data('rating'));
                });

                this.element.children().on('mouseleave', {self: this}, function (e) {
                    e.data.self.live(e.data.self.options.rating);
                });
            }
        }

        data() {
            var rating = this.element.data('rating');
            if (rating) {
                this.options.rating = rating;
            }

            var active = this.element.data('rating-active');
            if (active) {
                this.options.active = active;
            }

            var stars = this.element.data('rating-stars');
            if (stars) {
                this.options.stars = stars;
            }

            var input = this.element.data('rating-input');
            if (input) {
                this.options.input = input;
            }
        }

        html() {
            if (this.options.stars) {
                var html = '';
                for (var i = 1; i <= this.options.stars; i++) {
                    html += '<i data-uk-icon="star-line" data-rating="' + i + '"></i>';
                }
                if (this.options.input) {
                    html += '<input name="' + this.options.input + '" value="' + this.options.rating + '" type="hidden" />';
                }
                this.element.append(html).addClass('rating-star');
            }
        }

        save(element) {
            var rating = element.data('rating');
            if (rating) {
                this.live(rating);
                this.options.rating = rating;
                $('.rating-star input[name="' + this.options.input + '"]').val(rating);
            }
        }

        live(rating) {
            var children = this.element.children();
            for (var i = 0; i < this.options.stars; i++) {
                var child = children[i];
                if (i < rating) {
                    $(child).attr('data-uk-icon','star');
                } else {
                    $(child).attr('data-uk-icon','star-line');
                }
            }
        }
    }

})();
