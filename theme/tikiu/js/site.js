//on scroll off hover
var root = document.documentElement, timer;
window.addEventListener( 'scroll', function() {
clearTimeout( timer );
if ( !root.style.pointerEvents ) {
    root.style.pointerEvents = 'none';
}
timer = setTimeout(function() {
    root.style.pointerEvents = '';
}, 500 );
}, false );
//to top and back прпрар рвпв павп ва
var bottom_position = 0;
var flag_bottom = false;
var flag_animate = false;
$(function () {
  var backTop = $('<div class="in_top uk-visible@s"><span>⬆</span></div>');
 $('body').append(backTop);
  $('.in_top').click(function(){
    flag_animate = true;
    if(flag_bottom){
      $("body,html").animate({"scrollTop":bottom_position}, 250, function(){
        flag_animate = false;
      });
      flag_bottom = false;
      $('.in_top span').html('⬆');
    }else{
      $("body,html").animate({"scrollTop":0}, 200, function(){
        flag_animate = false;
      });
      bottom_position = $(window).scrollTop();
      flag_bottom = true;
      $('.in_top span').html('⬇');
    }
  });
  $(window).scroll(function(event){
    var countScroll = $(window).scrollTop();
    if (countScroll > 100 && !flag_animate){
      $('.in_top').show();
      if(flag_bottom){
        flag_bottom = false;
        $('.in_top span').html('⬆');
      }
    }else{
      if(!flag_bottom){
        $('.in_top').hide();
      }
    }
  });
});

function openWindow(seite, name, w, h, scroll) {
    if (typeof w === 'undefined' || w === '') {
        w = screen.width;
    }
    if (typeof h === 'undefined' || h === '') {
        h = screen.height;
    }
    var left = screen.width ? (screen.width - w) / 2 : 0;
    var top = screen.height ? (screen.height - h) / 2 : 0;
    var settings = 'height=' + h + ',width=' + w + ',top=' + top + ',left=' + left + ',scrollbars=' + scroll + ',resizable';
    window.open(seite, name, settings);
}
function multiCheck() {
    var obj = document.kform;
    for (var i = 0; i < obj.elements.length; i++) {
        var e = obj.elements[i];
        if (e.name !== 'allbox' && e.type === 'checkbox' && !e.disabled) {
            e.checked = obj.allbox.checked;
        }
    }
}
function toggleContent(click, target) {
    $(function () {
        var timer = 0;
        $('#' + target).css({
            'position': 'absolute',
            'left': $('#' + click).offset().left + 'px',
            'top': ($('#' + click).offset().top + $('#' + click).height()) + 'px'
        }).slideToggle(300);
        $(document).on('click', function (e) {
            if ($(e.target).closest('#' + click + ', #' + target).length === 0) {
                $('#' + target).slideUp(300);
                e.stopPropagation();
            }
        });
        $('#' + click + ', #' + target).mouseover(function () {
            clearTimeout(timer);
        }).mouseout(function () {
            clearTimeout(timer);
            timer = setTimeout(function () {
                $('#' + target).slideUp(300);
            }, 1000);
        });
    });
}
function toggleSpoiler(elem) {
    $(function () {
        $(elem).next().slideToggle();
        $(elem).children('i').toggleClass('icon-plus-squared-alt').toggleClass('icon-minus-squared-alt');
    });
}
function closeWindow(reload) {
    $(function () { 
      parent.window.location.reload(false);
    });
  }
function newWindow(url, width, height) {
    $(function () {
        $.colorbox({
            href: url,
            width: width + 'px',
            height: height + 'px',
            iframe: true
        });
    });
}
