{script file="$jspath/jsuggest.js" position='head'}
<script>
<!-- //
$(function () {
    $('#ms').suggest('index.php?p=manufacturer&action=quicksearch&key=' + Math.random());

    $('.linkextern').on('click', function () {
        var options = {
            target: '#plick',
            url: 'index.php?action=updatehitcount&p=manufacturer&id=' + $(this).data('id'),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>

<div id="plick"></div>
<div class="wrapper">
  <h1 class="title">{#Manufacturers#}</h1>
  {if $items}
      <form method="post" action="index.php?p=manufacturer&amp;area={$area}">
        <div class="input-group">
          <input class="form-control" id="ms" name="q" value="{$smarty.request.q|sanitize}" type="text" />
          <span class="input-group-append">
            <input class="btn btn-primary" type="submit" value="{#Manufacturer_search#}" />
          </span>
        </div>
      </form>

      <div class="spacer flex-line justify-content-center">
        <span class="mr-3">{#SortBy#}:</span>
        <a class="mr-3" href="index.php?p=manufacturer&amp;area={$area}&amp;page={$smarty.request.page|default:1}&amp;sort={$datesort|default:'dateasc'}{if !empty($smarty.request.q)}&amp;q={$smarty.request.q}{/if}">
          {#SortDate#}<i class="{$img_date|default:'icon-sort'}"></i>
        </a>
        <a class="mr-3" href="index.php?p=manufacturer&amp;area={$area}&amp;page={$smarty.request.page|default:1}&amp;sort={$namesort|default:'nameasc'}{if !empty($smarty.request.q)}&amp;q={$smarty.request.q}{/if}">
          {#SortName#}<i class="{$img_name|default:'icon-sort'}"></i>
        </a>
        <a class="mr-3" href="index.php?p=manufacturer&amp;area={$area}&amp;page={$smarty.request.page|default:1}&amp;sort={$hitssort|default:'hitsdesc'}{if !empty($smarty.request.q)}&amp;q={$smarty.request.q}{/if}">
          {#Global_Hits#}<i class="{$img_hits|default:'icon-sort'}"></i>
        </a>
      </div>

      {foreach from=$items item=res}
          <div class="box-content">
            <h3 class="listing-head">
              <a class="linkextern" data-id="{$res->Id}" title="{$res->Name|sanitize}" href="index.php?p=manufacturer&amp;area={$area}&amp;action=showdetails&amp;id={$res->Id}&amp;name={$res->Name|translit}">{$res->Name|sanitize}</a>
            </h3>
            <div class="clearfix">
              {if !empty($res->Bild)}
                  <a href="index.php?p=manufacturer&amp;area={$area}&amp;action=showdetails&amp;id={$res->Id}&amp;name={$res->Name|translit}">
                    <img class="img-fluid listing-img-right" src="uploads/manufacturer/{$res->Bild}" alt="{$res->Name|sanitize}" />
                  </a>
              {/if}
              <div class="text-justify">
                {$res->Beschreibung|truncate:500}
              </div>
            </div>

            <div class="listing-foot flex-line justify-content-center">
              <span class="mr-4" data-toggle="tooltip" title="{#Added#}"><i class="icon-calendar"></i>{$res->Datum|date_format:$lang.DateFormatSimple}</span>
              <span class="mr-4" data-toggle="tooltip" title="{#Global_Hits#}"><i class="icon-eye"></i>{$res->Hits}</span>
              <span class="mr-4" data-toggle="tooltip" title="{#Products#}"><i class="icon-basket"></i>{$res->ProdCount}</span>
              <a href="index.php?p=manufacturer&amp;area={$area}&amp;action=showdetails&amp;id={$res->Id}&amp;name={$res->Name|translit}">
                <i class="icon-right-open"></i>{#MoreDetails#}
              </a>
            </div>
          </div>
      {/foreach}
  {/if}
</div>

{if !empty($Navi)}
    <div class="wrapper">
      {$Navi}
    </div>
{/if}
