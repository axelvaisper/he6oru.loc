{if !empty($SiteNavigation)}
<nav data-uk-navbar="mode:click" class="zindex-2 uk-navbar-container" data-uk-sticky="show-on-up: true; animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-primary uk-box-shadow-small uk-navbar-sticky; cls-inactive: uk-navbar-transparent; top: 0">

    {*MOBILE NAV links*}
    <div class="uk-navbar-left overlay-2 uk-hidden@m uk-visible">
      <a href="#" data-uk-toggle="target: #offcanvas-mobnav"
      class="uk-margin-small-left" data-uk-icon="icon:menu; ratio: 2"></a>
    </div>

    <div class="uk-navbar-center overlay-2 uk-hidden@m uk-visible">
      <a href="/" class="uk-navbar-item" data-uk-tooltip title="На главную">
        <img src="{$imgpath}/page/logo.svg" width="45" height="45" alt="">
      </a>
    </div>

    {* <div class="uk-navbar-right overlay-2 uk-hidden@m uk-visible">
      <a class="uk-navbar-item uk-button uk-text-bold uk-button-text" href="#order" >Связаться</a>
    </div> *}
    {*/MOBILE NAV*}

  {* PC NAV *}
  <div class="uk-navbar-left overlay-2  uk-text-center uk-visible@m">
    <a href="/" class="uk-navbar-item uk-logo" data-uk-tooltip title="На главную">
      <img src="{$imgpath}/page/logo.svg" width="45" height="45" alt="">
      <div class="uk-margin-small-left uk-dark uk-margin-small-bottom uk-visible@l">
        <small>{$settings.Firma|sanitize}</small>
        <h6 class="uk-margin-remove uk-text-bold uk-text-uppercase">{$settings.Seitenname|sanitize}</h6>
      </div>
    </a>
  </div>

  <div class="uk-navbar-center overlay-2  uk-visible@m">
  <ul class="uk-navbar-nav uk-text-bold">

    {* навигация пк*}
    {foreach from=$SiteNavigation item=navi}
    {if empty($navi->sub_navi)}
      <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
        <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}" >
          {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
          {$navi->title|sanitize}<br>
          {$navi->AltTitle|sanitize}
        </a>
      </li>
    {else}
      <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
        <a href="{$navi->document|escape:'html'}">
          {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
          {$navi->title|sanitize}<br>
          {$navi->AltTitle|sanitize}
          <span class="uk-margin-small-left" data-uk-icon="icon:chevron-down;ratio:1"></span>
        </a>
        <div class="uk-margin-remove uk-padding-remove" data-uk-dropdown="animation: uk-animation-slide-top-small; mode:click">
          <ul class="uk-nav uk-navbar-dropdown-nav">
          {foreach from=$navi->sub_navi item=sub_navi}
            <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
              <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                {* <i class="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
                {$sub_navi->title|sanitize}<br>
                {$sub_navi->AltTitle|sanitize}
              </a>
            </li>
          {/foreach}
          </ul>
        </div>
      </li>
    {/if}
    {/foreach}
    <li><a href="index.php?p=articles&area=1">Статьи</a></li>

  </ul>
  </div>

  {* <div class="uk-visible@m uk-navbar-right overlay-2">
    <ul class="uk-navbar-nav uk-text-bold">
      <li><a class="uk-navbar-item" href="#order" >Связаться</a></li>
    </ul>
  </div> *}



  {* overlay search *}
  <div class="uk-navbar-left uk-flex-1 uk-background-default uk-box-shadow-small uk-dark overlay-2" hidden>
    <div class="uk-navbar-item uk-width-expand">
      {$shop_search_small}
    </div>
    <a class="uk-navbar-toggle" data-uk-icon="icon:close; ratio: 2" data-uk-toggle="target:.overlay-2" href="#"></a>
  </div>

</nav>

{*MOB. AND PC OFFCANVAS SIDE LEFT*}
<div id="offcanvas-mobnav" data-uk-offcanvas="overlay: true; mode: slide;">
  <div class="uk-offcanvas-bar uk-padding-remove">

    <button class="uk-offcanvas-close uk-width-1-1" type="button" data-uk-close></button>

    <div>
    <ul class="uk-nav-primary uk-nav-parent-icon" data-uk-nav>

    {* моб.навигация *}
    {foreach from=$SiteNavigation item=navi}
    {if empty($navi->sub_navi)}
        <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}">
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}
            {$navi->AltTitle|sanitize}
          </a>
        </li>
    {else}
        <li class="uk-parent {if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a {* href="{$navi->document|escape:'html'}" *} class="top-item">
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}
            {$navi->AltTitle|sanitize}
          </a>
            <ul class="uk-nav-sub">
            {foreach from=$navi->sub_navi item=sub_navi}
              <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
                <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                  {* <i class="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
                  {$sub_navi->title|sanitize}
                  {$sub_navi->AltTitle|sanitize}
                </a>
              </li>
            {/foreach}
            </ul>
        </li>
        {/if}
    {/foreach}
      </ul>
    </div>
  </div>
</div>

{/if}
