{if !empty($SiteNavigation)}
{* <h4>{$navi_title}</h4> *}
<ul>
  {foreach from=$SiteNavigation item=navi}
      {if empty($navi->sub_navi)}
          <li class="{if !empty($navi->active) || $navi->document == $document} active{/if}">
            <a title="{$navi->AltTitle|sanitize}" href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}">
              <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> {$navi->title|sanitize}
            </a>
          </li>
      {else}
          <li class="{if !empty($navi->active) || $navi->document == $document} active{/if}">
            <a href="{$navi->document|escape:'html'}">
              <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> {$navi->title|sanitize}
            </a>
            <div >
              {foreach from=$navi->sub_navi item=sub_navi}
                  <a class="{if !empty($sub_navi->active) || $sub_navi->document == $document} active{/if}" title="{$sub_navi->AltTitle|sanitize}" href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                    <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> {$sub_navi->title|sanitize}
                  </a>
              {/foreach}
            </div>
          </li>
      {/if}
  {/foreach}
</ul>

{/if}