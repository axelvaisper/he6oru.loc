<nav data-uk-navbar="mode:click" class="uk-hidden@m uk-visible uk-navbar-container uk-container uk-container-large zindex-3" data-uk-sticky="show-on-up: true; animation: uk-animation-slide-bottom; sel-target: .uk-navbar-container; cls-active: uk-sticky-fixed fixed-bottom uk-box-shadow-small; cls-inactive: uk-navbar-transparent;">

  {* LEFT *}
  <div class="uk-navbar-left">
    <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-categ" href="javascript:void(0)" data-uk-tooltip title="{#Title_Navi#}" data-uk-icon="icon:menu; ratio: 1">
    </a>

    <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-search" href="javascript:void(0)" data-uk-tooltip title="{#Search#}" data-uk-icon="icon:search; ratio: 1">
    </a>
  </div>

  {* CENTER *}
  <div class="uk-navbar-center">
    <a {if permission('adminpanel')}
          href="#" data-uk-toggle="target: #offcanvas-admin" title="Админ панель"
        {else}
          href="/" title="На главную"
        {/if}
          class="uk-navbar-item" data-uk-tooltip>

      <img src="{$baseurl}/logo.svg" width="45" height="45" alt="">

      <div class="uk-margin-small-left uk-dark uk-margin-small-bottom uk-visible@s">
      <small>{$settings.Firma|sanitize}</small>
      <h6 class="uk-margin-remove uk-text-bold uk-text-uppercase">{$settings.Seitenname|sanitize}</h6>
      </div>
    </a>
  </div>

  {* RIGHT *}
  <div class="uk-navbar-right">
    {if $loggedin}
      <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-login" data-uk-icon="icon:user; ratio:1.5" title="{#MyAccount#}" href="javascript:void(0)" data-uk-tooltip></a>
    {else}
      <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-login" data-uk-icon="icon:sign-in; ratio:1" href="javascript:void(0)" title="{#LoginExtern#}" data-uk-tooltip></a>
    {/if}

    {if get_active('shop')}
      <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-cart" data-uk-icon="icon:shop-basket; ratio: 1" href="javascript:void(0)" data-uk-tooltip title="{#Shop_myBasket#}"></a>
    {/if}
  </div>

</nav>


{* OFFCANVAS PANELS *}

<div id="offcanvas-categ" data-uk-offcanvas="overlay:true; mode:slide">
  <div class="uk-offcanvas-bar uk-padding-remove uk-flex">
      {navigation id=5 tpl='home/home-vertical-mob.tpl'}
  </div>
</div>

<div id="offcanvas-search" data-uk-offcanvas="flip:false; overlay:true; mode:slide">
  <div class="uk-offcanvas-bar uk-padding-small uk-flex">
      {include file="$incpath/shop/shop_search_menuext.tpl"}
  </div>
</div>
