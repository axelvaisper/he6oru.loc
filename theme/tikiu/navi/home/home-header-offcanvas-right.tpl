{if get_active('shop')}
<div id="offcanvas-cart" data-uk-offcanvas="flip:true; overlay:true; mode:slide">
  <div class="uk-offcanvas-bar uk-padding-small uk-flex">
      <a onclick="{if $basket_products_price > 0 || $basket_products_all >= 1}location.href = '{$baseurl}/index.php?p=shop&amp;action=showbasket';{else}javascript: alert('{#Shop_basket_empty#}'){/if}" href="javascript:void(0);">кор</a>
      {$basket_small}
  </div>
</div>
{/if}


<div id="offcanvas-login" data-uk-offcanvas="flip:true; overlay:true; mode:slide">
  <div class="uk-offcanvas-bar uk-padding-small uk-flex">
      {$user_login}
  </div>
</div>

