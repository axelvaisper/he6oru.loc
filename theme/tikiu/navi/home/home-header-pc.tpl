<nav data-uk-navbar="mode:click" class="uk-visible@m uk-container uk-navbar-container zindex-3" data-uk-sticky="show-on-up: true; animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-sticky-fixed fixed-top uk-background-default uk-box-shadow-large; cls-inactive: uk-navbar-transparent;">

{* LEFT *}
<div class="uk-navbar-left uk-text-center">
  <a {if permission('adminpanel')}
      href="#" data-uk-toggle="target: #offcanvas-admin" title="Админ панель"
      {else}
      href="/" title="На главную"
      {/if}
    class="uk-navbar-item" data-uk-tooltip>

    <img data-src="{$baseurl}/logo.svg" width="45" height="45" alt="" data-uk-img>

    <div class="uk-margin-small-left uk-dark uk-margin-small-bottom">
      <small>{$settings.Firma|sanitize}</small>
      <h6 class="uk-margin-remove uk-text-bold uk-text-uppercase">{$settings.Seitenname|sanitize}</h6>
    </div>
  </a>
</div>

{* CENTER *}
<div class="uk-navbar-center">
  {include file="$incpath/shop/shop_search_menuext.tpl"}
</div>

{* RIGHT *}
<div class="uk-navbar-right">
    {if $loggedin}
    <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-login" data-uk-icon="icon:user; ratio:1.5" title="{#MyAccount#}" href="javascript:void(0)" data-uk-tooltip></a>
  {else}
    <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-login" data-uk-icon="icon:sign-in; ratio:1.5" title="{#LoginExtern#}" href="javascript:void(0)" data-uk-tooltip></a>
  {/if}

  {if get_active('shop')}
    <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-cart" data-uk-icon="icon:shop-basket; ratio: 1.5" data-uk-tooltip title="{#Shop_myBasket#}" href="javascript:void(0)"></a>
  {/if}
</div>

</nav>