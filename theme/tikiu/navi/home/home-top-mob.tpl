{* LEFT *}
<div class="uk-navbar-left uk-hidden@m uk-visible">
  {* выбор валюты *}
  {$curreny_selector}
</div>

{* CENTER *}
<div class="uk-navbar-center uk-hidden@m uk-visible">

</div>

{* RIGHT *}
<div class="uk-navbar-right uk-hidden@m uk-visible">
  {* смена языка *}
  {if !empty($langchooser)}
      {$langchooser}
  {/if}
</div>

