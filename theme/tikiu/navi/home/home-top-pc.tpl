<nav data-uk-navbar="mode:click" class="uk-visible@m uk-container uk-container-large uk-navbar-container uk-navbar-transparent">

{* LEFT *}
<div class="uk-navbar-left">
  <ul class="uk-navbar-nav">
  </ul>
</div>

{* CENTER *}
<div class="uk-navbar-center">
  {* выбор валюты *}
  {$curreny_selector}
</div>

{* RIGHT *}
<div class="uk-navbar-right">
  {* смена языка *}
  {if !empty($langchooser)}
      {$langchooser}
  {/if}
</div>

</nav>

{* {include file="$incpath/navi/home/home-top-offcanvas-right.tpl"} *}
 