<a href="#" class="uk-navbar-item" data-uk-icon="chevron-down">
    Города
</a>
<div class="uk-navbar-dropdown uk-navbar-dropdown-bottom-center"
uk-drop="cls-drop: uk-navbar-dropdown; boundary: .uk-navbar-container; boundary-align: true; pos: bottom-justify; flip: x">
    <div class="uk-navbar-dropdown-grid uk-child-width-expand@m" data-uk-grid>
    <div class="uk-first-column">
        <div data-uk-grid>

        <div class="uk-width-auto@m">

            <ul class="uk-tab-left" data-uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
            {assign var=cid value=$smarty.request.cid}
            {foreach from=$MyShopNavi item=sn}
            <li><a class="{if $cid == $sn->Id || in_array($sn->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
                <span class="uk-margin-small-right">{$sn->Icon}</span>
                {$sn->Entry|sanitize}
            </a></li>
            {/foreach}
            </ul>
        </div>

        <div class="uk-width-expand@m">

            <ul id="component-tab-left" class="uk-switcher">
            {foreach from=$MyShopNavi item=sn}
            <li>
                {if !empty($sn->Sub1) && count($sn->Sub1)}
                <div class="uk-grid-divider uk-child-width-expand@s" data-uk-grid>
                {foreach from=$sn->Sub1 item=sub1}
                <div>
                <ul class="uk-nav">

                <li class="uk-parent"><a class="{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                    {$sub1->Entry|sanitize}</a></li>

                    {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                    {foreach from=$sub1->Sub2 item=sub2}
                    <ul class="uk-nav-sub">

                        <li><a class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                        {$sub2->Name|sanitize}</a></li>

                        {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                        {foreach from=$sub2->Sub3 item=sub3}
                        <ul class="uk-nav-sub">

                            <li><a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                            {$sub3->Name|sanitize}</a></li>

                            {if !empty($sub3->Sub4) && count($sub3->Sub4)}
                            {foreach from=$sub3->Sub4 item=sub4}

                                <li><a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                    {$sub4->Name|sanitize}</a></li>

                            {/foreach}
                            {/if}
                        </ul>
                        {/foreach}
                        {/if}
                    </ul>
                    {/foreach}
                    {/if}

                </ul>
                </div>
                {/foreach}
                </div>
                {/if}

            </li>
            {/foreach}
            </ul>
        </div>

        </div>
    </div>
    </div>
</div>
