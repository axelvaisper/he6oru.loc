{if permission('adminpanel')} 
<li>
  <a href="#">
    <div>
      <i data-uk-icon="form"></i>
      <br>
      <small>Формы</small>
    </div>
  </a>
  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.do) && isset($smarty.request.do) && $smarty.request.do == 'contactforms'}nav_subs_active{else}nav_subs{/if}" href="admin/index.php?do=contactforms">{#Gaming_articles_reviews#}</a></li>
      <li><a class="nav_subs colorbox" title="{#ContactForms_new#}" href="admin/index.php?do=contactforms&amp;sub=new&amp;noframes=1">{#Global_Add#}</a></li>
    </ul>
  </div>
</li>
{/if}
