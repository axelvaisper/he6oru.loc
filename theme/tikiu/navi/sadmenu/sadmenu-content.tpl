{if permission('adminpanel')} 
<li>
  <a href="#">
    <div>
      <i data-uk-icon="file"></i>
      <br>
      <small>Страницы</small>
    </div>
  </a>
  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'content' && isset($smarty.request.sub) && $smarty.request.sub == 'overview'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=content&amp;sub=overview">{#Gaming_articles_reviews#}</a></li>

      <li><a  title="{#Content_new#}" class="colorbox {if isset($smarty.request.do) && $smarty.request.do == 'content' && isset($smarty.request.sub) && $smarty.request.sub == 'addcontent'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=content&amp;sub=addcontent&amp;noframes=1">{#Global_Add#}</a></li>

      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'content' && isset($smarty.request.sub) && $smarty.request.sub == 'categories'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=content&amp;sub=categories">{#Global_Categories#}</a></li>
    </ul>
  </div>
</li>
{/if}
