{if permission('adminpanel')} 
<li>
  <a href="#">
    <div>
      <i data-uk-icon="paste"></i>
      <br>
      <small>Контент</small>
    </div>
  </a>
  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'insert' && isset($smarty.request.sub) && $smarty.request.sub == 'overview'}nav_subs_active{else}nav_subs{/if}" href="admin/index.php?do=insert">{#Gaming_articles_reviews#}</a></li>
      <li><a title="{#Global_Add#}" class="{if isset($smarty.request.do) && $smarty.request.do == 'insert' && isset($smarty.request.sub) && $smarty.request.sub == 'new'}nav_subs_active{else}nav_subs{/if} colorbox" href="admin/index.php?do=insert&amp;sub=new&amp;noframes=1">{#Global_Add#}</a></li>
      <li><a title="{#Global_Add#}" class="{if isset($smarty.request.do) && $smarty.request.do == 'insert' && isset($smarty.request.sub) && $smarty.request.sub == 'new'}nav_subs_active{else}nav_subs{/if} colorbox" href="admin/index.php?do=insert&amp;sub=new&amp;html=1&amp;noframes=1">{#Global_Add#} + CKEditor</a></li>
        <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'phrases'}nav_subs_active{else}nav_subs{/if}" href="admin/index.php?do=phrases">Случайный контент</a></li>
    </ul>
  </div>
</li>
{/if}
