{if permission('adminpanel')} 
<li>
  <a href="#">
    <div>
      <i data-uk-icon="list"></i>
      <br>
      <small>Навигация</small>
    </div>
  </a>
  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
        <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'navigation' && isset($smarty.request.sub) && $smarty.request.sub == 'list'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=navigation&amp;sub=list">Обзор</a></li>
        <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'navigation' && isset($smarty.request.sub) && $smarty.request.sub == 'list'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=navigation&sub=edit&id=1">Редактировать ID 1</a></li>
          {if admin_active('flashtag')}
          <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'navigation' && isset($smarty.request.sub) && $smarty.request.sub == 'flashtag'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=navigation&amp;sub=flashtag">Флешоблако</a></li>
          {/if}
      </ul>
    </div>
</li>
{/if}
