{if perm('settings')}
<li>
  <a href="#">
    <div>
      <i data-uk-icon="sxcms"></i>
      <br>
      <small>{#Forums_Field_options#}</small>
    </div>
  </a>
  <div class="uk-navbar-dropdown">
  <ul class="uk-nav">
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'global'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=global">Настройки</a></li>
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'admin_global'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=admin_global">Панель управления</a></li>
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'sectionsdisplay'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=sectionsdisplay">Секции</a></li>
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'sectionsettings'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=sectionsettings">Модули секции</a></li>
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'widgets'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=widgets">Виджеты секции</a></li>
  <br>
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'languages'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=languages">Язык сайта</a></li>
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'adminlanguages'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=adminlanguages">Язык админки</a></li>
      {if perm('lang_edit')}
      <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'lang_edit'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=lang_edit">Языковые файлы</a></li>
      {/if}
  <br>
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'money'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=money">Монетизация</a></li>
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'secure'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=secure">Настройки капчи</a></li>
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'cron'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=cron">Планировщик заданий</a></li>
    <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'expimp'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=expimp">Экспорт | Импорт</a></li>
  <br>
      {if $smarty.session.benutzer_id == 1}
      <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'phpedit'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=phpedit">Конфиг-файлы</a></li>
      <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'htaccess'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=settings&amp;sub=htaccess">Htaccess настройка </a></li>
      {/if}
    <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'logs'}uk-active{else}nav_subs{/if}" href="?do=settings&amp;sub=logs">Системные сообщения</a></li>
</ul>
</div>
</li>
{/if}
