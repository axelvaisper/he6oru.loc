{if get_active('shop')} 
<li>
  <a href="#">
    <div>
      <i data-uk-icon="shop"></i>
      <br>
      <small>{#Shop#}</small>
    </div>
  </a>
  <div class="uk-navbar-dropdown">
      <ul class="uk-nav">

        <li class="uk-grid-collapse" data-uk-grid>
          <a class="uk-width-expand {if isset($smarty.request.sub) && $smarty.request.sub == 'articles'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=articles">
            {#Products#}
          </a>
          <span class="uk-width-auto" data-uk-lightbox>
            <a class="uk-padding-small" data-type="iframe" class="{if isset($smarty.request.sub) && $smarty.request.sub == 'new'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=new&amp;noframes=1" data-uk-icon="plus"></a>
          </span>
        </li>
 
        <li class="uk-grid-collapse" data-uk-grid>
          <a class="uk-width-expand {if isset($smarty.request.sub) && $smarty.request.sub == 'categories'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=categories">
            {#Global_Categories#}
          </a>
          <span class="uk-width-auto" data-uk-lightbox>
            <a class="uk-padding-small" title="{#Global_AddCateg#}" data-type="iframe" href="/admin/index.php?do=shop&amp;sub=new_categ&amp;noframes=1" data-uk-icon="plus"></a>
          </span>
        </li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'settings'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=settings">{#Forums_Field_options#}</a></li>
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'regions'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=regions">Регионы | Налоги</a></li>
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'infomsg'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=infomsg">Сообщение магазина</a></li>
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'shopinfos'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=shopinfos">Информация магазина</a></li>

        <li><a title="" class="{if isset($smarty.request.sub) && $smarty.request.sub == 'orders'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=orders">{#Shop_myAccountOrd#}</a></li>

        <li><a title="{#Shop_showmoney_title#}" class="colorbox {if isset($smarty.request.sub) && $smarty.request.sub == 'showmoney'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=showmoney&amp;noframes=1">Оборот магазина</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'categories_addons'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=categories_addons">Сопутсвующие товары</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'paymentmethods'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=paymentmethods">{#Shop_payment_images_inf#}</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'shipper'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=shipper">{#Shop_shipping_method#}</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'tracking'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=tracking">{#Shop_tracking#}</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'taxes'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=taxes">Налоговые ставки</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'shippingready'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=shippingready">Сроки выполнения</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'availabilities'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=availabilities">{#Shop_Availablility#}</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'couponcodes'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=couponcodes">{#Shop_couponcodes_title#}</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'units'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=units">{#Shop_units_title#}</a></li>

        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'groupsettings'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=groupsettings">{#Shop_groupsettings#}</a></li>

        <li><a class="{if isset($smarty.request.do) && $smarty.request.do == 'shopimport'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shopimport">{#Import_art#}</a></li>
        <li><a class="{if isset($smarty.request.sub) && $smarty.request.sub == 'yamarket'}uk-active{else}nav_subs{/if}" href="admin/index.php?do=shop&amp;sub=yamarket">{#YaMarket#}</a></li>

    </ul>
  </div>
</li>
{/if}
