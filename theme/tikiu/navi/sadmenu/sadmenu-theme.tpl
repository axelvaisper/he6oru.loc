{if permission('adminpanel')} 
<li>
  <a href="#">
    <div>
      <i data-uk-icon="palette"></i>
      <br>
      <small>Оформление</small>
    </div>
  </a>
  <div class="uk-navbar-dropdown">
    <ul class="uk-nav">
      <li><a class="{if isset($smarty.request.sub) && ($smarty.request.sub == 'show_all_tpl' || $smarty.request.sub == 'show_tpl')} uk-active{else}nav_subs{/if}" href="admin/index.php?do=theme&amp;sub=show_all_tpl">Шаблоны</a></li>
      <li><a class="{if isset($smarty.request.sub) && ($smarty.request.sub == 'show_all_css' || $smarty.request.sub == 'show_css')} uk-active{else}nav_subs{/if}" href="admin/index.php?do=theme&amp;sub=show_all_css">Стили</a></li>
    </ul>
  </div>
</li>
{/if}
