{if permission('adminpanel')} 

<aside id="offcanvas-admin" data-uk-offcanvas="overlay: true">
  <div class="uk-offcanvas-bar uk-background-default uk-height-1-1 uk-width-medium uk-padding-remove">

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        <li class="uk-width-1-1"><a href="{$baseurl}/admin" target="_blank">Админка</a></li>
        {include file="$incpath/navi/sadmenu/sadmenu-settings.tpl"}
        {include file="$incpath/navi/sadmenu/sadmenu-theme.tpl"}
        {include file="$incpath/navi/sadmenu/sadmenu-navi.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navi/sadmenu/sadmenu-insert.tpl"}
        {include file="$incpath/navi/sadmenu/sadmenu-codewidgets.tpl"}
        {include file="$incpath/navi/sadmenu/sadmenu-content.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

    <div class="gridnav" data-uk-navbar="dropbar:true; dropbar-mode:push; mode:click;">
      <ul class="uk-width-1-1 uk-navbar-nav uk-child-width-1-3 uk-grid-collapse" data-uk-grid>
        {include file="$incpath/navi/sadmenu/sadmenu-contact.tpl"}
        {include file="$incpath/navi/sadmenu/sadmenu-banners.tpl"}
        {include file="$incpath/navi/sadmenu/sadmenu-shop.tpl"}
      </ul>
      <div class="uk-navbar-dropbar"></div>
    </div>

  </div>
</aside>

{/if}