{if !empty($SiteNavigation)}
<ul class="uk-nav">
  <li class="uk-nav-header">{$navi_title}</li>
  {foreach from=$SiteNavigation item=navi}
      {if empty($navi->sub_navi)}
          <li class="nav-item{if !empty($navi->active) || $navi->document == $document} active{/if}">
            <a class="nav-link" title="{$navi->AltTitle|sanitize}" href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}">
              <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> {$navi->title|sanitize}
            </a>
          </li>
      {else}
          <li class="nav-item dropdown{if !empty($navi->active) || $navi->document == $document} active{/if}">
            <a class="nav-link dropdown-toggle" href="{$navi->document|escape:'html'}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> {$navi->title|sanitize}
            </a>
            <div class="dropdown-menu">
              {foreach from=$navi->sub_navi item=sub_navi}
                  <a class="dropdown-item{if !empty($sub_navi->active) || $sub_navi->document == $document} active{/if}" title="{$sub_navi->AltTitle|sanitize}" href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                    <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> {$sub_navi->title|sanitize}
                  </a>
              {/foreach}
            </div>
          </li>
      {/if}
  {/foreach}
</ul>
{/if}