{if !empty($SiteNavigation)}
<nav data-uk-navbar="mode:click" class="uk-hidden@s zindex-0 uk-navbar-transparent uk-container uk-container-large">

  {* LEFT *}
  {* <div class="uk-navbar-left">
    
  </div> *}

  {* CENTER *}
  <div class="uk-navbar-center overlay-callback-m">
    <ul class="uk-navbar-nav uk-text-bold">

      {foreach from=$SiteNavigation item=navi}
      {if empty($navi->sub_navi)}
        <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}" data-uk-scroll>
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}<br>
            {$navi->AltTitle|sanitize}
          </a>
        </li>
      {else}
        <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a href="{$navi->document|escape:'html'}">
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}<br>
            {$navi->AltTitle|sanitize}
            <span class="uk-margin-small-left" data-uk-icon="icon:chevron-down;ratio:1"></span>
          </a>
          <div class="uk-margin-remove uk-padding-remove" data-uk-dropdown="animation: uk-animation-slide-top-small; mode:click">
            <ul class="uk-nav uk-navbar-dropdown-nav">
            {foreach from=$navi->sub_navi item=sub_navi}
              <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
                <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                  {* <i class="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
                  {$sub_navi->title|sanitize}<br>
                  {$sub_navi->AltTitle|sanitize}
                </a>
              </li>
            {/foreach}
            </ul>
          </div>
        </li>
      {/if}
      {/foreach}

    </ul>
  </div>

  {* RIGHT *}
  <div class="uk-navbar-right overlay-callback-m">
    <ul class="uk-navbar-nav uk-text-bold">
      <li>
        <a class="uk-navbar-item" data-uk-toggle="target: .overlay-callback-m; animation: uk-animation-slide-right-small; duration: 200" data-uk-icon="icon:phone; ratio:1.2" data-uk-tooltip="Заказать обратный звонок">
          {$settings.Telefon}
        </a>
      </li>
    </ul>
  </div>


  {* overlay callback *}
  <div class="uk-navbar-right overlay-callback-m" hidden>
    <div class="uk-navbar-item">
      {contact id=3 tpl='callback.tpl'}
      <a class="uk-navbar-toggle" data-uk-icon="icon:close; ratio: 1.2" data-uk-toggle="target:.overlay-callback-m;"></a>
    </div>
  </div>

</nav>
{/if}