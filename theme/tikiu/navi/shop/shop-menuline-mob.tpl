<nav data-uk-navbar="mode:click" class="uk-hidden@s uk-container uk-container-large uk-navbar-transparent">

  {* LEFT *}
  <div class="uk-navbar-left">
    {* {include file="$incpath/other/breadcrumb.tpl"} *}
  </div>

  {* CENTER *}
  {* <div class="uk-navbar-center">

  </div> *}

  {* RIGHT *}
  <div class="uk-navbar-right uk-visible@s">
  
  </div>

</nav>