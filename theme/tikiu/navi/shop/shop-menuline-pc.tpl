<nav data-uk-navbar="mode:click" class="uk-visible@s uk-container uk-container-large uk-navbar-transparent">

  {* LEFT *}
  <div class="uk-navbar-left">
    <ul class="uk-navbar-nav">
      <li>
      лево
      </li>
    </ul>
  </div>

  {* CENTER *}
  <div class="uk-navbar-center">
    центр
  </div>

   {* RIGHT *}
  <div class="uk-navbar-right">
    правая
  </div>

</nav>
