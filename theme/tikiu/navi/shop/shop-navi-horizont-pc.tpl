{foreach from=$SiteNavigation item=navi}
{if empty($navi->sub_navi)}
  <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
    <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}" data-uk-scroll>
      {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
      {$navi->title|sanitize}<br>
      {$navi->AltTitle|sanitize}
    </a>
  </li>
{else}
  <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
    <a href="{$navi->document|escape:'html'}">
      {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
      {$navi->title|sanitize}
      {$navi->AltTitle|sanitize}
      <span class="uk-margin-small-left" data-uk-icon="icon:chevron-down;ratio:1"></span>
    </a>
    <div class="uk-margin-remove uk-padding-remove" data-uk-dropdown="animation: uk-animation-slide-top-small; mode:click">
      <ul class="uk-nav uk-navbar-dropdown-nav">
      {foreach from=$navi->sub_navi item=sub_navi}
        <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
          <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
            {* <i class="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$sub_navi->title|sanitize}<br>
            {$sub_navi->AltTitle|sanitize}
          </a>
        </li>
      {/foreach}
      </ul>
    </div>
  </li>
{/if}
{/foreach}