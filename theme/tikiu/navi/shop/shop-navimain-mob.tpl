<nav data-uk-navbar="mode:click" class="uk-hidden@s  uk-navbar-container zindex-3" data-uk-sticky="show-on-up: true; animation: uk-animation-slide-bottom; sel-target: .uk-navbar-container; cls-active: fixed-bottom shadow-top; cls-inactive: uk-navbar-transparent;">

  {* LEFT *}
  <div class="uk-navbar-left">
    <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-categ" href="javascript:void(0)" data-uk-icon="icon:menu; ratio: 1.5">
    </a>

    <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-categ" href="javascript:void(0)" data-uk-icon="icon:search; ratio: 1">
    </a>
  </div>

  {* CENTER *}
  <div class="uk-navbar-center">
   <a {if permission('adminpanel')}
          href="#" data-uk-toggle="target: #offcanvas-admin" title="Админ панель"
        {else}
          href="/"
        {/if}
          class="uk-navbar-item">

      {* <img src="{$baseurl}/logo.svg" width="32" height="32" alt=""> *}
      <img src="{$baseurl}/favicon-32x32.png" alt="">

      <div class="uk-margin-small-left uk-dark uk-margin-small-bottom uk-visible@s">
      <small>{$settings.Firma|sanitize}</small>
      <h6 class="uk-margin-remove uk-text-bold uk-text-uppercase">{$settings.Seitenname|sanitize}</h6>
      </div>
    </a>
  </div>

  {* RIGHT *}
  <div class="uk-navbar-right">
    {if $loggedin}
      <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-login" data-uk-icon="icon:user; ratio:1" href="javascript:void(0)"></a>
    {else}
      <a class="uk-navbar-item" data-uk-toggle="target: #offcanvas-login" data-uk-icon="icon:sign-in; ratio:1" href="javascript:void(0)"></a>
    {/if}

   {if get_active('shop')}
       <a class="uk-navbar-item" onclick="{if $basket_products_price > 0 || $basket_products_all >= 1}location.href = '{$baseurl}/index.php?p=shop&amp;action=showbasket';{else}javascript: alert('{#Shop_basket_empty#}'){/if}" href="javascript:void(0);">
       <span data-uk-icon="shop-basket"></span>
       {* {$basket_small} *}
      </a>
    {/if}
  </div>

</nav>

