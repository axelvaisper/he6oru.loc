<div class="uk-visible@s uk-navbar-container" data-uk-sticky="show-on-up: false; animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: fixed-top zindex-5 uk-box-shadow-small; cls-inactive: uk-navbar-transparent;">
    <nav id="navali-boundary-1" class="uk-container uk-container-large" data-uk-navbar="mode:click">
        <div class="uk-navbar-left uk-grid-collapse uk-width-1-1" data-uk-grid>

            <div class="uk-width-auto">
                <ul class="uk-navbar-nav">
                    <li>{navigation id=6 tpl='shop/shop-navali.tpl'}</li>
                </ul>
            </div>

            <hr class="uk-divider-vertical">
            <div class="uk-width-expand">
                <ul class="uk-navbar-nav overscroll">
                    {navigation id=6 tpl='shop/shop-navi-horizont-pc.tpl'}
                </ul>
            </div>
            <hr class="uk-divider-vertical">

            <div class="uk-width-auto">
                <ul class="uk-navbar-nav uk-text-center">
                    {if get_active('shop')}
                    <li>
                        <a onclick="{if $basket_products_price > 0 || $basket_products_all >= 1}location.href = '{$baseurl}/index.php?p=shop&amp;action=showbasket';{else}javascript: alert('{#Shop_basket_empty#}'){/if}" href="javascript:void(0);">
                            <div>
                                <span class="uk-label" style="height:1.5em">{$basket_small}</span><br>
                                <small>{#Shop_myBasket#}</small>
                            </div>
                        </a>
                    </li>
                    {/if}

                    <li data-uk-lightbox>
                        <a data-type="iframe" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1">
                            <div>
                                <i data-uk-icon="list"></i><br>
                                <small>{#Merge#}</small>
                            </div>
                        </a>
                    </li>

                    <li>
                        {include file="$incpath/shop/shop_search_menuext.tpl"}
                    </li>

                    <li>
                        <a href="index.php?p=shop&amp;action=mylist">
                            <div>
                                <i data-uk-icon="heart"></i><br>
                                <small>{#Bookmarks#}</small>
                            </div>
                        </a>
                    </li>

                    {if $loggedin}
                    <li>
                        <a data-uk-toggle="target: #offcanvas-login" href="javascript:void(0)">
                            <div>
                                <i data-uk-icon="user"></i><br>
                                <small>{#MyAccount#}</small>
                            </div>
                        </a>
                    </li>
                    {else}
                    <li>
                        <a data-uk-toggle="target: #offcanvas-login" href="javascript:void(0)">
                            <div>
                                <i data-uk-icon="sign-in"></i><br>
                                <small>{#LoginExtern#}</small>
                            </div>
                        </a>
                    </li>
                    {/if}

                </ul>
            </div>

        </div>
    </nav>
</div>