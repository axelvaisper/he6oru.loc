<button class="uk-button uk-button-large bg-transparent">
  <i data-uk-icon="icon:menu; ratio:1.5" class="uk-margin-small-right"></i>
  <span class="uk-visible@m">{#Global_Categories#}</span>
</button>

<div class="uk-card-default" data-uk-drop="pos:bottom-left; boundary:#alimenu-boundary-1; boundary-align:true; duration:0; mode:click">
  <div id="alimenu-boundary-2">

    <ul class="uk-nav">

    {* КАТЕГОРИИ МАГАЗИНА *}
    {if get_active('shop')}

      {* главная категория *}
      {foreach from=$MyShopNavi item=sn}

      <li class=" {if $cid == $sn->Id || in_array($sn->Id, $navi_current)} uk-active{/if}">
        <a href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
          <span class="uk-margin-small-right">{$sn->Icon}</span>
          {$sn->Entry|truncate: 35|sanitize}
          {if !empty($sn->Sub1) && count($sn->Sub1)}
            <i class="uk-float-right" data-uk-icon="chevron-right"></i>
          {/if}
        </a>

        {* 1 уровень *}
        {if !empty($sn->Sub1) && count($sn->Sub1)}
        <div id="alimenu-boundary-3" class="uk-margin-remove uk-card-default uk-width-large" data-uk-drop="pos:right-justify; boundary:#alimenu-boundary-2; boundary-align:true; duration:0">
          <div class="uk-height-1-1">
            <ul class="uk-nav">

            {foreach from=$sn->Sub1 item=sub1}
                <li>
                  <a class="{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                    {$sub1->Entry|sanitize}
                    {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                      <i class="uk-float-right" data-uk-icon="chevron-right"></i>
                    {/if}
                  </a>

                    {* 2 уровень *}
                    {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                    <div class="uk-margin-remove uk-card-default" data-uk-drop="pos:right-justify; boundary:#alimenu-boundary-3; boundary-align:true; duration:0">
                      <div class="uk-height-1-1">
                        <ul class="uk-nav">

                          {foreach from=$sub1->Sub2 item=sub2}

                          <li class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} uk-active{/if}">
                            <a href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                              {$sub2->Name|sanitize}
                              {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                                <i class="uk-float-right" data-uk-icon="chevron-right"></i>
                              {/if}
                            </a>

                                  {* 3 уровень *}
                                  {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                                  <div class="uk-margin-remove uk-card-default" data-uk-drop="pos:right-justify; boundary:#alimenu-boundary-3; boundary-align:true; duration:0">
                                    <div class="uk-height-1-1">
                                      <ul class="uk-nav">

                                        {foreach from=$sub2->Sub3 item=sub3}
                                        <li>
                                          <a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                            {$sub3->Name|sanitize}
                                            {if !empty($sub3->Sub4) && count($sub3->Sub4)}
                                              <i class="uk-float-right" data-uk-icon="chevron-right"></i>
                                             {/if}
                                          </a>
                                        
                                              {* 4 уровень *}
                                              {if !empty($sub3->Sub4) && count($sub3->Sub4)}
                                              {foreach from=$sub3->Sub4 item=sub4}
                                              <li>
                                                <a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                                  {$sub4->Name|sanitize}
                                                </a>
                                              </li>
                                              {/foreach}
                                              {/if}
                                              {* /4 уровень *}
                                        </li>
                                        {/foreach}

                                      </ul>
                                    </div>
                                  </div>
                                  {/if}
                                  {* /3 уровень *}


                          </li>
                          {/foreach}
                          
                        </ul>
                      </div>
                    </div>
                    {/if}
                    {* /2 уровень *}

                </li>
              {/foreach}
            </ul>
          </div>
        </div>
        {/if}
        {* /1 уровень *}

      </li>
      {/foreach}
      {* /главная категория *}

    {/if}
    {* /КАТЕГОРИИ МАГАЗИНА *}
    <hr>
    {* НАВИГАЦИЯ *}
    {if !empty($SiteNavigation)}
    {foreach from=$SiteNavigation item=navi}

        {if empty($navi->sub_navi)}

            <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
              <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}">
                <i data-uk-icon="{$navi->AltTitle|sanitize}" aria-hidden="true"></i>
                {$navi->title|sanitize}
              </a>
            </li>

        {else}

            <li class=" {if !empty($navi->active) || $navi->document == $document} uk-active{/if}">

              <a href="{$navi->document|escape:'html'}">
                <i data-uk-icon="{$navi->AltTitle|sanitize}" aria-hidden="true"></i>
                {$navi->title|sanitize}
                <i class="uk-float-right" data-uk-icon="chevron-right"></i>
              </a>

              <div  class="uk-margin-remove uk-card-default" data-uk-drop="pos:right-justify; boundary:#alimenu-boundary-2; boundary-align:true; duration:0">
                <div class="uk-height-1-1">
                  <ul class="uk-nav">

                    {foreach from=$navi->sub_navi item=sub_navi}
                    <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
                      <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                        <i data-uk-icon="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i>
                        {$sub_navi->title|sanitize}
                      </a>
                    </li>
                    {/foreach}

                  </ul>
                </div>
              </div>
           
            </li>

        {/if}

    {/foreach}
    {/if}
    {* /НАВИГАЦИЯ *}

    </ul>

  </div>
</div>
