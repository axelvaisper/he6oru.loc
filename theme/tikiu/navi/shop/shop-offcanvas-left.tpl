{include file="$incpath/shop/search_small.tpl"}

<ul class="uk-nav uk-nav-default uk-nav-parent-icon uk-width-1-1" data-uk-nav>

  {if get_active('shop')}{* моб.категории магазина *}
    {foreach from=$MyShopNavi item=sn}
    <li {if !empty($sn->Sub1) && count($sn->Sub1)}class="uk-parent"{/if}>
      <a class="{if $cid == $sn->Id || in_array($sn->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
        <span class="uk-margin-small-right">{$sn->Icon}</span>
        {$sn->Entry|truncate: 22|sanitize}
      </a>
    {if !empty($sn->Sub1) && count($sn->Sub1)}
          <ul class="uk-nav-sub uk-padding-remove">
          {foreach from=$sn->Sub1 item=sub1}

                <li class="uk-nav-header uk-margin-remove uk-padding-remove">
                  <a class="{if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                    {$sub1->Entry|sanitize}
                  </a></li>
                  {if !empty($sub1->Sub2) && count($sub1->Sub2)}

                          {foreach from=$sub1->Sub2 item=sub2}
                              <li>
                                <a class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                                  {$sub2->Name|sanitize}
                                </a></li>
                                {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                                        {foreach from=$sub2->Sub3 item=sub3}
                                            <li>
                                              <a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                                {$sub3->Name|sanitize}
                                              </a></li>

                                              {if !empty($sub3->Sub4) && count($sub3->Sub4)}

                                                      {foreach from=$sub3->Sub4 item=sub4}
                                                          <li>
                                                            <a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                                              {$sub4->Name|sanitize}
                                                            </a>
                                                          </li>
                                                      {/foreach}
                                              {/if}
                                        {/foreach}
                                {/if}
                           
                          {/foreach}
                          <li class="uk-nav-divider "></li>
                  {/if}
            {/foreach}
            </ul>
    {/if}
    </li>
    {/foreach}
  {/if}

{* моб.навигация *}
{if !empty($SiteNavigation)}
  {foreach from=$SiteNavigation item=navi}
    {if empty($navi->sub_navi)}
        <li class="{if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a href="{$navi->document|escape:'html'}" target="{$navi->target|default:'_self'}">
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}
            {$navi->AltTitle|sanitize}
          </a>
        </li>
    {else}
        <li class="uk-parent {if !empty($navi->active) || $navi->document == $document} uk-active{/if}">
          <a {* href="{$navi->document|escape:'html'}" *} class="top-item">
            {* <i class="{$navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
            {$navi->title|sanitize}
            {$navi->AltTitle|sanitize}
          </a>
            <ul class="uk-nav-sub">
            {foreach from=$navi->sub_navi item=sub_navi}
              <li class="{if !empty($sub_navi->active) || $sub_navi->document == $document} uk-active{/if}">
                <a href="{$sub_navi->document|escape:'html'}" target="{$sub_navi->target|default:'_self'}">
                  {* <i class="{$sub_navi->AltTitle|sanitize}" aria-hidden="true"></i> *}
                  {$sub_navi->title|sanitize}
                  {$sub_navi->AltTitle|sanitize}
                </a>
              </li>
            {/foreach}
            </ul>
        </li>
        {/if}
    {/foreach}
      </ul>
{/if}
