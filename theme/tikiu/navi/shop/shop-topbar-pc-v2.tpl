{if !empty($SiteNavigation)}
<nav data-uk-navbar="mode:click" class="uk-visible@s uk-navbar-transparent uk-container uk-container-large">

  {* LEFT *}
  <div class="uk-navbar-left overlay-callback">
    <ul class="uk-navbar-nav uk-text-bold">
      <li>
        <a class="uk-navbar-item" data-uk-toggle="target: .overlay-callback; animation: uk-animation-slide-right-small; duration: 200" data-uk-icon="icon:phone; ratio:1.2" data-uk-tooltip="Заказать обратный звонок">
          {$settings.Telefon}
        </a>
      </li>
    </ul>
  </div>
  {* overlay callback *}
  <div class="uk-navbar-left overlay-callback uk-flex-left " hidden>
    <div class="uk-navbar-item">
      {contact id=3 tpl='callback.tpl'}
      <a class="uk-navbar-toggle" data-uk-icon="icon:close; ratio: 1.2" data-uk-toggle="target:.overlay-callback;"></a>
    </div>
  </div>

  {* CENTER *}
  <div class="uk-navbar-center visible@m overlay-callback">
  
  </div>

   {* RIGHT *}
  <div class="uk-navbar-right">
    {* выбор валюты *}
    {$curreny_selector}

    {* смена языка *}
    {if !empty($langchooser)}
        {$langchooser}
    {/if}
  </div>

</nav>
{/if}