<div class="uk-visible@s uk-padding-small-top">
  <nav data-uk-navbar="mode:click" class="uk-container uk-container-large">

    {* LEFT *}
    <div class="uk-navbar-left">
        {* лого *}
        <div class="uk-navbar-item uk-margin-small-right">

            {if permission('adminpanel')}
            <a href="#" data-uk-toggle="target: #offcanvas-admin" title="Админ панель" data-uk-tooltip>
                <div class="uk-grid-small uk-flex-middle" data-uk-grid>
                    <div class="uk-width-auto">
                        <img itemprop="image" src="{$baseurl}/logo45.png" alt="">
                    </div>
                    <div class="uk-width-expand">
                        <small class="uk-margin-remove">{$settings.Firma|sanitize}</small>
                        <h4 itemprop="name" class="uk-margin-remove-top">
                            {$settings.Seitenname|sanitize}
                        </h4>
                    </div>
                </div>
            </a>

            {else}
            <a href="{$baseurl}" title="На главную" data-uk-tooltip>
                <div class="uk-grid-small uk-flex-middle" data-uk-grid>
                    <div class="uk-width-auto">
                        <img itemprop="image" src="{$baseurl}/logo45.png" alt="">
                    </div>
                    <div class="uk-width-expand">
                        <small class="uk-margin-remove">{$settings.Firma|sanitize}</small>
                        <h4 itemprop="name" class="uk-margin-remove-top">
                            {$settings.Seitenname|sanitize}
                        </h4>
                    </div>
                </div>
            </a>
            {/if}

        </div>
    </div>

    {* CENTER *}
    <div class="uk-navbar-center">
        <hr class="uk-divider-vertical">
        <ul class="uk-navbar-nav overscroll max-width-50vw">
          {if !empty({$settings.Telefon})}
          <li class="uk-margin-small-left uk-margin-medium-right">
            <div>
              <a class="uk-text-muted" href="tel:{$settings.Telefon}">{$settings.Telefon} <i data-uk-icon="phone"></i></a><br>
              <a class="uk-text-muted" data-uk-toggle="target: #callback"><small>Заказать обратный звонок</small></a>
              <div id="callback" data-uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-border-rounded">
                  {contact id=3 tpl='callback.tpl'}
                </div>
              </div>
            </div>
          </li>
          {/if}

          {if !empty({$settings.Strasse})}
          <li class="uk-margin-medium-right" data-uk-lightbox>
            <a data-type="iframe" class="uk-text-muted uk-padding-remove" href="https://yandex.ru/map-widget/v1/?um=constructor%3Ad911f505cc3e0109e10e07f2dea67f373cfdd7e99c683cf561bbc13fdc8fc5f0&amp;source=constructor">
              <div>
                {$settings.Strasse} <i data-uk-icon="icon:location; ratio:1.2"></i>
                <br>
                <small>Открыть карту</small>
              </div>
            </a>
          </li>
          {/if}

          <li class="uk-text-muted uk-margin-small-right">
            <div>
              c 03:00 до 22:00
              <br>
              <small>Время работы</small>
            </div>
          </li>

        </ul>
         <hr class="uk-divider-vertical">
    </div>


    {* RIGHT *}
    <div class="uk-navbar-right">
      <ul class="uk-navbar-nav uk-text-center">

    

      </ul>
    </div>

  </nav>
</div>