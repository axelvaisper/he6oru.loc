<h1 class="title">{#Newsletter#}</h1>
<div class="box-content">
  {if !empty($email_error)}
      <div class="box-error">
        <div class=font-weight-bold>{#Newsletter_e_inf#}</div>
        {foreach from=$email_error item=e}
            <div>{$e}</div>
        {/foreach}
      </div>
  {/if}

  {if isset($Entry_Ok) && $Entry_Ok == 1}
      <div class="font-weight-bold">{#Newsletter_okT#}</div>
      {#Newsletter_ok#}
  {else}
      <div class="wrapper">
        {#Newsletter_info#}
      </div>
      <form method="post" action="index.php?p=newsletter&amp;area={$area}">
        <div class="row form-group">
          <div class="col-md-3 col-sm-12">
            <label class="col-form-label" for="nl-email">{#Email#}</label>
          </div>
          <div class="col-md-9 col-sm-12">
            <input class="form-control" id="nl-email" name="nl_email" type="email" value="{$smarty.post.nl_email|sanitize}" required="required" />
          </div>
        </div>

        {if $Nl_Count > 1}
            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label">{#Newsletter_sections#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                {foreach from=$nl_items item=nli}
                    <div class="form-check">
                      <input class="form-check-input" id="nl-welche[{$nli->Id}]" name="nl_welche[{$nli->Id}]" value="1" type="checkbox" />
                      <label class="form-check-label" for="nl-welche[{$nli->Id}]">{$nli->Name|sanitize}</label>
                    </div>
                {/foreach}
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="nl-format">{#Newsletter_format#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <select class="form-control" id="nl-format" name="nl_format">
                  <option value="html">{#GlobalHTML#}</option>
                  <option value="text">{#GlobalText#}</option>
                </select>
              </div>
            </div>

            <div class="text-center">
              <input class="btn btn-primary btn-block-sm" type="submit" value="{#Newsletter_aboButton#}" />
            </div>
            {if $Nl_Count < 2}
                {foreach from=$nl_items item=nli}
                    <input type="hidden" name="nl_welche[{$nli->Id}]" value="1" />
                {/foreach}
            {/if}
            <input type="hidden" name="action" value="abonew" />
        </form>
      {/if}
  {/if}
</div>
