<h1 class="title">{#Newsletter#}</h1>
<div class="box-content">
  {if !empty($error)}
      <div class="box-error">
        <div class="font-weight-bold">{#Newsletter_e_inf#}</div>
        {foreach from=$error item=e}
            <div>{$e}</div>
        {/foreach}
      </div>
  {else}
      <div class="font-weight-bold">{#Newsletter_unsubscribe#}</div>
      {#Newsletter_unsubscribe_ok#}
  {/if}
</div>
