<div class="iconpanel">
  {if get_active('shop')}
      <a href="index.php?exts=1&amp;s=1&amp;area={$area}&amp;p=shop&amp;action=showproducts">
      {else}
      <a href="index.php?q=empty&amp;where=all&amp;p=search">
      {/if}
      <i class="icon-search" data-toggle="tooltip" title="{#Search#}"></i>
  </a>
  {if get_active('shop')}
      <div class="position-relative">
        <a onclick="{if $basket_products_price > 0 || $basket_products_all >= 1}location.href = '{$baseurl}/index.php?p=shop&amp;action=showbasket';{else}javascript: alert('{#Shop_basket_empty#}'){/if}" href="javascript:void(0);">
          <i class="icon-basket" data-toggle="tooltip" title="{#Shop_myBasket#}"></i>
        </a>
        {$basket_small}
      </div>
  {/if}
  {if $loggedin}
      <a href="index.php?p=userlogin">
        <i class="icon-user" data-toggle="tooltip" title="{#MyAccount#}"></i>
      </a>
      <div>
        <form method="post" name="form-icon-logout" action="index.php">
          <input type="hidden" name="p" value="userlogin" />
          <input type="hidden" name="action" value="logout" />
          <input type="hidden" name="area" value="{$area}" />
          <input type="hidden" name="backurl" value="{'index.php'|base64encode}" />
        </form>
        <a onclick="return confirm('{#Confirm_Logout#}');" href="javascript: document.forms['form-icon-logout'].submit();">
          <i class="icon-off" data-toggle="tooltip" title="{#Logout#}"></i>
        </a>
      </div>
  {else}
      <a href="index.php?p=userlogin">
        <i class="icon-user" data-toggle="tooltip" title="{#LoginExtern#}"></i>
      </a>
      <a href="index.php?p=register&amp;lang={$langcode}&amp;area={$area}">
        <i class="icon-plus" data-toggle="tooltip" title="{#RegNew#}"></i>
      </a>
  {/if}
  {if !empty($langchooser)}
      {$langchooser}
  {/if}
</div>
