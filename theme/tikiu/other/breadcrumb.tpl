{if !empty($breadcrumb)}
<div class="uk-container uk-container-large uk-margin-small-top">
    <ul class="uk-breadcrumb">
        {foreach from=$breadcrumb item=item}
          <li>
            {$item}
          </li>
      {/foreach}
    </ul>
</div>
{/if}