{if $secure_active == 1} {if empty($secure_uniqid)}{assign var=secure_uniqid value="$secure_default"}{/if}
<script>
$(function () {
  $('#secure_reload_{$secure_uniqid}').on('click', function () {
    $.ajax({
      url: '{$baseurl}/lib/secure.php?action=reload&secure_uniqid={$secure_uniqid}&key=' + Math.random(),
      success: function (data) {
        // $('#secure_info_{$secure_uniqid}').html('{#Validate_required#}');
        $('#secure_image_{$secure_uniqid}').html(data);
        $('#{$secure_input_{$secure_uniqid}}').val('');
      }
    });
  });
  $('#{$secure_input_{$secure_uniqid}}').on('keyup focusout', function () {
    $.ajax({
      url: '{$baseurl}/lib/secure.php?action=validate&secure_uniqid={$secure_uniqid}&{$secure_input_{$secure_uniqid}}=' + $(this).val() + '&key=' + Math.random(),
      success: function (data) {
        $('#secure_info_{$secure_uniqid}').html(data === 'true' ? '<i class="uk-form-icon c-success uk-form-icon-flip" data-uk-icon="icon: check-circle">' : '<i class="uk-form-icon c-danger uk-form-icon-flip" data-uk-icon="icon: alert-danger">');
      }
    });
  });
});
</script>

<div class="uk-width-1-2@s" >
<div class="uk-grid-small bg-white uk-border-rounded" data-uk-grid>

  <div class="uk-width-1-2" >
    <a id="secure_reload_{$secure_uniqid}" data-uk-tooltip href="javascript:void(0);"
    title="{#ReloadCode#}">
      <div class="uk-grid-small uk-flex-middle" data-uk-grid>
        <div class="uk-width-auto">
          <i data-uk-icon="icon: reload"></i>
        </div>
        <div id="secure_image_{$secure_uniqid}" class="uk-width-expand">{$secure_image_{$secure_uniqid}}</div>
      </div>
    </a>
  </div>

  <div class="uk-inline uk-width-expand">
    <div class="uk-width-1-1 uk-position-center-right">
      <div id="secure_info_{$secure_uniqid}"></div>
      <input class="uk-input uk-text-uppercase" id="{$secure_input_{$secure_uniqid}}" name="{$secure_input_{$secure_uniqid}}" value=""
        type="text" placeholder="{#Validate_required#}">
    </div>
    <input class="uk-hidden" name="scode" value="" type="text" />
    <input name="secure_uniqid" value="{$secure_uniqid}" type="hidden" />
  </div>

</div>
</div>
{/if}