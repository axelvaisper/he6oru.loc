{foreach from=$languages item=lang}
  {if $langcode != $lang}
      <a class="" href="index.php?lang={$lang}&amp;lredirect={page_link|base64encode}&amp;rand={$smarty.now}">
        <img data-uk-tooltip title="{$lang}" src="{$imgpath}/flags/{$lang}.png" alt="" />
      </a>
  {/if}
{/foreach}
