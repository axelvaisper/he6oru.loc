<div class="bg-white card-vk uk-height-1-1">
  <div class="uk-flex uk-flex-middle uk-flex-center" style="height:15.2em">

    <div class="uk-text-center">
      <i class="uk-icon-button" data-uk-icon="user"></i>
      <br>
      {$welcome}, {#Guest#}
      <br><br>
      <a class="uk-button uk-button-default uk-button-small uk-border-rounded" href="index.php?p=userlogin"><i data-uk-icon="sign-in"></i> {#Login_Button#}</a>
      {* <a class="uk-button uk-button-default uk-button-small" data-uk-toggle="target: #offcanvas-login" data-uk-icon="sign-in" href="javascript:void(0)">{#Login_Button#}</a> *}
      {if get_active('Register')}
        <a class="uk-button uk-button-default uk-button-small uk-border-rounded" href="index.php?p=register&amp;lang={$langcode}&amp;area={$area}">{#RegNew#}</a>
      {/if}
        <br><br>
        <a class="uk-button uk-button-small" href="index.php?p=pwlost">{#PassLost#}</a>
    </div>

  </div>

  <div class="uk-tile uk-tile-primary uk-margin-small-top uk-text-center uk-margin-remove uk-h3">
    Регистрируйся сейчас и получи скидку :)
  </div>

</div>