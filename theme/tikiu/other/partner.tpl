{if $small_partners}
<script>
<!-- //
$(function() {
    $('.partner-click').on('click', function () {
        var options = {
            target: '#plick',
            url: 'index.php?p=partners&action=updatehitcount&id=' + $(this).data('id'),
            timeout: 3000
        };
        $(this).ajaxSubmit(options);
        return true;
    });
});
//-->
</script>

    <div class="wrapper panel">
      <div class="panel-head">{#Partners#}</div>
      <div class="panel-main">
        <div id="partner-click"></div>
        {foreach from=$small_partners item=sp}
            <div class="wrapper">
              <a class="partner-click" data-id="{$sp->Id}" {if $sp->Nofollow == 1}rel="nofollow" {/if}title="{$sp->PartnerName|sanitize}" href="{$sp->PartnerUrl}" target="_blank">
                {if $sp->Bild}
                    <img class="img-fluid" src="{$sp->Bild}" alt="{$sp->PartnerName|sanitize}" />
                {else}
                    <span class="font-weight-bold">{$sp->PartnerName|sanitize}</span>
                {/if}
              </a>
            </div>
        {/foreach}
      </div>
    </div>
{/if}
