<!doctype html>
<html class="uk-height-1-1" lang="{$langcode}" prefix="og: http://ogp.me/ns#">
{include file="$incpath/head/head-home.tpl"}
<body class="uk-height-1-1">
{result type='script' format='file' position='body_start'}
{result type='script' format='code' position='body_start'}
{result type='code'   format='code' position='body_start'}

{* анимация загрузки *}
<div id="preloader"><img id="anim_floating" src="{$baseurl}/logo.svg" alt="loading.."/></div>

{* offcanvas контейнер всего визуального *}
<div class="uk-offcanvas-content">

      <div class="uk-container">
        {$content}
      </div>

    {include file="$incpath/navi/home/home-footer.tpl"}
    {include file="$incpath/other/google.tpl"}

  {* админ меню *}
  {include file="$incpath/navi/sadmenu/sadmenu.tpl"}

</div>
{* /offcanvas *}

{* {include file="$incpath/other/vk-chat.tpl"} *}

{* {include file="$incpath/js/jsend-home.tpl"} *}

{result type='code'   format='code' position='body_end'}
{result type='script' format='file' position='body_end'}
{result type='script' format='code' position='body_end'}
</body>
</html>
