<!doctype html>
<html lang="{$langcode}" prefix="og: http://ogp.me/ns#">
{include file="$incpath/head/head-forum.tpl"}
<body>
{result type='script' format='file' position='body_start'}
{result type='script' format='code' position='body_start'}
{result type='code'   format='code' position='body_start'}


{* анимация загрузки *}
<div id="preloader"><img id="anim_floating" src="{$baseurl}/logo45.png" alt="loading.."/></div>

<div class="uk-offcanvas-content">

  <div class="uk-inline uk-margin-small-bottom" >
    <img src="{$imgpath}/page/bg-main.jpg" alt="cover image" style="min-height:120px">
    <div class="uk-position-cover uk-overlay-default"></div>

    <div class="uk-position-top zindex-2">
      {navigation id=2 tpl='navi-topbar.tpl'}{* navi/ *}
      <hr class="uk-margin-remove" style="border-color:#0000001a">
      {navigation id=1 tpl='shopnav.tpl'}{* navi/ *}
    </div><!--pos-top-->

  </div><!--uk-inline-->

  <div data-uk-height-viewport="expand:true" class="uk-container-expand">
        {$content}
        {include file="$incpath/other/outlinks.tpl"}
  </div>

  {include file="$incpath/section/contact-info.tpl"}
  {* {include file="$incpath/section/map-yandex.tpl"} *}
  {include file="$incpath/footer/footer-forum.tpl"}
  {* {include file="$incpath/newsletter/newsletter-big.tpl"} *}

</div><!--offcanvas-->


{* {include file="$incpath/other/vk-chat.tpl"} *}

{* {include file="$incpath/js/jsend-forum.tpl"} *}


{result type='code'   format='code' position='body_end'}
{result type='script' format='file' position='body_end'}
{result type='script' format='code' position='body_end'}
</body>
</html>
