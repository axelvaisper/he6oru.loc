<!doctype html>
<html lang="{$langcode}" prefix="og: http://ogp.me/ns#">
{include file="$incpath/head/head-home.tpl"}
<body>
{result type='script' format='file' position='body_start'}
{result type='script' format='code' position='body_start'}
{result type='code'   format='code' position='body_start'}

{* анимация загрузки *}
<div id="preloader"><img id="anim_floating" src="{$baseurl}/logo45.png" alt="loading.."/></div>

{* основной контейнер *}
<div class="uk-offcanvas-content">

  {#Noscript#}

  {* реализация прижима подвала)  *}
  <div data-uk-height-viewport="expand:true">

  {* navs and header cover *}
  <div class="uk-background-fixed uk-background-center-center uk-background-cover" style="background-image: url({$baseurl}/cover.jpg)">
    <div class="uk-overlay-cover uk-overlay-default">
      <div class="uk-inline uk-height-medium uk-width-1-1">

        <div class="uk-position-top">
          {navigation id=4 tpl='home/home-top-pc.tpl'}
          <hr class="uk-margin-remove" style="border-color:#0000001a">
          {navigation id=4 tpl='home/home-header-mob.tpl'}
          {navigation id=4 tpl='home/home-header-pc.tpl'}
          <hr class="uk-margin-remove" style="border-color:#0000001a">  
          <div class="uk-visible@m uk-container uk-container-large">
            <div class="uk-grid-collapse" data-uk-grid>
              {* {navigation id=5 tpl='home/home-vertical-pc.tpl'} *}
              {navigation id=6 tpl='home/home-horizontal-pc.tpl'}
            </div>
          </div>
        </div>

       {* <div class="uk-position-center uk-width-1-3@m">
           <h4 class="uk-heading-divider uk-margin-remove" data-uk-scrollspy="cls: uk-animation-slide-top-small;delay:1000">
          {$settings.Firma|sanitize}</h4>
          <h1 class="uk-margin-remove" data-uk-scrollspy="cls: uk-animation-slide-bottom-small;delay:1000">{$settings.Seitenname|sanitize}</h1> 
        </div>*}

      </div>
    </div>
  </div>

  {* vertical nav and slider *}
  <div class="uk-container uk-container-large">
    <div class="uk-grid-collapse" data-uk-grid>
      {navigation id=5 tpl='home/home-vertical-pc.tpl'}
      {$small_topseller}
    </div>
  </div>

{$content}

  {* {include file="$incpath/section/about.tpl"} *}
  {* {include file="$incpath/section/slides.tpl"} *}
  {* {include file="$incpath/section/why.tpl"} *}
  {* {include file="$incpath/section/service.tpl"} *}

  {* {include file="$incpath/section/filter.tpl"} *}
  {* {include file="$incpath/section/step4.tpl"}
  {include file="$incpath/section/stats.tpl"}
  {$PartnerDisplay} *}
  {* {include file="$incpath/section/order.tpl"} *}
 
  {* {include file="$incpath/section/contact-info.tpl"} *}
  {* {include file="$incpath/section/map-yandex.tpl"} *}
  {* {include file="$incpath/navi/main-footer.tpl"} *}
  {$Newsletter}

  {* RSYA *}
  {* [CODEWIDGET:1] *}

  {$insert.text}


  </div>
  {* /прижим подвала *}

  {* FOOTER *}
  {include file="$incpath/shop/shop-footer.tpl"}



  {* НИЖЕ КОНТЕНТ ВНЕ ПОЛЯ ЗРЕНИЯ) *}

  {* offcanvas left mob menu *}
  <div id="offcanvas-categ" data-uk-offcanvas="overlay:true; mode:slide">
    <div class="uk-offcanvas-bar uk-padding-remove uk-flex">
        {navigation id=6 tpl='home/home-vertical-mob.tpl'}
    </div>
  </div>

  {* offcanvas search *}
  <div id="offcanvas-search" data-uk-offcanvas="flip:false; overlay:true; mode:slide">
    <div class="uk-offcanvas-bar uk-padding-small uk-flex">
        {include file="$incpath/shop/shop_search_menuext.tpl"}
    </div>
  </div>

  {* {include file="$incpath/navi/shop/shop-header-offcanvas-left.tpl"} *}
  {include file="$incpath/navi/shop/shop-header-offcanvas-right.tpl"}

  {* {include file="$incpath/other/vk-chat.tpl"} *}

  {* ADMIN OFFCANVAS *}
  {include file="$incpath/navi/sadmenu/sadmenu.tpl"}

</div>
{* /OFFCANVAS CONTENT*}


{* НИЖЕ JS *}

{result type='code'   format='code' position='body_end'}
{result type='script' format='file' position='body_end'}
{result type='script' format='code' position='body_end'}
</body>
</html>
