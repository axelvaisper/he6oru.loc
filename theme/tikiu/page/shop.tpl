<!doctype html>
<html lang="{$langcode}" prefix="og: http://ogp.me/ns#">
{include file="$incpath/head/head-shop.tpl"}

<body>
{result type='script' format='file' position='body_start'}
{result type='script' format='code' position='body_start'}
{result type='code'   format='code' position='body_start'}

{* анимация загрузки *}
<div id="preloader"><img id="anim_floating" src="{$baseurl}/logo45.png" alt="loading.."/></div>

{* основной контейнер *}
<div class="uk-offcanvas-content">
  {#Noscript#}

  {* снег *}
  <div>
    <canvas id="sky" class="uk-position-absolute zindex-0"></canvas>
  </div>


  {* реализация прижима подвала)  *}
  <div data-uk-height-viewport="expand:true">

    {banner categ=1}

    {* ВЫВОДИМ НА СТРАНИЦЕ ТОВАРА *}
    {if strpos($smarty.server.REQUEST_URI,"show-product/") !== false}
      {* header заканчивается в шаблоне product.tpl *}
      <header>
        {include file="$incpath/shop/shop-header.tpl"}
        {include file="$incpath/shop/shop-product-breadcrumb.tpl"}
        {$content}
    {elseif strpos($smarty.server.REQUEST_URI,"showproduct&id") !== false}
      <header>
        {include file="$incpath/shop/shop-header.tpl"}
        {include file="$incpath/shop/shop-product-breadcrumb.tpl"}
        {$content}

    {* ВЫВОДИМ НА СТРАНИЦЕ КАТЕГОРИИ *}
    {elseif strpos($smarty.server.REQUEST_URI,"show-products/") !== false}
      {include file="$incpath/shop/shop-header.tpl"}
      {include file="$incpath/other/breadcrumb.tpl"}
      <div class="" data-uk-toggle="cls: uk-container uk-container-large; mode: media; media: @m">
        {$content}
      </div>
    {elseif strpos($smarty.server.REQUEST_URI,"showproducts&id") !== false}
      {include file="$incpath/shop/shop-header.tpl"}
      {include file="$incpath/other/breadcrumb.tpl"}
      <div class="" data-uk-toggle="cls: uk-container uk-container-large; mode: media; media: @m">
        {$content}
      </div>

    {* ВЫВОДИМ НА СТРАНИЦЕ ЗАКАЗОВ *}
    {elseif strpos($smarty.server.REQUEST_URI,"/my-orders/") !== false}

      {include file="$incpath/shop/shop-header.tpl"}
      {include file="$incpath/other/breadcrumb.tpl"}
      <div class="uk-container uk-container-large">
      {$content}
      </div>

    {* ВЫВОДИМ НА СТРАНИЦЕ КОРЗИНЫ *}
    {elseif strpos($smarty.server.REQUEST_URI,"/cart/") !== false}

      {include file="$incpath/shop/shop-header.tpl"}
      {include file="$incpath/other/breadcrumb.tpl"}
      <div class="uk-container uk-container-large">
      {$content}
      </div>

   {* ЗАКЛАДКИ *}
    {elseif strpos($smarty.server.REQUEST_URI,"/mylist/") !== false}

      {include file="$incpath/shop/shop-header.tpl"}
      {include file="$incpath/other/breadcrumb.tpl"}
      <div class="uk-container uk-container-large">
      {$content}
      </div>

    {* ЗАГРУЗКИ *}
    {elseif strpos($smarty.server.REQUEST_URI,"/downloads/") !== false}

      {include file="$incpath/shop/shop-header.tpl"}
      {include file="$incpath/other/breadcrumb.tpl"}
      <div class="uk-container uk-container-large">
      {$content}
      </div>

    {* ВЫВОДИМ НА СТРАНИЦЕ РЕГИСТРАЦИИ *}
    {elseif strpos($smarty.server.REQUEST_URI,"/register/") !== false}

      {include file="$incpath/shop/shop-header.tpl"}
      {include file="$incpath/other/breadcrumb.tpl"}
      <div class="uk-container uk-container-large">
      {$content}
      </div>

    {* ВЫВОДИМ НА  *}
    {elseif strpos($smarty.server.REQUEST_URI,"&") !== false}

      {include file="$incpath/shop/shop-header.tpl"}
      {include file="$incpath/other/breadcrumb.tpl"}
      <div class="uk-container uk-container-large">
      {$content}
      </div>

    {* ВЫВОДИМ НА ГЛАВНОЙ И ОСТАЛЬНЫХ СТРАНИЦАХ *}
    {else}

      {include file="$incpath/shop/shop-header.tpl"}
      <div class="uk-container uk-container-large">
        {$content}
      </div>
      {* {navigation id=5 tpl='shop/shop-navali.tpl'} *}
    {/if}

    {* {include file="$incpath/shop/basket_saved_small.tpl"} *}

  </div>
  {* /прижим подвала *}

  {* сообщение магазина *}
  {if !empty($ShopInfo)}
      {$ShopInfo}
  {/if}

  {* FOOTER *}
  {include file="$incpath/shop/shop-footer.tpl"}

  {* НИЖЕ КОНТЕНТ ВНЕ ПОЛЯ ЗРЕНИЯ) *}

  
  {* OFFCANVAS LEFT MOB MENU *}
  <div id="offcanvas-categ" data-uk-offcanvas="overlay:true; mode:slide">
    <div class="uk-offcanvas-bar uk-padding-remove">
        {navigation id=6 tpl='shop/shop-offcanvas-left.tpl'}
    </div>
  </div>

  {* OFFCANVAS LOGIN *}
  {include file="$incpath/navi/shop/shop-offcanvas-right.tpl"}

  {* {include file="$incpath/other/vk-chat.tpl"} *}

  {* ADMIN OFFCANVAS *}
  {include file="$incpath/navi/sadmenu/sadmenu.tpl"}

</div>
{* /OFFCANVAS CONTENT*}

{* НИЖЕ JS *}
{include file="$incpath/js/jsend-shop.tpl"}

{result type='code'   format='code' position='body_end'}
{result type='script' format='file' position='body_end'}
{result type='script' format='code' position='body_end'}
</body>
</html>
