{include file="$incpath/shop/steps.tpl"}

{if empty($payment_error)}
<script>
<!-- //
$(window).load(function() {
    setTimeout(function() {
        $('#process').submit();
    }, 2500);
});
//-->
</script>

<div class="h3 title">{#Shop_thankyou_title#}</div>
<div class="box-content">
  {if !empty($payment_data.Icon)}
      <div class="wrapper text-center">
        <img src="uploads/shop/payment_icons/{$payment_data.Icon}" alt="" />
      </div>
  {/if}

  {if !empty($payment_data.Text)}
      <div class="wrapper">
        {$payment_data.Text}
      </div>
  {/if}

  {if !empty($payment_data.TextLang)}
      {$payment_data.TextLang}
  {/if}

  <form method="post" name="process" id="process" action="http://www.interkassa.com/lib/payment.php">
    <input type="hidden" name="ik_shop_id" value="{$payment_data.Install_Id}" />
    <input type="hidden" name="ik_payment_amount" value="{$smarty.session.price_final}" />
    <input type="hidden" name="ik_payment_id" value="{$smarty.session.id_num_order}" />
    <input type="hidden" name="ik_payment_desc" value="{$inf_payment}" />
    <input type="hidden" name="ik_paysystem_alias" value="" />
    <input type="hidden" name="ik_baggage_fields" value="{$payment_hash}" />
    <input type="hidden" name="ik_sign_hash" value="{$ik_hash}" />
  </form>
</div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
