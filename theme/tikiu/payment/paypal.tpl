{include file="$incpath/shop/steps.tpl"}

{if empty($payment_error)}
<script>
<!-- //
$(window).load(function() {
    setTimeout(function() {
        $('#process').submit();
    }, 2500);
});
//-->
</script>

<div class="h3 title">{#Shop_thankyou_title#}</div>
<div class="box-content">
  {if !empty($payment_data.Icon)}
      <div class="wrapper text-center">
        <img src="uploads/shop/payment_icons/{$payment_data.Icon}" alt="" />
      </div>
  {/if}

  {if !empty($payment_data.Text)}
      <div class="wrapper">
        {$payment_data.Text}
      </div>
  {/if}

  {if !empty($payment_data.TextLang)}
      {$payment_data.TextLang}
  {/if}

  <form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="process" id="process">
    <input type="hidden" name="cmd" value="_xclick" />
    <input type="hidden" name="business" value="{$payment_data.Install_Id}" />
    <input type="hidden" name="item_name" value="{$payment_data.Betreff}" />
    <input type="hidden" name="item_number" value="{$smarty.session.order_number}" />
    <input type="hidden" name="amount" value="{$smarty.session.price_final}" />
    <input type="hidden" name="no_shipping" value="1" />
    <input type="hidden" name="return" value="" />
    <input type="hidden" name="no_note" value="1" />
    <input type="hidden" name="cancel_return" value="{$smarty.session.back_url}" />
    <input type="hidden" name="return" value="{$baseurl}/paypal.php" />
    <input type="hidden" name="currency_code" value="{$smarty.session.currency_registered}" />
  </form>
</div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
