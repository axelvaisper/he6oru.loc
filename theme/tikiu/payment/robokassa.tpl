{include file="$incpath/shop/steps.tpl"}

{if empty($payment_error)}
<script>
<!-- //
$(window).load(function() {
    setTimeout(function() {
        $('#process').submit();
    }, 2500);
});
//-->
</script>

<div class="h3 title">{#Shop_thankyou_title#}</div>
<div class="box-content">
  {if !empty($payment_data.Icon)}
      <div class="wrapper text-center">
        <img src="uploads/shop/payment_icons/{$payment_data.Icon}" alt="" />
      </div>
  {/if}

  {if !empty($payment_data.Text)}
      <div class="wrapper">
        {$payment_data.Text}
      </div>
  {/if}

  {if !empty($payment_data.TextLang)}
      {$payment_data.TextLang}
  {/if}

  <form method="post" name="process" id="process" action="https://merchant.roboxchange.com/Index.aspx">
    <input type=hidden name="MrchLogin" value="{$payment_data.Install_Id}" />
    <input type=hidden name="OutSum" value="{$smarty.session.price_final}" />
    <input type=hidden name="InvId" value="{$smarty.session.id_num_order}" />
    <input type=hidden name="Desc" value="{$inf_payment}" />
    <input type=hidden name="SignatureValue" value="{$crc}" />
    <input type=hidden name="IncCurrLabel" value="" />
    <input type=hidden name="Culture" value="ru" />
    <input type=hidden name="Shp_order" value="{$smarty.session.order_number}" />
    <input type=hidden name="Shp_hash" value="{$payment_hash}" />
  </form>
</div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
