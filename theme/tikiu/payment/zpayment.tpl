{include file="$incpath/shop/steps.tpl"}

{if empty($payment_error)}
<script>
<!-- //
$(window).load(function() {
    setTimeout(function() {
        $('#process').submit();
    }, 2500);
});
//-->
</script>

<div class="h3 title">{#Shop_thankyou_title#}</div>
<div class="box-content">
  {if !empty($payment_data.Icon)}
      <div class="wrapper text-center">
        <img src="uploads/shop/payment_icons/{$payment_data.Icon}" alt="" />
      </div>
  {/if}

  {if !empty($payment_data.Text)}
      <div class="wrapper">
        {$payment_data.Text}
      </div>
  {/if}

  {if !empty($payment_data.TextLang)}
      {$payment_data.TextLang}
  {/if}

  <form method="post" name="process" id="process" action="http://www.z-payment.ru/merchant.php">
    <input type="hidden" name="LMI_PAYEE_PURSE" value="{$payment_data.Install_Id}" />
    <input type="hidden" name="LMI_PAYMENT_AMOUNT" value="{$smarty.session.price_final}" />
    <input type="hidden" name="LMI_PAYMENT_DESC" value="{$inf_payment}" />
    <input type="hidden" name="LMI_PAYMENT_NO" value="{$smarty.session.id_num_order}" />
    <input type="hidden" name="CLIENT_MAIL" value="{$smarty.session.r_email}" />
    <input type="hidden" name="ZP_SIGN" value="{$payment_z_hash}" />
    <input type="hidden" name="FIELD_1" value="{$smarty.session.order_number}" />
    <input type="hidden" name="FIELD_2" value="{$payment_hash}" />
  </form>
</div>
{else}
    <div class="h3 title">{#Global_error#}</div>
    <div class="box-content">
      {#Payment_Error#}
    </div>
{/if}
