<div class="h3">{$Question|sanitize}</div>
{foreach from=$polls_once item=pas}
    {if $pas->Perc == 1}
        {assign var=PollVar value=0}
    {else}
        {assign var=PollVar value=$pas->Perc|replace: ',': '.'}
    {/if}
    <div class="spacer">
      {$pas->Frage|sanitize}
      <div class="progress">
        <div class="progress-bar progress-bar-striped" role="progressbar" style="width:{$PollVar|default:0}%;background-color:{$pas->Farbe}" aria-valuenow="{$PollVar|default:0}" aria-valuemin="0" aria-valuemax="100">
          {if !empty($PollVar)}
              {$PollVar|default:0}%
          {else}
              <span style="color:{$pas->Farbe}">{$PollVar|default:0}%</span>
          {/if}
        </div>
      </div>
    </div>
{/foreach}
