<div class="wrapper">
  <h1 class="title">{#Roadmap#}</h1>
  {foreach from=$items item=item}
      <div class="box-content">
        <h3>{$item.Name}</h3>
        <div class="spacer">
          {$item.Beschreibung}
        </div>

        {if $item.Edit}
            <div class="spacer">
              {#LastChange#}: {$item.Edit|date_format:'%d-%m-%Y, %H:%M'}
            </div>
        {/if}

        <div class="wrapper progress">
          <div class="progress-bar progress-bar-striped" role="progressbar" style="width: {$item.Closed}%;" aria-valuenow="{$item.Closed}" aria-valuemin="0" aria-valuemax="100">{$item.Closed}%</div>
        </div>

        <div class="row">
          <div class="col text-nowrap">
            {#ClosedTickets#}: <a href="index.php?p=roadmap&amp;action=display&amp;rid={$item.Id}&amp;closed=1&amp;area={$area}&amp;name={$item.Name|translit}">{$item.NumFertig}</a>
          </div>
          <div class="col text-nowrap">
            {#OpenTickets#}: <a href="index.php?p=roadmap&amp;action=display&amp;rid={$item.Id}&amp;closed=0&amp;area={$area}&amp;name={$item.Name|translit}">{$item.NumUFertig}</a>
          </div>
        </div>
      </div>
  {/foreach}
</div>
