<h1 class="title">{#Search#}</h1>
<div class="box-content">
  {#SearchHelp#}
</div>

<div class="box-content">
  <form method="get" action="index.php">
    <div class="form-group">
      <input class="form-control" name="q" type="text" value="{if $smarty.get.q != 'empty'}{$smarty.get.q|sanitize}{/if}" required="required" />
    </div>
    {include file="$incpath/search/areas.tpl"}
    <input type="hidden" name="p" value="search" />
    <div class="text-center">
      <input type="submit" class="btn btn-primary btn-block-sm" value="{#Search#}" />
    </div>
  </form>
</div>

{$Results}
