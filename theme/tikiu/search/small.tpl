<div class="wrapper panel">
  <div class="panel-head">{#Search#}</div>
  <div class="panel-main">
    <form method="get" action="index.php">
      <div class="form-group">
        <input class="form-control" name="q" type="text" value="{$smarty.get.q|sanitize}" required="required" maxlength="35" />
      </div>
      {include file="$incpath/search/areas.tpl"}
      <input type="hidden" name="p" value="search" />
      <input type="submit" class="btn btn-primary btn-block" value="{#Search#}" />
    </form>
  </div>
</div>
