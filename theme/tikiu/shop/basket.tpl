<h1 class="title">{#Shop_myBasket#}</h1>
<div class="box-content">
  {if !$product_array}
      {#Shop_basket_empty#}
  {else}
      <div class="wrapper">
        {include file="$incpath/shop/basket_items.tpl"}
        {include file="$incpath/shop/basket_summ.tpl"}
      </div>
      {if $status_error}
          <div class="box-error">
            <div class="font-weight-bold">{#Error#}</div>
            {if $status_error == 'to_much'}
                {#Shop_basket_summ_tohigh#} {$best_max|numformat} {$currency_symbol}
            {else}
                {#Shop_basket_summ_tolow#} {$best_min|numformat} {$currency_symbol}
            {/if}
          </div>
      {else}
          <div>
            <form method="post" action="index.php">
              <input type="hidden" name="p" value="shop" />
              <input type="hidden" name="area" value="{$area}" />
              <input type="hidden" name="action" value="shoporder" />
              <input type="hidden" name="subaction" value="step1" />
              <div class="text-center">
                <input class="btn btn-primary btn-block-sm" type="submit" value="{#Shop_go_payment#}" />
              </div>
            </form>
          </div>
      {/if}
  {/if}
</div>
