{if (isset($basket_products_price) && $basket_products_price > 0) || (isset($basket_products_all) && $basket_products_all >= 1)}
  {if $smarty.request.subaction != 'step4' && $smarty.request.subaction != 'final'}
     <b title="{$basket_products_price|numformat} {$currency_symbol}" data-uk-tooltip>{$basket_products_all|default:0}</b>
  {/if}
{/if}
