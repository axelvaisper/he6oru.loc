{if $smarty.request.s != 1 && ($sub_categs || $newin_shop || $offers_shop  || $topseller_shop)}

    {assign var='active' value=0}
      <ul  data-uk-tab>
        {if $sub_categs}
            <li class="uk-active">
              {if $active == 0}
                  {assign var='active' value=1}
                  <a id="categs-tab" href="#categs">
                  {else}
                      <a id="categs-tab" href="#categs">
                      {/if}
                      {#AllCategs#}
                  </a>
            </li>
        {/if}
        {if $newin_shop}
            <li>
              {if $active == 0}
                  {assign var='active' value=2}
                  <a id="newin-tab" href="#newin">
                  {else}
                      <a id="newin-tab" href="#newin">
                      {/if}
                      {#Shop_NewProducts#}
                  </a>
            </li>
        {/if}
        {if $offers_shop}
            <li>
              {if $active == 0}
                  {assign var='active' value=3}
                  <a id="offers-tab" href="#offers">
                  {else}
                      <a id="offers-tab" href="#offers">
                      {/if}
                      {#Shop_Offers#}
                  </a>
            </li>
        {/if}
        {if $topseller_shop}
            <li>
              {if $active == 0}
                  {assign var='active' value=4}
                  <a id="topseller-tab" href="#topseller">
                  {else}
                      <a id="topseller-tab" href="#topseller">
                      {/if}
                      {#Shop_Topseller#}
                  </a>
            </li>
        {/if}
      </ul>

    <ul class="uk-switcher uk-padding-small">
        {if $sub_categs}
            <li class="{if $active == 1} uk-active{/if}" id="categs">
            {include file="$incpath/shop/browse_tabs_categ.tpl"}
            </li>
        {/if}
        {if $newin_shop}
            <li class="{if $active == 2}uk-active{/if}" id="newin">
            {$newin_shop}
            </li>
        {/if}
        {if $offers_shop}
            <li class="{if $active == 3}uk-active{/if}" id="offers">
            {$offers_shop}
            </li>
        {/if}
        {if $topseller_shop}
            <li class="{if $active == 4}uk-active{/if}" id="topseller">
            {$topseller_shop}
            </li>
        {/if}
    </ul>
{/if}