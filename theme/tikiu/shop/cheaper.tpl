<script>
<!-- //
$(function() {
    $('#cheaper_link').colorbox({ width: '700px', inline: true, href: '#cheaper_content' });
    $('#cheaper_form').validate({
        rules: {
            cheaper_email: { required: true, email: true },
            cheaper_where: { required: true, minlength: 10 }
        },
        messages: { },
        submitHandler: function() {
            document.forms['cheaper'].submit();
        }
    });
    $('#cheaper_link').on('click', function() {
        var price = $('#new_price').text();
	$('#cheaper_price').val(price);
        $('#cheaper_current').text(price);
    });
});
//-->
</script>

<div style="display: none">
  <div id="cheaper_content">
  <div class="h1 title">{#cheaper_name#}</div>
    <div class="box-popup">

      <div class="wrapper box-block">
        <div class="wrapper">
          {#cheaper_content#}
        </div>
        {#cheaper_content_inf#}
      </div>

      <div class="wrapper box-block">
        <div class="wrapper">
          {#cheaper_action#}
        </div>
        {#cheaper_action_inf#}
      </div>

      <div class="wrapper box-block">
        <div class="wrapper">
          {#cheaper_product#}
        </div>
        <div>
          {#GlobalTitle#}: {$cheaper_product}
        </div>
        {#Products_price#}: <span id="cheaper_current">0</span>
      </div>

      <form name="cheaper" method="post" id="cheaper_form" action="">
        <div class="form-group">
          <label for="cheaper-mail">{#SendEmail_Email#}</label>
          <input class="form-control" id="cheaper-mail" name="cheaper_email" type="email" value="{$smarty.request.cheaper_email|default:$smarty.session.login_email|sanitize}" />
        </div>

        <div class="form-group">
          <label for="cheaper-where">{#cheaper_where#}</label>
          <textarea class="form-control" id="cheaper-where" name="cheaper_where" rows="3">{$smarty.request.cheaper_where}</textarea>
        </div>

        <div class="form-group">
          <label for="cheaper-text">{#GlobalMessage#}</label>
          <textarea class="form-control" id="cheaper-text" name="cheaper_text" rows="5">{$smarty.request.cheaper_text}</textarea>
        </div>

        <div class="text-center">
          <input class="btn btn-primary" value="{#SendEmail_Send#}" type="submit">
          <input class="btn btn-secondary" onclick="closeWindow();" value="{#WinClose#}" type="button">
        </div>

        <input type="hidden" name="cheaper_price" id="cheaper_price" value="0" />
        <input type="hidden" name="cheaper_link" value="{page_link}" />
        <input type="hidden" name="cheaper_send" value="1" />
      </form>

    </div>
  </div>
</div>
