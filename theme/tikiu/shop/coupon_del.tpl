{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#ajaxcp').validate({
        rules: { },
        messages: { },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                target: '#ajaxcpresult',
                timeout: 6000,
                clearForm: false,
                resetForm: false
            });
        }
    });
});
//-->
</script>

<div class="wrapper">{#ShopCouponQuestOk#}</div>

{if !empty($coupon_hersteller)}
    <div class="wrapper">
      <div class="">{#ShopCouponInfo#}</div>
      {foreach name=man from=$coupon_hersteller item=man}
          <span class="spacer mr-3">
            <a target="_blank" href="index.php?p=manufacturer&amp;area={$area}&amp;action=showdetails&amp;id={$man.Id}&amp;name={$man.Name|translit}">
              {$man.Name|sanitize}
            </a>
          </span>
      {/foreach}
    </div>
{/if}

<form  id="ajaxcp" name="ajaxcp" action="{$baseurl}/index.php?action=ajaxcoupondel&p=shop">
  <div class="text-center">
    <input class="btn btn-primary btn-block-sm" type="submit" value="{#Shop_del_coupon#}" />
  </div>
</form>
