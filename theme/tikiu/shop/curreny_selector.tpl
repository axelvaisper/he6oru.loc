{if !isset($smarty.request.action) || isset($smarty.request.action) && ($smarty.request.action != 'shoporder' || $smarty.request.action != 'showbasket')}
{if $curr_change == 1 && ($cu_array.Waehrung_2 || $cu_array.Waehrung_3)}
    {assign var='foo' value='1'}

    <div class="uk-margin-small-right">

        <button class="uk-text-bold" data-uk-icon="icon:chevron-down; ratio:.5" title="{#Shop_changeCurrency#}" data-uk-tooltip>
          {if $cu_array.Waehrung_1 == 0}
            {if (isset($smarty.request.currency) && $smarty.request.currency == 1) || (isset($smarty.session.currency) && $smarty.session.currency == 1) || empty($smarty.session.currency)}
            {$cu_array.WaehrungSymbol_1}
            {/if}
          {/if}

          {if $cu_array.Waehrung_2 == 0}
            {if (isset($smarty.request.currency) && $smarty.request.currency == 2) || (isset($smarty.session.currency) && $smarty.session.currency == 2) || empty($smarty.session.currency)}
            {$cu_array.WaehrungSymbol_2}
            {/if}
          {/if}

          {if $cu_array.Waehrung_3 == 0}
            {if (isset($smarty.request.currency) && $smarty.request.currency == 3) || (isset($smarty.session.currency) && $smarty.session.currency == 3) || empty($smarty.session.currency)}
              {$cu_array.WaehrungSymbol_3}
            {/if}
          {/if}
        </button>

        <div data-uk-dropdown="mode: click">
            <ul class="uk-subnav uk-dropdown-nav">
                {if $cu_array.Waehrung_1}
                    {if (isset($smarty.request.currency) && $smarty.request.currency == 1) || (isset($smarty.session.currency) && $smarty.session.currency == 1) || empty($smarty.session.currency)}
                        <li class="uk-active">{$cu_array.Waehrung_1}</li>
                    {else}
                        <li><a href="index.php?p=shop&amp;currency=1">{$cu_array.Waehrung_1}</a></li>
                    {/if}
                {/if}

                {if $cu_array.Waehrung_2}
                    {if (isset($smarty.request.currency) && $smarty.request.currency == 2) || (isset($smarty.session.currency) && $smarty.session.currency == 2)}
                        <li class="uk-active">{$cu_array.Waehrung_2}</li>
                    {else}
                        <li><a href="index.php?p=shop&amp;currency=2">{$cu_array.Waehrung_2}</a></li>
                    {/if}
                {/if}

                {if $cu_array.Waehrung_3}
                    {if (isset($smarty.request.currency) && $smarty.request.currency == 3) || (isset($smarty.session.currency) && $smarty.session.currency == 3)}
                        <li class="uk-active">{$cu_array.Waehrung_3}</li>
                    {else}
                        <li><a href="index.php?p=shop&amp;currency=3">{$cu_array.Waehrung_3}</a></li>
                    {/if}
                {/if}
            </ul>

            <br>
            {if $cu_array.Waehrung_2}
                1 {$cu_array.WaehrungSymbol_2} = {($foo/$cu_array.Multiplikator_2)|numformat} {$cu_array.WaehrungSymbol_1}
            {/if}
            <br>

            {if $cu_array.Waehrung_3}
            1 {$cu_array.WaehrungSymbol_3} = {($foo/$cu_array.Multiplikator_3)|numformat} {$cu_array.WaehrungSymbol_1}
            {/if}

        </div>
    </div>

{/if}
{/if}
