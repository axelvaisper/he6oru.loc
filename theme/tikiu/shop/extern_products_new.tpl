{if $extern_products_array}
<script>
<!-- //
$(function() {
    var options = { target: '#ajaxbasket', timeout: 3000 };
    $('.start-products').submit(function() {
        showModal($('#basket-start'), 10000);
        $(this).ajaxSubmit(options);
        return false;
    });
});
//-->
</script>

{include file="$incpath/shop/notification.tpl" dialog='start'}

            {assign var=tscount value=0}
            {foreach from=$extern_products_array item=p name=pro}
 
                    <form class="start-products" method="post" action="{if empty($p.Vars) && $p.Lagerbestand>0}index.php?p=shop{else}index.php?p=shop&amp;action=showproduct&amp;id={$p.Id}{/if}">

                      <div data-toggle="tooltip" title="{$p.Beschreibung|tooltip:200}">
                        <div class="spacer">
                          <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                            <img class="img-fluid height-6" src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" />
                          </a>
                        </div>
                        <div class="spacer">
                          <h5 class="text-truncate">
                            <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                              {$p.Titel|truncate:25|sanitize}
                            </a>
                          </h5>
                        </div>
                      </div>

                      {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                          <div class="spacer">
                            {if $p.Preis > 0}
                                {if $p.Preis_Liste != $p.Preis}
                                    <div>
                                      {#Shop_instead#} <span class="shop-price-old">{$p.Preis_Liste|numformat} {$currency_symbol}</span>
                                    </div>
                                {/if}
                                {if !empty($p.Vars)}
                                    {#Shop_priceFrom#}
                                {/if}
                                <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                                {if $no_nettodisplay != 1}
                                    {if $price_onlynetto != 1}
                                        <div class="small">
                                          {if $shopsettings->NettoKlein == 1}
                                              {#Shop_netto#} {$p.netto_price|numformat} {$currency_symbol}
                                          {/if}
                                        </div>
                                    {/if}
                                    {if $price_onlynetto == 1 && !empty($p.price_ust_ex)}
                                        <div class="small">
                                          {include file="$incpath/shop/tax_inf_small.tpl"}
                                        </div>
                                    {/if}
                                {/if}
                            {else}
                                <span class="shop-price">{#Zvonite#}</span>
                            {/if}
                          </div>

                          <div class="spacer">
                            {if $p.Fsk18 == 1 && $fsk_user != 1}
                                <button class="btn btn-secondary btn-block-sm" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
                            {else}
                                {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                                    <input type="hidden" name="amount" value="1" />
                                    <input type="hidden" name="action" value="to_cart" />
                                    <input type="hidden" name="redir" value="{page_link}" />
                                    <input type="hidden" name="product_id" value="{$p.Id}" />
                                    <input type="hidden" name="ajax" value="1" />
                                    <noscript>
                                    <input type="hidden" name="ajax" value="0" />
                                    </noscript>
                                    <button class="btn btn-primary btn-block-sm" type="submit">{#Shop_toBasket#}</button>
                                {else}
                                    <input type="hidden" name="cid" value="{$p.Kategorie}" />
                                    <input type="hidden" name="parent" value="{$p.Parent}" />
                                    <input type="hidden" name="navop" value="{$p.Navop}" />
                                    <button class="btn btn-secondary btn-block-sm" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
                                {/if}
                            {/if}
                          </div>
                      {else}
                          {#Shop_prices_justforUsers#}
                      {/if}
                    </form>
 
                {assign var=tscount value=$tscount+1}
                {if $tscount % $newest_colums == 0 && !$smarty.foreach.pro.last}
  

              {/if}
            {/foreach}

{/if}
