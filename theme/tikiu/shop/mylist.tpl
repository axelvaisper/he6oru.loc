<div class="uk-container uk-container-large">

  <h1 class="uk-heading-line"><span>{#Shop_mylist#}</span></h1>

  <div data-uk-alert class="uk-alert-primary card-vk">
      {#Shop_mylist_inf#}
  </div>

  {if !$product_array}
      <div data-uk-alert class="uk-alert-warning card-vk">
        {#Shop_mylist_false#}
      </div>
  {else}
      <div class="uk-text-center uk-grid-collapse uk-child-width-1-3@s" data-uk-grid data-uk-sticky="cls-active: uk-padding-small uk-background-muted zindex-1000 uk-box-shadow-small;">
        <div>{#Products#}</div>
        <div>{#Shop_basket_amount2#}</div>
        <div>{#Products_price#} | {#Shop_basket_ovall#}</div>
        {* <div class="col uk-text-right">{#GlobalActions#}</div> *}
      </div>
        <hr>

        <div class="card-vk">
          {foreach from=$product_array item=p}
          <div data-uk-grid class="uk-child-width-1-3@s uk-grid-collapse {cycle name='list' values='uk-background-muted,uk-background-default'}">

                <div class="uk-padding-small">
                  <a href="{$p->ProdLink}">
                    <img data-src="{$p->Bild}" alt="" data-uk-img/>
                  </a>
                  <div class="uk-text-center">
                  <form class="uk-inline" name="to_basket_{$p->Id}" method="post" action="index.php">
                    <input name="amount" type="hidden" value="{$p->Anzahl}" />
                    {foreach from=$p->Varianten item=x}
                        <input type="hidden" name="mod[]" value="{$x}" />
                    {/foreach}
                    <input type="hidden" name="p" value="shop" />
                    <input type="hidden" name="area" value="{$area}" />
                    <input type="hidden" name="action" value="to_cart" />
                    <input type="hidden" name="redir" value="{page_link}" />
                    <input type="hidden" name="product_id" value="{$p->Id}" />
                    {if $p->Endpreis > 0}
                        <a class="uk-button uk-button-small" href="javascript: document.forms['to_basket_{$p->Id}'].submit();">
                          <i data-uk-icon="shop-basket" data-uk-tooltip title="{#Shop_toBasket#}"></i>
                        </a>
                    {/if}
                  </form>
                  <form class="uk-inline" name="delete_item_{$p->Id}" method="post" action="index.php">
                    <input name="amount" type="hidden" value="{$p->Anzahl}" />
                    {foreach from=$p->Varianten item=x}
                        <input type="hidden" name="mod[]" value="{$x}" />
                    {/foreach}
                    <input type="hidden" name="p" value="shop" />
                    <input type="hidden" name="area" value="{$area}" />
                    <input type="hidden" name="action" value="delitem_mylist" />
                    <input type="hidden" name="redir" value="{page_link}" />
                    <input type="hidden" name="product_id" value="{$p->Id}" />
                    <a class="uk-button uk-button-small" href="javascript: document.forms['delete_item_{$p->Id}'].submit();">
                      <i data-uk-icon="close" data-uk-tooltip title="{#Delete#}"></i>
                    </a>
                  </form>
                  </div>

                  <a href="{$p->ProdLink}">{$p->Titel|sanitize}</a>
                  <div class="small">{#Shop_f_artNr#}: {$p->Artikelnummer}</div>
                  {foreach from=$p->Vars item=v}
                      <div class="small">{$v->KatName}: {$v->Name} ({$v->Wert|numformat} {$currency_symbol})</div>
                  {/foreach}
                </div>

                <div class="uk-padding-small uk-text-center">
                  {$p->Anzahl}
                </div>

                <div class="uk-padding-small">
                  {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                      {if $p->Preis > 0}
                          <div class="small">{#Shop_netto#} {$p->Preis|numformat} {$currency_symbol}</div>
                          {if $show_vat_table == 1}
                              <div class="small">{#Shop_brutto#} {$p->Preis_b|numformat} {$currency_symbol}</div>
                          {/if}
                      {else}
                          {#Zvonite#}
                      {/if}
                  {else}
                      {#Shop_prices_justforUsers#}
                  {/if}
          
                  {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                      {if $p->Endpreis > 0}
                          <div class="small">{#Shop_netto#} {$p->Endpreis|numformat} {$currency_symbol}</div>
                          {if $show_vat_table == 1}
                              <div class="small">{#Shop_brutto#} {$p->Preis_bs|numformat} {$currency_symbol}</div>
                          {/if}
                      {else}
                          {#Zvonite#}
                      {/if}
                  {else}
                      {#Shop_prices_justforUsers#}
                  {/if}
                </div>

          </div>
          {/foreach}
        </div>

        <form method="post" action="index.php">
          <input type="hidden" name="p" value="shop" />
          <input type="hidden" name="area" value="{$area}" />
          <input type="hidden" name="action" value="new_list" />

          <div class="uk-text-center">
            <input class="uk-button uk-button-default uk-button-small" type="submit" value="{#Shop_mylist_new#}" />
          </div>
        </form>

      </div>

        {if !$loggedin}
            {#Shop_mylist_save_notlogged#}
        {else}
            <form method="post" action="index.php">
              <input type="hidden" name="p" value="shop" />
              <input type="hidden" name="area" value="{$area}" />
              <input type="hidden" name="action" value="mylist" />
              <input type="hidden" name="subaction" value="save_list" />

              <div class="input-group">
                <input class="form-control" name="Name_Merkzettel" value="{#GlobalTitle#}" type="text" />
                <span class="input-group-append">
                  <input class="uk-button uk-button-default" value="{#Shop_mylist_save#}" type="submit" />
                </span>
              </div>
            </form>
        {/if}

  {/if}


  <h1 class="uk-heading-line"><span>{#Shop_mylist_load#}</span></h1>

    {if !$loggedin}
        <div data-uk-alert class="card-vk uk-alert-warning">
          {#Shop_mylist_load_notlogged#}
        </div>
    {else}
        {if $myLists}
          <div class="card-vk">
            <div data-uk-grid class="uk-child-width-1-3@s uk-grid-collapse">
              <div>{#Shop_mylist_list#}</div>
              <div>{#Date#}</div>
              <div>{#GlobalActions#}</div>
            </div>

            {foreach from=$myLists item=ml}
                <div data-uk-grid class="uk-child-width-1-3@s uk-grid-collapse {cycle name='mylist' values='uk-background-muted,uk-background-default'}">
 
                  <div>
                    <a href="index.php?p=shop&amp;action=mylist&amp;subaction=load_list&amp;id={$ml->Id}">{$ml->Name|sanitize}</a>
                  </div>
                  <div>{$ml->Datum|date_format:$lang.DateFormat}</div>
                  <div>
                    <a href="index.php?p=shop&amp;action=mylist&amp;subaction=del_list&amp;id={$ml->Id}">
                      <i data-uk-icon="close" data-uk-tooltip title="{#Shop_mylist_dellist#}"></i>
                    </a>
                  </div>

                </div>
            {/foreach}
          </div>
        {else}
          <div data-uk-alert class="uk-alert-warning">
            {#Shop_mylist_nothing#}
          </div>
        {/if}
    {/if}

</div>