<div class="h1 title">{#Shop_go_myorders#}</div>
<div class="box-content">
  {if !$loggedin}
      {#Shop_myListError#}
  {else}

{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('.mydownloads a').colorbox({ height: '95%', width: '80%', iframe: true });
    $.validator.setDefaults({
        submitHandler: function() {
            document.forms['cf'].submit();
        }
    });
    $('#requestform').validate({
	rules: {
            subject: { required: true,minlength: 5 },
	    text: { required: true,minlength: 50 }
        },
	messages: { }
    });
    {if !empty($error)}
    $('#request').show();
    {/if}
});
function orderRequest(id) {
    var text = '{#order_text1#}';
    text = text.replace(/__ORDER__/gi, id);
    text = text.replace(/__USER__/gi, '{$whole_name}');
    $('#request_text').val(text);
    $('#requestsubject').val('{#order_text2#} ' + id);
    $('#request').show();
}
//-->
</script>

      <div class="wrapper">
        {#Shop_myorders_inf#}
      </div>

      <div id="request" style="display: none">
        <div class="h3 title">{#Shop_zapros#}</div>
        {if !empty($error)}
            <div class="box-error">
              {foreach from=$error item=err}
                  <div>{$err}</div>
              {/foreach}
            </div>
        {/if}
        <div class="wrapper" id="request_form">
          <form id="requestform" name="cf" method="post" action="#request">

            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="requestsubject">{#GlobalTheme#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <input class="form-control" id="requestsubject" name="subject" type="text" value="{$smarty.post.subject}" />
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-3 col-sm-12">
                <label class="col-form-label" for="request_text">{#GlobalMessage#}</label>
              </div>
              <div class="col-md-9 col-sm-12">
                <textarea class="form-control" id="request_text" name="text" rows="5">{$smarty.post.text}</textarea>
              </div>
            </div>

            <div class="text-center">
              <input class="btn btn-primary btn-block-sm" value="{#ButtonSend#}" type="submit" />
            </div>

            <input type="hidden" name="sub" value="sendrequest" />
          </form>
        </div>
      </div>

      {foreach from=$orders_array item=o}
          <div class="wrapper">

            <div class="row row-header text-center">
              <div class="col-3">{#Date#}</div>
              <div class="col-2">{#Shop_f_ovall#}</div>
              <div class="col-1">{#GlobalStatus#}</div>
              <div class="col-2">{#Paid#}</div>
              <div class="col">{#Global_Action#}</div>
              <div class="col">{#Shop_tracking#}</div>
            </div>

            <div class="row spacer text-center {cycle name=$o->Id values='row-primary,row-secondary'}">
              <div class="col-3">{$o->Datum|date_format:$lang.DateFormat}</div>
              <div class="col-2">{$o->Betrag|numformat} {$currency_symbol}</div>
              <div class="col-1">
                {if $o->Status == 'ok'}
                    <div data-toggle="tooltip" title="{$o->SText}" class="shop-indicator bg-success"></div>
                {elseif $o->Status == 'progress'}
                    <div data-toggle="tooltip" title="{$o->SText}" class="shop-indicator bg-warning"></div>
                {elseif $o->Status == 'wait'}
                    <div data-toggle="tooltip" title="{$o->SText}" class="shop-indicator bg-dark"></div>
                {elseif $o->Status == 'oksend'}
                    <div data-toggle="tooltip" title="{$o->SText}" class="shop-indicator bg-primary"></div>
                {elseif $o->Status == 'oksendparts'}
                    <div data-toggle="tooltip" title="{$o->SText}" class="shop-indicator bg-info"></div>
                {else}
                    <div data-toggle="tooltip" title="{$o->SText}" class="shop-indicator bg-danger"></div>
                {/if}
              </div>
              <div class="col-2">
                {if $o->Payment == 1}
                    <div data-toggle="tooltip" title="{$lang.Yes}" class="shop-indicator bg-success"></div>
                {else}
                    <div data-toggle="tooltip" title="{$lang.No}" class="shop-indicator bg-danger"></div>
                {/if}
              </div>

              <div class="col">
                <a href="index.php?p=misc&amp;do=viewmyorder&amp;oid={$o->Id}">
                  <i class="icon-search" data-toggle="tooltip" title="{$lang.Shop_myorders_show}"></i>
                </a>
                <a href="javascript:void(0);" onclick="orderRequest({$o->Id});">
                  <i class="icon-chat" data-toggle="tooltip" title="{$lang.Shop_zapros}"></i>
                </a>
                {if $o->Viewpayorder == 1}
                    <a href="index.php?p=misc&amp;do=viewpayorder&amp;oid={$o->Id}">
                      <i class="icon-print" data-toggle="tooltip" title="{$lang.Print}"></i>
                    </a>
                {/if}
                {if $o->DownloadsCustom == 1}
                    <a href="index.php?p=misc&amp;do=mypersonaldownloads&amp;oid={$o->Id}">
                      <i class="icon-download" data-toggle="tooltip" title="{$lang.Shop_personalDownloads}"></i>
                    </a>
                {/if}
              </div>

              <div class="col">
                {if $o->TrackingLink}
                    <a target="_blank" href="{$o->TrackingLink}">{#Shop_trackingClick#}</a>
                    <br />
                    {#Shop_status_tracking#}: {$o->Tracking_Code}
                {else}
                    -
                {/if}
              </div>
            </div>

            {if $o->Status != 'failed'}
                <div class="spacer">
                  <div class="row-header">
                    {#Shop_status_sendeda#}
                    {if !empty($o->TrackingName)}
                        - {#Shop_sendedBy#} {$o->TrackingName}
                    {/if}
                  </div>
                  {assign var=count value=0}
                  {foreach from=$o->Items item=i}
                      {if in_array($i->Artikelnummer, $o->Verschickt)}
                          {assign var=count value=$count+1}
                          <div class="row row-secondary">
                            <div class="col-2">{$i->Artikelnummer}</div>
                            <div class="col">
                              {if !empty($i->ArtName)}
                                  <a href="index.php?p=shop&amp;action=showproduct&amp;id={$i->ArtId}&amp;cid={$i->CatId}&amp;pname={$i->ArtlName}">
                                    {$i->ArtName} ({$i->Anzahl})
                                  </a>
                              {else}
                                  {$i->Anzahl}
                              {/if}
                            </div>
                          </div>
                      {/if}
                  {/foreach}
                </div>

                <div class="spacer">
                  <div class="row-header">{#Shop_status_sendetopen#}</div>
                  {assign var=count value=0}
                  {foreach from=$o->Items item=i}
                      {if !in_array($i->Artikelnummer, $o->Verschickt)}
                          {assign var=count value=$count+1}
                          <div class="row row-secondary">
                            <div class="col-2">{$i->Artikelnummer}</div>
                            <div class="col">
                              <a href="index.php?p=shop&amp;action=showproduct&amp;id={$i->ArtId}&amp;cid={$i->CatId}&amp;pname={$i->ArtlName}">
                                {$i->ArtName} ({$i->Anzahl})
                              </a>
                            </div>
                          </div>
                      {/if}
                  {/foreach}
                </div>
            {/if}
          </div>
      {/foreach}
  </div>

  {if !empty($pages)}
      <div class="wrapper">
        {$pages}
      </div>
  {/if}

  <div class="box-content">
    <div class="row">
      <div class="col">
        <div class="shop-indicator"></div> {#GlobalStatus#}: <a href="index.php?p=shop&amp;action=myorders&amp;show=all">{#Shop_status_all#}</a>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="shop-indicator bg-success"></div> {#GlobalStatus#}: <a href="index.php?p=shop&amp;action=myorders&amp;show=ok">{#Shop_status_ok#}</a>
      </div>
      <div class="col">
        {$orders_summ.ok|numformat} {$currency_symbol}
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="shop-indicator bg-primary"></div> {#GlobalStatus#}: <a href="index.php?p=shop&amp;action=myorders&amp;show=oksend">{#Shop_status_oksend#}</a>
      </div>
      <div class="col">
        {$orders_summ.oksend|numformat} {$currency_symbol}
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="shop-indicator bg-info"></div> {#GlobalStatus#}: <a href="index.php?p=shop&amp;action=myorders&amp;show=oksendparts">{#Shop_status_oksendparts#}</a>
      </div>
      <div class="col">
        {$orders_summ.oksendparts|numformat} {$currency_symbol}
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="shop-indicator bg-dark"></div> {#GlobalStatus#}: <a href="index.php?p=shop&amp;action=myorders&amp;show=wait">{#Shop_status_wait#}</a>
      </div>
      <div class="col">
        {$orders_summ.wait|numformat} {$currency_symbol}
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="shop-indicator bg-danger"></div> {#GlobalStatus#}: <a href="index.php?p=shop&amp;action=myorders&amp;show=failed">{#Shop_status_failed#}</a>
      </div>
      <div class="col">
        {$orders_summ.failed|numformat} {$currency_symbol}
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="shop-indicator bg-warning"></div> {#GlobalStatus#}: <a href="index.php?p=shop&amp;action=myorders&amp;show=progress">{#Shop_status_progress#}</a>
      </div>
      <div class="col">
        {$orders_summ.progres|numformat} {$currency_symbol}
      </div>
    </div>
  </div>
{/if}
