{if empty($dialog)}
    {assign var=dialog value='default'}
{/if}

<script>
<!-- //
$(function() {
    $('#to-basket-{$dialog}').on('click', function() {
        {if isset($smarty.request.blanc) && $smarty.request.blanc == 1}parent.{/if}document.location = 'index.php?action=showbasket&p=shop';
    });
});
//-->
</script>

<div id="basket-{$dialog}" data-uk-modal>
  <div class="uk-modal-dialog uk-modal-body uk-border-rounded uk-text-center">
    <button class="uk-modal-close-default" type="button" onclick="closeWindow(true);" data-uk-close></button>

    <h3>{#Shop_ProdAddedToBasket#}</h3>
    <div>
        <button type="button" class="uk-button uk-button-success uk-border-rounded" id="to-basket-{$dialog}">{#Shop_go_basket#}</button>
    </div>
  </div>
</div>

<div id="favorite-{$dialog}" data-uk-modal>
  <div class="uk-modal-dialog uk-modal-body uk-border-rounded uk-text-center" role="document">
    <button class="uk-modal-close-default" type="button" onclick="closeWindow(true);" data-uk-close></button>

    <h3>{#Shop_ProdAddedToList#}</h3>
    <div>
        <a class="uk-button uk-button-primary uk-border-rounded" href="index.php?p=shop&amp;action=mylist">
          {#Shop_mylist#}
        </a>
    </div>

  </div>
</div>
