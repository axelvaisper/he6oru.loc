<div class="uk-container uk-container-small">
  <div class="uk-grid-small uk-child-width-1-2@s" data-uk-grid>

    <div>
      <h3>{#Shop_priceAlert#}</h3>
      <div class="wrapper">
        {#Shop_priceAlertInf#}
      </div>

      {if !empty($error)}
          <div class="box-error">
            {foreach from=$error item=err}
                <div>{$err}</div>
            {/foreach}
          </div>
      {/if}
    </div>

    <div>
      <form method="post" action="#pricealert">
          <label for="alert-mail">{#Email#}
            <input class="uk-border-rounded uk-input" id="alert-mail" name="pricealert_email" type="email" value="{$smarty.post.pricealert_email|sanitize}" required="required" />
          </label>

          <label for="alert-price">{#Shop_priceAlertYPrice#}
            <input class="uk-border-rounded uk-input" id="alert-price" name="pricealert_newprice" type="text" value="{$smarty.post.pricealert_newprice|sanitize}" required="required" />
          </label>

        <div class="uk-margin-small uk-text-center">
          <input class="uk-border-rounded uk-button uk-button-default uk-button-small" type="submit" value="{#ButtonSend#}" />
        </div>

        <input type="hidden" name="pricealert_send" value="1" />
        <input type="hidden" name="red" value="{$red}" />
      </form>
    </div>

  </div>
</div>