{* Выводим слайдшоу когда много картинок *}
{if $images}

<div class="uk-position-relative uk-visible-toggle uk-grid-collapse" data-uk-slideshow="min-height:450; max-height:450; animation:push" data-uk-grid>

        {* МИНИАТЮРЫ показываем на больших экранах *}
        <div class="uk-visible@s uk-width-auto uk-padding-small">
            <ul class="uk-thumbnav uk-thumbnav-vertical">
                <li class="uk-active" data-uk-slideshow-item="0">
                  <a href="#">
                    <div class="uk-cover-container">
                      <img src="{$p.Bild}" alt="" uk-cover>
                    </div>
                  </a>
                </li>
                {foreach from=$images item=im name=categ}
                <li data-uk-slideshow-item="{counter}">
                  <a href="#">
                    <div class="uk-cover-container">
                      <img src="{$im.Bild}" alt="" uk-cover>
                    </div>
                  </a>
                </li>
                {/foreach}
            </ul>
        </div>

        {* СЛАЙДЫ *}
        <ul class="uk-drag uk-slideshow-items uk-width-expand" data-uk-lightbox>

          {* первый *}
          {if empty($p.NoBild)}
            <li>
              <a href="{$p.BildPopLink}" class="uk-inline uk-height-1-1 uk-flex uk-flex-center uk-flex-middle" data-type="iframe">
                <div class="uk-width-1-1 uk-height-1-1 uk-background-contain uk-background-norepeat" style="background-image: url({$p.Bild});" data-adaptive-background data-ab-css-background>
                </div>
                <div class="uk-overlay uk-position-cover"></div>
              </a>
            </li>
          {else}
            <div class="uk-inline uk-height-1-1 uk-flex uk-flex-center uk-flex-middle">
              <div class="uk-width-1-1 uk-height-1-1 uk-background-contain uk-background-norepeat" style="background-image: url({$p.Bild});" data-adaptive-background data-ab-css-background>
              </div>
              <div class="uk-overlay uk-position-cover"></div>
            </div>
          {/if}

            {assign var=icount value=0}
            {assign var=showlink value=0}
            {* остальные *}
            {foreach from=$images item=im name=categ}
                {assign var=icount value=$icount+1}
                {if $icount < 5}
                    <li>
                      <a href="{$im.Bild_GrossLink}" class="uk-inline uk-height-1-1 uk-flex uk-flex-center uk-flex-middle" data-type="iframe">
                        <div class="uk-width-1-1 uk-height-1-1 uk-background-contain uk-background-norepeat" style="background-image: url({$im.Bild_GrossLink});">
                        </div>
                        <div class="uk-overlay uk-position-cover"></div>
                      </a>
                    </li>
                {else}
                    {assign var=showlink value=1}
                    <div style="display:none">
                      <a href="{$im.Bild_GrossLink}">
                        <img src="{$im.Bild_GrossLink}" alt="" />
                      </a>
                    </div>
                {/if}
            {/foreach}
        </ul>

        <div class="uk-slidenav-container uk-position-bottom-right uk-visible@s">
          <a class="uk-hidden-hover uk-slidenav-large" href="#" data-uk-slidenav-previous uk-slideshow-item="previous"></a>
          <a class="uk-hidden-hover uk-slidenav-large" href="#" data-uk-slidenav-next data-uk-slideshow-item="next"></a>
        </div>

        {* ТОЧКИ показываем на малых экранах *}
        <ul class="uk-hidden@s uk-slideshow-nav uk-dotnav uk-position-bottom uk-flex-center uk-margin-small-bottom"></ul>

    </div>

    {if $showlink == 1}
          <a href="index.php?p=misc&do=shopimgages&prodid={$p.Id}">
            <div class="small">{#Shop_moreImages#}</div>
          </a>
    {/if}

{* Выводим только картинку *}
{else}

  <div class="uk-position-relative uk-height-large">

    {if empty($p.NoBild)}
      <div class="uk-height-large" data-uk-lightbox>
        <a href="{$p.BildPopLink}" class="uk-inline uk-height-1-1 uk-flex uk-flex-center uk-flex-middle" data-type="iframe">
          <div class="uk-width-1-1 uk-height-1-1 uk-background-contain uk-background-norepeat" style="background-image: url({$p.Bild});" data-adaptive-background data-ab-css-background>
          </div>
        </a>
      </div>
    {else}
      <div class="uk-height-large">
        <div class="uk-inline uk-height-1-1 uk-flex uk-flex-center uk-flex-middle">
          <div class="uk-width-1-1 uk-height-1-1 uk-background-contain uk-background-norepeat" style="background-image: url({$p.Bild});" data-adaptive-background data-ab-css-background>
          </div>
        </div>
      </div>
    {/if}

  </div>

{/if}
