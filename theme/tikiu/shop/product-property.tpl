<div class="uk-container uk-margin-top uk-margin-bottom">

    {if !empty($article_pages)}
        <div class="wrapper">
          {$article_pages}
        </div>
    {/if}

<table class="uk-table uk-table-striped">

    {if intval($p.GewichtF) > 0}
        <tr>
          <td>{#Shop_ProdWeight#}</td>
          <td>
            {#Shop_ProdWeightca#} {$p.GewichtF}
            {if !empty($p.GewichtRaw)}
                ({$p.GewichtRaw} {#Shop_ProdWeightRaw#})
            {/if}
          </td>
        </tr>
    {/if}
  
    {if $p.Abmessungen}
        <tr>
          <td>{#Shop_ProdHBL#}</td>
          <td>{$p.Abmessungen|sanitize}{#Shop_ProdHBLC#}</td>
        </tr>
    {/if}
  
    {if !empty($p.EAN_Nr)}
        <tr>
          <td>EAN</td>
          <td>{$p.EAN_Nr|sanitize}</td>
        </tr>
    {/if}
  
    {if !empty($p.ISBN_Nr)}
        <tr>
          <td>ISBN</td>
          <td>{$p.ISBN_Nr|sanitize}</td>
        </tr>
    {/if}
  
    {if $p.man->Id}
        <tr>
          <td>{#Manufacturer#}</td>
          <td><a href="index.php?p=shop&amp;action=showproducts&amp;man={$p.man->Id}">{$p.man->Name}</a></td>
        </tr>
    {/if}
  
    {if !empty($p.PrCountry)}
        <tr>
          <td>{#PrCountry#}</td>
          <td>{$p.PrCountry|sanitize}</td>
        </tr>
    {/if}
  
    {if $det_spez.Spez_1 && $p.Spez_1}
        <tr>
          <td>{$det_spez.Spez_1|specialchars}</td>
          <td>{$p.Spez_1|cleantoentities|default:'-'|autowords}</td>
        </tr>
    {/if}
  
    {if $det_spez.Spez_2 && $p.Spez_2}
        <tr>
          <td>{$det_spez.Spez_2|specialchars}</td>
          <td>{$p.Spez_2|cleantoentities|default:'-'|autowords}</td>
        </tr>
    {/if}
  
    {if $det_spez.Spez_3 && $p.Spez_3}
        <div>
          <div>{$det_spez.Spez_3|specialchars}</div>
          <div>{$p.Spez_3|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_4 && $p.Spez_4}
        <div>
          <div>{$det_spez.Spez_4|specialchars}</div>
          <div>{$p.Spez_4|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_5 && $p.Spez_5}
        <div>
          <div>{$det_spez.Spez_5|specialchars}</div>
          <div>{$p.Spez_5|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_6 && $p.Spez_6}
        <div>
          <div>{$det_spez.Spez_6|specialchars}</div>
          <div>{$p.Spez_6|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_7 && $p.Spez_7}
        <div>
          <div>{$det_spez.Spez_7|specialchars}</div>
          <div>{$p.Spez_7|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_8 && $p.Spez_8}
        <div>
          <div>{$det_spez.Spez_8|specialchars}</div>
          <div>{$p.Spez_8|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_9 && $p.Spez_9}
        <div>
          <div>{$det_spez.Spez_9|specialchars}</div>
          <div>{$p.Spez_9|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_10 && $p.Spez_10}
        <div>
          <div>{$det_spez.Spez_10|specialchars}</div>
          <div>{$p.Spez_10|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_11 && $p.Spez_11}
        <div>
          <div>{$det_spez.Spez_11|specialchars}</div>
          <div>{$p.Spez_11|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_12 && $p.Spez_12}
        <div>
          <div>{$det_spez.Spez_12|specialchars}</div>
          <div>{$p.Spez_12|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_13 && $p.Spez_13}
        <div>
          <div>{$det_spez.Spez_13|specialchars}</div>
          <div>{$p.Spez_13|cleantoentities|default:'-'|autowords}</div>
        </div>
    {/if}
  
    {if $det_spez.Spez_14 && $p.Spez_14}
        <tr>
          <div>{$det_spez.Spez_14|specialchars}</div>
          <div>{$p.Spez_14|cleantoentities|default:'-'|autowords}</div>
        </tr>
    {/if}
  
    {if $det_spez.Spez_15 && $p.Spez_15}
        <tr>
          <td>{$det_spez.Spez_15|specialchars}</td>
          <td>{$p.Spez_15|cleantoentities|default:'-'|autowords}</td>
        </tr>
    {/if}
  
    {if $shopsettings->Zeige_ErschienAm == 1}
        <tr>
          <td>{#Shop_releasedProductDetail#}</td>
          <td>{$p.Erstellt|date_format: $lang.DateFormatSimple}</td>
        </tr>
    {/if}
  </table>
</div>