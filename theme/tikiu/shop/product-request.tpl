{if $smarty.request.subaction == 'product_request' || $shopsettings->AnfrageForm == 1}

{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file = "$incpath/other/jsvalidate.tpl"}
$(function () {
    $('#product_request').validate({
        rules: {
            product_request_email: { required: true, email: true },
            product_request_name: { required: true },
            product_request_text: { required: true, minlength: 10 }
        },
        submitHandler: function () {
            document.forms['product_request'].submit();
        }
    });
});
//-->
</script>

<div >
    <div class="uk-container uk-container-small uk-margin-top uk-margin-bottom">

        <h3>{#Shop_prod_request#}</h3>
        <div name="product_request">
        {if isset($msg_send) && $msg_send == 1}
            {#Shop_prod_request_thankyou#}
        {else}
            {if !empty($error)}
                <div class="box-error">
                    {foreach from=$error item=err}
                        <div>{$err}</div>
                    {/foreach}
                </div>
            {/if}

            <form name="product_request" id="product_request" method="post" action="{page_link}#product_request">
                <div class="uk-child-width-1-2@s" data-uk-grid>
                    <label for="request-email">
                        <span class="uk-text-muted">{#SendEmail_Email#}</span>
                        <input class="uk-input uk-border-rounded" id="request-email" name="product_request_email" value="{$smarty.request.product_request_email|default:$smarty.session.login_email|sanitize}" type="email" />
                    </label>

                    <label for="request-name">
                        <span class="uk-text-muted">{#Contact_myName#}</span>
                        <input class="uk-input uk-border-rounded" id="request-name" name="product_request_name" value="{$smarty.request.product_request_name|default:$whole_name|sanitize}" type="text" />
                    </label>
                </div>

                <label for="request-text">
                    <span class="uk-text-muted">{#GlobalMessage#}</span>
                    <textarea class="uk-textarea uk-border-rounded" id="request-text" name="product_request_text" rows="3">{$smarty.request.product_request_text|sanitize}</textarea>
                </label>

                {include file="$incpath/other/captcha.tpl"}

                <div class="uk-text-center uk-margin-small">
                    <input class="uk-button uk-button-default uk-border-rounded" type="submit" value="{#ButtonSend#}" />
                </div>

                <input type="hidden" name="subaction" value="product_request" />
                <input type="hidden" name="id" value="{$smarty.request.id}" />
                <input type="hidden" name="cid" value="{$smarty.request.cid}" />
                <input type="hidden" name="red" value="{$red}" />
                <input type="hidden" name="sub" value="product_request" />
                <input type="hidden" name="prod_name" value="{$p.Titel|sanitize}" />
            </form>
        {/if}
        </div>
    </div>
</div>
{/if}