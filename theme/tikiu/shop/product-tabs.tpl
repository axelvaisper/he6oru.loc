<div>
<ul data-uk-tab class="uk-flex-center" role="tablist" data-uk-sticky="media: @s; animation: uk-animation-slide-top; 
cls-active: zindex-3 uk-margin-small-top bg-gray-vk uk-box-shadow-small; offset:45">

  <li class="uk-active">
    <a href="#detail" data-uk-scroll="offset:90">
      {#buttonDetails#}
    </a>
  </li>

  <li>
    <a href="#property" data-uk-scroll="offset:90">
      {#Property#}
    </a>
  </li>

  {if $Zub_a_products_array}
      <li>
        <a>
          {$tabs->TAB1|sanitize}
        </a>
      </li>
  {/if}

  {if $Zub_b_products_array}
      <li>
        <a>
          {$tabs->TAB2|sanitize}
        </a>
      </li>
  {/if}

  {if $Zub_c_products_array}
      <li>
        <a>
          {$tabs->TAB3|sanitize}
        </a>
      </li>
  {/if}

  {if $shopsettings->similar_product == 1 && $Zub_d_products_array}
      <li>
        <a href="#similar_product" data-uk-scroll="offset:90">
          {#Shop_detailSimilar#}
        </a>
      </li>
  {/if}

  {if $prod_downloads}
      <li>
        <a>
          {#Shop_Downloads#}
        </a>
      </li>
  {/if}

</ul>

{* контент табов *}
<ul class="uk-switcher">

  <li id="detail" class="uk-active">
    {include file="$incpath/shop/products_details.tpl"}
  </li>

  <li id="property">
    {include file="$incpath/shop/product-property.tpl"}
  </li>

  {if $Zub_a_products_array}
      <li>
        {$Zub_a_products}
      </li>
  {/if}

  {if $Zub_b_products_array}
      <li>
        {$Zub_b_products}
      </li>
  {/if}

  {if $Zub_c_products_array}
      <li>
        {$Zub_c_products}
      </li>
  {/if}

  {if $shopsettings->similar_product == 1 && $Zub_d_products_array}
      <li id="similar_product">
        {$Zub_d_products}
      </li>
  {/if}

  {if $prod_downloads}
      <li>
        <div class="box-content">
          {foreach from=$prod_downloads item=pdd}
              <div class="row">
                <div class="col-auto">
                  <a  class="img-fluid" href="{$baseurl}/uploads/shop/product_downloads/{$pdd->Datei}">
                    <img src="{$imgpath}/filetypes/{$pdd->Icon}" alt="" />
                  </a>
                </div>

                <div class="col">
                  <a href="{$baseurl}/uploads/shop/product_downloads/{$pdd->Datei}">
                    {$pdd->DlName}
                  </a>
                  {$pdd->Beschreibung}
                </div>

                <div class="col">{$pdd->Size}</div>
              </div>
          {/foreach}
        </div>
      </li>
  {/if}

</ul>
</div>