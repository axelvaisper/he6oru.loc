<h1>
   {if permission('adminpanel')}
    <span data-uk-lightbox><a data-type="iframe" data-uk-icon="edit" href="/admin?do=shop&amp;sub=edit_article&amp;id={$p.Id}&amp;noframes=1&amp;langcode=1"></a></span>
    {/if}
    {$p.Titel|sanitize}
</h1>

{if $shop_bewertung == 1}
<a class="uk-button uk-button-small uk-border-rounded uk-button-default" href="#vote" data-uk-scroll="offset:90">
{#Shop_prod_vote_votesall#} ({$votes|@count})</a>
{/if}
&nbsp;
{#Shop_go_myorders#} ({$p.Klicks})
<hr>

{if $p.Fsk18 == 1}
  <div class="uk-width-auto">
      <img src="{$imgpath_page}special.png" alt="{#Shop_isFSKWarning#}" />
      <div class="small">{#Shop_isFSKWarning#}</div>
  </div>
{/if}

<input type="hidden" value="{$p.Preis|jsnum}" id="price_hidden" name="price_h" />

{* краткое описание *}
{if !empty($p.Beschreibung)}
  {$p.Beschreibung|autowords}
  <hr>
{/if}

{include file="$incpath/shop/product_price.tpl"}

{if $shopsettings->PreiseGaeste == 1 || $loggedin}
    {include file="$incpath/shop/product_amount_submit.tpl"}
    {include file="$incpath/shop/product_vars.tpl"}
    {include file="$incpath/shop/product_config.tpl"}
{/if}

{* задать вопрос, сигнал цены, хочу дешевле *}
<div>
    <ul class="uk-subnav">
        <li>
            <a href="#product_request" onclick="document.forms['product_request_form'].submit();
              return false;">{#Shop_prod_request_link#}</a>
        </li>

        {if get_active('shop_preisalarm') && $shopsettings->PreiseGaeste == 1 || $loggedin}
        <li>
            <a href="#pricealert">{#Shop_priceAlert#}</a>
        </li>
        {/if}

        {if isset($shop_cheaper)}
        <li>
            <a id="cheaper_link" href="#" title="{#cheaper_name#}">{#cheaper_name#}</a>
        </li>
        {/if}
    </ul>
</div>

<div class="uk-margin">
    <span class="uk-text-muted">{#Shop_ArticleNumber#}:</span> {$p.Artikelnummer}
</div>