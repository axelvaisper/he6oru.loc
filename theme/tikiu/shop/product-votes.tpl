{if $shop_bewertung == 1}{* отзывы *}
<div id="vote" class="bg-gray-dark">
    <div class="uk-container uk-container-large">
        <hr>
        <div class="uk-grid-small" data-uk-grid>

                {if $votes}
                    <div class="uk-width-expand@s">

                        <h3>{#Shop_prod_votes_link#} {$p.Titel|sanitize}</h3>
                        {foreach from=$votes item=v}
                        <div>
                            <div class="uk-margin-small">
                               {$v->Benutzer}, <span class="uk-text-muted">{$v->Datum|date_format:$lang_settings.Zeitformat}</span>
                               <br>
                                <span class="uk-text-muted">{#Shop_prod_vote_points#}</span>
                                <div class="rating-star" data-rating="{$v->Bewertung_Punkte}"></div>
                            </div>

                            <div>
                                <span class="uk-text-muted">{#Shop_prod_vote_auttext#}</span>
                                <br>
                                {$v->Bewertung}
                            </div>
                            <hr>
                        </div>
                        {/foreach}
                    </div>
                {else}
                    <div class="uk-width-expand@s">
                    {#Shop_prod_vote_novotes#}
                    </div>
                {/if}

                {if !permission('shop_vote')}
                    {#Shop_prod_vote_login#}<br>
                    <div>
                        <a class="uk-button uk-button-default uk-button-small uk-border-rounded" href="index.php?p=userlogin"><i data-uk-icon="sign-in"></i>{#Login_Button#}</a>
                        &nbsp;
                        {if get_active('Register')}
                            <a class="uk-button uk-button-default uk-button-small uk-border-rounded" href="index.php?p=register&amp;lang={$langcode}&amp;area={$area}">{#RegNew#}</a>
                        {/if}
                    </div>
                {else}

                    <div class="uk-width-auto@s uk-padding-small"> 
                        {script file="$jspath/jvalidate.js" position='head'}
                        <script>
                        <!-- //
                        {include file="$incpath/other/jsvalidate.tpl"}
                        $(function () {
                            $('#prod_vote_form').validate({
                                rules: {
                                    prod_vote_text: { required: true, minlength: 10 }
                                },
                                submitHandler: function () {
                                    document.forms['prod_vote_form'].submit();
                                }
                            });
                        });
                        //-->
                        </script>

                        {assign var=secure_uniqid value="two"}
                        <a name="vote_form" hidden></a>
                        {if !empty($error{$secure_uniqid})}
                            <div class="uk-alert-warning" data-uk-alert>
                                {foreach from=$error{$secure_uniqid} item=err}
                                    <div>{$err}</div>
                                {/foreach}
                            </div>
                        {/if}

                        <form name="prod_vote_form" id="prod_vote_form" method="post" action="{page_link}#vote_form">
                            <input type="hidden" name="id" value="{$smarty.request.id}" />
                            <input type="hidden" name="red" value="{$red}" />
                            <input type="hidden" name="sub" value="prod_vote" />
                            <input type="hidden" name="prod_name" value="{$p.Titel|sanitize}" />

                            <label for="prod-vote-text">
                                <textarea class="uk-textarea uk-border-rounded" id="prod-vote-text" name="prod_vote_text" rows="5" placeholder="{#Shop_prod_vote_auttext#}">{$smarty.request.prod_vote_text|sanitize}</textarea>
                            </label>

                            {include file="$incpath/other/captcha.tpl"}

                            <div class="uk-grid-small uk-margin" data-uk-grid >
                                <div class="rating-star" data-rating="4" data-rating-active="true" data-rating-input="prod_vote_points">
                                {#Shop_prod_vote_points#}</div>

                                <input class="uk-button uk-button-default uk-button-small uk-border-rounded" type="submit" value="{#RateThis#}" />
                            </div>

                        </form>
                    </div>
                {/if}

        </div>
        <hr>
    </div>
</div>
{/if}