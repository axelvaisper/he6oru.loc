{if $p.Fsk18 == 1 && $fsk_user != 1}
    {assign var="not_possible_to_buy" value=1}
{/if}

{if isset($notfound) && $notfound == 1}
    <div class="card-vk uk-text-center uk-alert-warning" data-uk-alert>
        {#Shop_errorProduct#}
    </div>
{else}

    {script file="$jspath/jvalidate.js" position='head'}
    <script>
    <!-- //
    {include file = "$incpath/other/jsvalidate.tpl"}
    $(function() {
        $('.img-popup').colorbox({
            photo: true,
            maxHeight: "90%",
            maxWidth: "100%",
            slideshow: true,
            slideshowAuto: false,
            slideshowSpeed: 2500,
            current: "{#GlobalImage#} {ldelim}current{rdelim} {#PageNavi_From#} {ldelim}total{rdelim}",
            slideshowStart: "{#GlobalStart#}",
            slideshowStop: "{#GlobalStop#}",
            previous: "{#GlobalBack#}",
            next: "{#GlobalNext#}",
            close: "{#GlobalGlose#}"
        });
        $('.img-popup-more').colorbox({ height: '95%', width: '80%', iframe: true });
        $('#tobasket').validate({
            rules: {
            {if !empty($p.Frei_1) && $p.Frei_1_Pflicht == 1}
            free_1: { required: true },
            {/if}
            {if !empty($p.Frei_2) && $p.Frei_2_Pflicht == 1}
            free_2: { required: true },
            {/if}
            {if !empty($p.Frei_3) && $p.Frei_3_Pflicht == 1}
            free_3: { required: true },
            {/if}
            amount: { required: true, number: true }
            },
            messages: { },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    {if empty($smarty.request.blanc)}
                    target: '#ajaxbasket',
                    {/if}
                    timeout: 6000,
                    success: showResponse,
                    clearForm: false,
                    resetForm: false
                });
            }
        });
    });

    function showResponse() {
        if ($('#to_mylist').val() == 1) {
          UIkit.modal('#favorite-product').show();
        } else {
            UIkit.modal('#basket-product').show();
        }
        $('#to_mylist').val(0)
    }
    //-->
    </script>


    {include file="$incpath/shop/notification.tpl" dialog='product'}

    <form method="post" name="product_request_form" action="{page_link}#product_request">
        <input type="hidden" name="subaction" value="product_request" />
    </form>

    {* IMAGE and TEXT *}
    <form class="uk-container uk-container-large" method="post" id="tobasket" action="index.php?p=shop&amp;area={$area}">
        <div class="uk-grid-small" data-uk-grid>

            <div class="uk-width-expand@m">
                <div class="uk-margin">
                    {if $not_possible_to_buy == 1}
                        <div class="box-content">
                        {$shopsettings->Fsk18}
                        </div>
                    {/if}

                    {if $shipping_free == 1}
                        <div class="shop-sticker">
                        {#Shop_freeshipping#}
                        </div>
                    {/if}

                    {if $p.diffpro > 0}
                        <div class="uk-badge uk-border-rounded">
                        {#Shop_Billiger#}{$p.diffpro|numformat}%
                        </div>
                    {/if}
                </div>
                <div data-uk-sticky="media: @s; bottom: true; offset:60; cls-active: zindex-1">
                    {include file="$incpath/shop/product-images.tpl"}
                </div>
            </div>

            <div class="uk-width-1-3@m">
                {include file="$incpath/shop/product-text.tpl"}
            </div>

        </div>
    </form>
    </header>
    {* /начало header в шаблоне shop.tpl *}

    {$shop_cheaper}
    {include file = "$incpath/shop/product-tabs.tpl"}
    {include file = "$incpath/shop/product-votes.tpl"}
    {include file = "$incpath/shop/product-request.tpl"}

    {if get_active('shop_preisalarm') && ($shopsettings->PreiseGaeste == 1 || $loggedin)}
        <div class="bg-white uk-padding" id="pricealert">
          {$price_alert}
        </div>
    {/if}

{/if}

{include file="$incpath/shop/small_seen_products.tpl"}

{if $shopsettings->vat_info_product == 1}
    {include file="$incpath/shop/vat_info.tpl"}
{/if}