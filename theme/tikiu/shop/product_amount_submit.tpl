<div class="uk-grid-small uk-margin" data-uk-grid>
{if $not_on_store == 1}

    <div class="uk-h5">{$p.VMsg|sanitize}</div>

{else}

    <input type="hidden" name="ajax" value="1" />
    <noscript>
        <input type="hidden" name="ajax" value="0" />
    </noscript>

    <div class="uk-width-expand">
        {if $p.EinzelBestellung == 1}
            <input class="uk-input uk-border-rounded" name="dis_amount" type="number" value="1" maxlength="1" disabled="disabled" />
        {else}
            <input class="uk-input uk-border-rounded" {if $not_possible_to_buy == 1}disabled="disabled"{/if} name="amount" type="number" value="{if $p.MinBestellung != 0}{$p.MinBestellung}{else}1{/if}" />
        {/if}
        <small class="uk-width1-1">
        {if $p.EinzelBestellung != 1}
            {if $p.MinBestellung != 0}
                {#Shop_min_order#} {$p.MinBestellung}
            {/if}
            &nbsp;
            {if $p.MaxBestellung != 0}
                {#Shop_max_order#} {$p.MaxBestellung}
            {else}
                {if $p.MaxBestellung == 0 && $p.MinBestellung == 0}
                    {#Shop_amount#}
                {/if}
            {/if}
        {/if}
        </small>
    </div>

    {* в корзину *}
    <div class="uk-width-auto">
        {if $not_on_store != 1}
            {if $p.Preis > 0}
            <button {if $not_possible_to_buy == 1}disabled="disabled"{/if} class="uk-button uk-border-rounded uk-button-success" type="submit">
                <span data-uk-icon="shop-basket" class="uk-margin-small-right"></span>
                {#Shop_toBasket#}
            </button>
            {/if}
             {if $p.Preis > 0}
                <div class="uk-text-center">
                    {include file="$incpath/shop/tax_inf_small.tpl"}
                </div>
            {/if}
        {/if}
    </div>

    {* срок доставки *}
    <div class="uk-width-1-1">
    {if $not_on_store == 1}
        {$p.VMsg|sanitize}
    {else}
        <span class="uk-text-muted">{#Shop_shipping_timeinf#}:</span>
        {if $order_for_you == 1}
            {$available_array.3->Name|sanitize}
        {else}
            {$p.Lieferzeit|sanitize}
        {/if}

        {if $p.Lagerbestand > 0}
            {if $low_amount == 1}
                <div class="text-danger">
                {#Shop_lowAmount#}
                <div>
                    {$lang.ShopLowWarnInf|replace:'__COUNT__':$p.Lagerbestand}
                </div>
                </div>
            {elseif $shopsettings->Zeige_Lagerbestand == 1}
                {#Shop_av_storeAv#}: {$p.Lagerbestand}
            {/if}
        {/if}
    {/if}
    </div>

    <div class="uk-width-1-1">
        <ul class="uk-subnav">
            <li>
                <button {if $not_possible_to_buy == 1}disabled="disabled"{/if} onclick="document.getElementById('to_mylist').value = '1';" type="submit" class="uk-button uk-button-link">
                    <span data-uk-icon="heart-line" title="{#Shop_WishList#}"></span>
                    <small class="uk-visible@s">{#Shop_WishList#}</small>
                </button>
                <input type="hidden" name="mylist" id="to_mylist" value="0" />
            </li>

            {if get_active('shop_merge')}
                <li data-uk-lightbox>
                    <a data-type="iframe" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1&amp;prodid={$p.Id}&amp;cid={$p.Kategorie}">
                        <i data-uk-icon="list" title="{#Merge#}"></i>
                        <small>{#Merge#}</small>
                    </a>
                </li>
            {/if}

            <li>
                <a href="https://vk.com/share.php?url={$baseurl}/{$p.ProdLink}" title="{#Share#}">
                    <i data-uk-icon="share" title="{#Share#}"></i>
                    <small class="uk-visible@s">{#Share#}</small>
                </a>
            </li>
        </ul>
        <div class="uk-margin">{#Shop_Availablility#}: {$p.VIcon}</div>
    </div>

    {if $not_possible_to_buy != 1}
        <input type="hidden" name="action" value="to_cart" />
        <input type="hidden" name="redir" value="{page_link|urldecode}" />
        <input type="hidden" name="product_id" value="{$p.Id}" />
    {/if}

{/if}
</div>