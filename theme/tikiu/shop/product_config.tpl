{if (!empty($p.Frei_1) || !empty($p.Frei_2) || !empty($p.Frei_3))}
    <div class="uk-margin">
      <div class="h5">
        {#Konfiguration#}
      </div>

      {if $p.Frei_1}
          <div class="row form-group">
            <div class="col-md-3 col-sm-12">
              <label class="col-form-label{if $p.Frei_1_Pflicht == 1} required{/if}" for="lfree_1">{$p.Frei_1}</label>
            </div>
            <div class="col-md-9 col-sm-12">
              <input class="form-control" name="free_1" id="lfree_1" type="text" />
            </div>
          </div>
      {/if}

      {if $p.Frei_2}
          <div class="row form-group">
            <div class="col-md-3 col-sm-12">
              <label class="col-form-label{if $p.Frei_2_Pflicht == 1} required{/if}" for="lfree_2">{$p.Frei_2}</label>
            </div>
            <div class="col-md-9 col-sm-12">
              <input class="form-control" name="free_2" id="lfree_2" type="text" />
            </div>
          </div>
      {/if}

      {if $p.Frei_3}
          <div class="row form-group">
            <div class="col-md-3 col-sm-12">
              <label class="col-form-label{if $p.Frei_3_Pflicht == 1} required{/if}" for="lfree_3">{$p.Frei_3}</label>
            </div>
            <div class="col-md-9 col-sm-12">
              <input class="form-control" name="free_3" id="lfree_3" type="text" />
            </div>
          </div>
      {/if}
    </div>
{/if}
