<script>
<!-- //
function switchImage(data) {
    document.getElementById('img').innerHTML = '<img class="img-fluid" src="' + data + '" alt="" />';
}
//-->
</script>

<div class="h1 title">{$title_html}</div>
<div class="wrapper">
  <div class="d-flex flex-wrap justify-content-center">
    {foreach from=$images item=im}
        <a href="{$im.Bild_Normal}" onclick="switchImage('{$im.Bild_Normal}');return false;">
          <img class="img-thumbnail height-4" src="{$im.Bild}" alt="" />
        </a>
    {/foreach}
  </div>
</div>
<div class="wrapper text-center">
  <span id="img">
    <img class="img-fluid" src="{$prod_image}" alt="" />
  </span>
</div>

<div class="text-center">
  <button class="btn btn-secondary" onclick="closeWindow();">{#WinClose#}</button>
</div>
