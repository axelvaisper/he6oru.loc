{if get_active('shop_merge')}
    <div class="h1 title">{#Shop_mergeTitle#}</div>
    <div class="wrapper">
      {#Shop_mergeHeadInf#}
    </div>
    {if !$smarty.session.cid}
        <div class="wrapper h4">{#Shop_mergeEmpty#}</div>
    {else}
        <div class="row form-group">
          <div class="col-md-3 col-sm-12">
            <label class="col-form-label" for="cid_s">{#Shop_mergeList#}</label>
          </div>
          <div class="col-md-9 col-sm-12">
            <div class="input-group form-group">
              <select class="form-control" name="cid" id="cid_s" onchange="eval(this.options[this.selectedIndex].value);">
                {foreach from=$cats item=c}
                    {assign var=session_cid value=$c->Id}
                    {if $smarty.session.cid[$session_cid]}
                        <option value="location.href='{page_link|replace:'redir=1':'redir=0'}&categ={$c->Id}'" {if $smarty.request.categ == $c->Id}selected="selected" {/if}>{$c->CatName}</option>
                    {/if}
                {/foreach}
              </select>
              <span class="input-group-append">
                <input class="btn btn-primary" type="button" onclick="eval(document.getElementById('cid_s').options[document.getElementById('cid_s').selectedIndex].value);" value="{#GlobalShow#}" />
              </span>
            </div>
          </div>
        </div>

        {if $merged}
            {foreach from=$cats item=c}
                {if $smarty.request.categ == $c->Id}
                    <div class="h3 title">{$c->CatName}</div>
                {/if}
            {/foreach}

            <div class="row flex-nowrap spacer">
              <div class="col"></div>
              {foreach from=$merged item=p}
                  <div class="col">
                    <a href="#" onclick="window.opener.location.href = '{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&blanc=1{/if}';">
                      <img class="img-fluid" src="{$p.Bild}" alt="" />
                    </a>
                  </div>
              {/foreach}
            </div>

            <div class="row flex-nowrap spacer">
              <div class="col">{#Shop_priceNow#}</div>
              {foreach from=$merged item=p}
                  <div class="col">
                    {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                        {if $p.Preis > 0}
                            {if $p.no_vars == 1}
                                {if $p.not_on_store == 1}
                                    <div class="small">
                                      {#Shop_notAvailableInf#}
                                    </div>
                                {/if}
                                {#Shop_priceNow#}
                                <span class="h4 shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                                {if $price_onlynetto != 1}
                                    <div class="small">
                                      {#Shop_netto#} {$p.netto_price|numformat} {$currency_symbol}
                                    </div>
                                    <div class="small">
                                      ({#Shop_icludes#} {$p.product_ust}% {#Shop_vat#})
                                    </div>
                                {/if}
                                {if $price_onlynetto == 1 && !empty($p.price_ust_ex)}
                                    <div class="small">
                                      {#Shop_exclVat#} {$p.product_ust}% {#Shop_vat#}
                                    </div>
                                    <div class="small">
                                      ({$p.price_ust_ex|numformat} {$currency_symbol})
                                    </div>
                                {/if}
                            {/if}
                            {if $p.no_vars != 1}
                                {if !empty($smarty.request.first_value)}
                                    {#Shop_priceNow#}
                                {else}
                                    {#Shop_priceFrom#}
                                {/if}
                                <span class="h4 shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                                {if $price_onlynetto != 1}
                                    <div class="small">
                                      {#Shop_netto#} {$p.netto_price|numformat} {$currency_symbol}
                                    </div>
                                    <div class="small">
                                      ({#Shop_icludes#} {$p.product_ust}% {#Shop_vat#})
                                    </div>
                                {/if}
                                {if $price_onlynetto == 1 && !empty($p.price_ust_ex)}
                                    <div class="small">
                                      {#Shop_exclVat#} {$p.product_ust}% {#Shop_vat#}
                                    </div>
                                    <div class="small">
                                      ({$p.price_ust_ex|numformat} {$currency_symbol})
                                    </div>
                                {/if}
                            {/if}
                        {else}
                            <div class="h4 shop-price">{#Zvonite#}</div>
                        {/if}
                    {else}
                        {#Shop_prices_justforUsers#}
                    {/if}
                  </div>
              {/foreach}
            </div>

            <div class="row flex-nowrap spacer">
              <div class="col">{#Shop_Availablility#}</div>
              {foreach from=$merged item=p}
                  <div class="col">
                    {if $p.not_on_store != 1}
                        {$p.VIcon}
                    {/if}
                  </div>
              {/foreach}
            </div>

            {if $det_spez.Spez_1}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_1|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_1|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_2}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_2|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_2|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_3}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_3|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_3|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_4}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_4|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_4|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_5}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_5|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_5|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_6}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_6|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_6|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_7}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_7|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_7|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_8}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_8|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_8|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_9}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_9|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_9|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_10}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_10|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_10|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_11}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_11|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_11|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_12}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_12|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_12|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_13}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_13|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_13|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_14}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_14|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_14|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}
            {if $det_spez.Spez_15}
                <div class="row flex-nowrap spacer">
                  <div class="col">{$det_spez.Spez_15|specialchars}</div>
                  {foreach from=$merged item=p}
                      <div class="col">
                        {$p.Spez_15|specialchars|default:'-'}
                      </div>
                  {/foreach}
                </div>
            {/if}

            <div class="row flex-nowrap spacer">
              <div class="col">{#GlobalActions#}</div>
              {foreach from=$merged item=p}
                  <div class="col">
                    {if $p.no_vars == 1 && $p.not_on_store != 1 && $p.Preis > 0}
                        {if $smarty.request.reload == 1}
<script>
<!-- //
window.opener.location.reload();
//-->
</script>
                        {/if}
                        <form class="d-inline" method="post" name="merge_to_basket_{$p.Id}" action="index.php?p=shop&amp;area={$area}">
                          <input type="hidden" name="amount" value="1" />
                          <input type="hidden" name="action" value="to_cart" />
                          <input type="hidden" name="ajax" value="2" />
                          <input type="hidden" name="redir" value="{page_link}" />
                          <input type="hidden" name="product_id" value="{$p.Id}" />

                          <a href="javascript:void(0);" onclick="javascript:document.forms['merge_to_basket_{$p.Id}'].submit();">
                            <i class="icon-basket size-xl" data-toggle="tooltip" title="{#Shop_toBasket#}"></i>
                          </a>
                        </form>
                    {elseif $p.no_vars != 1 && $p.not_on_store != 1}
                        <a href="javascript:void(0);" onclick="window.opener.location.href = '{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}';window.close();">
                          <i class="icon-info-circled  size-xl" data-toggle="tooltip" title="{#buttonDetails#}"></i>
                        </a>
                    {/if}
                    <a href="javascript:void(0)" onclick="location.href = 'index.php?delproduct={$p.Id}&redir=0&p=misc&do=mergeproduct&cid={$smarty.request.categ}&categ={$smarty.request.categ}&red=0';">
                      <i class="icon-cancel size-xl" data-toggle="tooltip" title="{#Shop_delItem#}"></i>
                    </a>

                  </div>
              {/foreach}
            </div>
        {else}
            {#Shop_mergeEmptyList#}
        {/if}
    {/if}

{/if}
