{if $shopsettings->PreiseGaeste == 1 || $loggedin}

    {if $price_onlynetto != 1}
        {include file="$incpath/shop/price_detail_netto.tpl"}
    {elseif $price_onlynetto == 1 && !empty($p.price_ust_ex)}
        {include file="$incpath/shop/price_detail.tpl"}
    {else}
        {include file="$incpath/shop/price_detail_novat.tpl"}
    {/if}

    {include file="$incpath/shop/product_volumes.tpl"}

    <div class="uk-grid-small" data-uk-grid>

        {if $p.Preis_Liste > 0}
        <div class="uk-width-auto">
            <span class="shop-price-old">
            <s id="price_list">{$p.Preis_Liste|numformat}</s> {$currency_symbol}
            </span>
            <br>
            <span id="you_saved" style="display: none"></span>
            <small>
            {#Shop_usave#} {$p.diff|numformat} {$currency_symbol} ({$p.diffpro|numformat}%)
            </small>
        </div>
        {/if}

        <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="uk-width-expand uk-text-right shop-price">
            <meta itemprop="availability" content="InStock">
            <meta itemprop="price" content="{$p.Preis|numformat}">
            <meta itemprop="priceCurrency" content="{$currency_symbol}">
            {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                {if $p.Preis > 0}
                    <span id="new_price">{$p.Preis|numformat}</span> {$currency_symbol}
                {else}
                    <span class="shop-price-text">{#Zvonite#}</span>
                {/if}
            {else}
                {#Shop_prices_justforUsers#}
            {/if}
        </div>

    </div>

{else}
    {#Shop_prices_justforUsers#}
{/if}