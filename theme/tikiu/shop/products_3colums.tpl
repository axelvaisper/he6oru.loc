<script>
$(function() {
    var options = { target: '#ajaxbasket', timeout: 3000 };
    $('.ajax_products').submit(function() {
        var id = '#mylist_' + $(this).attr('id');
        if ($(id).val() == 1) {
            UIkit.modal('#favorite-products').show();
        } else {
             UIkit.modal('#basket-products').show();
        }
        $(this).ajaxSubmit(options);
        $(id).val(0);
        return false;
    });
});
</script>

{include file="$incpath/shop/notification.tpl" dialog='products'}



{* описание категории и табы *}
{if $shopsettings->TopNewOffersPos == 'top'}
<div class="uk-margin-small-top uk-margin-small-bottom bg-white card-vk uk-child-width-expand@m uk-grid-collapse" data-uk-grid>
    {if $cat_desc}
    <div class="uk-width-auto@m uk-padding-small">
        {$cat_desc}
    </div>
    {/if}

    <div>
        {include file="$incpath/shop/categ_tabs.tpl"}
    </div>
</div>
{/if}


<div class="bg-white card-vk uk-padding-small">
    <div class="uk-grid-small" data-uk-grid>

        {* {if $smarty.request.s == 1} *}
        <div class="uk-width-1-5@m uk-flex-last uk-flex-first@m">
            {include file="$incpath/shop/search_extended.tpl"}
        </div>
        {* {/if} *}

        <div class="uk-width-expand@m">
            {include file="$incpath/shop/products_headernavi.tpl" position="top"}

            {* если пусто *}
            {if !$products}

                {if !empty($smarty.request.shop_q)}
                    {#Shop_searchNull#}
                {else}
                    {#Shop_noProducts#}
                {/if}

            {* карточки *}
            {else}
                <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-grid-small" data-uk-grid>

                    {assign var=pcc value=0}
                    {foreach from=$products item=p name=pro}
                    <div id="prod_anchor_{$p.Id}">
                        <form method="post" name="products_{$p.Id}" id="ajax_{$p.Id}" class="ajax_products" action="{if empty($p.Vars)}index.php?p=shop&amp;area={$area}{else}index.php?p=shop&amp;area={$area}&amp;action=showproduct&amp;id={$p.Id}{/if}">
                        <div class="uk-card card-vk">

                            {* image *}
                            <div class="uk-card-media-top" data-toggle="tooltip" title="{$p.Beschreibung|tooltip:500}">
                                {if $shopsettings->popup_product == 1}
                                    <span data-uk-lightbox><a data-type="iframe" href="{$p.ProdLink}&amp;blanc=1">
                                        <img data-src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" data-uk-img/>
                                    </a></span>
                                {else}
                                    <a href="{$p.ProdLink}">
                                        <img data-src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" data-uk-img/>
                                    </a>
                                {/if}
                            </div>

                            {* text *}
                            <div class="uk-padding-small">
                                <p>
                                    {if $shopsettings->popup_product == 1}
                                        <span data-uk-lightbox><a data-type="iframe" href="{$p.ProdLink}&amp;blanc=1">{$p.Titel|truncate:45|sanitize}</a></span>
                                    {else}
                                        <a href="{$p.ProdLink}">{$p.Titel|truncate:45|sanitize}</a>
                                    {/if}
                                </p>

                                {if $shopsettings->PreiseGaeste == 1 || !$loggedin}
                                    <div class="small">
                                    {if $p.Preis_Liste != $p.Preis}
                                        {#Shop_instead#}
                                        <span class="shop-price-old">{$p.Preis_Liste|numformat} {$currency_symbol}</span>
                                    {/if}
                                    </div>
                                    {if !empty($p.Vars)}
                                        {#Shop_priceFrom#}
                                    {/if}
                                    {if $p.Preis > 0}
                                        <div class="uk-text-muted">
                                            {include file="$incpath/shop/tax_inf_small.tpl"}
                                            <div class="uk-float-right">{#Shop_go_myorders#} ({$p.Klicks})</div>
                                        </div>
                                        <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                                    {else}
                                        <span class="shop-price">{#Zvonite#}</span>
                                    {/if}
                                {else}
                                    {#Shop_prices_justforUsers#}
                                {/if}

                                {if $p.Fsk18 == 1 && $fsk_user != 1}
                                    {if $shopsettings->popup_product == 1}
                                        <button class="uk-button uk-button-default uk-button-small" type="button" onclick="newWindow('{$p.ProdLink}&amp;blanc=1', '90%', '97%');">{#buttonDetails#}</button>
                                    {else}
                                        <button class="uk-button uk-button-default uk-button-small" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
                                    {/if}
                                {else}
                                    {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                                        <input type="hidden" name="action" value="to_cart" />
                                        <input type="hidden" name="redir" value="{page_link}#prod_anchor_{$p.Id}" />
                                        <input type="hidden" name="product_id" value="{$p.Id}" />
                                        <input type="hidden" name="mylist" id="mylist_ajax_{$p.Id}" value="0" />
                                        <input type="hidden" name="ajax" value="1" />
                                        <noscript>
                                        <input type="hidden" name="ajax" value="0" />
                                        </noscript>
                                        <button class="uk-button uk-button-small uk-border-rounded uk-button-success" title="{#Shop_toBasket#}" type="submit" data-uk-tooltip>
                                            <span data-uk-icon="shop-basket"></span>
                                        </button>
                                        <a class="uk-button uk-button-small" onclick="document.getElementById('mylist_ajax_{$p.Id}').value = '1';" type="submit" title="{#Shop_WishList#}" data-uk-icon="heart-line" data-uk-tooltip></a>
                                    {else}
                                        <input type="hidden" name="parent" value="{$p.Parent}" />
                                        <input type="hidden" name="navop" value="{$p.Navop}" />
                                        <input type="hidden" name="cid" value="{$p.Kategorie}" />
                                        {if $shopsettings->popup_product == 1}
                                            <button class="uk-button uk-button-small uk-button-default uk-border-rounded" type="button" onclick="newWindow('{$p.ProdLink}&amp;blanc=1', '90%', '97%');">{#buttonDetails#}</button>
                                        {else}
                                            <button class="uk-button uk-button-small uk-button-default uk-border-rounded" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
                                        {/if}
                                    {/if}
                                {/if}

                                {if get_active('shop_merge')}
                                    <span data-uk-lightbox><a data-type="iframe" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1&amp;prodid={$p.Id}&amp;cid={$p.Kategorie}" data-uk-icon="list" title="{#Merge#}" data-uk-tooltip>
                                    </a></span>
                                {/if}
                                <a href="https://vk.com/share.php?url={$baseurl}/{$p.ProdLink}" data-uk-icon="share" class="uk-button uk-button-small" title="{#Share#}" data-uk-tooltip></a>
                            </div>
                            </div>
                        </form>
                        </div>

                    {/foreach}
                </div>
            {/if}
        </div>

    </div>
</div>

{include file="$incpath/shop/products_headernavi.tpl" position="bottom"}

{* описание категории и табы *}
<div class="uk-margin-small-top uk-margin-small-bottom bg-white card-vk uk-grid-small" data-uk-grid>
    {if $cat_desc}
    <div class="uk-width-auto@m uk-padding-small">
        {$cat_desc}
    </div>
    {/if}

{if $shopsettings->TopNewOffersPos == 'bottom'}
    <div class="uk-width-expand@m">
        {include file="$incpath/shop/categ_tabs.tpl"}
    </div>
{/if}
</div>

{if $smarty.request.s != '1'}
<div>
    {include file="$incpath/shop/products_navi_bottom.tpl"}
</div>
{/if}

{include file="$incpath/shop/small_seen_products.tpl"}

{if $shopsettings->vat_info_cat == 1}
<div>
    {include file="$incpath/shop/vat_info.tpl"}
</div>
{/if}
