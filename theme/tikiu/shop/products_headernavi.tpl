{if empty($no_colums)}
<script>
<!-- //
$(function () {
    $('#avail-click-{$position}').on('click', function (e) {
        $('#avail-popup-{$position}').slideToggle(300);
        e.stopPropagation();
    });
    $(document).on('click', function () {
        $('#avail-popup-{$position}').slideUp(300);
    });
});
//-->
</script>

    {assign var=shop_q value=$smarty.request.shop_q|urlencode|default:'empty'}
    {assign var=avail value=''}
    {if !empty($smarty.request.avail)}
        {assign var=avail value='&amp;avail='|cat:$smarty.request.avail}
    {/if}

    {if $position == 'top'}
        <div class="uk-child-width-1-2@s uk-margin-bottom" data-uk-grid>
          {if !isset($smarty.request.action) || $smarty.request.action != 'start'}
          <div>
            {* {#SortBy#}: *}
            <ul class="uk-subnav">
              <li>
                <a class="mr-3" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$title_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
                  {#SortName#}&nbsp;<i data-uk-icon="{$img_title_sort|default:"more-vertical"}"></i>
                </a>
              </li>
              <li><a class="mr-3" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$art_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
                {#SortArtikel#}&nbsp;<i data-uk-icon="{$img_art_sort|default:"more-vertical"}"></i>
              </a></li>
              <li><a class="mr-3" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$price_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
                {#Shop_sortPrice#}&nbsp;<i data-uk-icon="{$img_price_sort|default:"more-vertical"}"></i>
              </a></li>
              <li><a class="mr-3" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$date_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
                {#SortDate#}&nbsp;<i data-uk-icon="{$img_date_sort|default:"more-vertical"}"></i>
              </a></li>
              <li>
                <a class="mr-3" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$klick_sort}&amp;s={$smarty.request.s|default:'0'}{$avail}">
                  {#SortKlicks#}&nbsp;<i data-uk-icon="{$img_klick_sort|default:"more-vertical"}"></i>
                </a>
              </li>
            </ul>
          </div>
          {/if}

          {* <div class="shop-avail-cont">
            <img id="avail-click-{$position}" src="{$imgpath}/shop/avail-{$smarty.request.avail|default:'0'}.png" alt="" data-toggle="tooltip" title="{#ShopShowLegend#}" />
            <div class="shop-avail-popup" id="avail-popup-{$position}">
              <a data-toggle="tooltip" title="{#ShopAllProducts#}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=0"><img src="{$imgpath}/shop/avail-0.png" alt="" /></a>
              <a data-toggle="tooltip" title="{$available_array.0->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=1"><img src="{$imgpath}/shop/avail-1.png" alt="" /></a>
                {if $shopsettings->AvailType == 1}
                <a data-toggle="tooltip" title="{$available_array.4->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=5"><img src="{$imgpath}/shop/avail-5.png" alt="" /></a>
                <a data-toggle="tooltip" title="{$available_array.1->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=2"><img src="{$imgpath}/shop/avail-2.png" alt="" /></a>
                <a data-toggle="tooltip" title="{$available_array.2->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=3"><img src="{$imgpath}/shop/avail-3.png" alt="" /></a>
                <a data-toggle="tooltip" title="{$available_array.3->Name|tooltip}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit={$smarty.request.limit|default:'20'}&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}&amp;avail=4"><img src="{$imgpath}/shop/avail-4.png" alt="" /></a>
                {/if}
            </div>
          </div>*}

        </div>
    {/if}

    {if $position == 'bottom'}
        <div class="uk-child-width-1-2@s uk-margin" data-uk-grid>
          <div class="uk-text-center">
            <span class="mr-2">{#DataRecords#}:</span>
            <a class="{if $smarty.request.limit == 6}page_active{else}page_navigation{/if}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=6&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">6</a>
            <a class="{if $smarty.request.limit == 10}page_active{else}page_navigation{/if}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=10&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">10</a>
            <a class="{if $smarty.request.limit == 20 || empty($smarty.request.limit)}page_active{else}page_navigation{/if}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=20&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">20</a>
            <a class="{if $smarty.request.limit == 50}page_active{else}page_navigation{/if}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=50&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">50</a>
            <a class="{if $smarty.request.limit == 100}page_active{else}page_navigation{/if}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=100&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">100</a>
            <a class="{if $smarty.request.limit == 200}page_active{else}page_navigation{/if}" href="index.php?shop_q={$smarty.request.shop_q|urlencode|default:'empty'}&amp;man={$smarty.request.man|default:'0'}&amp;p=shop&amp;action=showproducts&amp;cid={$smarty.request.cid|default:'0'}&amp;page={$smarty.request.page|default:'1'}&amp;limit=200&amp;pf={$smarty.request.pf}&amp;pt={$smarty.request.pt}&amp;list={$smarty.request.list|default:$shopsettings->Sortable_Produkte}&amp;s={$smarty.request.s|default:'0'}{$avail}">200</a>
          </div>

          {if !empty($pages)}
          <div class="uk-text-center">
              {$pages}
          </div>
          {/if}
        </div>
    {/if}
{/if}
