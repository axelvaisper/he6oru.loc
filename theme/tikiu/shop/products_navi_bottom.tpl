{assign var=categ value=$smarty.request.cid|default:'0'}
<ul class="uk-subnav uk-flex-center">
    {if get_active('shop_newinshop_navi')}
        <li>
            <a class="m-1 page_navigation" href="index.php?p=shop&amp;action=showproducts&amp;page=1&amp;cid={$categ}&amp;limit={$smarty.request.limit|default:$plim}">{#Shop_newProducts#}</a>
        </li>
    {/if}
    {if get_active('shop_angebote')}
        <li>
            <a class="m-1 page_navigation" href="index.php?p=shop&amp;action=showproducts&amp;page=1&amp;offers=1&amp;cid={$categ}&amp;limit={$smarty.request.limit|default:$plim}">{#Shop_Offers#}</a>
        </li>
    {/if}
    {if get_active('shop_topseller')}
        <li>
            <a class="m-1 page_navigation" href="index.php?p=shop&amp;action=showproducts&amp;page=1&amp;topseller=1&amp;cid={$categ}&amp;limit={$smarty.request.limit|default:$plim}">{#Shop_Topseller#}</a>
        </li>
    {/if}
    {if $shopsettings->menu_low_amount == 1}
        <li>
            <a class="m-1 page_navigation" href="index.php?p=shop&amp;action=showproducts&amp;page=1&amp;lowamount=1&amp;cid={$categ}&amp;limit={$smarty.request.limit|default:$plim}">{#Shop_lowProducts#}</a>
        </li>
    {/if}
</ul>
