{* {script file="$jspath/jsuggest.js" position='head'}
<script>
<!-- //
$(function () {
    $('#qs').suggest('{$baseurl}/lib/ajax.php?action=shop&key=' + Math.random(), {
        onSelect: function () {
            document.forms['shopssform'].submit();
        }
    });
});
//-->
</script> *}
<form method="post" action="" class="uk-border-rounded uk-grid-collapse" data-uk-grid>

	<select class="uk-select uk-width-1-1" id="ext-cid" name="cid">
	<option value="0"> - {#AllCategs#}</option>
	{foreach from=$MySearchCategs item=mysc}
			<option value="{$mysc->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $mysc->Id}selected="selected" {/if}>{$mysc->Entry|sanitize}</option>
			{if $mysc->Sub1}
					{foreach from=$mysc->Sub1 item=sub1}
							<option value="{$sub1->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $sub1->Id}selected="selected" {/if}>-&nbsp;{$sub1->Entry|sanitize}</option>
							{if $sub1->Sub2}
									{foreach from=$sub1->Sub2 item=sub2}
											<option value="{$sub2->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $sub2->Id}selected="selected" {/if}>-&nbsp;-&nbsp;{$sub2->Name|sanitize}</option>
											{if $sub2->Sub3}
													{foreach from=$sub2->Sub3 item=sub3}
															<option value="{$sub3->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $sub3->Id}selected="selected" {/if}>-&nbsp;-&nbsp;-&nbsp;{$sub3->Name|sanitize}</option>
															{if $sub3->Sub4}
																	{foreach from=$sub3->Sub4 item=sub4}
																			<option value="{$sub4->Id}" {if isset($smarty.request.cid) && $smarty.request.cid == $sub4->Id}selected="selected" {/if}>-&nbsp;-&nbsp;-&nbsp;-&nbsp;{$sub4->Name|sanitize}</option>
																	{/foreach}
															{/if}
													{/foreach}
											{/if}
									{/foreach}
							{/if}
					{/foreach}
			{/if}
	{/foreach}
	</select>

	<div class="uk-inline uk-width-1-1">
		<input class="uk-input" id="ext-search" name="shop_q" type="text" value="{if $smarty.request.shop_q != 'empty'}{$smarty.request.shop_q|escape:html}{/if}" placeholder="{#Search#}.."/>
		<button class="uk-width-1-6 uk-form-icon uk-form-icon-flip" type="submit" title="{#StartSearch#}" data-uk-icon="icon: search" data-uk-tooltip></button>
	</div>

	<input type="hidden" name="p" value="shop" />
	<input type="hidden" name="action" value="showproducts" />
	<input type="hidden" name="list" value="{$smarty.request.list|default:$shopsettings->Sortable_Produkte}" />
	<input type="hidden" name="limit" value="{$smarty.request.limit|default:$shopsettings->Produkt_Limit_Seite}" />
	<input type="hidden" name="s" value="1" />
	<input type="hidden" name="page" value="{$smarty.request.page|default:'1'}" />
	<input type="hidden" name="avail" value="{$smarty.request.avail|default:'0'}" />
</form>
