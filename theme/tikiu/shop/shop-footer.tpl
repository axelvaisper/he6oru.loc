{* TODO сделать подвал по высоте больше обычного *}

<div class="uk-container uk-container-small uk-tile">
    <div data-uk-grid>
        <div>
            <h3>Подпишитесь на наше сообщество Вконтакте</h3>
            <p>Рассказываем там о самом интересном.</p>
        </div>
        <div>
            <a href="https://vk.com/widget_community.php?act=a_subscribe_box&oid=-{$insert.vkid}&state=1" class="uk-button uk-button-default uk-border-rounded">Подписаться</a>
        </div>
    </div>
</div>

<div class="bg-white uk-tile">
    <div class="uk-container uk-container-small uk-text-center uk-margin">
    <h4>Знаете, как улучшить эту страницу?</h4> <a href="https://vk.com/im?sel=-{$insert.vkid}" class="uk-button uk-button-small uk-button-default uk-border-rounded">Расскажите нам</a>
    </div>
</div>



<footer class="uk-margin-medium-top">
<hr>

<div class="uk-container uk-container-large">
<div class="uk-child-width-1-4@s uk-grid-small" data-uk-grid>

    <div itemscope itemtype="http://schema.org/LocalBusiness">

      <a href="{$baseurl}">
          <div class="uk-grid-small uk-flex-middle" data-uk-grid>
              <div class="uk-width-auto">
                  <img itemprop="image" src="{$baseurl}/logo45.png" alt="">
              </div>
              <div class="uk-width-expand">
                  <small class="uk-margin-remove">{$settings.Firma|sanitize}</small>
                  <h4 itemprop="name" class="uk-margin-remove-top">
                      {$settings.Seitenname|sanitize}
                  </h4>
              </div>
          </div>
      </a>

      <ul class="uk-nav">
        {if !empty({$settings.Zip})}
            <li>
              {$settings.Zip}
            </li>
        {/if}
        {if !empty({$settings.Strasse})}
            <li>
              <a itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" href="/index.php?p=imprint">{$settings.Strasse} </a>
            </li>
        {/if}
        {if !empty({$settings.Stadt})}
            <li>
              {$settings.Stadt}
            </li>
        {/if}
        {if !empty({$settings.Telefon})}
            <li>
              <a href="tel:{$settings.Telefon|regex_replace:'/[^\d\+]/':''}">
                <span itemprop="telephone">{$settings.Telefon}</span></a>
            </li>
        {/if}
            <li>
              <a href="https://vk.com/{$insert.vkname}">
              vk.com/{$insert.vkname}</a>
            </li>
        {if !empty({$settings.Mail_Absender})}
            <li >
              <a href="mailto:{$settings.Mail_Absender}" rel="nofollow">
                <span itemprop="email">{$settings.Mail_Absender}</span></a>
            </li>
        {/if}
      </ul>

    </div>


    <div>
       {navigation id=7 tpl='shop/shop-footer-nav.tpl'}
    </div>

    <div>
      {navigation id=8 tpl='shop/shop-footer-nav.tpl'}
    </div>

     <div>
      {navigation id=6 tpl='shop/shop-footer-nav.tpl'}
    </div>

</div>
</div>

<hr>

<div class="uk-section">
  <ul class="uk-subnav uk-flex-center">
    <li><a href="http://sx-cms.ru" title="{#meta_generator#}" data-uk-tooltip>Работает на
    <span data-uk-icon="sxcms"></span> </a>
    </li>
    <li><a href="http://getuikit.com" title="на базе UIKIT" data-uk-tooltip>тема <b>Tikiu</b> <span data-uk-icon="uikit"></span> </a></li>
    <li><a href="https://vk.com/{$insert.vkname}">Дизайн студия <b>He6oru</b></a></li>
  </ul>
</div>

</footer>