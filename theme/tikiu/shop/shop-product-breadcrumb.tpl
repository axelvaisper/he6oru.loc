<div class="uk-container uk-container-large uk-margin-small-top">
    <div class="uk-grid-small" data-uk-grid>
        <div class="uk-width-expand@m"></div>
        {if !empty($breadcrumb)}
        <div class="uk-width-1-3@m">
            <ul class="uk-breadcrumb">
                {foreach from=$breadcrumb item=item}
                <li>
                    {$item}
                </li>
            {/foreach}
            </ul>
        </div>
        {/if}
    </div>
</div>
