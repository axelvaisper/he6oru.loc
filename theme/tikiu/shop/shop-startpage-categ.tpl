
<ul class="bg-white card-vk uk-overflow-auto uk-subnav">

      {* КАТЕГОРИИ МАГАЗИНА *}
      {if get_active('shop')}

        {* главная категория *}
        {foreach from=$MyShopNavi item=sn}

        <li class=" {if $cid == $sn->Id || in_array($sn->Id, $navi_current)} uk-active{/if}">
          <a href="index.php?p=shop&amp;action=showproducts&amp;cid={$sn->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sn->Entry|translit}">
            <span class="uk-margin-small-right">{$sn->Icon}</span>
            {$sn->Entry|truncate: 35|sanitize}
            {if !empty($sn->Sub1) && count($sn->Sub1)}
              <i class="uk-float-right" data-uk-icon="chevron-right"></i>
            {/if}
          </a>

          {* 2 уровень *}
          {if !empty($sn->Sub1) && count($sn->Sub1)}
          <div class="uk-margin-remove uk-card-default" style="width:50vw" data-uk-drop="pos:right-justify; boundary:#navali-start-boundary-2; boundary-align:true; duration:0">
            <div id="navali-start-boundary-3" class="uk-height-1-1 uk-grid-collapse uk-child-width-1-2@s uk-child-width-1-3@m" data-uk-grid="masonry: true">

              {foreach from=$sn->Sub1 item=sub1}
                <ul class="uk-nav uk-nav-default">
                  <li class="uk-nav-header uk-margin-remove uk-padding-remove">
                    <a class="uk-text-bold {if $cid == $sub1->Id || in_array($sub1->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub1->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub1->Entry|translit}">
                      {$sub1->Entry|sanitize}
                      {* {if !empty($sub1->Sub2) && count($sub1->Sub2)}
                        <i class="uk-float-right" data-uk-icon="chevron-right"></i>
                      {/if} *}
                    </a>
                  </li>

                      {* 3 уровень *}
                      {if !empty($sub1->Sub2) && count($sub1->Sub2)}

                          <ul class="uk-nav uk-nav-default">

                            {foreach from=$sub1->Sub2 item=sub2}

                            <li class="{if $cid == $sub2->Id || in_array($sub2->Id, $navi_current)} uk-active{/if}">
                              <a href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub2->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub2->Name|translit}">
                                {$sub2->Name|sanitize}
                                {if !empty($sub2->Sub3) && count($sub2->Sub3)}
                                  <i class="uk-float-right" data-uk-icon="chevron-right"></i>
                                {/if}
                              </a>

                                    {* 4 уровень *}
                                    {if !empty($sub2->Sub3) && count($sub2->Sub3)}


                                        <ul class="uk-nav">

                                          {foreach from=$sub2->Sub3 item=sub3}
                                          <li>
                                            <a class="{if $cid == $sub3->Id || in_array($sub3->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub3->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub3->Name|translit}">
                                              {$sub3->Name|sanitize}
                                              {if !empty($sub3->Sub4) && count($sub3->Sub4)}
                                                <i class="uk-float-right" data-uk-icon="chevron-right"></i>
                                               {/if}
                                            </a>
                                          
                                                {* 5 уровень *}
                                                {if !empty($sub3->Sub4) && count($sub3->Sub4)}
                                                {foreach from=$sub3->Sub4 item=sub4}
                                                <li>
                                                  <a class="{if $cid == $sub4->Id || in_array($sub4->Id, $navi_current)} uk-active{/if}" href="index.php?p=shop&amp;action=showproducts&amp;cid={$sub4->Id}&amp;page=1&amp;limit={$smarty.request.limit|default:$plim}&amp;t={$sub4->Name|translit}">
                                                    {$sub4->Name|sanitize}
                                                  </a>
                                                </li>
                                                {/foreach}
                                                {/if}
                                                {* /5 уровень *}
                                          </li>
                                          {/foreach}

                                        </ul>


                                    {/if}
                                    {* /4 уровень *}


                            </li>
                            {/foreach}
                            
                          </ul>

                      {/if}
                      {* /3 уровень *}

                  </ul>
                {/foreach}
              
            </div>
          </div>
          {/if}
          {* /2 уровень *}

        </li>
        {/foreach}
        {* /главная категория *}

      {/if}
      {* /КАТЕГОРИИ МАГАЗИНА *}

</ul>