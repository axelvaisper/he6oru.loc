{if $shopsettings->seen_cat == 1}
<script>
<!-- //
$(function() {
    var options = { target: '#ajaxbasket', timeout: 3000 };
    $('.seen_products').submit(function() {
        var id = '#mylist_' + $(this).attr('id');
        if ($(id).val() == 1) {
            UIkit.modal('#favorite-small-seen').show();
        } else {
             UIkit.modal('#basket-small-seen').show();
        }
        $(this).ajaxSubmit(options);
        $(id).val(0);
        return false;
    });
});
//-->
</script>

{include file="$incpath/shop/notification.tpl" dialog='small-seen'}

<div>
  <div class="uk-container uk-container-large">

    {if $smarty.request.action != 'showproduct'}
        <h3>{#Shop_detailLastSeen#}</h3>
    {/if}

    {foreach from=$seen_products_array item=p}
        <div class="row wrapper">

          <div class="col-1" data-toggle="tooltip" title="{$p.Beschreibung|tooltip:500}">
            <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
              <img class="img-fluid" src="{$p.Bild_Klein}" alt="" />
            </a>
          </div>

          <div class="col" data-toggle="tooltip" title="{$p.Beschreibung|tooltip:500}">
            <h5 class="text-truncate">
              <a  href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">{$p.Titel|sanitize}</a>
            </h5>
            {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                {if $p.Preis > 0}
                    <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                {else}
                    <span class="shop-price">{#Zvonite#}</span>
                {/if}
                {if $p.Fsk18 == 1 && $fsk_user != 1}
                    <div class="small">{#Shop_isFSKWarning#}</div>
                {/if}
            {else}
                {#Shop_prices_justforUsers#}
            {/if}
          </div>

          <div class="col-auto">
            {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                {if $p.Fsk18 == 1 && $fsk_user != 1}
                    <form method="post" action="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                      <button class="btn btn-secondary btn-sm" type="submit">{#buttonDetails#}</button>
                    </form>
                {else}
                    {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                        <form method="post" class="seen_products" id="seen_{$p.Id}" action="{if empty($p.Vars)}index.php?p=shop{else}index.php?p=shop&amp;action=showproduct&amp;id={$p.Id}{/if}">
                          <input type="hidden" name="amount" value="{if $p.MinBestellung == 0}1{else}{$p.MinBestellung}{/if}" maxlength="2" />
                          <input type="hidden" name="action" value="to_cart" />
                          <input type="hidden" name="redir" value="{page_link}#prod_anchor_{$p.Id}" />
                          <input type="hidden" name="product_id" value="{$p.Id}" />
                          <input type="hidden" name="mylist" id="mylist_seen_{$p.Id}" value="0" />
                          <input type="hidden" name="ajax" value="1" />
                          <noscript>
                          <input type="hidden" name="ajax" value="0" />
                          </noscript>
                          <button class="uk-button uk-button-small uk-button-success uk-border-rounded" type="submit" title="{#Shop_toBasket#}" data-uk-icon="shop-basket" data-uk-tooltip></button>
                          <button class="uk-button uk-button-small" onclick="document.getElementById('mylist_seen_{$p.Id}').value = '1';" type="submit" data-uk-icon="heart-line" title="{#Shop_WishList#}" data-uk-tooltip></button>
                        </form>
                    {else}
                        <form method="post" action="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                          <button class="uk-button uk-button-small uk-button-default uk-border-rouded" type="submit">{#buttonDetails#}</button>
                        </form>
                    {/if}
                {/if}
            {else}
                {#Shop_prices_justforUsers#}
            {/if}
          </div>
        </div>
    {/foreach}

    <a href="index.php?{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}blanc=1&amp;{/if}p=shop&amp;area={$area}&amp;action=showseenproducts">
    {#Shop_showSeenProducts#}
    </a>

  </div>
</div>
{/if}