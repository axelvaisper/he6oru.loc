{* vertical nav and slider *}
<div class="uk-margin-small-top uk-margin-bottom uk-container uk-container-large">
  {* uk-height-match="target: .card-vk" *}
  <div class="uk-grid-small" data-uk-grid>
    <div class="uk-width-1-3@m uk-width-1-4@l uk-visible@m">
      {navigation id=5 tpl='shop/shop-navali-start.tpl'}
    </div>

    {* слайдшоу *}
    <div class="uk-width-expand">
      {include file="$incpath/shop/start_topseller.tpl"}
    </div>

    <div class="uk-width-1-4 uk-visible@l">
      {if $loggedin}
        {include file="$incpath/shop/start_products_new.tpl"}
      {else}
        {include file="$incpath/other/login-start.tpl"}
      {/if}
    </div>

  </div>

</div>

{* {include file="$incpath/shop/shop-startpage-categ.tpl"} *}

{include file="$incpath/shop/start_offers.tpl"}

{if $shopsettings->seen_cat == 1}
    {$small_seen_products}
{/if}
{if $shopsettings->vat_info_cat == 1}
    {include file="$incpath/shop/vat_info.tpl"}
{/if}
