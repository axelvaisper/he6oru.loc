{if $angebote_array}
  <script>
  <!-- //
  $(function() {
      var options = { target: '#ajaxbasket', timeout: 3000 };
      $('.offer_products').submit(function() {
          UIkit.modal('#basket-offer').show();
          $(this).ajaxSubmit(options);
          return false;
      });
  });
  //-->
  </script>

  {include file="$incpath/shop/notification.tpl" dialog='offer'}

  <h2>{#Shop_Offers#}</h2>
  <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-grid-small" data-uk-grid>

            {* {assign var=tscount value=0} *}
            {foreach from=$angebote_array item=p name=offers}

                    <form class="offer_products" method="post" action="{if empty($p.Vars) && $p.Lagerbestand>0}index.php?p=shop{else}index.php?p=shop&amp;action=showproduct&amp;id={$p.Id}{/if}">
                      <div class="uk-card card-vk bg-white">

                          {* image *}
                          <div class="uk-card-media-top" data-toggle="tooltip" title="{$p.Beschreibung|tooltip:500}">
                              {if $shopsettings->popup_product == 1}
                                  <span data-uk-lightbox><a data-type="iframe" href="{$p.ProdLink}&amp;blanc=1">
                                      <img data-src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" data-uk-img/>
                                  </a></span>
                              {else}
                                  <a href="{$p.ProdLink}">
                                      <img data-src="{$p.Bild_Mittel}" alt="{$p.Titel|sanitize}" data-uk-img/>
                                  </a>
                              {/if}
                          </div>
  
                        {* text *}
                        <div class="uk-padding-small">
                            <p>
                            
                                {if $shopsettings->popup_product == 1}
                                    <span data-uk-lightbox><a data-type="iframe" href="{$p.ProdLink}&amp;blanc=1">{$p.Titel|truncate:45|sanitize}</a></span>
                                {else}
                                    <a href="{$p.ProdLink}">{$p.Titel|truncate:45|sanitize}</a>
                                {/if}
                            </p>

                            <div class="uk-text-muted">
                                {include file="$incpath/shop/tax_inf_small.tpl"}
                                <div class="uk-float-right">{#Shop_go_myorders#} ({$p.Klicks})</div>
                            </div>

                      {if $shopsettings->PreiseGaeste == 1 || $loggedin}
                            {if $p.Preis > 0}
                                {if $p.Preis_Liste != $p.Preis}
                                    <div>
                                      {#Shop_instead#} <span class="shop-price-old">{$p.Preis_Liste|numformat} {$currency_symbol}</span>
                                    </div>
                                {/if}
                                {if !empty($p.Vars)}
                                    {#Shop_priceFrom#}
                                {/if}
                                <span class="shop-price">{$p.Preis|numformat} {$currency_symbol}</span>
                                {if $no_nettodisplay != 1}
                                    {if $price_onlynetto != 1}
                                        <div class="small">
                                          {if $shopsettings->NettoKlein == 1}
                                              {#Shop_netto#} {$p.netto_price|numformat} {$currency_symbol}
                                          {/if}
                                        </div>
                                    {/if}
                                    {if $price_onlynetto == 1 && !empty($p.price_ust_ex)}
                                        <div class="small">
                                          {include file="$incpath/shop/tax_inf_small.tpl"}
                                        </div>
                                    {/if}
                                {/if}
                            {else}
                                <span class="shop-price">{#Zvonite#}</span>
                            {/if}

                            {if $p.Fsk18 == 1 && $fsk_user != 1}
                                <button class="uk-button uk-button-default uk-border-rounded uk-button-small" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
                            {else}
                                {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                                    <input type="hidden" name="amount" value="1" />
                                    <input type="hidden" name="action" value="to_cart" />
                                    <input type="hidden" name="redir" value="{page_link}" />
                                    <input type="hidden" name="product_id" value="{$p.Id}" />
                                    <input type="hidden" name="ajax" value="1" />
                                    <noscript>
                                    <input type="hidden" name="ajax" value="0" />
                                    </noscript>
                                    <button class="uk-button uk-button-success uk-border-rounded uk-button-small" type="submit" title="{#Shop_toBasket#}">
                                      <span data-uk-icon="shop-basket"></span>
                                    </button>
                                {else}
                                    <input type="hidden" name="cid" value="{$p.Kategorie}" />
                                    <input type="hidden" name="parent" value="{$p.Parent}" />
                                    <input type="hidden" name="navop" value="{$p.Navop}" />
                                    <button class="uk-button uk-button-default uk-border-rounded uk-button-small" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
                                {/if}
                            {/if}

                                {if get_active('shop_merge')}
                                    <span data-uk-lightbox><a data-type="iframe" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1&amp;prodid={$p.Id}&amp;cid={$p.Kategorie}" data-uk-icon="list" title="{#Merge#}" data-uk-tooltip>
                                    </a></span>
                                {/if}
                                <a href="https://vk.com/share.php?url={$baseurl}/{$p.ProdLink}" data-uk-icon="share" class="uk-button uk-button-small" title="{#Share#}" data-uk-tooltip></a>
                          </div>
                        </div>
                      {else}
                          {#Shop_prices_justforUsers#}
                      {/if}
                    </form>
            {/foreach}
  </div>
{/if}