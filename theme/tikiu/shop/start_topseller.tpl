{if $topseller_array}
<script>
<!-- //
$(function() {

  var options = { target: '#ajaxbasket', timeout: 3000 };
  $('.top-products').submit(function() {
    UIkit.modal('#basket-top').show();
      $(this).ajaxSubmit(options);
      return false;
  });

});
//-->
</script>

{include file="$incpath/shop/notification.tpl" dialog='top'}

<div class="card-vk uk-position-relative uk-visible-toggle" data-uk-slideshow="min-height:450; max-height:450; animation:push">

  <ul class="uk-slideshow-items">

    {assign var=tscount value=0}
    {foreach from=$topseller_array item=p name=topseller}
   
      <form class="top-products" method="post" action="{if empty($p.Vars) && $p.Lagerbestand>0}index.php?p=shop{else}index.php?p=shop&amp;action=showproduct&amp;id={$p.Id}{/if}">
        <li class="bg-white">

          {* IMAGE *}
          <div class="uk-height-large">
            <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}" class="uk-inline uk-height-1-1 uk-flex uk-flex-center uk-flex-middle">
              <div class="uk-width-1-1 uk-height-1-1 uk-background-cover uk-background-norepeat uk-animation-kenburns uk-animation-reverse" style="background-image: url({$p.Bild_Mittel});">
              </div>
              <div class="uk-overlay uk-position-cover"></div>
            </a>
          </div>

          {* TEXT *}
          {* <div class="uk-overlay uk-overlay-default uk-padding-small uk-position-center">
            <h4>
              {if permission('adminpanel')}
              <span data-uk-lightbox><a data-uk-icon="edit" data-type="iframe" href="{$baseurl}/admin?do=shop&amp;sub=edit_article&amp;id={$p.Id}&amp;noframes=1&amp;langcode=1"></a></span>
              {/if}
              <a href="{$p.ProdLink}{if isset($smarty.request.blanc) && $smarty.request.blanc == 1}&amp;blanc=1{/if}">
                {$p.Titel|sanitize}
              </a>
            </h4>
          </div> *}

          {* {if $shopsettings->PreiseGaeste == 1 || $loggedin}
            <div class="spacer">
              {if $p.Preis > 0}
                  {if $p.Preis_Liste != $p.Preis}
                      <div>
                        {#Shop_instead#} <span class="shop-price-old">{$p.Preis_Liste|numformat} {$currency_symbol}</span>
                      </div>
                  {/if}
                  {if !empty($p.Vars)}
                      {#Shop_priceFrom#}
                  {/if}
                  <span class="uk-badge uk-text-success bg-white uk-margin-small-top uk-margin-small-right uk-text-bold uk-position-top-right">{$p.Preis|numformat} {$currency_symbol}</span>
                  {if $no_nettodisplay != 1}
                      {if $price_onlynetto != 1}
                          <div class="small">
                            {if $shopsettings->NettoKlein == 1}
                                {#Shop_netto#} {$p.netto_price|numformat} {$currency_symbol}
                            {/if}
                          </div>
                      {/if}
                      {if $price_onlynetto == 1 && !empty($p.price_ust_ex)}
                          <div class="small">
                            {include file="$incpath/shop/tax_inf_small.tpl"}
                          </div>
                      {/if}
                  {/if}
              {else}
                  <span class="shop-price">{#Zvonite#}</span>
              {/if}
            </div>

            <div class="spacer">
              {if $p.Fsk18 == 1 && $fsk_user != 1}
                  <button class="uk-button uk-button-default uk-border-rounded" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
              {else}
                  {if empty($p.Vars) && $p.Lagerbestand > 0 && $p.Preis > 0 && empty($p.Frei_1) && empty($p.Frei_2) && empty($p.Frei_3)}
                      <input type="hidden" name="amount" value="1" />
                      <input type="hidden" name="action" value="to_cart" />
                      <input type="hidden" name="redir" value="{page_link}" />
                      <input type="hidden" name="product_id" value="{$p.Id}" />
                      <input type="hidden" name="ajax" value="1" />
                      <noscript>
                      <input type="hidden" name="ajax" value="0" />
                      </noscript>
                      <button class="uk-button uk-button-small" type="submit">{#Shop_toBasket#}</button>
                  {else}
                      <input type="hidden" name="cid" value="{$p.Kategorie}" />
                      <input type="hidden" name="parent" value="{$p.Parent}" />
                      <input type="hidden" name="navop" value="{$p.Navop}" />
                      <button class="uk-button uk-button-small" type="button" onclick="location.href = '{$p.ProdLink}';">{#buttonDetails#}</button>
                  {/if}
                {/if}
              </div>
              {else}
                  {#Shop_prices_justforUsers#}
              {/if} *}

       </li>
      </form>
  
    {/foreach}

  </ul>

  <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-previous uk-slideshow-item="previous"></a>
  <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" data-uk-slidenav-next uk-slideshow-item="next"></a>

  <ul class="uk-slideshow-nav uk-dotnav uk-position-bottom uk-flex-center uk-margin-small-bottom"></ul>

</div>
{/if}
