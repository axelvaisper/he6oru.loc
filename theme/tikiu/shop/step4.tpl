{include file="$incpath/shop/steps.tpl"}

<div class="h3 title">{#Shop_text_data#}</div>
{if $agb_error == 1}
    <div class="box-content">
      <div class="box-error">
        {#Shop_agb_accept_f#}
      </div>
    </div>
{/if}
<div class="box-content">
  {include file="$incpath/shop/basket_items.tpl"}
  {include file="$incpath/shop/basket_summ.tpl"}
</div>

<form method="post" name="rla" action="index.php#diff_adress">
  <input type="hidden" name="p" value="shop" />
  <input type="hidden" name="area" value="{$area}" />
  <input type="hidden" name="action" value="shoporder" />
  <input type="hidden" name="subaction" value="step2" />
  {if !$loggedin}
      <input type="hidden" name="order" value="guest" />
  {/if}
</form>

<div class="h3 title clearfix">
  {#Shop_f_shipping_billing_adress#}
  <div class="float-right">
    <a href="javascript: document.forms['rla'].submit();">
      <i class="icon-pencil" data-toggle="tooltip" title="{#Shop_f_change#}"></i>
    </a>
  </div>
</div>
<div class="box-content">

  <div class="row">
    <div class="col ">
      <div class="h5 title">
        {#Shop_f_billingadress#}
      </div>
    </div>
    <div class="col">
      {if $smarty.session.ship_ok == 1}
          <div class="h5 title">
            {#Shop_f_shippingadress#}
          </div>
      {/if}
    </div>
  </div>

  <div class="row wrapper">
    <div class="col">
      {if !empty($smarty.session.r_nachname) || !empty($smarty.session.r_vorname)}
          <div>
            {#Profile_Buyer#}: {$smarty.session.r_nachname} {$smarty.session.r_vorname}
            {if !empty($smarty.session.r_middlename)}
                {$smarty.session.r_middlename}
            {/if}
          </div>
      {/if}
      {if !empty($smarty.session.r_email)}
          <div>
            {#Email#}: {$smarty.session.r_email}
          </div>
      {/if}
      {if !empty($smarty.session.r_telefon)}
          <div>
            {#Phone#}: {$smarty.session.r_telefon}
          </div>
      {/if}
      {if !empty($smarty.session.r_fax)}
          <div>
            {#Fax#}: {$smarty.session.r_fax}
          </div>
      {/if}
      {if !empty($smarty.session.r_land_lang)}
          <div>
            {#Country#}: {$smarty.session.r_land_lang}
          </div>
      {/if}
      {if !empty($smarty.session.r_plz)}
          <div>
            {#Profile_Zip#}: {$smarty.session.r_plz}
          </div>
      {/if}
      {if !empty($smarty.session.r_ort)}
          <div>
            {#Town#}: {$smarty.session.r_ort}
          </div>
      {/if}
      {if !empty($smarty.session.r_strasse)}
          <div>
            {#Profile_Street#}: {$smarty.session.r_strasse}
          </div>
      {/if}
      {if !empty($smarty.session.r_firma)}
          <div>
            {#Profile_company#}: {$smarty.session.r_firma}
          </div>
      {/if}
      {if !empty($smarty.session.r_ustid)}
          <div>
            {#Profile_vatnum#}: {$smarty.session.r_ustid}
          </div>
      {/if}
      {if $settings.Reg_Bank == 1 && !empty($smarty.session.r_ustid)}
          <div>
            {#Profile_Bank#}:
          </div>
          {$smarty.session.r_bankname}
      {/if}
    </div>

    {if $smarty.session.ship_ok == 1}
        <div class="col">
          {if $smarty.session.diff_rl == 'liefer_gleich'}
              {#Shop_f_same_sa#}
          {else}
              {if !empty($smarty.session.l_nachname) || !empty($smarty.session.l_vorname)}
                  <div>
                    {#Recipient#}: {$smarty.session.l_nachname} {$smarty.session.l_vorname}
                    {if !empty($smarty.session.l_middlename)}
                        {$smarty.session.l_middlename}
                    {/if}
                  </div>
              {/if}
              {if !empty($smarty.session.l_firma)}
                  <div>
                    {#Profile_company#}: {$smarty.session.l_firma}
                  </div>
              {/if}
              {if !empty($smarty.session.l_telefon)}
                  <div>
                    {#Phone#}: {$smarty.session.l_telefon}
                  </div>
              {/if}
              {if !empty($smarty.session.l_fax)}
                  <div>
                    {#Fax#}: {$smarty.session.l_fax}
                  </div>
              {/if}
              {if !empty($smarty.session.l_land_lang)}
                  <div>
                    {#Country#}: {$smarty.session.l_land_lang}
                  </div>
              {/if}
              {if !empty($smarty.session.l_plz)}
                  <div>
                    {#Profile_Zip#}: {$smarty.session.l_plz}
                  </div>
              {/if}
              {if !empty($smarty.session.l_ort)}
                  <div>
                    {#Town#}: {$smarty.session.l_ort}
                  </div>
              {/if}
              {if !empty($smarty.session.l_strasse)}
                  <div>
                    {#Profile_Street#}: {$smarty.session.l_strasse}
                  </div>
              {/if}
          {/if}
        </div>
    {/if}
  </div>

  {if !empty($smarty.session.r_nachricht)}
      <div class="h5 title">{#Shop_f_ordermessage#}</div>
      {$smarty.session.r_nachricht}
  {/if}
</div>

<form method="post" name="vza" action="index.php">
  <input type="hidden" name="p" value="shop" />
  <input type="hidden" name="area" value="{$area}" />
  <input type="hidden" name="action" value="shoporder" />
  <input type="hidden" name="subaction" value="step1" />
  {if !$loggedin}
      <input type="hidden" name="order" value="guest" />
  {/if}
</form>

<div class="h3 title clearfix">
  {#Shop_f_shipping_billing#}
  <div class="float-right">
    <a href="javascript: document.forms['vza'].submit();">
      <i class="icon-pencil" data-toggle="tooltip" title="{#Shop_f_change#}"></i>
    </a>
  </div>
</div>

<div class="box-content">
  <div class="row">
    <div class="col ">
      <div class="h5 title">
        {#Shop_f_shipping_method#}
      </div>
    </div>
    <div class="col">
      <div class="h5 title">
        {#Shop_payment_methods#}
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col ">
      {if $smarty.session.ship_ok == 1}
          {$smarty.session.shipper_name}
      {else}
          {#No#}
      {/if}
    </div>
    <div class="col">
      {$smarty.session.payment_name}
    </div>
  </div>
</div>

<div class="h3 title" id="return">{#Shop_f_rcall_inf#}</div>
<div class="box-content">
  <div class="box-block" style="height:200px;overflow:auto">
    {$widerruf_belehrung}
  </div>
</div>

{include file="$incpath/shop/send_order_form.tpl"}
