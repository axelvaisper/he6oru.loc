<div class="h3 title" id="friends">{#Profile_Friends#}</div>
<div class="box-content">
  {if !empty($friends)}
      <div class="spacer">
        {#SeenFriend#} {$num} {#FriendsIs#} <a href="index.php?p=user&amp;id={$smarty.request.id}&amp;area={$area}&amp;friends=all#friends">{$num_all} {#Friends#}</a>
      </div>
  {/if}

  {if $smarty.request.id != $smarty.session.benutzer_id && $smarty.session.benutzer_id != '0'}
      {if $isf == 0}
          <div class="spacer">
            {#WaitFriend#}
          </div>
      {/if}
      {if $isf == 1}
          <div class="spacer">
            <a href="index.php?p=user&amp;action=friends&amp;do=add&amp;id={$smarty.request.id}&amp;area={$area}">{#FriendshipCreate#}</a>
          </div>
      {/if}
  {/if}

  {if !empty($friends)}
      <div class="row spacer">
        {assign var='lcc' value=0}
        {foreach from=$friends item=f name=fiend}
            <div class="col d-flex justify-content-center align-self-end">
              <div class="text-center">
                <div>
                  <a title="{$f.Freundname}" href="index.php?p=user&amp;id={$f.FreundId}&amp;area={$area}">{$f.Avatar}</a>
                </div>
                <div class="small font-weight-bold">
                  <a href="index.php?p=user&amp;id={$f.FreundId}&amp;area={$area}">{$f.Freundname}</a>
                </div>
                {if $smarty.request.id == $smarty.session.benutzer_id}
                    <div class="small">
                      <a onclick="return confirm('{#FriendshipDel_C#}');" href="index.php?p=user&amp;action=friends&amp;do=del&amp;id={$f.FreundId}&amp;area={$area}">{#Delete#}</a>
                    </div>
                {/if}
              </div>
            </div>
            {assign var='lcc' value=$lcc+1}
            {if $lcc % $NLine == 0 && !$smarty.foreach.fiend.last}
            </div>
            <div class="row spacer">
            {/if}
        {/foreach}
      </div>
  {else}
      <div class="spacer">
        {#NoFriends#}
      </div>
  {/if}

  {if $newfriends && $smarty.request.id == $smarty.session.benutzer_id}
      <div class="spacer">
        {#AddUsersFriend#}
      </div>

      <div class="row spacer">
        {assign var='lcc' value=0}
        {foreach from=$newfriends item=newf name=new}
            <div class="col d-flex justify-content-center align-self-end">
              <div class="text-center">
                <div>
                  <a title="{$newf.Freundname}" href="index.php?p=user&amp;id={$newf.BenutzerId}">{$newf.Avatar}</a>
                </div>
                <div class="small font-weight-bold">
                  <a href="index.php?p=user&amp;id={$newf.BenutzerId}">{$newf.Freundname}</a>
                </div>
                <div class="small">
                  <a href="index.php?p=user&amp;action=friends&amp;do=confirm&amp;id={$newf.Id}&amp;area={$area}">{#Gl_Ok#}</a>
                </div>
                <div class="small">
                  <a href="index.php?p=user&amp;action=friends&amp;do=del&amp;id={$newf.BenutzerId}&amp;area={$area}">{#Gl_No#}</a>
                </div>
              </div>
            </div>
            {assign var='lcc' value=$lcc+1}
            {if $lcc % $NLine == 0 && !$smarty.foreach.new.last}
            </div>
            <div class="row spacer">
            {/if}
        {/foreach}
      </div>
  {/if}
</div>
