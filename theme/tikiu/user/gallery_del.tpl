<script>
<!-- //
function selectWert(obj) {
    with (obj) return options[selectedIndex].value;
}
function deleteId() {
    var check = confirm('{#ConfirmDel#}');
    if(check !== false) {
        location = "{$baseurl}/index.php?p=user&action=gal&do=del&id=" + selectWert(document.getElementById('rem-gal')) + "&area={$area}";
    }
}
//-->
</script>

<div class="h1 title">{#DelAlbum#}</div>
<div class="box-error">
  {#DelInfoGal#}
</div>


<div class="row form-group">
  <div class="col-md-auto col-sm-12">
    <label class="col-form-label" for="rem-gal">{#DelAlbum#}</label>
  </div>
  <div class="col-md col-sm-12">
    <div class="input-group">
      <select class="form-control" name="album" id="rem-gal">
        {foreach from=$albums item=a}
            <option value="{$a.Id}">{$a.Name}</option>
        {/foreach}
      </select>
      <span class="input-group-append">
        <button class="btn btn-primary" onclick="deleteId();">{#Delete#}</button>
      </span>
    </div>
  </div>
</div>

<div class="text-center">
  <button class="btn btn-secondary" onclick="closeWindow(true);">{#WinClose#}</button>
</div>
