{script file="$jspath/jupload.js" position='head'}
<script>
<!-- //
function selectWert(obj) {
    with (obj) return options[selectedIndex].value;
 }
function changeID() {
    location = "index.php?p=user&action=gal&do=edit&id=" + selectWert(document.getElementById('Aid')) + "&area={$area}";
}
function getIMG(id) {
    var check = confirm('{#ConfirmDel#}');
    if(check !== false) {
	$.get('index.php?p=user&action=gal&do=ajax&aj=1&img=1&id=' + id + '&key=' + Math.random(), function() {
            window.location.reload();
        });
    }
}
function fileUpload(divid) {
    $(document).ajaxStart(function() {
        $('UpInf_' + divid).hide();
        $('#loading_' + divid).show();
        $('#buttonUpload_' + divid).val('{#Global_Wait#}').prop('disabled', true);
    }).ajaxComplete(function() {
        $('#loading_' + divid).hide();
        $('#buttonUpload_' + divid).val('{#UploadButton#}').prop('disabled', false);
    });
    $.ajaxFileUpload ({
        url: 'index.php?action=upload&p=user&divid=' + divid,
        secureuri: false,
        fileElementId: 'fileToUpload_' + divid,
        dataType: 'json',
        success: function (data) {
	    if(typeof(data.result) !== 'undefined') {
                document.getElementById('UpInf_' + divid).innerHTML = data.result;
                if(data.filename !== '') {
                    document.getElementById('newFile_' + divid).value = data.filename;
                    var nextid = eval(divid + '+' + 1);
                    $('#tab_' + nextid).show();
                }
	    }
        },
        error: function (data, status, e)  {
            document.getElementById('UpInf_' + divid).innerHTML = e;
        }
    });
    return false;
}
//-->
</script>

<div class="h1 title">{#EditAlbum#}</div>
<form action="" method="post" enctype="multipart/form-data">
  <div class="row form-group">
    <div class="col-md-3 col-sm-12">
      <label class="col-form-label" for="rem-gal">{#EditAlbum#}</label>
    </div>
    <div class="col-md col-sm-12">
      <div class="input-group">
        <select class="form-control" name="album" id="Aid">
          {foreach from=$albums item=a}
              <option value="{$a.Id}" {if $smarty.request.id == $a.Id}selected="selected"{/if}>{$a.Name}</option>
          {/foreach}
        </select>
        <span class="input-group-append">
          <input class="btn btn-primary" type="button" name="ch" onclick="changeID();" value="{#GotoButton#}" />
        </span>
      </div>
    </div>
  </div>

  {if $smarty.request.id}
      <div class="row form-group">
        <div class="col-md-3 col-sm-12">
          <label class="col-form-label" for="select-gal">{#AlbumTitle#}</label>
        </div>
        <div class="col-md col-sm-12">
          <input class="form-control" name="title" type="text" id="select-gal" value="{if isset($smarty.post.title)}{$smarty.post.title|escape: html}{else}{$item.Name}{/if}" />
        </div>
      </div>
  {/if}

  {if $smarty.request.id}
      <div class="wrapper">
        <div class="d-flex flex-wrap justify-content-center">
          {foreach from=$images item=im}
              <a href="javascript:void(0);" onclick="getIMG('{$im->Id}');return false;">
                <img class="img-thumbnail height-4" src="{$im->Bild}" alt="{$im->Name|sanitize}" />
              </a>
          {/foreach}
        </div>
      </div>

      {section name='i' start=0 loop=$loop step=1}
          <div id="tab_{$smarty.section.i.index}" {if $smarty.section.i.index != 1 && $smarty.section.i.index != $next && (empty($title[$smarty.section.i.index]) || $file[$smarty.section.i.index] == '')}{/if}>
            <div class="row align-items-center">
              <div class="col-md-2 col-sm-12 form-group">
                <label class="col-form-label" for="new-image{$smarty.section.i.index}">{#GlobalTitle#}</label>
              </div>
              <div class="col-md-auto col-sm-12 form-group">
                <input class="form-control" id="new-image{$smarty.section.i.index}" type="text" name="feld_title[{$smarty.section.i.index}]" value="{$title[$smarty.section.i.index]}" />
              </div>

              <div class="col-md col-sm-12 form-group">
                <div id="UpInf_{$smarty.section.i.index}">{$pic[$smarty.section.i.index]}</div>
                <div id="loading_{$smarty.section.i.index}" style="display:none;">
                  <img src="{$imgpath_page}ajaxbar.gif" alt="" />
                </div>
                <div class="input-group">
                  <input class="form-control" id="fileToUpload_{$smarty.section.i.index}" type="file" name="fileToUpload_{$smarty.section.i.index}" />
                  <span class="input-group-append">
                    <input class="btn btn-primary" type="button" id="buttonUpload_{$smarty.section.i.index}" onclick="fileUpload('{$smarty.section.i.index}');" value="{#UploadButton#}" />
                  </span>
                </div>
                <input type="hidden" name="feld_file[{$smarty.section.i.index}]" id="newFile_{$smarty.section.i.index}" value="{$file[$smarty.section.i.index]}" />
              </div>
            </div>
          </div>
      {/section}

      <div class="text-center">
        <input class="btn btn-primary" type="submit" value="{#Save#}" />
        <input class="btn btn-secondary" type="button" onclick="closeWindow();" value="{#WinClose#}" />
      </div>
      <input type="hidden" name="save" value="1" />
  {/if}
</form>
