{if !isset($smarty.request.subaction) || ($smarty.request.subaction != 'step2' && $smarty.request.subaction != 'step3' && $smarty.request.subaction != 'step4')}
<script>
<!-- //
$(function() {
    $('#login-form-send').submit(function () {
        $(this).ajaxSubmit({
            target: '#login-form-user', timeout: 3000
        });
        $('#ajl').hide();
        $('#ajlw').show();
        return false;
    });
});
//-->
</script>


<div id="login-form-user">
  {include file="$incpath/user/login_raw.tpl"}
  </div>
  
  <div id="ajlw" style="display:none">
    <img id="anim_floating" src="{$baseurl}/logo.svg" alt="loading.."/>
  </div>
  
  <div id="ajl">
    <form method="post" action="{$baseurl}/index.php?p=userlogin&amp;action=ajaxlogin" id="login-form-send">

      <input type="text" class="uk-input uk-form-blank" name="login_email" id="login-email" placeholder="{#LoginMailUname#}.." required="required" autofocus />
      <input type="password" class="uk-input uk-form-blank" name="login_pass" id="login-pass" placeholder="{#Pass#}.." required="required" />

      <div class="uk-grid-small uk-child-width-1-2 uk-margin-small-top" data-uk-grid>
        <div>
          <label class="uk-button uk-button-link uk-margin-small-top">
            <input class="uk-checkbox uk-border-pill" name="staylogged" type="checkbox" checked>
            {#PassCookieT#}
          </label>
        </div>
        <div>
          <input class="uk-button uk-button-default uk-border-rounded" type="submit" value="{#Login_Button#}" />
        </div>
      </div>

      <input type="hidden" name="p" value="userlogin" />
      <input type="hidden" name="action" value="ajaxlogin" />
      <input type="hidden" name="area" value="{$area}" />
      <input type="hidden" name="backurl" value="{page_link|base64encode}" />

      <hr>

      {if get_active('Register')}
      <a class="uk-button uk-button-link" href="index.php?p=register&amp;lang={$langcode}&amp;area={$area}">{#RegNew#}</a>
      {/if}
      <br>
      <a class="uk-button uk-button-link" href="index.php?p=pwlost">{#PassLost#}</a>
    </form>
  </div>
  
{/if}
