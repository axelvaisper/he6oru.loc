{if permission('own_avatar')}
{script file="$jspath/jupload.js" position='head'}
{/if}
{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    $('#pform').validate({
        rules: {
            {if permission('username')}
            reg_username: { required: true },
            {/if}
            {if $settings.Reg_DataPflichtFill == 1}
            Vorname: { required: true },
            Nachname: { required: true },
            {/if}
            {if $settings.Reg_AddressFill == 1}
            Strasse_Nr: { required: true },
            Postleitzahl: { required: true, number: true },
            Ort: { required: true },
            {/if}
            send: { }
        },
        messages: { },
        submitHandler: function() {
            document.forms['pform'].submit();
        }
    });
});
{if permission('own_avatar')}
function fileUpload(sub, divid) {
    $(document).ajaxStart(function() {
        $('#loading_' + divid).show();
        $('#buttonUpload_' + divid).val('{#Global_Wait#}').prop('disabled', true);
    }).ajaxComplete(function() {
        $('#loading_' + divid).hide();
        $('#buttonUpload_' + divid).val('{#UploadButton#}').prop('disabled', false);
    });
    $.ajaxFileUpload({
        url: 'index.php?action=' + sub + '&p=useraction&divid=' + divid,
        secureuri: false,
        fileElementId: 'fileToUpload_' + divid,
        dataType: 'json',
        success: function (data) {
	    if(typeof(data.result) !== 'undefined') {
                document.getElementById('UpInf_' + divid).innerHTML = data.result;
                if(data.filename !== '') {
                    document.getElementById('newFile_' + divid).value = data.filename;
                }
	    }
        },
        error: function (data, status, e) {
            document.getElementById('UpInf_' + divid).innerHTML = e;
        }
    });
    return false;
}
{/if}
//-->
</script>

<div class="wrapper">
  <h1 class="title">{#MyAccount#}</h1>
  {if $error}
      <div class="box-content">
        <div class="box-error">
          <div class="font-weight-bold">{#ErrorOcured#}</div>
          {foreach from=$error item=reg_error}
              <div>{$reg_error}</div>
          {/foreach}
        </div>
      </div>
  {/if}

  <form id="pform" name="pform" action="index.php?p=useraction&amp;action=profile" method="post" enctype="multipart/form-data">
    <div class="wrapper" id="accordion">
      <div class="card">
        <div class="card-header collapsed" data-toggle="collapse" href="#collapse">
          <a class="card-title">{#Profile_Cdata#}</a>
        </div>
        <div id="collapse" class="card-body collapse" data-parent="#accordion">

          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              <label class="col-form-label" for="form-user">{#Username#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              {if permission('username')}
                  <input class="form-control" id="form-user" name="reg_username" type="text" value="{$smarty.post.reg_username|default:$data.Benutzername|sanitize}" maxlength="20" />
              {else}
                  <input class="form-control" id="form-user" name="not" type="text" value="{$smarty.session.user_name}" disabled="disabled" />
              {/if}
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              {#Profile_Email#}
            </div>
            <div class="col-md-7 col-sm-12">
              {$smarty.session.login_email}
            </div>
          </div>

          {if permission('email_change')}
              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-mail">{#Profile_EmailNew#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-mail" name="newmail" type="email" value="{$smarty.post.newmail|sanitize}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-mail2">{#Profile_EmailNew2#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-mail2" name="newmail2" type="email" value="{$smarty.post.newmail2|sanitize}" />
                </div>
              </div>
          {/if}
        </div>

        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
          <a class="card-title">{#PersonalData#}</a>
        </div>
        <div id="collapse2" class="card-body collapse" data-parent="#accordion">
          {if $settings.Reg_DataPflicht == 1}
              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label{if $settings.Reg_DataPflichtFill == 1} required{/if}" for="form-name">{#LastName#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-name" name="Nachname" type="text" value="{$smarty.post.Nachname|default:$data.Nachname|sanitize}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label{if $settings.Reg_DataPflichtFill == 1} required{/if}" for="form-gname">{#GlobalName#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-gname" name="Vorname" type="text" value="{$smarty.post.Vorname|default:$data.Vorname|sanitize}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-mname">{#Profile_MiddleName#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-mname" name="MiddleName" type="text" value="{$smarty.post.MiddleName|default:$data.MiddleName|sanitize}" />
                </div>
              </div>
          {/if}
          {if $settings.Reg_Fon == 1}
              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-phone">{#Phone#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-phone" name="Telefon" type="text" value="{$smarty.post.Telefon|default:$data.Telefon|sanitize}" />
                </div>
              </div>
          {/if}
          {if $settings.Reg_Fax == 1}
              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-fax">{#Fax#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-fax" name="Telefax" type="text" value="{$smarty.post.Telefax|default:$data.Telefax|sanitize}" />
                </div>
              </div>
          {/if}
          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              <label class="col-form-label" for="form-country">{#Country#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              <select class="form-control" name="country" id="form-fax">
                {foreach from=$countries item=c}
                    <option value="{$c.Code}" {if $smarty.request.send != 1}{if $data.LandCode|upper == $c.Code}selected="selected"{/if}{else}{if $smarty.post.country == $c.Code}selected="selected"{/if}{/if}>{$c.Name}</option>
                {/foreach}
              </select>
            </div>
          </div>
          {if $settings.Reg_Address == 1}
              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label{if $settings.Reg_AddressFill == 1} required{/if}" for="form-zip">{#Profile_Zip#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-zip" name="Postleitzahl" type="number" value="{$smarty.post.Postleitzahl|default:$data.Postleitzahl|sanitize}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label{if $settings.Reg_AddressFill == 1} required{/if}" for="form-ort">{#Town#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-ort" name="Ort" type="text" value="{$smarty.post.Ort|default:$data.Ort|sanitize}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label{if $settings.Reg_AddressFill == 1} required{/if}" for="form-street">{#Profile_Street#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-street" name="Strasse_Nr" type="text" value="{$smarty.post.Strasse_Nr|default:$data.Strasse_Nr|sanitize}" />
                </div>
              </div>
          {/if}
          {if $settings.Reg_Firma == 1}
              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-firma">{#Profile_company#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-firma" name="Firma" type="text" value="{$smarty.post.Firma|default:$data.Firma|sanitize}" />
                </div>
              </div>
          {/if}
          {if $settings.Reg_Ust == 1}
              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-vat">{#Profile_vatnum#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-vat" name="UStId" type="text" value="{$smarty.post.UStId|default:$data.UStId|sanitize}" />
                </div>
              </div>
          {/if}
          {if $settings.Reg_Bank == 1}
              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-bank">{#Profile_Bank#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-bank" name="BankName" rows="3">{$smarty.post.BankName|default:$data.BankName|sanitize}</textarea>
                </div>
              </div>
          {/if}
          <div class="small">{#Profile_RequiredInf#}</div>
        </div>

        {if get_active('optional_data')}
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
              <a class="card-title">{#Profile_Optional#}</a>
            </div>
            <div id="collapse3" class="card-body collapse" data-parent="#accordion">

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-gender">{#Profile_Gender#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <select class="form-control" id="form-gender" name="Geschlecht">
                    <option value="-" {if $data.Geschlecht == '-'}selected="selected" {/if}>{#User_NoSettings#}</option>
                    <option value="m" {if $data.Geschlecht == 'm'}selected="selected" {/if}>{#User_Male#}</option>
                    <option value="f" {if $data.Geschlecht == 'f'}selected="selected" {/if}>{#User_Female#}</option>
                  </select>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-birth">{#Birth#}</label>
                </div>
                <div class="col-md-7 col-sm-12" data-toggle="tooltip" title="{#BirthFormat#}">
                  <input class="form-control" id="form-birth" name="birth" type="text" value="{$smarty.post.birth|default:$data.Geburtstag|sanitize}" maxlength="10" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-icq">{#Profile_ICQ#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-icq" name="icq" type="text" value="{$smarty.post.icq|default:$data.icq|sanitize}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-skype">{#Profile_Scype#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-skype" name="skype" type="text" value="{$smarty.post.skype|default:$data.skype|sanitize}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-msn">{#Profile_MSN#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-msn" name="msn" type="text" value="{$smarty.post.msn|default:$data.msn|sanitize}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-web">{#Web#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-web" name="Webseite" type="text" value="{$smarty.post.Webseite|default:$data.Webseite|sanitize}" />
                </div>
              </div>
              {if $signatur_erlaubt == 1}
                  <div class="row form-group">
                    <div class="col-md-5 col-sm-12">
                      <label class="col-form-label" for="form-sign">{#Profile_Sig#}</label>
                      <div class="small">{#Profile_SigLength#}{$signatur_laenge}</div>
                      {if $signatur_syscode == 1}
                          <div class="small">{#Profile_SysCode#} {#Profile_IsAllowed#}</div>
                      {/if}
                    </div>
                    <div class="col-md-7 col-sm-12">
                      <textarea class="form-control" id="form-sign" name="Signatur" rows="3">{$smarty.post.Signatur|default:$data.Signatur|escape:html}</textarea>
                    </div>
                  </div>
              {/if}
              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-status">{#Profile_Status#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-status" name="Status" rows="1">{$smarty.post.Status|default:$data.Status|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-job">{#Profile_Job#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-job" name="Beruf"rows="1">{$smarty.post.Beruf|default:$data.Beruf|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-int">{#Profile_Int#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-int" name="Interessen" rows="1">{$smarty.post.Interessen|default:$data.Interessen|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-hobbys">{#Profile_Hobbys#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-hobbys" name="Hobbys" rows="1">{$smarty.post.Hobbys|default:$data.Hobbys|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-essen">{#Profile_Food#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-essen" name="Essen" rows="1">{$smarty.post.Essen|default:$data.Essen|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-musik">{#Profile_Music#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-musik" name="Musik" rows="1">{$smarty.post.Musik|default:$data.Musik|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-films">{#Profile_Films#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-films" name="Films" rows="1">{$smarty.post.Films|default:$data.Films|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-tele">{#Profile_Tele#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-tele" name="Tele" rows="1">{$smarty.post.Tele|default:$data.Tele|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-book">{#Profile_Book#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-book" name="Book" rows="1">{$smarty.post.Book|default:$data.Book|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-game">{#Profile_Game#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-game" name="Game" rows="1">{$smarty.post.Game|default:$data.Game|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-citat">{#Profile_Citat#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-citat" name="Citat" rows="1">{$smarty.post.Citat|default:$data.Citat|sanitize}</textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-citat">{#Profile_Other#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <textarea class="form-control" id="form-citat" name="Other" rows="1">{$smarty.post.Other|default:$data.Other|sanitize}</textarea>
                </div>
              </div>

            </div>

            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
              <a class="card-title">{#SocialNetworks#}</a>
            </div>
            <div id="collapse4" class="card-body collapse" data-parent="#accordion">

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-vk">{#Vkontakte#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-vk" name="Vkontakte" type="text" value="{$smarty.post.Vkontakte|default:$data.Vkontakte|escape:html}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-ok">{#Odnoklassniki#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-ok" name="Odnoklassniki" type="text" value="{$smarty.post.Odnoklassniki|default:$data.Odnoklassniki|escape:html}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-mm">{#Mymail#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-mm" name="Mymail" type="text" value="{$smarty.post.Mymail|default:$data.Mymail|escape:html}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-gg">{#Google#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-gg" name="Google" type="text" value="{$smarty.post.Google|default:$data.Google|escape:html}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-fb">{#Facebook#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-fb" name="Facebook" type="text" value="{$smarty.post.Facebook|default:$data.Facebook|escape:html}" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-tr">{#Twitter#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-tr" name="Twitter" type="text" value="{$smarty.post.Twitter|default:$data.Twitter|escape: html}" />
                </div>
              </div>

            </div>
        {/if}

        {if get_active('user_videos')}
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
              <a class="card-title">{#Forums_UserVideosMy#}</a>
            </div>
            <div id="collapse5" class="card-body collapse" data-parent="#accordion">

              <div class="box-content">
                {#Forums_UserVideosMyInf#}
              </div>

              {if $myvideos}
                  <div class="h4 title">{#Forums_UserVideosCurr#}</div>
                  <div class="box-content">
                    <div class="row text-center text-nowrap">
                      <div class="col-3">{#Forums_UserVideosVideoId#}</div>
                      <div class="col-3">{#GlobalTitle#}</div>
                      <div class="col-2">{#Forums_UserVideosVideoSrc#}</div>
                      <div class="col-2">{#Forums_UserVideosVideoPos#}</div>
                      <div class="col-2">{#Delete#}</div>
                    </div>
                    {foreach from=$myvideos item=u}
                        <div class="row form-group">
                          <div class="col-3">
                            <input class="form-control" name="Video[{$u->Id}]" type="text" value="{$u->Video}" maxlength="20" />
                          </div>
                          <div class="col-3">
                            <input class="form-control" name="Name[{$u->Id}]" type="text" value="{$u->Name|sanitize}" maxlength="100" />
                          </div>
                          <div class="col-2">
                            <select class="form-control" name="VideoSource[{$u->Id}]">
                              <option value="youtube" {if $u->VideoSource == 'youtube'}selected="selected"{/if}>{#YouTube#}</option>
                            </select>
                          </div>
                          <div class="col-2">
                            <input class="form-control" name="Position[{$u->Id}]" type="number" value="{$u->Position}" maxlength="2" />
                          </div>
                          <div class="col-2 text-center">
                            <input name="DelVideo[{$u->Id}]" type="checkbox" value="1" />
                          </div>
                        </div>
                    {/foreach}
                  </div>
              {/if}
              {if $myvideosCount < 4}
                  <div class="h4 title">{#Forums_UserVideosNew#}</div>
                  <div class="box-content">
                    <div class="row text-center text-nowrap">
                      <div class="col-4 col-sm-3">{#Forums_UserVideosVideoId#}</div>
                      <div class="col-4 col-sm-3">{#GlobalTitle#}</div>
                      <div class="col-2 col-sm-3">{#Forums_UserVideosVideoSrc#}</div>
                      <div class="col-2 col-sm-3">{#Forums_UserVideosVideoPos#}</div>
                    </div>
                    {section name=new loop=$myvideosCountLoop}
                        <div class="row form-group">
                          <div class="col-4 col-sm-3">
                            <input class="form-control" name="VideoNeu[]" type="text" value="" maxlength="20" />
                          </div>
                          <div class="col-4 col-sm-3">
                            <input class="form-control" name="NameNeu[]" type="text" value="" maxlength="100" />
                          </div>
                          <div class="col-2 col-sm-3">
                            <select class="form-control" name="VideoSourceNeu[]">
                              <option value="youtube">{#YouTube#}</option>
                            </select>
                          </div>
                          <div class="col-2 col-sm-3">
                            <input class="form-control" name="PositionNeu[]" type="number" value="1" maxlength="2" />
                          </div>
                        </div>
                    {/section}
                  </div>
              {/if}
            </div>
        {/if}

        {if get_active('forums')}
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
              <a class="card-title">{#Forums_avatar#}</a>
            </div>
            <div id="collapse6" class="card-body collapse" data-parent="#accordion">

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Gravatar#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gravatar" value="1" type="radio" {if $data.Gravatar == 1}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gravatar" value="0" type="radio" {if $data.Gravatar == 0}checked="checked"{/if} /> {#No#}
                    </label>
                  </div>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_AvatarStandard#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Avatar_Default" type="radio" value="1" {if $data.Avatar_Default == 1}checked="checked" {/if}/> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Avatar_Default" type="radio" value="0" {if $data.Avatar_Default == 0}checked="checked" {/if}/> {#No#}
                    </label>
                  </div>
                </div>
              </div>
              {if $avatar && permission('own_avatar')}
                  <div class="row form-group">
                    <div class="col-md-5 col-sm-12">
                      <label>{#Profile_AvatarDelete#}</label>
                    </div>
                    <div class="col-md-7 col-sm-12">
                      <div class="form-check form-check-inline">
                        <label class="form-check-label">
                          <input class="form-check-input" name="Avatar_Del" type="radio" value="1" /> {#Yes#}
                        </label>
                      </div>
                      <div class="form-check form-check-inline">
                        <label class="form-check-label">
                          <input class="form-check-input" name="Avatar_Del" type="radio" value="0" checked="checked" /> {#No#}
                        </label>
                      </div>
                    </div>
                  </div>
              {/if}

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">{#Profile_AvatarImage#}</div>
                <div class="col-md-7 col-sm-12">
                  {if $avatar}
                      {$avatar}
                  {else}
                      {#Profile_AvatarNone#}
                  {/if}
                </div>
              </div>
              {if permission('own_avatar')}
                  <div class="row form-group">
                    <div class="col-md-5 col-sm-12">
                      <label class="col-form-label" for="form-avatar">{#Profile_AvatarNew#}</label>
                    </div>
                    <div class="col-md-7 col-sm-12">
                      <div id="UpInf_1"></div>
                      <div id="loading_1" style="display: none;">
                        <img src="{$imgpath_page}ajaxbar.gif" alt="" />
                      </div>
                      <div class="input-group">
                        <input class="form-control" id="fileToUpload_1" type="file" name="fileToUpload_1" />
                        <span class="input-group-append">
                          <input type="button" class="btn btn-primary" id="buttonUpload_1" onclick="fileUpload('avatarupload', 1);" value="{#UploadButton#}" />
                        </span>
                      </div>
                      <input type="hidden" name="newAvatar" id="newFile_1" />
                    </div>
                  </div>
              {/if}
            </div>
        {/if}

        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse7">
          <a class="card-title">{#Profile_Profile#}</a>
        </div>
        <div id="collapse7" class="card-body collapse" data-parent="#accordion" >

          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              <label>{#Profile_Public#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Profil_public" type="radio" value="1" {if $data.Profil_public == 1}checked="checked"{/if} /> {#Yes#}
                </label>
              </div>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Profil_public" type="radio" value="0" {if $data.Profil_public == 0}checked="checked"{/if} /> {#No#}
                </label>
              </div>
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              <label>{#Profile_PublicAll#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Profil_Alle" type="radio" value="1" {if $data.Profil_Alle == 1}checked="checked"{/if} /> {#Yes#}
                </label>
              </div>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Profil_Alle" type="radio" value="0" {if $data.Profil_Alle == 0}checked="checked"{/if} /> {#Profile_PublicAll2#}
                </label>
              </div>
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              <label>{#Profile_TownPublic#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Ort_Public" type="radio" value="1" {if $data.Ort_Public == 1}checked="checked"{/if} /> {#Yes#}
                </label>
              </div>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Ort_Public" type="radio" value="0" {if $data.Ort_Public == 0}checked="checked"{/if} /> {#No#}
                </label>
              </div>
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              <label>{#Profile_SBirth#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Geburtstag_public" type="radio" value="1" {if $data.Geburtstag_public == 1}checked="checked"{/if} /> {#Yes#}
                </label>
              </div>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Geburtstag_public" type="radio" value="0" {if $data.Geburtstag_public == 0}checked="checked"{/if} /> {#No#}
                </label>
              </div>
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              <label>{#Profile_BInv#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Unsichtbar" type="radio" value="1" {if $data.Unsichtbar == 1}checked="checked"{/if} /> {#Yes#}
                </label>
              </div>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Unsichtbar" type="radio" value="0" {if $data.Unsichtbar == 0}checked="checked"{/if} /> {#No#}
                </label>
              </div>
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              <label>{#Profile_RNl#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Newsletter" type="radio" value="1" {if $data.Newsletter == 1}checked="checked"{/if} /> {#Yes#}
                </label>
              </div>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Newsletter" type="radio" value="0" {if $data.Newsletter == 0}checked="checked"{/if} /> {#No#}
                </label>
              </div>
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-5 col-sm-12">
              <label>{#Profile_REm#}</label>
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Emailempfang" type="radio" value="1" {if $data.Emailempfang == 1}checked="checked"{/if} /> {#Yes#}
                </label>
              </div>
              <div class="form-check form-check-inline">
                <label class="form-check-label">
                  <input class="form-check-input" name="Emailempfang" type="radio" value="0" {if $data.Emailempfang == 0}checked="checked"{/if} /> {#No#}
                </label>
              </div>
            </div>
          </div>

        </div>

        {if get_active('forums')}
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse8">
              <a class="card-title">{#Profile_ForumsSettings#}</a>
            </div>
            <div id="collapse8" class="card-body collapse" data-parent="#accordion">

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-thead">{#Profile_ForumsSettingsThreads#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-thead" name="Forum_Themen_Limit" type="number" value="{$smarty.post.Forum_Themen_Limit|default:$data.Forum_Themen_Limit|sanitize}" maxlength="2" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-post">{#Profile_ForumsSettingsPosts#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-post" name="Forum_Beitraege_Limit" type="number" value="{$smarty.post.Forum_Beitraege_Limit|default:$data.Forum_Beitraege_Limit|sanitize}" maxlength="2" />
                </div>
              </div>

            </div>
        {/if}

        {if get_active('pn')}
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse9">
              <a class="card-title">{#Profile_Guestbook_PrivateMessages#}</a>
            </div>
            <div id="collapse9" class="card-body collapse" data-parent="#accordion">

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_RPn#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Pnempfang" type="radio" value="1" {if $data.Pnempfang == 1}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Pnempfang" type="radio" value="0" {if $data.Pnempfang == 0}checked="checked"{/if} /> {#No#}
                    </label>
                  </div>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_REPn#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="PnEmail" type="radio" value="1" {if $data.PnEmail == 1}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="PnEmail" type="radio" value="0" {if $data.PnEmail == 0}checked="checked"{/if} /> {#No#}
                    </label>
                  </div>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_PopUpPn#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="PnPopup" type="radio" value="1" {if $data.PnPopup == 1}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="PnPopup" type="radio" value="0" {if $data.PnPopup == 0}checked="checked"{/if} /> {#No#}
                    </label>
                  </div>
                </div>
              </div>

            </div>
        {/if}

        {if get_active('user_guestbook')}
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse10">
              <a class="card-title">{#Profile_GuestbookUser#}</a>
            </div>
            <div id="collapse10" class="card-body collapse" data-parent="#accordion">

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_AGb#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch" type="radio" value="1" {if $data.Gaestebuch == 1}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch" type="radio" value="0" {if $data.Gaestebuch == 0}checked="checked"{/if} /> {#No#}
                    </label>
                  </div>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_Guestbook_OptNoGuests#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_KeineGaeste" type="radio" value="0" {if $data.Gaestebuch_KeineGaeste == 0}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_KeineGaeste" type="radio" value="1" {if $data.Gaestebuch_KeineGaeste == 1}checked="checked"{/if} /> {#Profile_Guestbook_OptNoGuests2#}
                    </label>
                  </div>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_Guestbook_Moderated#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_Moderiert" type="radio" value="0" {if $data.Gaestebuch_Moderiert == 0}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_Moderiert" type="radio" value="1" {if $data.Gaestebuch_Moderiert == 1}checked="checked"{/if} /> {#Profile_Guestbook_Moderated2#}
                    </label>
                  </div>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label class="col-form-label" for="form-chars">{#Profile_Guestbook_MaxChars1#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <input class="form-control" id="form-chars" name="Gaestebuch_Zeichen" type="number" id="Gaestebuch_Zeichen" value="{$data.Gaestebuch_Zeichen}" maxlength="4" />
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_Guestbook_AllowSmilies#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_smilies" type="radio" value="1" {if $data.Gaestebuch_smilies == 1}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_smilies" type="radio" value="0" {if $data.Gaestebuch_smilies == 0}checked="checked"{/if} /> {#No#}
                    </label>
                  </div>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_Guestbook_AllowBBcode#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_bbcode" type="radio" value="1" {if $data.Gaestebuch_bbcode == 1}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_bbcode" type="radio" value="0" {if $data.Gaestebuch_bbcode == 0}checked="checked"{/if} /> {#No#}
                    </label>
                  </div>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-5 col-sm-12">
                  <label>{#Profile_Guestbook_AllowImgCode#}</label>
                </div>
                <div class="col-md-7 col-sm-12">
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_imgcode" type="radio" value="1" {if $data.Gaestebuch_imgcode == 1}checked="checked"{/if} /> {#Yes#}
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" name="Gaestebuch_imgcode" type="radio" value="0" {if $data.Gaestebuch_imgcode == 0}checked="checked"{/if} /> {#No#}
                    </label>
                  </div>
                </div>
              </div>

            </div>
        {/if}
      </div>
    </div>
    <div class="text-center">
      <input class="btn btn-primary btn-block-sm" type="submit" value="{#SaveProfile#}" />
    </div>
    <input name="area" type="hidden" value="{$area}" />
    <input name="lang" type="hidden" value="{$langcode}" />
    <input name="send" type="hidden" value="1" />
  </form>
</div>
