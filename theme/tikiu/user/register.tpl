{script file="$jspath/jvalidate.js" position='head'}
<script>
<!-- //
{include file="$incpath/other/jsvalidate.tpl"}
$(function() {
    var validator = $('#regform').validate( {
        rules: {
            {if $settings.Reg_Pass == 1}
            reg_pass: { required: true, minlength: 6},
            reg_pass2: { required: true, equalTo: '#reg-pass' },
            {/if}
            {if $settings.Reg_AgbPflicht == 1}
            agb_checked: { required: true },
            {/if}
            {if $settings.Reg_DataPflichtFill == 1}
            Vorname: { required: true },
            Nachname: { required: true },
            {/if}
            {if $settings.Reg_AddressFill == 1}
            Strasse_Nr: { required: true },
            Postleitzahl: { required: true, number: true, minlength: 4 },
            Ort: { required: true },
            {/if}
            reg_email: { required: true, email: true, remote: '{$baseurl}/index.php?do=checkuserdata&p=register' },
            reg_email2: { required: true, equalTo: '#reg-email' },
            reg_username: { required: true, remote: '{$baseurl}/index.php?do=checkuserdata&p=register' }
        },
        messages: {
            {if $settings.Reg_AgbPflicht == 1}
            agb_checked: { required: '{#Reg_agb_failed#}' },
            {/if}
            reg_email: { remote: $.validator.format('{#Validate_usedMail#}') },
            reg_username: { remote: $.validator.format('{#Validate_usedUName#}') }
        },
        submitHandler: function() {
            document.forms['regform'].submit();
        }
    });
});
// -->
</script>

{if $shop == 1}
    {include file="$incpath/shop/steps.tpl"}
{/if}
{if !empty($error)}
      <div class="uk-alert-warning card-vk" data-uk-alert>
        <div >{#Reg_Failed#}</div>
        {foreach from=$error item=err}
            <div>{$err}</div>
        {/foreach}
      </div>
{/if}

<form autocomplete="off" method="post" name="regform" id="regform" action="">
<div class="uk-flex-center uk-child-width-1-2@s bg-white card-vk uk-grid-collapse" data-uk-grid>

  <div class="uk-padding-small">

    <h1>{#Reg_LoginData#}</h1>
    <div class="row form-group">
      <div class="col-md-4 col-sm-12">
        <label class="col-form-label required" for="reg-email">{#Email#}</label>
      </div>
      <div class="col-md-8 col-sm-12">
        <input class="uk-input uk-border-rounded" id="reg-email" name="reg_email" type="email" value="{$smarty.post.reg_email|sanitize}" />
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-4 col-sm-12">
        <label class="col-form-label required" for="reg-email2">{#Reg_Email2#}</label>
      </div>
      <div class="col-md-8 col-sm-12">
        <input class="uk-input uk-border-rounded" id="reg-email2" name="reg_email2" type="email" value="{$smarty.post.reg_email2|sanitize}" />
      </div>
    </div>

    <div class="row form-group">
      <div class="col-md-4 col-sm-12">
        <label class="col-form-label required" for="reg-name">{#Reg_Username#}</label>
      </div>
      <div class="col-md-8 col-sm-12">
        <input class="uk-input uk-border-rounded" id="reg-name" name="reg_username" type="text" value="{$smarty.post.reg_username|sanitize}" />
      </div>
    </div>

    {if $settings.Reg_Pass == 1}
        <div class="row form-group">
          <div class="col-md-4 col-sm-12">
            <label class="col-form-label required" for="reg-pass">{#Pass#}</label>
          </div>
          <div class="col-md-8 col-sm-12">
            <input class="uk-input uk-border-rounded" id="reg-pass" name="reg_pass" type="password" value="{$smarty.post.reg_pass|sanitize}" />
          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-4 col-sm-12">
            <label class="col-form-label required" for="reg-pass2">{#Reg_Pass2#}</label>
          </div>
          <div class="col-md-8 col-sm-12">
            <input class="uk-input uk-border-rounded" id="reg-pass2" name="reg_pass2" type="password" value="{$smarty.post.reg_pass2|sanitize}" />
          </div>
        </div>
    {/if}
  </div>

  {if $settings.Reg_DataPflicht == 1}
  <div class=" uk-padding-small">

        <h1>{#PersonalData#}</h1>
        {if $settings.Reg_DataPflicht == 1}
            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label{if $settings.Reg_DataPflichtFill == 1} required{/if}" for="reg-lastname">{#LastName#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="uk-input uk-border-rounded" id="reg-lastname" name="Nachname" type="text" value="{$smarty.post.Nachname|default:$data.Nachname|sanitize}" />
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label{if $settings.Reg_DataPflichtFill == 1} required{/if}" for="reg-vorname">{#GlobalName#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="uk-input uk-border-rounded" id="reg-vorname" name="Vorname" type="text" value="{$smarty.post.Vorname|default:$data.Vorname|sanitize}" />
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label{if $settings.Reg_DataPflichtFill == 1} required{/if}" for="reg-mdname">{#Profile_MiddleName#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="uk-input uk-border-rounded" id="reg-mdname" name="MiddleName" type="text" value="{$smarty.post.MiddleName|default:$data.MiddleName|sanitize}" />
              </div>
            </div>
        {/if}
        {if $settings.Reg_Birth == 1}
            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label" for="reg-birth">{#Birth#}</label>
              </div>
              <div class="col-md-8 col-sm-12" data-toggle="tooltip" title="{#BirthFormat#}">
                <input class="uk-input uk-border-rounded" id="reg-birth" name="birth" type="text" value="{$smarty.post.birth|sanitize}" maxlength="10" />
              </div>
            </div>
        {/if}
        {if $settings.Reg_Fon == 1}
            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label" for="reg-phone">{#Phone#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="uk-input uk-border-rounded" id="reg-phone" name="Telefon" type="text" value="{$smarty.post.Telefon|default:$data.Telefon|sanitize}" />
              </div>
            </div>
        {/if}
        {if $settings.Reg_Fax == 1}
            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label" for="reg-fax">{#Fax#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="uk-input uk-border-rounded" id="reg-fax" name="Telefax" value="{$smarty.post.Telefax|default:$data.Telefax|sanitize}" />
              </div>
            </div>
        {/if}
        <div class="row form-group">
          <div class="col-md-4 col-sm-12">
            <label class="col-form-label" for="reg-country">{#Country#}</label>
          </div>
          <div class="col-md-8 col-sm-12">
            <select class="uk-select uk-border-rounded" id="reg-country" name="country">
              {foreach from=$countries item=c}
                  <option value="{$c.Code}" {if $smarty.request.send != 1}{if $settings.Land|upper == $c.Code}selected="selected"{/if}{else}{if $smarty.post.country == $c.Code}selected="selected"{/if}{/if}>{$c.Name}</option>
              {/foreach}
            </select>
          </div>
        </div>
        {if $settings.Reg_Address == 1}
            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label{if $settings.Reg_AddressFill == 1} required{/if}" for="reg-zip">{#Profile_Zip#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="uk-input uk-border-rounded" id="reg-zip" name="Postleitzahl" type="text" value="{$smarty.post.Postleitzahl|default:$data.Postleitzahl|sanitize}" />
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label{if $settings.Reg_AddressFill == 1} required{/if}" for="reg-town">{#Town#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="uk-input uk-border-rounded" id="reg-town" name="Ort" type="text" value="{$smarty.post.Ort|default:$data.Ort|sanitize}" />
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label{if $settings.Reg_AddressFill == 1} required{/if}" for="reg-street">{#Location#}</label>
              </div>
              <div class="col-md-8 col-sm-12" data-toggle="tooltip" title="{#Profile_Street#}">
                <input class="uk-input uk-border-rounded" id="reg-street" name="Strasse_Nr" type="text" value="{$smarty.post.Strasse_Nr|default:$data.Strasse_Nr|sanitize}" />
              </div>
            </div>
        {/if}
        {if $settings.Reg_Firma == 1}
            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label" for="reg-firma">{#Profile_company#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="uk-input uk-border-rounded" id="reg-firma" name="Firma" type="text" value="{$smarty.post.Firma|default:$data.Firma|sanitize}" />
              </div>
            </div>
        {/if}
        {if $settings.Reg_Ust == 1}
            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label" for="reg-ustid">{#Profile_vatnum#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <input class="uk-input uk-border-rounded" id="reg-ustid" name="UStId" type="text" value="{$smarty.post.UStId|default:$data.ustid|sanitize}" />
              </div>
            </div>
        {/if}
        {if $settings.Reg_Bank == 1}
            <div class="row form-group">
              <div class="col-md-4 col-sm-12">
                <label class="col-form-label" for="reg-bank">{#Profile_Bank#}</label>
              </div>
              <div class="col-md-8 col-sm-12">
                <textarea class="uk-textarea uk-border-rounded" id="reg-bank" name="BankName" rows="3">{$smarty.post.BankName|sanitize}</textarea>
              </div>
            </div>
        {/if}
        <div class="small">{#Profile_RequiredInf#}</div>
  </div>
  {else}
  <input type="hidden" name="country" value="{$settings.Land}" />
  {/if}

</div>


  {if $settings.Reg_AgbPflicht == 1}
      <script>
      <!-- //
      $(function() {
          $('#reg-agb-click').on('click', function () {
              UIkit.modal('#reg-agb-read').show();
          });
      });
      // -->
      </script>

      <div id="reg-agb-read" tabindex="-1" role="dialog" aria-labelledby="reg-agb-title" aria-hidden="true" data-uk-modal>
        <div class="uk-modal-dialog uk-modal-body" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <div class="modal-title h4" id="reg-agb-title">{#Reg_agb#}</div>
              <button type="button" class="close" data-dismiss="modal" aria-label="{#WinClose#}">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">{$settings.Reg_Agb}</div>
            <div class="modal-footer justify-content-center">
              <button type="button" class="btn btn-primary" data-dismiss="modal">{#WinClose#}</button>
            </div>
          </div>
        </div>
      </div>

      <div class="h3 title">{#Reg_agb#}</div>
      <div class="box-content">
      <div class="spacer d-flex justify-content-between">
          <div class="form-check">
            <input class="uk-checkbox uk-border-pill" id="reg-agb" name="agb_checked" type="checkbox" value="1" />
            <label class="form-check-label" for="reg-agb">{#Reg_agb_inf#}</label>
          </div>
          <a class="text-nowrap" id="reg-agb-click" href="javascript:void(0);">
            <i class="icon-right-open"></i>{#Read#}
          </a>
        </div>
      </div>
  {/if}

    {include file="$incpath/other/captcha.tpl"}

    <div class="uk-text-center">
      <input class="uk-button uk-button-success uk-border-rounded" type="submit" value="{#RegNew#}" />
      <input class="uk-button" type="reset" />
    </div>

    <input type="hidden" name="area" value="{$area}" />
    <input type="hidden" name="lang" value="{$langcode}" />
    <input type="hidden" name="p" value="register" />
    <input type="hidden" name="send" value="1" />
    {if $shop == 1}
        <input type="hidden" name="p" value="shop" />
        <input type="hidden" name="action" value="shoporder" />
        <input type="hidden" name="subaction" value="step2" />
        <input type="hidden" name="register" value="new" />
        <input type="hidden" name="mode" value="shop" />
    {/if}

</form>
