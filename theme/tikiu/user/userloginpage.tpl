<div class="uk-flex uk-flex-center uk-flex-middle">
<div class="card-vk bg-white uk-padding-small uk-width-1-3@s">

    <h1>{#MyAccount#}</h1>

    {if $loggedin}

        {if isset($smarty.get.success) && $smarty.get.success == 1}
            {#LoginExternSuccess#}
        {/if}

          {if get_active('shop')}
              {$welcome}, {$smarty.session.user_name}<br>
              {#LoginExternCustomerNr#}: {$smarty.session.benutzer_id}
          {else}
              {$welcome}, {$smarty.session.user_name}
          {/if}


        <ul class="uk-nav">

          {if get_active('pn')}
            <li><a href="index.php?p=pn">{#PN_inbox#} {newpn}</a></li>
          {/if}

          {if get_active('shop')}
            <li class="uk-nav-divider"></li>
            <li><a href="index.php?p=shop&amp;action=myorders">
              {#Shop_go_myorders#}
            </a></li>
            <li><a href="index.php?p=shop&amp;action=mydownloads">
              {#LoginExternVd#}
            </a></li>
            <li><a href="index.php?p=shop&amp;action=mylist">
              {#Shop_mylist#}
            </a></li>
            <li><a class="colorbox" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1">
              {#Shop_mergeListsMy#}
            </a></li>
          {/if}
          {if get_active('calendar')}
            <li class="uk-nav-divider"></li>
            <li><a href="index.php?p=calendar&amp;month={$smarty.now|date_format:'m'}&amp;year={$smarty.now|date_format:'Y'}&amp;area={$area}&amp;show=private">
                {#UserCalendar#}</a></li>
          {/if}
          <li><a href="index.php?p=user&amp;id={$smarty.session.benutzer_id}&amp;area={$area}">
            {#LoginExternVp#}
          </a></li>
          <li><a href="index.php?p=useraction&amp;action=profile">
            {#LoginExternPc#}
          </a></li>
          <li><a href="index.php?p=useraction&amp;action=changepass">
              {#LoginExternCp#}
            </a></li>

        <li class="uk-nav-divider"></li>

          {if permission('adminpanel')}
            <li><a href="javascript:void(0);" onclick="openWindow('{$baseurl}/admin', 'admin', '', '', 1);">{#AdminLink#}</a></li>
          {/if}
          {if permission('deleteaccount') && $smarty.session.benutzer_id != 1}
            <li><a href="index.php?p=useraction&amp;action=deleteaccount">
                {#AccountDel#}</a></li>
          {/if}

        <li class="uk-nav-divider"></li>

        <form method="post" name="logout_form" action="index.php">
          <input type="hidden" name="p" value="userlogin" />
          <input type="hidden" name="action" value="logout" />
          <input type="hidden" name="area" value="{$area}" />
          <input type="hidden" name="backurl" value="{'index.php'|base64encode}" />
          {* <a onclick="return confirm('{#Confirm_Logout#}');" href="javascript: document.forms['logout_form'].submit();">{#Logout#} <i data-uk-icon="sign-out"></i></a> *}
          <ul class="uk-nav uk-nav-default"><li><a href="javascript: document.forms['logout_form'].submit();">
            {#Logout#} <i data-uk-icon="sign-out"></i></a></li></ul>
        </form>

        </ul>

  {else}

      <script>
      $(function() {
          $('#login-button').on('click', function () {
              $('#login-content').hide();
              $('#login-loader').show();
          });
      });
      </script>

      <div id="login-content">
        {if isset($LoginError)}
            <div class="uk-alert-warning" data-uk-alert>
              <div class="font-weight-bold">{#Error#}</div>
              {#WrongLoginData#}
            </div>
        {/if}

          <form name="login" method="post" action="index.php?p=userlogin">
            <label class="col-form-label" for="form-login-mail">{#LoginMailUname#}
              <input class="uk-input uk-border-rounded" id="form-login-mail" name="login_email" type="text" />
            </label>

            <label>{#Pass#}
              <input class="uk-input uk-border-rounded" id="form-login-pass" name="login_pass" type="password" />
            </label>

            <div class="uk-margin-small-top uk-inline">
              <label class="uk-button">
                <input class="uk-checkbox uk-border-pill" name="staylogged" value="1" type="checkbox"{if $data.PnEmail == 0} checked="checked"{/if} /> {#PassCookieT#}
              </label>

              <input class="uk-button uk-button-default uk-border-rounded" id="login-button" type="submit" value="{#Login_Button#}" />
            </div>

            <input type="hidden" name="p" value="userlogin" />
            <input type="hidden" name="action" value="newlogin" />
            <input type="hidden" name="area" value="{$area}" />
            <input type="hidden" name="backurl" value="{page_link|base64encode}" />
          </form>

          <ul class="uk-subnav uk-text-center">
            {if get_active('Register')}
                <li><a class="mr-3" href="index.php?p=register&amp;area={$area}">
                  {#RegNew#}
                </a></li>
            {/if}
            <li><a href="index.php?p=pwlost">
              {#PassLost#}
            </a></li>
          </ul>

        <div class="loader" id="login-loader"></div>
      </div>
    {/if}

  </div>
</div>