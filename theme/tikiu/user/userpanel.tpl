<div class="uk-text-center uk-light">
  {$welcome}, {$smarty.session.user_name}<br>
  {if get_active('shop')}
    {#LoginExternCustomerNr#}: {$smarty.session.benutzer_id}
  {/if}
</div>

<ul class="uk-nav uk-nav-default uk-width-1-1">
  {if get_active('pn')}
    <li><a href="index.php?p=pn">{#PN_inbox#} {newpn}</a></li>
  {/if}

  {if get_active('shop')}
    <li class="uk-nav-divider"></li>
    <li><a href="index.php?p=shop&amp;action=myorders">
      {#Shop_go_myorders#}
    </a></li>
    <li><a href="index.php?p=shop&amp;action=mydownloads">
      {#LoginExternVd#}
    </a></li>
    <li><a href="index.php?p=shop&amp;action=mylist">
      {#Shop_mylist#}
    </a></li>
    <li><a class="colorbox" href="index.php?p=misc&amp;do=mergeproduct&amp;redir=1">
      {#Shop_mergeListsMy#}
    </a></li>
  {/if}
  {if get_active('calendar')}
    <li class="uk-nav-divider"></li>
    <li><a href="index.php?p=calendar&amp;month={$smarty.now|date_format:'m'}&amp;year={$smarty.now|date_format:'Y'}&amp;area={$area}&amp;show=private">
        {#UserCalendar#}</a></li>
  {/if}
  <li><a href="index.php?p=user&amp;id={$smarty.session.benutzer_id}&amp;area={$area}">
    {#LoginExternVp#}
  </a></li>
  <li><a href="index.php?p=useraction&amp;action=profile">
    {#LoginExternPc#}
  </a></li>
  <li><a href="index.php?p=useraction&amp;action=changepass">
      {#LoginExternCp#}
    </a></li>

  {if permission('adminpanel')}
  <li class="uk-nav-divider"></li>
  <li><a href="{$baseurl}/admin" target="_blank">{#AdminLink#}</a>
  </li>
  {/if}
  {if permission('deleteaccount') && $smarty.session.benutzer_id != 1}
    <li><a href="index.php?p=useraction&amp;action=deleteaccount">
        {#AccountDel#}</a>
    </li>
    <li class="uk-nav-divider"></li>
  {/if}

<form method="post" name="logout_form" action="index.php">
  <input type="hidden" name="p" value="userlogin" />
  <input type="hidden" name="action" value="logout" />
  <input type="hidden" name="area" value="{$area}" />
  <input type="hidden" name="backurl" value="{'index.php'|base64encode}" />
  {* <a onclick="return confirm('{#Confirm_Logout#}');" href="javascript: document.forms['logout_form'].submit();">{#Logout#} <i data-uk-icon="sign-out"></i></a> *}
  <ul class="uk-nav uk-nav-default"><li><a href="javascript: document.forms['logout_form'].submit();">
    {#Logout#} <i data-uk-icon="sign-out"></i></a></li></ul>
</form>

</ul>